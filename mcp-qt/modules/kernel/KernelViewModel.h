/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <QObject>
#include <QtQml>

#include <modules/kernel/KernelRepositoryBase.h>
#include <modules/kernel/KernelServiceBase.h>

#include <ProgressNotifier.h>

#include "KernelListModel.h"

namespace MCP::Qt::Kernel {
namespace {
using namespace Common;
}
class KernelViewModel : public QObject
{
    Q_OBJECT

    QML_ELEMENT

    Q_PROPERTY(KernelListModel *model READ model NOTIFY modelChanged)
    Q_PROPERTY(ProgressNotifier *progressNotifier READ progressNotifier NOTIFY
                   progressNotifierChanged CONSTANT)
    Q_PROPERTY(QString currentTransactionKernelName READ currentTransactionKernelName WRITE
                   setCurrentTransactionKernelName NOTIFY currentTransactionKernelNameChanged)

public:
    KernelViewModel(Lib::Modules::Kernel::KernelServiceBase &service,
                    Lib::Modules::Kernel::KernelRepositoryBase &repository,
                    KernelListModel &model)
        : m_service(service), m_repository(repository), m_model(model)
    {
        init();
    }

    ~KernelViewModel() = default;

    KernelListModel *model() const;

    Q_INVOKABLE void installKernel(const QString &pkgName);
    Q_INVOKABLE void removeKernel(const QString &pkgName);
    Q_INVOKABLE void getExtraModules(const QString &pkgName, const QJSValue &cb);
    Q_INVOKABLE void getChangelog(int major, int minor, const QJSValue &cb);

    ProgressNotifier *progressNotifier();

    QString currentTransactionKernelName() const;
    void setCurrentTransactionKernelName(const QString &newCurrentTransactionKernelName);

Q_SIGNALS:
    void modelChanged();
    void progressChanged();
    void progressNotifierChanged();

    void currentTransactionKernelNameChanged(QString currentTransactionKernelName);

private:
    void init();

    KernelListModel &m_model;
    Lib::Modules::Kernel::KernelServiceBase &m_service;
    Lib::Modules::Kernel::KernelRepositoryBase &m_repository;

    ProgressNotifier m_progressNotifier;
    QString m_currentTransactionKernelName;
};
} // namespace MCP::Qt::Kernel

struct KernelViewModelService : kgr::single_service<MCP::Qt::Kernel::KernelViewModel,
                                                    kgr::dependency<KernelServiceBaseDependency,
                                                                    KernelRepositoryBaseDependency,
                                                                    KernelListModelDependency>>
{};
