/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <QAbstractListModel>
#include <QJSValue>

#include <modules/kernel/Kernel.h>
#include <modules/kernel/KernelRepository.h>
#include <modules/kernel/KernelRepositoryBase.h>

namespace MCP::Qt::Kernel {

class KernelListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Role {
        Name = ::Qt::UserRole + 1,
        IsInstalled,
        Version,
        IsLTS,
        IsRecommended,
        IsRealTime,
        IsEOL,
        IsExperimental,
        IsInUse,
        MajorVersion,
        MinorVersion,
        Category,
        Changelog
    };
    Q_ENUM(Role)

    KernelListModel(Lib::Modules::Kernel::KernelRepositoryBase &repository)
        : m_repository(repository)
    {
        init();
    }

    const std::vector<Lib::Modules::Kernel::Kernel> &list() const;
    void setList(const std::vector<Lib::Modules::Kernel::Kernel> &newList);
    void update();

Q_SIGNALS:
    void listChanged();

private:
    void init();
    std::vector<MCP::Lib::Modules::Kernel::Kernel> m_list;
    MCP::Lib::Modules::Kernel::KernelRepositoryBase &m_repository;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
};

} // namespace MCP::Qt::Kernel

struct KernelListModelDependency
    : kgr::single_service<MCP::Qt::Kernel::KernelListModel,
                          kgr::dependency<KernelRepositoryBaseDependency>>
{};
