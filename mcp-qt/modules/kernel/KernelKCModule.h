/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <KQuickAddons/ManagedConfigModule>

#include <kangaru/container.hpp>

#include "KernelViewModel.h"

namespace {
using namespace MCP::Qt::Kernel;
}
class KernelKCModule : public KQuickAddons::ManagedConfigModule
{
    Q_OBJECT

    Q_PROPERTY(KernelViewModel *vm READ vm WRITE setVm NOTIFY vmChanged)

public:
    KernelKCModule(QObject *parent, const KPluginMetaData &data, const QVariantList &args);
    virtual ~KernelKCModule() override = default;

    KernelViewModel *vm() const;
    void setVm(KernelViewModel *newVm);

public Q_SLOTS:
    virtual void load() override;

Q_SIGNALS:
    void vmChanged();

private:
    kgr::container m_container;
    KernelViewModel *m_vm = nullptr;
};
