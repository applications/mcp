import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.15 as Kirigami

ListView {
    id: listView
    signal showChangelog(int minor, int major)
    signal install(string pkgName)
    signal remove(string pkgName)

    delegate: KernelDelegate {
        anchors.horizontalCenter: parent.horizontalCenter

        onShowChangelog: {
            listView.showChangelog(model.majorVersion, model.minorVersion)
        }

        onInstall: {
            listView.install(model.name)
        }

        onRemove: {
            listView.remove(model.name)
        }
    }
    section {
        property: "category"
        delegate: Kirigami.ListSectionHeader {
            text: section
        }
    }
}
