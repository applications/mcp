import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.manjaro.mcp.components 1.0 as Components

Kirigami.ScrollablePage {

    id: root

    Kirigami.PromptDialog {
        id: errorDialog
        standardButtons: Kirigami.Dialog.NoButton
        title: qsTr("Error")
    }

    Components.InstallationDialog {
        id: installationDialog

        preferredWidth: parent.width * 0.85

        Connections {
            target: vm.progressNotifier

            function onProgressChanged(progress) {
                if (progress.action !== "")
                    installationDialog.details = progress.action

                installationDialog.progress = progress.progress * 100
            }

            function onStarted() {
                installationDialog.open()
            }
            function onFinished(res) {
                installationDialog.close()
            }
            function onErrorOccured(error) {
                errorDialog.subtitle = error
                errorDialog.open()
            }
        }
    }

    Components.ConfirmationDialog {
        id: confirmationDialog

        ColumnLayout {
            Label {
                textFormat: Text.MarkdownText
                text: !confirmationDialog.uninstallation ? qsTr("New kernel (%1) is ready to install<br />Would you like to continue?").arg(vm.currentTransactionKernelName) : qsTr(
                                                               "Do you want to remove %1?").arg(
                                                               vm.currentTransactionKernelName)
            }
            ListView {
                visible: (model.length > 0)
                         && !confirmationDialog.uninstallation
                Layout.preferredHeight: contentHeight
                model: vm ?? vm.getExtraModules(vm.currentTransactionKernelName)
                header: Label {
                    text: qsTr("Following extra packages will be installed:")
                }

                delegate: Label {
                    textFormat: Text.MarkdownText
                    width: parent.width
                    height: paintedHeight
                    text: "* " + modelData
                }
            }
        }
        onAccepted: {
            if (!confirmationDialog.uninstallation) {
                vm.installKernel(vm.currentTransactionKernelName)
            } else {
                vm.removeKernel(vm.currentTransactionKernelName)
            }
        }

        onRejected: {
            vm.currentTransactionKernelName = ""
        }
    }

    ChangelogSheet {
        id: changelogSheet

        function showChangelog(changelogText, major, minor) {
            changelogSheet.majorVersion = major
            changelogSheet.minorVersion = minor
            changelogSheet.text = changelogText
            changelogSheet.open()
        }

        onSheetOpenChanged: {
            if (!sheetOpen) {
                changelogSheet.text = ""
            }
        }
    }

    KernelListView {
        id: kernelListView
        model: vm.model

        onShowChangelog: (major, minor) => {
                             vm.getChangelog(major, minor, changelog => {
                                                 changelogSheet.showChangelog(
                                                     changelog, major, minor)
                                             })
                         }

        onInstall: pkgName => {
                       vm.currentTransactionKernelName = pkgName
                       confirmationDialog.uninstallation = false
                       confirmationDialog.open()
                   }
        onRemove: pkgName => {
                      vm.currentTransactionKernelName = pkgName
                      confirmationDialog.uninstallation = true
                      confirmationDialog.open()
                  }
    }
}
