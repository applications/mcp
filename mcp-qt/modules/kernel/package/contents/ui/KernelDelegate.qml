import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import org.kde.kirigami 2.15 as Kirigami

Kirigami.BasicListItem {
    clip: true
    id: rootItem

    signal showChangelog
    signal install
    signal remove

    contentItem: RowLayout {
        anchors.margins: 5
        Label {
            horizontalAlignment: Text.AlignHCenter
            Layout.preferredWidth: 75
            font.pointSize: 20
            font.family: "Comfortaa"
            id: icon
            text: model.majorVersion + "." + model.minorVersion
        }
        ColumnLayout {
            Label {
                elide: Text.ElideRight
                font.pointSize: 15
                Layout.fillWidth: true
                id: name
                text: "Linux " + model.version
            }
            RowLayout {
                Layout.rightMargin: 5
                spacing: 5
                opacity: 0.6

                Label {
                    font.pointSize: 8
                    id: pkgname
                    text: model.name
                }
                Kirigami.Icon {
                    visible: model.isRecommended
                    source: "starred-symbolic"
                    Layout.preferredHeight: 12
                    Layout.preferredWidth: 12
                }

                Label {
                    visible: model.isLTS
                    font.weight: Font.Bold
                    text: model.isLTS ?? "LTS"
                    Layout.preferredWidth: 20
                }

                Kirigami.Icon {
                    visible: model.isRealTime
                    source: "clock"
                    Layout.preferredHeight: 12
                    Layout.preferredWidth: 12
                }
            }
        }

        Button {
            id: changelogButton
            flat: true
            icon.name: "text-x-changelog"
            onClicked: rootItem.showChangelog()
        }

        Button {
            id: installButton
            enabled: vm.currentTransactionKernelName === ""
            visible: !model.isInUse
                     && vm.currentTransactionKernelName !== model.name
            flat: true
            icon.name: model.isInstalled ? "delete" : "download"
            onClicked: model.isInstalled ? rootItem.remove(
                                               ) : rootItem.install()
        }
        BusyIndicator {
            visible: vm.currentTransactionKernelName === model.name
            Layout.preferredHeight: 20
            Layout.preferredWidth: 32
        }
        Kirigami.Icon {
            visible: model.isInUse
            source: "emblem-system-symbolic"
            Layout.preferredHeight: 20
            Layout.preferredWidth: 30
        }
    }
}
