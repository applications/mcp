add_library(mcp-qt-page-kernel SHARED
    KernelListModel.cpp
    KernelListModel.h
    KernelModuleDescription.cpp
    KernelModuleDescription.h
    KernelViewModel.cpp
    KernelViewModel.h
    kernel.qrc
)

target_link_libraries(mcp-qt-page-kernel
    PRIVATE
    libmcp-kernel
    mcp-qt-common
)
target_compile_definitions(mcp-qt-page-kernel PUBLIC QT_NO_KEYWORDS)

set_property(TARGET mcp-qt-page-kernel PROPERTY CXX_STANDARD 20)

install(TARGETS mcp-qt-page-kernel)

if(MCP_BUILD_KCMS)

    # KI18N Translation Domain for this library
    add_definitions(-DTRANSLATION_DOMAIN=\"mcp_kernel\")


    add_library(mcp_kernel MODULE
        KernelKCModule.cpp
        KernelKCModule.h)

    target_link_libraries(mcp_kernel
        libmcp-kernel
        mcp-qt-common
        mcp-qt-page-kernel
        KF5::KCMUtils
        KF5::CoreAddons
        KF5::QuickAddons
        KF5::I18n
        Qt5::DBus
    )

    target_compile_definitions(mcp_kernel PUBLIC QT_NO_KEYWORDS)

    kcoreaddons_desktop_to_json(mcp_kernel "mcp_kernel.desktop" SERVICE_TYPES kcmodule.desktop)

    install(FILES mcp_kernel.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
    install(TARGETS mcp_kernel DESTINATION ${CMAKE_INSTALL_PLUGINDIR})
    kpackage_install_package(package mcp_kernel kcms)

endif(MCP_BUILD_KCMS)
