#include "KernelKCModule.h"

#include <KAboutData>
#include <KPluginFactory>
#include <KernelListModel.h>
#include <KernelService.h>

K_PLUGIN_FACTORY_WITH_JSON(KCMKernelFactory, "mcp_kernel.json", registerPlugin<KernelKCModule>();)

KernelKCModule::KernelKCModule(QObject *parent,
                               const KPluginMetaData &data,
                               const QVariantList &args)
    : KQuickAddons::ManagedConfigModule(parent, data, args)
{
    setButtons(NoAdditionalButton);
}

void KernelKCModule::load()
{
    ManagedConfigModule::load();

    m_container.emplace<PamacDatabaseService>(gi::repository::Pamac::Database::new_(
        gi::repository::Pamac::Config::new_("/etc/mcp/pamac.conf")));

    m_container.service<KernelServiceDependency>();
    m_container.service<KernelRepositoryDependency>();

    setVm(&m_container.service<KernelViewModelService>());
}

KernelViewModel *KernelKCModule::vm() const
{
    return m_vm;
}

void KernelKCModule::setVm(KernelViewModel *newVm)
{
    if (m_vm == newVm)
        return;
    m_vm = newVm;
    Q_EMIT vmChanged();
}

#include "KernelKCModule.moc"
