/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "KernelListModel.h"

#include <QMetaEnum>
#include <QNetworkReply>
#include <QNetworkRequest>

namespace MCP::Qt::Kernel {

int KernelListModel::rowCount(const QModelIndex &parent) const
{
    return m_list.size();
}

QVariant KernelListModel::data(const QModelIndex &index, int role) const
{
    auto &el = m_list[index.row()];

    switch (role) {
    case Name:
        return QString::fromStdString(el.package_name);
    case IsInstalled:
        return el.installed;
    case Version:
        return QString::fromStdString(el.version.to_string());
    case IsLTS:
        return el.lts;
    case IsRecommended:
        return el.recommended;
    case IsRealTime:
        return el.real_time;
    case IsEOL:
        return el.not_supported;
    case IsExperimental:
        return el.experimental;
    case IsInUse:
        return el.in_use;
    case MajorVersion:
        return el.version.major;
    case MinorVersion:
        return el.version.minor;
    case Category:
        if (el.in_use)
            return tr("In use");
        if (el.installed)
            return tr("Installed");
        return tr("Available");
    }

    return QVariant::Invalid;
}

const std::vector<Lib::Modules::Kernel::Kernel> &KernelListModel::list() const
{
    return m_list;
}

void KernelListModel::setList(const std::vector<Lib::Modules::Kernel::Kernel> &newList)
{
    if (m_list == newList)
        return;
    beginResetModel();

    m_list = newList;

    auto partitionEndIterator = std::partition(m_list.begin(), m_list.end(), [](const auto &a) {
        return a.installed;
    });
    std::partition(m_list.begin(), m_list.end(), [](const auto &a) { return a.in_use; });

    std::sort(partitionEndIterator, m_list.end(), [](const auto &a, const auto &b) {
        return (a.version > b.version);
    });

    endResetModel();

    Q_EMIT listChanged();
}

void KernelListModel::update()
{
    m_repository.get_kernels([this](const auto &kernels) {
        QMetaObject::invokeMethod(
            this, [this, kernels]() { setList(kernels); }, ::Qt::QueuedConnection);
    });
}

void KernelListModel::init()
{
    update();
}

QHash<int, QByteArray> KernelListModel::roleNames() const
{
    QHash<int, QByteArray> result;
    auto enumeration = QMetaEnum::fromType<Role>();
    for (int i = 0; i < enumeration.keyCount(); i++) {
        result[i + ::Qt::UserRole + 1] = enumeration.key(i);
        result[i + ::Qt::UserRole + 1][0] = std::tolower(result[i + ::Qt::UserRole + 1][0]);
    }
    return result;
}
} // namespace MCP::Qt::Kernel
