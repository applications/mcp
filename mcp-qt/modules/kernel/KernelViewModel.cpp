/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "KernelViewModel.h"
#include <QQmlEngine>
#include <QQuickItem>

#include <ranges>

namespace MCP::Qt::Kernel {

void KernelViewModel::init()
{
    connect(&m_progressNotifier, &Common::ProgressNotifier::finished, this, [this]() {
        setCurrentTransactionKernelName(QString{});

        m_model.update();
    });
}

KernelListModel *KernelViewModel::model() const
{
    return &m_model;
}

void KernelViewModel::installKernel(const QString &pkgName)
{
    setCurrentTransactionKernelName(pkgName);
    return m_service.install_kernel(pkgName.toStdString(), m_progressNotifier);
}

void KernelViewModel::removeKernel(const QString &pkgName)
{
    setCurrentTransactionKernelName(pkgName);
    return m_service.remove_kernel(pkgName.toStdString(), m_progressNotifier);
}

void KernelViewModel::getExtraModules(const QString &pkgName, const QJSValue &cb)
{
    if (!cb.isCallable()) {
        return;
    }

    m_service.get_extra_module_names(pkgName.toStdString(), [this, cb](const auto &modules) {
        QStringList result;
        result.reserve(modules.size());

        std::ranges::transform(modules, std::back_inserter(result), QString::fromStdString);

        QMetaObject::invokeMethod(
            this,
            [cb, result, this]() {
                QJSValue cbCopy(cb);
                auto engine = qjsEngine(this);

                cbCopy.call({engine->toScriptValue(result)});
            },
            ::Qt::QueuedConnection);
    });
}

void KernelViewModel::getChangelog(int major, int minor, const QJSValue &cb)
{
    if (!cb.isCallable()) {
        return;
    }

    m_service.pull_changelog({major, minor}, [this, cb](const std::optional<std::string> &changelog) {
        if (!changelog || changelog->empty()) {
            return;
        }
        QMetaObject::invokeMethod(
            this,
            [cb, changelog, this]() {
                QJSValue cbCopy(cb);
                auto engine = qjsEngine(this);

                cbCopy.call({engine->toScriptValue(QString::fromStdString(*changelog))});
            },
            ::Qt::QueuedConnection);
    });
}

Common::ProgressNotifier *KernelViewModel::progressNotifier()
{
    return &m_progressNotifier;
}

QString KernelViewModel::currentTransactionKernelName() const
{
    return m_currentTransactionKernelName;
}

void KernelViewModel::setCurrentTransactionKernelName(const QString &newCurrentTransactionKernelName)
{
    if (m_currentTransactionKernelName == newCurrentTransactionKernelName)
        return;
    m_currentTransactionKernelName = newCurrentTransactionKernelName;
    Q_EMIT currentTransactionKernelNameChanged(m_currentTransactionKernelName);
}

} // namespace MCP::Qt::Kernel
