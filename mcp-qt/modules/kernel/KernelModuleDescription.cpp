/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "KernelModuleDescription.h"

void initResources()
{
    Q_INIT_RESOURCE(kernel);
}

void MCP::Qt::Kernel::KernelPlugin::registerTypes(const char *uri)
{
    initResources();

    // @uri Kernel
    static int t1 = qmlRegisterUncreatableType<Kernel::KernelViewModel>(
        uri, 1, 0, "ViewModel", "You can't create view model, use `vm` context property instead");

    static int t2 = qmlRegisterUncreatableType<Kernel::KernelListModel>(
        uri,
        1,
        0,
        "ListModel",
        "You can't create list model, use `vm.model` context property instead");

    qmlRegisterUncreatableType<Common::ProgressNotifier>(
        uri,
        1,
        0,
        "ProgressNotifier",
        "You can't create view model, use `vm` context property instead");
}

void MCP::Qt::Kernel::KernelPlugin::registerDependencies(kgr::container &diContainer)
{
    diContainer.service<KernelServiceDependency>();
    diContainer.service<KernelRepositoryDependency>();
}
