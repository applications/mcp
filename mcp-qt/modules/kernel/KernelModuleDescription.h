/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#pragma once

#include "KernelViewModel.h"

#include <ModuleDescription.h>

#include <modules/kernel/KernelRepository.h>
#include <modules/kernel/KernelService.h>

#include <QtQml/QQmlExtensionPlugin>

namespace MCP::Qt::Kernel {

class KernelPlugin
{
public:
    static void registerTypes(const char *uri);

    static void registerDependencies(kgr::container &diContainer);
};

static constexpr char ModuleUrl[] = "qrc:/kernel-page/package/contents/ui/View.qml";
static constexpr char ModuleName[] = "Kernel";
static constexpr char ModuleDescription[] = "Description";
static constexpr char ModuleIcon[] = "preferences-system-linux";
using KernelModuleDescription = MCP::Qt::Common::QmlModuleDescription<ModuleUrl,
                                                                      ModuleName,
                                                                      ModuleDescription,
                                                                      ModuleIcon,
                                                                      KernelPlugin,
                                                                      KernelViewModelService>;
} // namespace MCP::Qt::Kernel
