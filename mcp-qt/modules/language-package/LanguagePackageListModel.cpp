/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "LanguagePackageListModel.h"
#include <QMetaEnum>
#include <QUrl>
#include <QtDebug>

#include <algorithm>

const std::vector<MCP::Lib::Modules::LanguagePackage::LanguagePackage>
    &MCP::Qt::LanguagePackage::LanguagePackageListModel::list() const
{
    return m_list;
}

void MCP::Qt::LanguagePackage::LanguagePackageListModel::setList(
    const std::vector<Lib::Modules::LanguagePackage::LanguagePackage> &newList)
{
    if (m_list == newList)
        return;

    m_list.clear();

    beginResetModel();

    std::ranges::copy_if(newList, std::back_inserter(m_list), [](const auto &lhs) {
        return lhs.needed;
    });

    std::partition(m_list.begin(), m_list.end(), [](const auto &lhs) { return !lhs.installed; });

    endResetModel();
    Q_EMIT listChanged();
}

void MCP::Qt::LanguagePackage::LanguagePackageListModel::update()
{
    m_service.get_available_packages([this](const auto &languages) {
        QMetaObject::invokeMethod(
            this, [this, languages]() { setList(languages); }, ::Qt::QueuedConnection);
    });
}

int MCP::Qt::LanguagePackage::LanguagePackageListModel::rowCount(const QModelIndex &parent) const
{
    return m_list.size();
}

QVariant MCP::Qt::LanguagePackage::LanguagePackageListModel::data(const QModelIndex &index,
                                                                  int role) const
{
    auto &langPack = m_list[index.row()];
    switch (role) {
    case Name:
        return QString::fromStdString(langPack.package_name);
    case Description:
        return QString::fromStdString(langPack.description);
    case Language:
        return QString::fromStdString(langPack.language);
    case Territory:
        return QString::fromStdString(langPack.territory);
    case IsNeeded:
        return langPack.needed;
    case IsInstalled:
        return langPack.installed;
    case Parents: {
        QStringList parents;

        parents.reserve(langPack.parent_packages.size());

        std::ranges::transform(langPack.parent_packages,
                               std::back_inserter(parents),
                               QString::fromStdString);

        return parents;
    }
    case FlagSource: {
        auto path = langPack.flag_path();
        if (path.empty()) {
            return QUrl();
        }
        return "file:/" + QString::fromStdString(path);
    }
    }

    return QVariant::Invalid;
}

QHash<int, QByteArray> MCP::Qt::LanguagePackage::LanguagePackageListModel::roleNames() const
{
    QHash<int, QByteArray> result;
    auto enumeration = QMetaEnum::fromType<Role>();
    for (int i = 0; i < enumeration.keyCount(); i++) {
        result[i + ::Qt::UserRole + 1] = enumeration.key(i);
        result[i + ::Qt::UserRole + 1][0] = std::tolower(result[i + ::Qt::UserRole + 1][0]);
    }
    return result;
}
