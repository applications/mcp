/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "LanguagePackageModuleDescription.h"

namespace MCP::Qt::LanguagePackage {

void Plugin::registerTypes(const char *uri)
{
    // @uri Kernel
    static int t1 = qmlRegisterUncreatableType<LanguagePackage::LanguagePackageViewModel>(
        uri, 1, 0, "ViewModel", "You can't create view model, use `vm` context property instead");

    static int t2 = qmlRegisterUncreatableType<LanguagePackage::LanguagePackageListModel>(
        uri,
        1,
        0,
        "ListModel",
        "You can't create list model, use `vm.model` context property instead");

    qRegisterMetaType<ProgressNotifier *>("ProgressNotifier");
}

void Plugin::registerDependencies(kgr::container &diContainer)
{
    diContainer.service<LanguageServiceDependency>();
}

} // namespace MCP::Qt::LanguagePackage
