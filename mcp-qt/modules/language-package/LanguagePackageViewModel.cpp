/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "LanguagePackageViewModel.h"
#include <QQmlEngine>

namespace MCP::Qt::LanguagePackage {

LanguagePackageViewModel::LanguagePackageViewModel(
    Lib::Modules::LanguagePackage::LanguagePackageServiceBase &service,
    LanguagePackageListModel &model)
    : m_service(service), m_model(model)
{
    connect(&m_progressNotifier, &Common::ProgressNotifier::finished, this, [this]() {
        m_model.update();
    });
}

LanguagePackageListModel *LanguagePackageViewModel::model()
{
    return &m_model;
}

Common::ProgressNotifier *LanguagePackageViewModel::progressNotifier()
{
    return &m_progressNotifier;
}

void LanguagePackageViewModel::installMissing()
{
    m_service.install_packages(m_progressNotifier);
}

} // namespace MCP::Qt::LanguagePackage
