/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include "LanguagePackageViewModel.h"

#include <ModuleDescription.h>

namespace MCP::Qt::LanguagePackage {

class Plugin
{
public:
    static void registerTypes(const char *uri);

    static void registerDependencies(kgr::container &diContainer);
};

static constexpr char ModuleUrl[] = "qrc:/language-package-page/package/contents/ui/View.qml";
static constexpr char ModuleName[] = "Language Packages";
static constexpr char ModuleDescription[] = "Description";
static constexpr char ModuleIcon[] = "preferences-desktop-locale";
using LanguagePackageModuleDescription
    = MCP::Qt::Common::QmlModuleDescription<ModuleUrl,
                                            ModuleName,
                                            ModuleDescription,
                                            ModuleIcon,
                                            Plugin,
                                            LanguagePackageViewModelService>;

} // namespace MCP::Qt::LanguagePackage
