add_library(mcp-qt-page-language-package SHARED
    LanguagePackageViewModel.cpp
    LanguagePackageViewModel.h
    LanguagePackageListModel.cpp
    LanguagePackageListModel.h
    LanguagePackageModuleDescription.cpp
    LanguagePackageModuleDescription.h
    language-package.qrc
)

target_link_libraries(mcp-qt-page-language-package
    PRIVATE
    libmcp-language-package
    mcp-qt-common
)
target_compile_definitions(mcp-qt-page-language-package PUBLIC QT_NO_KEYWORDS)

set_property(TARGET mcp-qt-page-language-package PROPERTY CXX_STANDARD 20)

install(TARGETS mcp-qt-page-language-package)


if(MCP_BUILD_KCMS)

    # KI18N Translation Domain for this library
    add_definitions(-DTRANSLATION_DOMAIN=\"mcp_kernel\")


    add_library(mcp_language_package MODULE
        LanguagePackageKCModule.cpp
        LanguagePackageKCModule.h)

    target_link_libraries(mcp_language_package
        libmcp-language-package
        mcp-qt-common
        mcp-qt-page-language-package
        KF5::KCMUtils
        KF5::CoreAddons
        KF5::QuickAddons
        KF5::I18n
        Qt5::DBus
    )

    target_compile_definitions(mcp_language_package PUBLIC QT_NO_KEYWORDS)

    kcoreaddons_desktop_to_json(mcp_language_package "mcp_language_package.desktop" SERVICE_TYPES kcmodule.desktop)

    install(FILES mcp_language_package.desktop DESTINATION ${KDE_INSTALL_KSERVICES5DIR})
    install(TARGETS mcp_language_package DESTINATION ${CMAKE_INSTALL_PLUGINDIR})
    kpackage_install_package(package mcp_language_package kcms)

endif(MCP_BUILD_KCMS)
