import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.15 as Kirigami

Kirigami.BasicListItem {
    property string name
    property string description
    id: rootItem
    contentItem: GridLayout {
        clip: true
        anchors.margins: 5

        Image {
            Layout.fillWidth: false
            Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
            Layout.column: 1
            Layout.row: 0
            Layout.rowSpan: 2

            source: model.flagSource
            Layout.preferredWidth: 30
            sourceSize.width: Layout.preferredWidth
        }
        Label {
            Layout.fillWidth: true
            elide: Text.ElideRight
            font.pointSize: 11
            Layout.column: 0
            Layout.row: 0
            text: rootItem.description
            wrapMode: Label.WrapAtWordBoundaryOrAnywhere
        }

        Label {
            Layout.fillWidth: true
            Layout.column: 0
            Layout.row: 1
            font.pointSize: Kirigami.Theme.smallFont.pointSize
            opacity: 0.6
            id: pkgname
            text: rootItem.name
        }
    }
}
