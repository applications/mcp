import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.19 as Kirigami
import org.manjaro.mcp.components 1.0 as Components
import org.kde.kcm 1.4 as KCM

KCM.AbstractKCM {

    id: root

    property var vm: kcm.vm
    framedView: false
    extraFooterTopPadding: false

    View {
        anchors.fill: parent
    }
}
