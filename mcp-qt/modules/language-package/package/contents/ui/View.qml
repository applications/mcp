import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.20 as Kirigami
import org.manjaro.mcp.components 1.0 as Components

Kirigami.ScrollablePage {
    id: root

    Kirigami.PromptDialog {
        id: errorDialog
        standardButtons: Kirigami.Dialog.NoButton
        title: qsTr("Error")
    }

    Components.InstallationDialog {
        id: installationDialog

        preferredWidth: parent.width * 0.85

        Connections {
            target: vm.progressNotifier

            function onProgressChanged(progress) {
                if (progress.action !== "")
                    installationDialog.details = progress.action

                installationDialog.progress = progress.progress * 100
            }

            function onStarted() {
                installationDialog.open()
            }
            function onFinished(res) {
                installationDialog.close()
            }
            function onErrorOccured(error) {
                errorDialog.subtitle = error
                errorDialog.open()
            }
        }
    }

    Components.ConfirmationDialog {
        id: confirmationDialog

        text: qsTr("Do you want to install missing language packages?")

        onAccepted: {
            vm.installMissing()
        }
    }

    LanguagePackListView {

        model: vm.model

        id: listView
    }
}
