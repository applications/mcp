/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <QAbstractListModel>
#include <QIcon>
#include <QObject>
#include <QPixmap>

#include <kangaru/service.hpp>
#include <modules/language-package/LanguagePackage.h>
#include <modules/language-package/LanguageService.h>
#include <vector>
namespace MCP::Qt::LanguagePackage {
class LanguagePackageListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Role {
        Name = ::Qt::UserRole + 1,
        Description,
        Language,
        Territory,
        IsNeeded,
        IsInstalled,
        Parents,
        FlagSource
    };
    Q_ENUM(Role)
    LanguagePackageListModel(Lib::Modules::LanguagePackage::LanguagePackageServiceBase &service)
        : m_service(service)
    {
        update();
    }

    const std::vector<Lib::Modules::LanguagePackage::LanguagePackage> &list() const;
    void setList(const std::vector<Lib::Modules::LanguagePackage::LanguagePackage> &newList);

    static QPixmap getCountryPixmap(const QString &territory)
    {
        auto pixmap = QIcon(":/country-flags/" + territory).pixmap(24, 24);

        return pixmap;
    }
    void update();

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

Q_SIGNALS:
    void listChanged();

private:
    std::vector<Lib::Modules::LanguagePackage::LanguagePackage> m_list;
    Lib::Modules::LanguagePackage::LanguagePackageServiceBase &m_service;
};
} // namespace MCP::Qt::LanguagePackage

struct LanguagePackageListModelDependency
    : kgr::single_service<MCP::Qt::LanguagePackage::LanguagePackageListModel,
                          kgr::dependency<LanguageServiceBaseDependency>>
{};
