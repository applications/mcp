/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: %YEAR% Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <KQuickAddons/ManagedConfigModule>

#include <kangaru/container.hpp>

#include "LanguagePackageViewModel.h"

namespace {
using namespace MCP::Qt::LanguagePackage;
}

class LanguagePackageKCModule : public KQuickAddons::ManagedConfigModule
{
    Q_OBJECT

    Q_PROPERTY(LanguagePackageViewModel *vm READ vm WRITE setVm NOTIFY vmChanged)

public:
    LanguagePackageKCModule(QObject *parent, const KPluginMetaData &data, const QVariantList &args);
    virtual ~LanguagePackageKCModule() override = default;

    LanguagePackageViewModel *vm() const;
    void setVm(LanguagePackageViewModel *newVm);

public Q_SLOTS:
    virtual void load() override;

Q_SIGNALS:
    void vmChanged();

private:
    kgr::container m_container;
    LanguagePackageViewModel *m_vm = nullptr;
};
