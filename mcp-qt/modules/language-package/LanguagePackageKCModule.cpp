/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: %YEAR% Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "LanguagePackageKCModule.h"

#include <KAboutData>
#include <KPluginFactory>
#include <LanguagePackageServiceBase.h>
#include <common/PamacDatabaseService.h>

K_PLUGIN_FACTORY_WITH_JSON(KCMLanguagePackageFactory,
                           "mcp_language_package.json",
                           registerPlugin<LanguagePackageKCModule>();)

LanguagePackageKCModule::LanguagePackageKCModule(QObject *parent,
                                                 const KPluginMetaData &data,
                                                 const QVariantList &args)
    : KQuickAddons::ManagedConfigModule(parent, data, args)
{
    setButtons(NoAdditionalButton);
}

void LanguagePackageKCModule::load()
{
    ManagedConfigModule::load();

    m_container.emplace<PamacDatabaseService>(gi::repository::Pamac::Database::new_(
        gi::repository::Pamac::Config::new_("/etc/mcp/pamac.conf")));

    m_container.service<LanguageServiceDependency>();

    setVm(&m_container.service<LanguagePackageViewModelService>());
}

LanguagePackageViewModel *LanguagePackageKCModule::vm() const
{
    return m_vm;
}

void LanguagePackageKCModule::setVm(LanguagePackageViewModel *newVm)
{
    if (m_vm == newVm)
        return;
    m_vm = newVm;
    Q_EMIT vmChanged();
}

#include "LanguagePackageKCModule.moc"
