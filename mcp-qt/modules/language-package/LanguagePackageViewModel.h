/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once
#include <iostream>

#include <ProgressNotifier.h>
#include <QQuickItem>

#include <modules/language-package/LanguageService.h>

#include <kangaru/service.hpp>

#include "LanguagePackageListModel.h"

namespace MCP::Qt::LanguagePackage {
namespace {
using namespace Common;
}
class LanguagePackageViewModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(LanguagePackageListModel *model READ model NOTIFY modelChanged)
    Q_PROPERTY(ProgressNotifier *progressNotifier READ progressNotifier NOTIFY
                   progressNotifierChanged CONSTANT)

public:
    LanguagePackageViewModel(Lib::Modules::LanguagePackage::LanguagePackageServiceBase &service,
                             LanguagePackageListModel &model);
    LanguagePackageListModel *model();
    ProgressNotifier *progressNotifier();

    void setModel(LanguagePackageListModel *newModel);

    Q_INVOKABLE void installMissing();

Q_SIGNALS:
    void modelChanged();
    void progressNotifierChanged();

private:
    Lib::Modules::LanguagePackage::LanguagePackageServiceBase &m_service;
    LanguagePackageListModel &m_model;

    ProgressNotifier m_progressNotifier;
};

} // namespace MCP::Qt::LanguagePackage

struct LanguagePackageViewModelService
    : kgr::single_service<
          MCP::Qt::LanguagePackage::LanguagePackageViewModel,
          kgr::dependency<LanguageServiceBaseDependency, LanguagePackageListModelDependency>>
{};
