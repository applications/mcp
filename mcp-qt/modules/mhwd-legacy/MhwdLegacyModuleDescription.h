/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: %YEAR% Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include "MhwdPage.h"
#include <ModuleDescription.h>

namespace MCP::Qt::MhwdLegacy {

constexpr char Name[] = "Hardware";
constexpr char Description[] = "Description";
constexpr char Icon[] = "preferences-devices-cpu";

using ModuleDescription = Common::LegacyModuleDescription<Name, Description, Icon, MhwdPage>;

} // namespace MCP::Qt::MhwdLegacy
