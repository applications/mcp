project(mcp-qt LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)
set(KF_MIN_VERSION "5.68.0")

find_package(ECM ${KF_MIN_VERSION} REQUIRED NO_MODULE)

set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

include(KDEInstallDirs)
include(KDECMakeSettings)

find_package(Qt5 REQUIRED Widgets Quick Qml QuickWidgets QuickControls2)
find_package(KF5 ${KF_MIN_VERSION} REQUIRED COMPONENTS Kirigami2 I18n CoreAddons Auth ConfigWidgets Declarative KCMUtils)

add_subdirectory(common)

add_subdirectory(app)

target_include_directories(mcp-qt PRIVATE ${CMAKE_CURRENT_LIST_DIR})

add_subdirectory(modules)

target_link_libraries(mcp-qt PRIVATE
    mcp-qt-page-kernel
    mcp-qt-page-language-package
    msm_page_mhwd)
