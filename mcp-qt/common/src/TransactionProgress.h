/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once
#include <QObject>
#include <QString>

#include <common/TransactionProgress.h>

namespace MCP::Qt::Common {

struct TransactionProgress
{
    Q_GADGET

    Q_PROPERTY(QString action READ action)
    Q_PROPERTY(QString details READ details)
    Q_PROPERTY(double progress READ progress)

public:
    QString action() const { return QString::fromStdString(m_progressHandle.action); }

    QString details() const { return QString::fromStdString(m_progressHandle.details); }

    double progress() const { return m_progressHandle.progress; }

    MCP::Lib::Common::TransactionProgress m_progressHandle;
};
} // namespace MCP::Qt::Common

Q_DECLARE_METATYPE(MCP::Qt::Common::TransactionProgress)
