/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "ProgressNotifier.h"

namespace MCP::Qt::Common {

ProgressNotifier::ProgressNotifier(QObject *parent) : QObject(parent) {}

void ProgressNotifier::start()
{
    QMetaObject::invokeMethod(
        this, [this]() { Q_EMIT started(); }, ::Qt::QueuedConnection);
}

void ProgressNotifier::report(const Lib::Common::TransactionProgress &value)
{
    QMetaObject::invokeMethod(
        this,
        [this, value]() { Q_EMIT progressChanged(TransactionProgress{value}); },
        ::Qt::QueuedConnection);
}

void ProgressNotifier::finish(bool result)
{
    QMetaObject::invokeMethod(
        this, [this, result]() { Q_EMIT finished(result); }, ::Qt::QueuedConnection);
}

void ProgressNotifier::error(const std::string &message)
{
    QMetaObject::invokeMethod(
        this,
        [this, message]() { Q_EMIT errorOccured(QString::fromStdString(message)); },
        ::Qt::QueuedConnection);
}

} // namespace MCP::Qt::Common
