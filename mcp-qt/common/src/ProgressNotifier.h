/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include "TransactionProgress.h"

#include <QObject>

#include <common/TransactionProgressNotifierBase.h>

namespace MCP::Qt::Common {

class ProgressNotifier : public QObject, public MCP::Lib::Common::TransactionProgressNotifierBase
{
    Q_OBJECT
public:
    explicit ProgressNotifier(QObject *parent = nullptr);

    void start() override;
    void report(const MCP::Lib::Common::TransactionProgress &value) override;
    void finish(bool result) override;
    void error(const std::string &message) override;

Q_SIGNALS:
    void started();
    void progressChanged(const MCP::Qt::Common::TransactionProgress &progress);
    void finished(bool success);
    void errorOccured(const QString &message);
};

} // namespace MCP::Qt::Common
