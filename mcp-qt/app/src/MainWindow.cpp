/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "MainWindow.h"
#include <ui_MainWindow.h>
#include <QQmlEngine>
#include <QQuickItem>
#include <QQuickView>
#include <QQuickWidget>
#include <QToolButton>

#include <KCoreAddons>

#include <pamac/pamac.hpp>

MainWindow::MainWindow() : QMainWindow(), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->quickWidget->setSource(QUrl("qrc:/MainSideMenu.qml"));
    ui->quickWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
    setWindowTitle("Manjaro Control Panel");

    connect(ui->quickWidget->rootObject(), SIGNAL(itemClicked(int)), this, SLOT(changeItem(int)));
    connect(this, &MainWindow::itemChanged, [this](int index) {
        QMetaObject::invokeMethod(ui->quickWidget->rootObject(), "highlightItem", Q_ARG(QVariant, index));
    });
    connect(m_manager,
            &MCP::Qt::ModuleManager::moduleAdded,
            this,
            [this](const auto &info, auto *view) {
                QVariantList model = ui->quickWidget->rootObject()->property("modules").toList();

                model.append(MCP::Qt::mapModuleToVariantMap(info));

                ui->quickWidget->rootObject()->setProperty("modules", model);
            });

    m_stack->setManager(m_manager);

    ui->centralwidget->layout()->addWidget(m_stack);
    setVersions();
}

void MainWindow::setVersions()
{
    auto about = ui->quickWidget->rootObject()->findChild<QObject *>("about");

    about->setProperty("pamacVersion", QString::fromStdString(gi::repository::Pamac::get_version()));
    about->setProperty("qtVersion", QString(qVersion()));
    about->setProperty("kfVersion", KCoreAddons::versionString());
}

void MainWindow::changeItem(int index)
{
    m_stack->setCurrentIndex(index);

    Q_EMIT itemChanged(index);
}
