/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include <QApplication>

#include <QtQuickControls2/QQuickStyle>

#include <MainWindow.h>

#include <common/PamacDatabaseService.h>

#include <modules/kernel/KernelModuleDescription.h>
#include <modules/language-package/LanguagePackageModuleDescription.h>
#include <modules/mhwd-legacy/MhwdLegacyModuleDescription.h>

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    if (qgetenv("XDG_CURRENT_DESKTOP") != "KDE") {
        QQuickStyle::setFallbackStyle("Fusion");
    }

    MainWindow window;

    auto &diContainer = window.manager()->container();

    diContainer.emplace<PamacDatabaseService>(gi::repository::Pamac::Database::new_(
        gi::repository::Pamac::Config::new_("/etc/mcp/pamac.conf")));

    window.manager()->load_from_description<MCP::Qt::Kernel::KernelModuleDescription>();
    window.manager()
        ->load_from_description<MCP::Qt::LanguagePackage::LanguagePackageModuleDescription>();
    window.manager()->load_from_description<MCP::Qt::MhwdLegacy::ModuleDescription>();

    window.show();

    return QApplication::exec();
}
