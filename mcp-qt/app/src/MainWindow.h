/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <ModuleManager.h>

#include <ModuleStack.h>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

    Q_PROPERTY(MCP::Qt::ModuleManager *manager READ manager WRITE setManager NOTIFY managerChanged)

public:
    MainWindow();

    MCP::Qt::ModuleManager *manager() const { return m_manager; }

public Q_SLOTS:
    void changeItem(int index);

Q_SIGNALS:
    void itemChanged(int index);

    void managerChanged();

private:
    void setManager(MCP::Qt::ModuleManager *new_manager)
    {
        if (m_manager == new_manager)
            return;
        m_manager = new_manager;
        Q_EMIT managerChanged();
    }
    void setVersions();
    MCP::Qt::ModuleManager *m_manager = new MCP::Qt::ModuleManager(this);

protected:
    MCP::Qt::ModuleStack *m_stack = new MCP::Qt::ModuleStack(this);
    Ui::MainWindow *ui;
};
