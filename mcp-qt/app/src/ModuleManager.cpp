/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "ModuleManager.h"
#include <QWidget>

namespace MCP::Qt {
ModuleManager::ModuleManager(QObject *parent) : QObject{parent}
{
    module_added.connect([this](const auto &info, auto *view) { Q_EMIT moduleAdded(info, view); });
}

ModuleManager::~ModuleManager() {}

QVariantMap mapModuleToVariantMap(const Lib::Common::Modules::ModuleMetaInfo &info)
{
    return {{"name", QString::fromStdString(info.name)},
            {"description", QString::fromStdString(info.description)},
            {"icon", QString::fromStdString(info.icon)}};
}

} // namespace MCP::Qt
