/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <QObject>

#include <modulemanager/ModuleManager.h>

#include <QVariantMap>

namespace MCP::Qt {

class ModuleManager : public QObject, public Lib::Common::Modules::ModuleManagerBase<QWidget>
{
    Q_OBJECT

public:
    explicit ModuleManager(QObject *parent = nullptr);
    ~ModuleManager();

Q_SIGNALS:
    void moduleAdded(const Lib::Common::Modules::ModuleMetaInfo &info, QWidget *module);
};

QVariantMap mapModuleToVariantMap(const Lib::Common::Modules::ModuleMetaInfo &info);

} // namespace MCP::Qt
