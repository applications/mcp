/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <ModuleManager.h>
#include <QStackedWidget>
#include <QWidget>

namespace MCP::Qt {

class ModuleStack : public QStackedWidget
{
    Q_OBJECT

    Q_PROPERTY(ModuleManager *manager READ manager WRITE setManager NOTIFY managerChanged)

public:
    explicit ModuleStack(QWidget *parent = nullptr);

    ModuleManager *manager() const { return m_manager; }
    void setManager(ModuleManager *new_manager)
    {
        if (m_manager == new_manager)
            return;
        deinitManager();
        m_manager = new_manager;
        initManager();
        Q_EMIT managerChanged();
    }

Q_SIGNALS:
    void managerChanged();

private:
    void addModule();

    void initManager();
    void deinitManager();
    ModuleManager *m_manager = nullptr;
};

} // namespace MCP::Qt
