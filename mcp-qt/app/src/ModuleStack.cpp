/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "ModuleStack.h"

namespace MCP::Qt {

ModuleStack::ModuleStack(QWidget *parent) : QStackedWidget(parent) {}

void ModuleStack::initManager()
{
    if (!m_manager)
        return;

    connect(m_manager, &ModuleManager::moduleAdded, this, [this](const auto &info, QWidget *m) {
        this->addWidget(qobject_cast<QWidget *>(m));
    });
}

void ModuleStack::deinitManager()
{
    if (!m_manager)
        return;

    disconnect(m_manager, nullptr, this, nullptr);
}

} // namespace MCP::Qt
