import QtQuick 2.15
import QtQuick.Controls 2.15
import org.kde.kirigami 2.15 as Kirigami

Page {

    Kirigami.Theme.colorSet: Kirigami.Theme.View

    property var modules: []
    property int highlighted: -1

    signal itemClicked(int index)
    id: root

    function highlightItem(index) {
        highlighted = index
    }
    ListView {
        anchors.fill: parent
        model: modules

        boundsBehavior: ListView.StopAtBounds

        delegate: Kirigami.BasicListItem {
            action: Kirigami.Action {
                icon.name: modelData.icon
            }

            id: delegate

            height: 35
            width: parent.width
            highlighted: root.highlighted === modelData.stackIndex

            text: modelData.name

            onClicked: itemClicked(index)
        }
    }
    Rectangle {
        width: 1
        z: 10
        anchors {
            top: parent.top
            right: parent.right
            bottom: parent.bottom
        }

        color: Kirigami.ColorUtils.linearInterpolation(
                   Kirigami.Theme.backgroundColor,
                   Kirigami.Theme.textColor, 0.15)
    }
    Button {
        anchors {
            left: parent.left
            bottom: parent.bottom
            right: parent.right
        }
        z: 1
        flat: true
        icon.name: "help-about-symbolic"
        text: qsTr("About")
        onClicked: about.visible = true
    }

    AboutWindow {
        id: about
        objectName: "about"
    }
}
