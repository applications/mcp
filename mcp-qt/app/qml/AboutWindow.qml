import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import org.kde.kirigami 2.15 as Kirigami
import QtQuick.Layouts 1.15

Window {
    width: 600
    height: 280

    maximumHeight: height
    maximumWidth: width

    minimumHeight: height
    minimumWidth: width

    property string qtVersion
    property string kfVersion
    property string pamacVersion

    title: qsTr("About")
    SystemPalette {
        id: palette
    }

    Rectangle {
        anchors.fill: parent
        color: palette.window
    }
    Row {
        id: titleRow
        anchors {
            top: parent.top
            left: parent.left
            margins: 20
        }
        spacing: 10

        Image {
            height: 50
            width: height
            id: mcpLogo
            source: "qrc:/assets/icon.svg"
            sourceSize.width: width
            sourceSize.height: height
        }
        Label {
            anchors.verticalCenter: parent.verticalCenter
            font.family: "Comfortaa"
            font.pointSize: 12
            text: "Manjaro Control Panel"
        }
    }

    ColumnLayout {
        id: details
        width: parent.width * 0.65
        anchors.margins: 10
        anchors.left: parent.left
        anchors.top: titleRow.bottom

        Label {
            Layout.fillWidth: true
            textFormat: Label.MarkdownText
            wrapMode: Label.WrapAtWordBoundaryOrAnywhere
            text: qsTr("A control center for Manjaro Linux maintainance")
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: 10
            ColumnLayout {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop
                Label {
                    font.pointSize: 12

                    text: qsTr("Authors")
                }

                Repeater {
                    model: ["**Artem Grinev** ‒ developer", "**Bogdan Covaciu** ‒ icon, design ideas", "**Mark Wagie** ‒ QA"]

                    Label {
                        textFormat: Label.MarkdownText
                        font.pointSize: 8
                        text: modelData
                    }
                }
            }
            ColumnLayout {
                Layout.alignment: Qt.AlignTop | Qt.AlignRight
                Label {
                    font.pointSize: 12

                    text: "Libraries"
                }

                Repeater {
                    model: ["**Qt** " + qtVersion, "**KDE Frameworks** "
                        + kfVersion, "**Kirigami 2**", "**libpamac** " + pamacVersion]

                    Label {
                        textFormat: Label.MarkdownText
                        font.pointSize: 8
                        text: modelData
                    }
                }
            }
        }

        RowLayout {
            ToolButton {
                icon.name: "git"
                text: qsTr("Contibute")
                onClicked: Qt.openUrlExternally(
                               "https://gitlab.com/applications/mcp")
            }
            ToolButton {
                icon.name: "license"
                text: "GNU GPLv3"
                onClicked: Qt.openUrlExternally(
                               "https://www.gnu.org/licenses/gpl-3.0.en.html")
            }
        }
    }
    Image {
        transform: Scale {
            id: transform
            origin.x: mascot.width / 2
        }

        SequentialAnimation {
            id: flipAnimation

            NumberAnimation {
                target: transform
                easing.type: Easing.InOutQuad
                property: "xScale"
                to: -1
                duration: 200
            }
            NumberAnimation {
                easing.type: Easing.InOutQuad
                target: transform
                property: "xScale"
                to: 1
                duration: 300
            }
            NumberAnimation {
                target: transform
                easing.type: Easing.InOutQuad
                property: "xScale"
                to: -1
                duration: 400
            }
            NumberAnimation {
                easing.type: Easing.InOutQuad
                target: transform
                property: "xScale"
                to: 1
                duration: 500
            }
        }
        id: mascot
        source: "qrc:/assets/mascot.svg"
        anchors.right: parent.right
        anchors.left: details.right
        anchors.margins: parent.width * 0.07
        anchors.verticalCenter: parent.verticalCenter
        sourceSize.width: width
        fillMode: Image.PreserveAspectFit
        MouseArea {
            anchors.fill: parent
            onClicked: flipAnimation.start()
        }
    }
}
