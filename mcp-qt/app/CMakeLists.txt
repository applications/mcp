add_executable(mcp-qt)

target_link_libraries(mcp-qt PRIVATE
    mcp-qt-common)

target_compile_definitions(mcp-qt PRIVATE QT_NO_KEYWORDS)
set_property(TARGET mcp-qt PROPERTY CXX_STANDARD 20)

add_subdirectory(qml)

add_subdirectory(src)

add_subdirectory(ui)

add_subdirectory(assets)

install(TARGETS mcp-qt)
