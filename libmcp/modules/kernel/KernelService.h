/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once
#include "KernelServiceBase.h"

#include <pamac/pamac.hpp>

#include <sigc++/sigc++.h>

#include <optional>
#include <vector>

#include <common/PamacDatabaseService.h>

namespace MCP::Lib::Modules::Kernel {

class KernelService : public KernelServiceBase
{
public:
    explicit KernelService(gi::repository::Pamac::Database &database);

    virtual ~KernelService() = default;

    virtual void install_kernel(const std::string &package_name,
                                MCP::Lib::Common::TransactionProgressNotifierBase &progress) override;
    virtual void remove_kernel(const std::string &package_name,
                               MCP::Lib::Common::TransactionProgressNotifierBase &progress) override;

    virtual void pull_changelog(
        const std::pair<int, int> &version,
        const std::function<void(const std::optional<std::string> &)> &cb) override;
    virtual void get_extra_module_names(
        const std::string &package_name,
        const std::function<void(const std::vector<std::string> &)> &cb) override;

private:
    gi::repository::Pamac::Database &m_database;
};

} // namespace MCP::Lib::Modules::Kernel

struct KernelServiceDependency : kgr::single_service<MCP::Lib::Modules::Kernel::KernelService,
                                                     kgr::dependency<PamacDatabaseService>>,
                                 kgr::overrides<KernelServiceBaseDependency>
{};
