/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "KernelRepository.h"

#include "KernelPackageMapper.h"

#include <memory>
#include <ranges>

#include <gio/asyncresult.hpp>

MCP::Lib::Modules::Kernel::KernelRepository::KernelRepository(
    gi::repository::Pamac::Database &database)
    : m_database(database)
{}

void MCP::Lib::Modules::Kernel::KernelRepository::get_kernels(
    const std::function<void(const std::vector<Kernel> &)> &callback)
{
    m_database.search_repos_pkgs_async(
        "^linux([0-9][0-9]?([0-9])|[0-9][0-9]?([0-9])-rt)$",
        [callback, this](gi::repository::GObject::Object obj, gi::repository::Gio::AsyncResult res) {
            auto pkgs = gi::detail::wrap_list<gi::repository::Pamac::AlpmPackage>(
                m_database.search_pkgs_finish(res).gobj_(), gi::transfer_none);

            std::vector<MCP::Lib::Modules::Kernel::Kernel> result;
            result.reserve(pkgs.size());

            std::ranges::transform(pkgs, std::back_inserter(result), Mapper::kernel_from_package);

            callback(result);
        });
}
