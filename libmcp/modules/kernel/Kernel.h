/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <compare>
#include <fmt/format.h>
#include <string>

namespace MCP::Lib::Modules::Kernel {

struct Kernel
{
    Kernel() = default;

    std::string package_name;

    struct
    {
        int major;
        int minor;
        std::string patch;

        std::string to_string() const { return fmt::format("{}.{}.{}", major, minor, patch); }

        auto operator<=>(const auto &rhs) const
        {
            const auto &lhs = *this;
            if (lhs.major > rhs.major) {
                return std::strong_ordering::greater;
            } else if (lhs.major < rhs.major) {
                return std::strong_ordering::less;
            }

            if (lhs.minor > rhs.minor) {
                return std::strong_ordering::greater;
            } else if (lhs.minor < rhs.minor) {
                return std::strong_ordering::less;
            }

            return std::strong_ordering::equal;
        }
        bool operator==(const auto &rhs) const
        {
            return this->operator<=>(rhs) == std::strong_ordering::equal;
        }
    } version;

    bool lts : 1;
    bool recommended : 1;
    bool installed : 1;
    bool not_supported : 1;
    bool real_time : 1;
    bool in_use : 1;
    bool experimental : 1;
};

inline bool operator==(const Kernel &lhs, const Kernel &rhs)
{
    return lhs.package_name == rhs.package_name && lhs.version == rhs.version;
}

} // namespace MCP::Lib::Modules::Kernel
