/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <kangaru/kangaru.hpp>

#include <common/TransactionProgressNotifierBase.h>

#include <future>
#include <optional>
#include <sigc++/signal.h>
#include <string>
#include <vector>

namespace MCP::Lib::Modules::Kernel {

struct KernelServiceBase
{
    KernelServiceBase() = default;
    virtual ~KernelServiceBase() = default;

    virtual void install_kernel(const std::string &package_name,
                                Common::TransactionProgressNotifierBase &progress)
        = 0;
    virtual void remove_kernel(const std::string &package_name,
                               Common::TransactionProgressNotifierBase &progress)
        = 0;

    virtual void pull_changelog(const std::pair<int, int> &version,
                                const std::function<void(const std::optional<std::string> &)> &cb)
        = 0;

    virtual void get_extra_module_names(
        const std::string &package_name,
        const std::function<void(const std::vector<std::string> &)> &cb)
        = 0;
};

} // namespace MCP::Lib::Modules::Kernel

struct KernelServiceBaseDependency
    : kgr::abstract_service<MCP::Lib::Modules::Kernel::KernelServiceBase>
{};
