/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include "Kernel.h"
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <sys/utsname.h>

#include <pamac/pamac.hpp>

namespace MCP::Lib::Modules::Kernel::Mapper {

namespace {
decltype(Kernel::version) parse_version(const std::string &version)
{
    std::vector<std::string> version_elements;
    boost::split(version_elements, version, boost::is_any_of("\\."));

    if (version_elements.size() < 2) {
        return {};
    }
    if (version_elements.size() < 3) {
        return {.major = std::stoi(version_elements[0]), .minor = std::stoi(version_elements[1])};
    }

    return {.major = std::stoi(version_elements[0]),
            .minor = std::stoi(version_elements[1]),
            .patch = version_elements[2]};
}
} // namespace

Kernel kernel_from_package(const gi::repository::Pamac::Package &package)
{
    Kernel result;

    result.package_name = package.property_name().get();

    result.lts = false;
    result.recommended = false;

    auto version = package.property_version().get();

    result.installed = !package.property_installed_version().get().empty();

    result.real_time = result.package_name.contains("-rt");

    result.experimental = version.contains("rc");

    utsname nm;
    uname(&nm);

    result.version = parse_version(version);
    auto current_version = parse_version(nm.release);

    auto is_current_rt = std::string(nm.release).contains("-rt");

    result.in_use = (result.version.major == current_version.major
                     && result.version.minor == current_version.minor
                     && is_current_rt == result.real_time);

    result.not_supported = false;

    return result;
}

} // namespace MCP::Lib::Modules::Kernel::Mapper
