set(CMAKE_CXX_STANDARD 23)

add_library(libmcp-kernel SHARED
    Kernel.h
    KernelPackageMapper.h
    KernelRepository.cpp
    KernelRepository.h
    KernelRepositoryBase.h
    KernelService.cpp
    KernelService.h
    KernelServiceBase.h
)

target_link_libraries(libmcp-kernel PUBLIC
    libmcp-common)

target_include_directories(libmcp-kernel PUBLIC ${CMAKE_CURRENT_LIST_DIR})
target_compile_definitions(libmcp-kernel
    PUBLIC INSTALL_PREFIX=\"${CMAKE_INSTALL_PREFIX}\")

if(CACHE_CHANGELOGS)

    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/kernel/changelogs/)

    add_custom_target(
        cache_changelogs ALL
        COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/kernel/cache_changelog.py
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/kernel/changelogs/
        COMMENT "Caching changelogs..."
    )
    install( DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/kernel/changelogs/ DESTINATION share/mcp/changelogs/)

    add_dependencies(libmcp cache_changelogs)

endif(CACHE_CHANGELOGS)

install(TARGETS libmcp-kernel)
