import gi
gi.require_version('Pamac', '11')
from gi.repository import GLib, Pamac
from os.path import exists
import re   
from multiprocessing.pool import ThreadPool as Pool
import urllib.request, urllib.error, urllib.parse

def parse_ver(versionstring):	
    v = versionstring.split('.')
    v0 = int(re.search(r'\d+', v[0]).group())
    v1 = int(re.search(r'\d+', v[1]).group())

    return [v0, v1]

def download_changelog(pkg):
    ver = parse_ver(pkg.get_version())
    
    if exists(f'{ver[0]}.{ver[1]}.html'):
        print("Version already exists:", ver)
        return

    url = f'https://kernelnewbies.org/Linux_{ver[0]}.{ver[1]}?action=print'
    try:
        response = urllib.request.urlopen(url)
        webContent = response.read().decode('UTF-8')

        open(f'{ver[0]}.{ver[1]}.html', 'w').write(webContent)
    except:
        print("Version not found:", ver)
	
pool_obj = Pool()

if __name__ == "__main__":
    loop = GLib.MainLoop()
    config = Pamac.Config(conf_path="/etc/pamac.conf")
    db = Pamac.Database(config=config)
    pkgs = db.search_repos_pkgs('^linux([0-9][0-9]?([0-9])|[0-9][0-9]?([0-9])-rt)$')

    pool_obj.map(download_changelog, pkgs)
