/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "KernelService.h"
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <filesystem>
#include <fmt/format.h>

#include <fstream>

#include <sys/utsname.h>

MCP::Lib::Modules::Kernel::KernelService::KernelService(gi::repository::Pamac::Database &database)
    : KernelServiceBase(), m_database(database)
{}

void run_transaction(gi::repository::Pamac::Transaction &transaction,
                     MCP::Lib::Common::TransactionProgressNotifierBase &progress)
{
    progress.connect_transaction(transaction);

    namespace gir = gi::repository;

    transaction.run_async([&progress](gir::GObject::Object object, gir::Gio::AsyncResult res) {
        auto transaction = gi::object_cast<gir::Pamac::Transaction>(object);

        bool result = transaction.run_finish(res);

        progress.finish(result);
        transaction.quit_daemon();
    });
    progress.start();
}

template<typename TCallback>
void is_updated(gi::repository::Pamac::Database &database, const TCallback &cb)
{
    database.get_updates_async(
        true, [cb](gi::repository::GObject::Object obj, gi::repository::Gio::AsyncResult result) {
            auto updates = gi::object_cast<gi::repository::Pamac::Database>(obj).get_updates_finish(
                result);

            auto packages = gi::detail::wrap_list<gi::repository::Pamac::AlpmPackage>(
                updates.get_repos_updates().gobj_(), gi::transfer_none);

            cb(packages.empty());
        });
}

void MCP::Lib::Modules::Kernel::KernelService::install_kernel(
    const std::string &package_name, MCP::Lib::Common::TransactionProgressNotifierBase &progress)
{
    is_updated(m_database, [&progress, package_name, this](bool updated) {
        if (!updated) {
            progress.start();
            progress.error("Installation is not possible, please update your system first.");
            progress.finish(false);
            return;
        }
        auto transaction = gi::repository::Pamac::Transaction::new_(m_database);

        transaction.add_pkg_to_install(package_name);
        transaction.add_pkg_to_install(fmt::format("{}-headers", package_name));

        run_transaction(transaction, progress);
    });
}

void MCP::Lib::Modules::Kernel::KernelService::remove_kernel(
    const std::string &package_name, MCP::Lib::Common::TransactionProgressNotifierBase &progress)
{
    auto transaction = gi::repository::Pamac::Transaction::new_(m_database);

    transaction.set_cascade(true);

    transaction.add_pkg_to_remove(package_name);
    transaction.add_pkg_to_remove(fmt::format("{}-headers", package_name));

    run_transaction(transaction, progress);
}

void MCP::Lib::Modules::Kernel::KernelService::pull_changelog(
    const std::pair<int, int> &version,
    const std::function<void(const std::optional<std::string> &)> &cb)
{
    std::thread([version, cb]() {
        auto name = fmt::format("{}/share/mcp/changelogs/{}.{}.html",
                                INSTALL_PREFIX,
                                version.first,
                                version.second);

        if (!std::filesystem::exists(name)) {
            cb({});
        }

        std::ifstream ifs(name);
        std::string data((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());

        if (!ifs.good()) {
            return cb({});
        }

        cb(data);
    }).detach();
}
std::optional<std::string> current_kernel()
{
    utsname nm;
    if (uname(&nm) < 0) {
        return {};
    }

    std::vector<std::string> version;
    boost::split(version, nm.release, boost::is_any_of("\\."));

    if (version.size() < 2) {
        return {};
    }

    return fmt::format("linux{}{}", version[0], version[1]);
}

void MCP::Lib::Modules::Kernel::KernelService::get_extra_module_names(
    const std::string &package_name, const std::function<void(const std::vector<std::string> &)> &cb)
{
    std::thread([this, package_name, cb]() {
        auto current_package = current_kernel();

        auto group_pkgs = m_database.get_group_pkgs(
            fmt::format("{}-extramodules", *current_package));

        auto list = gi::detail::wrap_list<gi::repository::Pamac::Package>(group_pkgs.gobj_(),
                                                                          gi::transfer_none);

        std::vector<std::string> result;

        for (const auto &module : list) {
            if (module.property_installed_version().get().empty()) {
                continue;
            }

            auto module_name = module.property_name().get();

            auto dash_pos = module_name.find("-");

            auto target_module_name = module_name.substr(dash_pos);

            if (target_module_name.empty()) {
                continue;
            }

            target_module_name = package_name + target_module_name;

            auto pkg = m_database.get_pkg(target_module_name);

            if (!pkg) {
                continue;
            }

            result.emplace_back(target_module_name);
        }

        cb(result);
    }).detach();
}
