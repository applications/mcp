/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <future>
#include <vector>

#include "Kernel.h"

#include <kangaru/service.hpp>

namespace MCP::Lib::Modules::Kernel {

struct KernelRepositoryBase
{
    KernelRepositoryBase() = default;
    virtual ~KernelRepositoryBase() = default;

    virtual void get_kernels(const std::function<void(const std::vector<Kernel> &)> &) = 0;
};

} // namespace MCP::Lib::Modules::Kernel

struct KernelRepositoryBaseDependency
    : kgr::abstract_service<MCP::Lib::Modules::Kernel::KernelRepositoryBase>
{};
