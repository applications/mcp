/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include "KernelRepositoryBase.h"

#include <pamac/pamac.hpp>

#include <common/PamacDatabaseService.h>

namespace MCP::Lib::Modules::Kernel {
class KernelRepository : public KernelRepositoryBase
{
public:
    explicit KernelRepository(gi::repository::Pamac::Database &database);
    virtual ~KernelRepository() = default;

    void get_kernels(const std::function<void(const std::vector<Kernel> &)> &) override;

private:
    gi::repository::Pamac::Database &m_database;
};
} // namespace MCP::Lib::Modules::Kernel

struct KernelRepositoryDependency : kgr::single_service<MCP::Lib::Modules::Kernel::KernelRepository,
                                                        kgr::dependency<PamacDatabaseService>>,
                                    kgr::overrides<KernelRepositoryBaseDependency>
{};
