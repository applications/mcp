/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "LanguagePackage.h"

std::filesystem::__cxx11::path MCP::Lib::Modules::LanguagePackage::LanguagePackage::flag_path() const
{
    if (territory == "global") {
        return "";
    }

    return std::string(INSTALL_PREFIX) + "/share/mcp/country-flags/" + territory + ".svg";
}
