/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include "LanguagePackage.h"
#include "LanguagePackageServiceBase.h"

#include <kangaru/service.hpp>
#include <pamac/pamac.hpp>

#include <common/PamacDatabaseService.h>
#include <common/TransactionProgressNotifierBase.h>

#include <vector>

namespace MCP::Lib::Modules::LanguagePackage {

class LanguageService : public LanguagePackageServiceBase
{
public:
    LanguageService(gi::repository::Pamac::Database &db) : m_db(db) {}
    ~LanguageService() = default;

    virtual void get_available_packages(
        const std::function<void(const std::vector<LanguagePackage>)> &callback) override;
    virtual void install_packages(
        MCP::Lib::Common::TransactionProgressNotifierBase &progress) override;

private:
    gi::repository::Pamac::Database &m_db;
};

} // namespace MCP::Lib::Modules::LanguagePackage

struct LanguageServiceDependency
    : kgr::single_service<MCP::Lib::Modules::LanguagePackage::LanguageService,
                          kgr::dependency<PamacDatabaseService>>,
      kgr::overrides<LanguageServiceBaseDependency>
{};
