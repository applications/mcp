/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <functional>
#include <string>

#include "LanguagePackage.h"

#include <common/TransactionProgressNotifierBase.h>

#include <kangaru/service.hpp>

namespace MCP::Lib::Modules::LanguagePackage {

struct LanguagePackageServiceBase
{
    virtual ~LanguagePackageServiceBase() = default;

    virtual void get_available_packages(
        const std::function<void(const std::vector<LanguagePackage>)> &callback)
        = 0;
    virtual void install_packages(MCP::Lib::Common::TransactionProgressNotifierBase &progress) = 0;
};

} // namespace MCP::Lib::Modules::LanguagePackage

struct LanguageServiceBaseDependency
    : kgr::abstract_service<MCP::Lib::Modules::LanguagePackage::LanguagePackageServiceBase>
{};
