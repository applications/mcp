/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "LanguageService.h"

#include "nlohmann/json.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>
#include <thread>

static const auto lower_transformer = [](char c) { return std::tolower(c); };
static const auto upper_transformer = [](char c) { return std::toupper(c); };

std::vector<std::string> mutate_locale(const std::string &language, const std::string &territory)
{
    std::vector<std::string> result;

    std::string lower_language(language), lower_territory(territory);
    std::string upper_language(language), upper_territory(territory);

    std::ranges::transform(language, lower_language.begin(), lower_transformer);
    std::ranges::transform(territory, lower_territory.begin(), lower_transformer);
    std::ranges::transform(territory, upper_territory.begin(), upper_transformer);

    // Example: firefox-i18n-% -> firefox-i18n-en-US
    result.emplace_back(lower_language + "-" + upper_territory);
    // Example: firefox-i18n-% -> firefox-i18n-en-us
    result.emplace_back(lower_language + "-" + lower_territory);
    // Example: firefox-i18n-% -> firefox-i18n-en_US
    result.emplace_back(lower_language + "_" + upper_territory);
    // Example: firefox-i18n-% -> firefox-i18n-en_us
    result.emplace_back(lower_language + "_" + lower_territory);
    // Example: firefox-i18n-% -> firefox-i18n-en
    result.emplace_back(lower_language);

    return result;
}

std::vector<std::pair<std::string, std::string>> get_locales()
{
    std::ifstream file("/etc/locale.gen", std::ios::in);
    std::string locale;
    std::vector<std::pair<std::string, std::string>> locale_list;

    while (std::getline(file, locale)) {
        if (locale.length() == 0 || locale.front() == '#') { continue; }

        std::size_t delimiterIndex = locale.find(" ");
        locale = locale.substr(0, delimiterIndex);

        if ((delimiterIndex = locale.find(".")) != std::string::npos) {
            locale = locale.substr(0, delimiterIndex);
        }

        delimiterIndex = locale.find("_");

        auto lng = locale.substr(0, delimiterIndex);
        auto ter =
            locale.substr(delimiterIndex + 1, locale.size() - delimiterIndex);

        locale_list.emplace_back(lng, ter);
    }

    file.close();
    return locale_list;
}
struct LanguagePackageEntry {
    std::string name;
    std::string l10n_package;
    std::vector<std::string> parent_packages;
};

std::vector<LanguagePackageEntry> get_localizable_packages()
{
    std::vector<LanguagePackageEntry> result;

    std::string name =
        std::string(INSTALL_PREFIX) + "/share/mcp/language_packages.json";
    std::ifstream input_stream(name);
    nlohmann::json json_document;
    try {
        input_stream >> json_document;

        for (auto &lngPackageEntry : json_document["Packages"]) {
            LanguagePackageEntry entry;

            entry.name = lngPackageEntry["name"].get<std::string>();
            entry.l10n_package =
                lngPackageEntry["l10n_package"].get<std::string>();

            auto parents = lngPackageEntry["parent_packages"];
            entry.parent_packages.resize(parents.size());

            std::ranges::transform(parents, entry.parent_packages.begin(), [](const auto &package) {
                return package.template get<std::string>();
            });

            result.emplace_back(entry);
        }
        return result;
    } catch (const std::exception &e) {
        std::cout << e.what();
        return result;
    }
}

void MCP::Lib::Modules::LanguagePackage::LanguageService::get_available_packages(
    const std::function<void(const std::vector<LanguagePackage>)> &callback)
{
    std::thread([callback, this]() {
        std::set<LanguagePackage> result;

        auto locales = get_locales();
        auto localizable_packages = get_localizable_packages();

        for (const auto &[language, territory] : locales) {
            auto mutations = mutate_locale(language, territory);

            for (const auto &localizable_package : localizable_packages) {
                gi::repository::Pamac::AlpmPackage pkg;
                std::size_t index;
                bool global = false;

                if ((index = localizable_package.l10n_package.find("%")) != std::string::npos) {
                    for (const auto &mutation : mutations) {
                        auto mutated_package_name = localizable_package.l10n_package;

                        mutated_package_name.replace(index, 1, mutation);

                        pkg = m_db.get_pkg(mutated_package_name);

                        if (pkg != nullptr) {
                            break;
                        }
                    }
                    global = false;

                } else {
                    pkg = m_db.get_pkg(localizable_package.l10n_package);
                    global = true;
                }

                if (pkg == nullptr) {
                    continue;
                }

                LanguagePackage package{.package_name = pkg.property_name().get(),
                                        .parent_packages = localizable_package.parent_packages,
                                        .description = pkg.property_desc().get(),
                                        .language = language,
                                        .installed = !pkg.get_installed_version().empty(),
                                        .needed = false};

                if (global) {
                    package.territory = "global";
                } else {
                    std::ranges::transform(territory,
                                           std::back_inserter(package.territory),
                                           lower_transformer);
                }
                for (auto &pkg : localizable_package.parent_packages) {
                    auto installedParent = m_db.get_installed_pkg(pkg);

                    if (installedParent != nullptr) {
                        package.needed = true;
                        break;
                    }
                }
                result.emplace(package);
            }
        }
        callback(std::vector(result.begin(), result.end()));
    }).detach();
}

void MCP::Lib::Modules::LanguagePackage::LanguageService::install_packages(
    MCP::Lib::Common::TransactionProgressNotifierBase &progress)
{
    get_available_packages([this, &progress](const auto &pkgs) {
        auto transaction = gi::repository::Pamac::Transaction::new_(m_db);

        for (auto &pkg : pkgs) {
            if (pkg.needed && !pkg.installed) {
                transaction.add_pkg_to_install(pkg.package_name);
            }
        }

        progress.connect_transaction(transaction);

        progress.start();
        transaction.run_async([&progress](gi::repository::GObject::Object object,
                                          gi::repository::Gio::AsyncResult result) {
            auto transaction = gi::object_cast<gi::repository::Pamac::Transaction>(object);

            auto result_value = transaction.run_finish(result);

            progress.finish(result_value);
        });

        return true;
    });
}
