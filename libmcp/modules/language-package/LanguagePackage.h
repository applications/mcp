/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <filesystem>
#include <string>
#include <vector>

namespace MCP::Lib::Modules::LanguagePackage {

struct LanguagePackage {
    std::string package_name;
    std::vector<std::string> parent_packages;
    std::string description;
    std::string language;
    std::string territory;

    bool installed : 1;
    bool needed : 1;

    std::filesystem::path flag_path() const;
};
inline bool operator<(const LanguagePackage &lhs, const LanguagePackage &rhs)
{
    return lhs.package_name < rhs.package_name;
}
inline bool operator==(const LanguagePackage &lhs, const LanguagePackage &rhs)
{
    return lhs.package_name == rhs.package_name;
}

} // namespace MCP::Lib::Modules::LanguagePackage
