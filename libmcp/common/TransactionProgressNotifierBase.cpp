/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "TransactionProgressNotifierBase.h"

namespace MCP::Lib::Common {

void connect_transaction_progress(
    gi::repository::Pamac::Transaction &transaction,
    MCP::Lib::Common::ProgressNotifierBase<MCP::Lib::Common::TransactionProgress> &progress)
{}

void TransactionProgressNotifierBase::disconnect_transaction()
{
    for (gulong &connection_id : m_connections) {
        if (connection_id == 0) {
            continue;
        }

        m_current_transaction.disconnect(connection_id);
        connection_id = 0;
    }
}

bool TransactionProgressNotifierBase::connect_transaction(
    gi::repository::Pamac::Transaction &transaction)
{
    disconnect_transaction();
    if (transaction == nullptr) {
        return false;
    }
    const auto report_action_status_progress = [this](const auto &,
                                                      const std::string &action,
                                                      const std::string &status,
                                                      gdouble progress_value) {
        report(MCP::Lib::Common::TransactionProgress{.action = action,
                                                     .details = status,
                                                     .progress = double(progress_value)});
    };

    m_connections[0] = transaction.signal_emit_action().connect([this](const auto &,
                                                                       const std::string &action) {
        report(
            MCP::Lib::Common::TransactionProgress{.action = action, .details = "", .progress = -1});
    });

    m_connections[1] = transaction.signal_emit_action_progress().connect(
        report_action_status_progress);

    m_connections[2] = transaction.signal_emit_download_progress().connect(
        report_action_status_progress);

    m_connections[3] = transaction.signal_emit_hook_progress().connect(
        [this](const auto &,
               const std::string &action,
               const std::string &details,
               [[maybe_unused]] const std::string &status,
               gdouble progress_value) {
            report(MCP::Lib::Common::TransactionProgress{.action = action,
                                                         .details = details,
                                                         .progress = double(progress_value)});
        });
    m_connections[4] = transaction.signal_emit_script_output().connect(
        [this](const auto &, const std::string &details) {
            report(MCP::Lib::Common::TransactionProgress{.action = "",
                                                         .details = details,
                                                         .progress = -1});
        });
    m_connections[5] = transaction.signal_emit_error().connect(
        [this](const auto &,
               const std::string &err,
               gi::repository::GLib::PtrArray details_gptrarray) { error(err); });

    m_current_transaction = transaction;

    return true;
}

} // namespace MCP::Lib::Common
