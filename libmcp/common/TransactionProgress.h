/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <string>

namespace MCP::Lib::Common {

struct TransactionProgress
{
    std::string action;
    std::string details;
    double progress;
};

}; // namespace MCP::Lib::Common
