/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <string>

namespace MCP::Lib::Common {

template<typename T>
struct ProgressNotifierBase
{
    virtual void start() = 0;
    virtual void report(const T &value) = 0;
    virtual void finish(bool result) = 0;
    virtual void error(const std::string &message) = 0;
};

} // namespace MCP::Lib::Common
