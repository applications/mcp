/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <common/ProgressNotifierBase.h>
#include <common/TransactionProgress.h>

#include <pamac/pamac.hpp>
#include <unordered_set>

namespace MCP::Lib::Common {

class TransactionProgressNotifierBase : public ProgressNotifierBase<TransactionProgress>
{
public:
    TransactionProgressNotifierBase() = default;
    virtual ~TransactionProgressNotifierBase() = default;

    void disconnect_transaction();

    bool connect_transaction(gi::repository::Pamac::Transaction &transaction);

private:
    std::array<gulong, 6> m_connections = {0};
    gi::repository::Pamac::Transaction m_current_transaction;
};

} // namespace MCP::Lib::Common
