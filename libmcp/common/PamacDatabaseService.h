/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include <kangaru/service.hpp>
#include <pamac/pamac.hpp>

struct PamacDatabaseService : kgr::single_service<gi::repository::Pamac::Database>
{};
