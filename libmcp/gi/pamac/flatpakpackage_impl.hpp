// AUTO-GENERATED

#ifndef _GI_PAMAC_FLATPAKPACKAGE_IMPL_HPP_
#define _GI_PAMAC_FLATPAKPACKAGE_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {


} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/flatpakpackage_extra_def_impl.hpp>)
#include <pamac/flatpakpackage_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/flatpakpackage_extra_impl.hpp>)
#include <pamac/flatpakpackage_extra_impl.hpp>
#endif
#endif

#endif
