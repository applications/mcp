// AUTO-GENERATED

#ifndef _GI_PAMAC_DATABASE_IMPL_HPP_
#define _GI_PAMAC_DATABASE_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// PamacDatabase* pamac_database_new (PamacConfig* config);
// ::PamacDatabase* pamac_database_new (::PamacConfig* config);
Pamac::Database base::DatabaseBase::new_ (Pamac::Config config) noexcept
{
  typedef ::PamacDatabase* (*call_wrap_t) (::PamacConfig* config);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_new;
  auto config_to_c = gi::unwrap (config, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (config_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_database_enable_appstream (PamacDatabase* self);
// void pamac_database_enable_appstream (::PamacDatabase* self);
void base::DatabaseBase::enable_appstream () noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_enable_appstream;
  call_wrap_v ((::PamacDatabase*) (gobj_()));
}

// void pamac_database_refresh (PamacDatabase* self);
// void pamac_database_refresh (::PamacDatabase* self);
void base::DatabaseBase::refresh () noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_refresh;
  call_wrap_v ((::PamacDatabase*) (gobj_()));
}

// void pamac_database_get_mirrors_countries_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_mirrors_countries_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_mirrors_countries_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_mirrors_countries_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_mirrors_countries_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_mirrors_countries_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_mirrors_countries_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_mirrors_countries_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_database_get_mirrors_choosen_country_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_mirrors_choosen_country_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_mirrors_choosen_country_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_mirrors_choosen_country_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// const gchar* pamac_database_get_mirrors_choosen_country_finish (PamacDatabase* self, GAsyncResult* _res_);
// const char* pamac_database_get_mirrors_choosen_country_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
std::string base::DatabaseBase::get_mirrors_choosen_country_finish (Gio::AsyncResult _res_) noexcept
{
  typedef const char* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_mirrors_choosen_country_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// gchar* pamac_database_get_alpm_dep_name (PamacDatabase* self, const gchar* dep_string);
// char* pamac_database_get_alpm_dep_name (::PamacDatabase* self, const char* dep_string);
std::string base::DatabaseBase::get_alpm_dep_name (const std::string & dep_string) noexcept
{
  typedef char* (*call_wrap_t) (::PamacDatabase* self, const char* dep_string);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_alpm_dep_name;
  auto dep_string_to_c = gi::unwrap (dep_string, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (dep_string_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GHashTable* pamac_database_get_clean_cache_details (PamacDatabase* self);
// ::GHashTable* pamac_database_get_clean_cache_details (::PamacDatabase* self);
//std::map<std::string, guint64> base::DatabaseBase::get_clean_cache_details () noexcept
//{
//  typedef ::GHashTable* (*call_wrap_t) (::PamacDatabase* self);
//  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_clean_cache_details;
//  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
//  return gi::detail::wrap_map<std::string, guint64> (_temp_ret, gi::transfer_full);
//}

// void pamac_database_get_clean_cache_details_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_clean_cache_details_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
//void base::DatabaseBase::get_clean_cache_details_async (Gio::AsyncReadyCallback _callback_) noexcept
//{
//  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
//  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_clean_cache_details_async;
//  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
//  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
//}

// GHashTable* pamac_database_get_clean_cache_details_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GHashTable* pamac_database_get_clean_cache_details_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
//std::map<std::string, guint64> base::DatabaseBase::get_clean_cache_details_finish (Gio::AsyncResult _res_) noexcept
//{
//  typedef ::GHashTable* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
//  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_clean_cache_details_finish;
//  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
//  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
//  return gi::detail::wrap_map<std::string, guint64> (_temp_ret, gi::transfer_full);
//}

// GHashTable* pamac_database_get_build_files_details (PamacDatabase* self);
// ::GHashTable* pamac_database_get_build_files_details (::PamacDatabase* self);
//std::map<std::string, guint64> base::DatabaseBase::get_build_files_details () noexcept
//{
//  typedef ::GHashTable* (*call_wrap_t) (::PamacDatabase* self);
//  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_build_files_details;
//  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
//  return gi::detail::wrap_map<std::string, guint64> (_temp_ret, gi::transfer_full);
//}

// void pamac_database_get_build_files_details_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_build_files_details_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
//void base::DatabaseBase::get_build_files_details_async (Gio::AsyncReadyCallback _callback_) noexcept
//{
//  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
//  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_build_files_details_async;
//  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
//  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
//}

// GHashTable* pamac_database_get_build_files_details_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GHashTable* pamac_database_get_build_files_details_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
//std::map<std::string, guint64> base::DatabaseBase::get_build_files_details_finish (Gio::AsyncResult _res_) noexcept
//{
//  typedef ::GHashTable* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
//  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_build_files_details_finish;
//  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
//  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
//  return gi::detail::wrap_map<std::string, guint64> (_temp_ret, gi::transfer_full);
//}

// gboolean pamac_database_is_installed_pkg (PamacDatabase* self, const gchar* pkgname);
// gboolean pamac_database_is_installed_pkg (::PamacDatabase* self, const char* pkgname);
bool base::DatabaseBase::is_installed_pkg (const std::string & pkgname) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, const char* pkgname);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_is_installed_pkg;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c));
  return _temp_ret;
}

// PamacAlpmPackage* pamac_database_get_installed_pkg (PamacDatabase* self, const gchar* pkgname);
// ::PamacAlpmPackage* pamac_database_get_installed_pkg (::PamacDatabase* self, const char* pkgname);
Pamac::AlpmPackage base::DatabaseBase::get_installed_pkg (const std::string & pkgname) noexcept
{
  typedef ::PamacAlpmPackage* (*call_wrap_t) (::PamacDatabase* self, const char* pkgname);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_pkg;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// gboolean pamac_database_has_installed_satisfier (PamacDatabase* self, const gchar* depstring);
// gboolean pamac_database_has_installed_satisfier (::PamacDatabase* self, const char* depstring);
bool base::DatabaseBase::has_installed_satisfier (const std::string & depstring) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, const char* depstring);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_has_installed_satisfier;
  auto depstring_to_c = gi::unwrap (depstring, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (depstring_to_c));
  return _temp_ret;
}

// PamacAlpmPackage* pamac_database_get_installed_satisfier (PamacDatabase* self, const gchar* depstring);
// ::PamacAlpmPackage* pamac_database_get_installed_satisfier (::PamacDatabase* self, const char* depstring);
Pamac::AlpmPackage base::DatabaseBase::get_installed_satisfier (const std::string & depstring) noexcept
{
  typedef ::PamacAlpmPackage* (*call_wrap_t) (::PamacDatabase* self, const char* depstring);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_satisfier;
  auto depstring_to_c = gi::unwrap (depstring, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (depstring_to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_database_get_installed_pkgs_by_glob (PamacDatabase* self, const gchar* glob);
// ::GPtrArray* pamac_database_get_installed_pkgs_by_glob (::PamacDatabase* self, const char* glob);
GLib::PtrArray base::DatabaseBase::get_installed_pkgs_by_glob (const std::string & glob) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, const char* glob);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_pkgs_by_glob;
  auto glob_to_c = gi::unwrap (glob, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (glob_to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// gboolean pamac_database_should_hold (PamacDatabase* self, const gchar* pkgname);
// gboolean pamac_database_should_hold (::PamacDatabase* self, const char* pkgname);
bool base::DatabaseBase::should_hold (const std::string & pkgname) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, const char* pkgname);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_should_hold;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c));
  return _temp_ret;
}

// GPtrArray* pamac_database_get_installed_pkgs (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_installed_pkgs (::PamacDatabase* self);
GLib::PtrArray base::DatabaseBase::get_installed_pkgs () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_pkgs;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_get_installed_pkgs_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_pkgs_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_installed_pkgs_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_installed_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_installed_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_installed_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_get_installed_apps_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_apps_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_installed_apps_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_apps_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_installed_apps_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_installed_apps_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_installed_apps_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_apps_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// GPtrArray* pamac_database_get_explicitly_installed_pkgs (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_explicitly_installed_pkgs (::PamacDatabase* self);
GLib::PtrArray base::DatabaseBase::get_explicitly_installed_pkgs () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_explicitly_installed_pkgs;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_get_explicitly_installed_pkgs_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_explicitly_installed_pkgs_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_explicitly_installed_pkgs_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_explicitly_installed_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_explicitly_installed_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_explicitly_installed_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_explicitly_installed_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_explicitly_installed_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// GPtrArray* pamac_database_get_foreign_pkgs (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_foreign_pkgs (::PamacDatabase* self);
GLib::PtrArray base::DatabaseBase::get_foreign_pkgs () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_foreign_pkgs;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_get_foreign_pkgs_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_foreign_pkgs_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_foreign_pkgs_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_foreign_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_foreign_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_foreign_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_foreign_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_foreign_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// GPtrArray* pamac_database_get_orphans (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_orphans (::PamacDatabase* self);
GLib::PtrArray base::DatabaseBase::get_orphans () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_orphans;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_get_orphans_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_orphans_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_orphans_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_orphans_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_orphans_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_orphans_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_orphans_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_orphans_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// gboolean pamac_database_is_sync_pkg (PamacDatabase* self, const gchar* pkgname);
// gboolean pamac_database_is_sync_pkg (::PamacDatabase* self, const char* pkgname);
bool base::DatabaseBase::is_sync_pkg (const std::string & pkgname) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, const char* pkgname);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_is_sync_pkg;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c));
  return _temp_ret;
}

// PamacAlpmPackage* pamac_database_get_sync_pkg (PamacDatabase* self, const gchar* pkgname);
// ::PamacAlpmPackage* pamac_database_get_sync_pkg (::PamacDatabase* self, const char* pkgname);
Pamac::AlpmPackage base::DatabaseBase::get_sync_pkg (const std::string & pkgname) noexcept
{
  typedef ::PamacAlpmPackage* (*call_wrap_t) (::PamacDatabase* self, const char* pkgname);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_sync_pkg;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// gboolean pamac_database_has_sync_satisfier (PamacDatabase* self, const gchar* depstring);
// gboolean pamac_database_has_sync_satisfier (::PamacDatabase* self, const char* depstring);
bool base::DatabaseBase::has_sync_satisfier (const std::string & depstring) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, const char* depstring);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_has_sync_satisfier;
  auto depstring_to_c = gi::unwrap (depstring, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (depstring_to_c));
  return _temp_ret;
}

// PamacAlpmPackage* pamac_database_get_sync_satisfier (PamacDatabase* self, const gchar* depstring);
// ::PamacAlpmPackage* pamac_database_get_sync_satisfier (::PamacDatabase* self, const char* depstring);
Pamac::AlpmPackage base::DatabaseBase::get_sync_satisfier (const std::string & depstring) noexcept
{
  typedef ::PamacAlpmPackage* (*call_wrap_t) (::PamacDatabase* self, const char* depstring);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_sync_satisfier;
  auto depstring_to_c = gi::unwrap (depstring, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (depstring_to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_database_get_sync_pkgs_by_glob (PamacDatabase* self, const gchar* glob);
// ::GPtrArray* pamac_database_get_sync_pkgs_by_glob (::PamacDatabase* self, const char* glob);
GLib::PtrArray base::DatabaseBase::get_sync_pkgs_by_glob (const std::string & glob) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, const char* glob);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_sync_pkgs_by_glob;
  auto glob_to_c = gi::unwrap (glob, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (glob_to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// PamacPackage* pamac_database_get_app_by_id (PamacDatabase* self, const gchar* app_id);
// ::PamacPackage* pamac_database_get_app_by_id (::PamacDatabase* self, const char* app_id);
Pamac::Package base::DatabaseBase::get_app_by_id (const std::string & app_id) noexcept
{
  typedef ::PamacPackage* (*call_wrap_t) (::PamacDatabase* self, const char* app_id);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_app_by_id;
  auto app_id_to_c = gi::unwrap (app_id, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (app_id_to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_database_get_url_stream (PamacDatabase* self, const gchar* url, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_url_stream (::PamacDatabase* self, const char* url, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_url_stream (const std::string & url, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* url, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_url_stream;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto url_to_c = gi::unwrap (url, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (url_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GInputStream* pamac_database_get_url_stream_finish (PamacDatabase* self, GAsyncResult* _res_, GError ** error);
// ::GInputStream* pamac_database_get_url_stream_finish (::PamacDatabase* self, ::GAsyncResult* _res_, GError ** error);
Gio::InputStream base::DatabaseBase::get_url_stream_finish (Gio::AsyncResult _res_)
{
  typedef ::GInputStream* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_url_stream_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c), &error);
  gi::check_error (error);
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}
Gio::InputStream base::DatabaseBase::get_url_stream_finish (Gio::AsyncResult _res_, GLib::Error * _error) noexcept
{
  typedef ::GInputStream* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_url_stream_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GPtrArray* pamac_database_search_installed_pkgs (PamacDatabase* self, const gchar* search_string);
// ::GPtrArray* pamac_database_search_installed_pkgs (::PamacDatabase* self, const char* search_string);
GLib::PtrArray base::DatabaseBase::search_installed_pkgs (const std::string & search_string) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, const char* search_string);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_installed_pkgs;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_search_installed_pkgs_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_installed_pkgs_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::search_installed_pkgs_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_installed_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_search_installed_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_installed_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::search_installed_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_installed_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// GPtrArray* pamac_database_search_repos_pkgs (PamacDatabase* self, const gchar* search_string);
// ::GPtrArray* pamac_database_search_repos_pkgs (::PamacDatabase* self, const char* search_string);
GLib::PtrArray base::DatabaseBase::search_repos_pkgs (const std::string & search_string) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, const char* search_string);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_repos_pkgs;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_search_repos_pkgs_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_repos_pkgs_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::search_repos_pkgs_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_repos_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_search_repos_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_repos_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::search_repos_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_repos_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// GPtrArray* pamac_database_search_uninstalled_apps (PamacDatabase* self, GPtrArray* search_terms);
// ::GPtrArray* pamac_database_search_uninstalled_apps (::PamacDatabase* self, ::GPtrArray* search_terms);
GLib::PtrArray base::DatabaseBase::search_uninstalled_apps (GLib::PtrArray search_terms) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GPtrArray* search_terms);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_uninstalled_apps;
  auto search_terms_to_c = gi::unwrap (search_terms, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GPtrArray*) (search_terms_to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_suggest_pkgnames_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_suggest_pkgnames_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::suggest_pkgnames_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_suggest_pkgnames_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_suggest_pkgnames_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_suggest_pkgnames_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::suggest_pkgnames_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_suggest_pkgnames_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// GPtrArray* pamac_database_search_pkgs (PamacDatabase* self, const gchar* search_string);
// ::GPtrArray* pamac_database_search_pkgs (::PamacDatabase* self, const char* search_string);
GLib::PtrArray base::DatabaseBase::search_pkgs (const std::string & search_string) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, const char* search_string);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_pkgs;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_search_pkgs_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_pkgs_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::search_pkgs_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_search_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::search_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// GPtrArray* pamac_database_search_aur_pkgs (PamacDatabase* self, const gchar* search_string);
// ::GPtrArray* pamac_database_search_aur_pkgs (::PamacDatabase* self, const char* search_string);
GLib::PtrArray base::DatabaseBase::search_aur_pkgs (const std::string & search_string) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, const char* search_string);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_aur_pkgs;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_search_aur_pkgs_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_aur_pkgs_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::search_aur_pkgs_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_aur_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_search_aur_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_aur_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::search_aur_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_aur_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

//  pamac_database_search_files (PamacDatabase* self, GPtrArray* files);
//  pamac_database_search_files (::PamacDatabase* self, ::GPtrArray* files);
// SKIP; inconsistent map element info

// GPtrArray* pamac_database_get_categories_names (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_categories_names (::PamacDatabase* self);
GLib::PtrArray base::DatabaseBase::get_categories_names () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_categories_names;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_database_get_category_pkgs_async (PamacDatabase* self, const gchar* category, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_category_pkgs_async (::PamacDatabase* self, const char* category, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_category_pkgs_async (const std::string & category, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* category, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_category_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto category_to_c = gi::unwrap (category, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (category_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_category_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_category_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_category_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_category_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// GPtrArray* pamac_database_get_repos_names (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_repos_names (::PamacDatabase* self);
GLib::PtrArray base::DatabaseBase::get_repos_names () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_repos_names;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_database_get_repo_pkgs (PamacDatabase* self, const gchar* repo);
// ::GPtrArray* pamac_database_get_repo_pkgs (::PamacDatabase* self, const char* repo);
GLib::PtrArray base::DatabaseBase::get_repo_pkgs (const std::string & repo) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, const char* repo);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_repo_pkgs;
  auto repo_to_c = gi::unwrap (repo, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (repo_to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_get_repo_pkgs_async (PamacDatabase* self, const gchar* repo, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_repo_pkgs_async (::PamacDatabase* self, const char* repo, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_repo_pkgs_async (const std::string & repo, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* repo, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_repo_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto repo_to_c = gi::unwrap (repo, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (repo_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_repo_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_repo_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_repo_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_repo_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// GPtrArray* pamac_database_get_groups_names (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_groups_names (::PamacDatabase* self);
GLib::PtrArray base::DatabaseBase::get_groups_names () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_groups_names;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_database_get_group_pkgs (PamacDatabase* self, const gchar* group_name);
// ::GPtrArray* pamac_database_get_group_pkgs (::PamacDatabase* self, const char* group_name);
GLib::PtrArray base::DatabaseBase::get_group_pkgs (const std::string & group_name) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, const char* group_name);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_group_pkgs;
  auto group_name_to_c = gi::unwrap (group_name, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (group_name_to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_get_group_pkgs_async (PamacDatabase* self, const gchar* group_name, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_group_pkgs_async (::PamacDatabase* self, const char* group_name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_group_pkgs_async (const std::string & group_name, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* group_name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_group_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto group_name_to_c = gi::unwrap (group_name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (group_name_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_group_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_group_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_group_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_group_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// PamacAlpmPackage* pamac_database_get_pkg (PamacDatabase* self, const gchar* pkgname);
// ::PamacAlpmPackage* pamac_database_get_pkg (::PamacDatabase* self, const char* pkgname);
Pamac::AlpmPackage base::DatabaseBase::get_pkg (const std::string & pkgname) noexcept
{
  typedef ::PamacAlpmPackage* (*call_wrap_t) (::PamacDatabase* self, const char* pkgname);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_pkg;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GFile* pamac_database_clone_build_files (PamacDatabase* self, const gchar* pkgname, gboolean overwrite_files, GCancellable* cancellable);
// ::GFile* pamac_database_clone_build_files (::PamacDatabase* self, const char* pkgname, gboolean overwrite_files, ::GCancellable* cancellable);
Gio::File base::DatabaseBase::clone_build_files (const std::string & pkgname, gboolean overwrite_files, Gio::Cancellable cancellable) noexcept
{
  typedef ::GFile* (*call_wrap_t) (::PamacDatabase* self, const char* pkgname, gboolean overwrite_files, ::GCancellable* cancellable);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_clone_build_files;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto overwrite_files_to_c = overwrite_files;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c), (gboolean) (overwrite_files_to_c), (::GCancellable*) (cancellable_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}
Gio::File base::DatabaseBase::clone_build_files (const std::string & pkgname, gboolean overwrite_files) noexcept
{
  typedef ::GFile* (*call_wrap_t) (::PamacDatabase* self, const char* pkgname, gboolean overwrite_files, ::GCancellable* cancellable);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_clone_build_files;
  auto cancellable_to_c = nullptr;
  auto overwrite_files_to_c = overwrite_files;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c), (gboolean) (overwrite_files_to_c), (::GCancellable*) (cancellable_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_database_clone_build_files_async (PamacDatabase* self, const gchar* pkgname, gboolean overwrite_files, GCancellable* cancellable, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_clone_build_files_async (::PamacDatabase* self, const char* pkgname, gboolean overwrite_files, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::clone_build_files_async (const std::string & pkgname, gboolean overwrite_files, Gio::Cancellable cancellable, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* pkgname, gboolean overwrite_files, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_clone_build_files_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto overwrite_files_to_c = overwrite_files;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c), (gboolean) (overwrite_files_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}
void base::DatabaseBase::clone_build_files_async (const std::string & pkgname, gboolean overwrite_files, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* pkgname, gboolean overwrite_files, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_clone_build_files_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  auto overwrite_files_to_c = overwrite_files;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c), (gboolean) (overwrite_files_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GFile* pamac_database_clone_build_files_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GFile* pamac_database_clone_build_files_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
Gio::File base::DatabaseBase::clone_build_files_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GFile* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_clone_build_files_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// gboolean pamac_database_regenerate_srcinfo (PamacDatabase* self, const gchar* pkgname, GCancellable* cancellable);
// gboolean pamac_database_regenerate_srcinfo (::PamacDatabase* self, const char* pkgname, ::GCancellable* cancellable);
bool base::DatabaseBase::regenerate_srcinfo (const std::string & pkgname, Gio::Cancellable cancellable) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, const char* pkgname, ::GCancellable* cancellable);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_regenerate_srcinfo;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c), (::GCancellable*) (cancellable_to_c));
  return _temp_ret;
}
bool base::DatabaseBase::regenerate_srcinfo (const std::string & pkgname) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, const char* pkgname, ::GCancellable* cancellable);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_regenerate_srcinfo;
  auto cancellable_to_c = nullptr;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c), (::GCancellable*) (cancellable_to_c));
  return _temp_ret;
}

// void pamac_database_regenerate_srcinfo_async (PamacDatabase* self, const gchar* pkgname, GCancellable* cancellable, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_regenerate_srcinfo_async (::PamacDatabase* self, const char* pkgname, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::regenerate_srcinfo_async (const std::string & pkgname, Gio::Cancellable cancellable, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* pkgname, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_regenerate_srcinfo_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}
void base::DatabaseBase::regenerate_srcinfo_async (const std::string & pkgname, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* pkgname, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_regenerate_srcinfo_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_database_regenerate_srcinfo_finish (PamacDatabase* self, GAsyncResult* _res_);
// gboolean pamac_database_regenerate_srcinfo_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
bool base::DatabaseBase::regenerate_srcinfo_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_regenerate_srcinfo_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// PamacAURPackage* pamac_database_get_aur_pkg (PamacDatabase* self, const gchar* pkgname);
// ::PamacAURPackage* pamac_database_get_aur_pkg (::PamacDatabase* self, const char* pkgname);
Pamac::AURPackage base::DatabaseBase::get_aur_pkg (const std::string & pkgname) noexcept
{
  typedef ::PamacAURPackage* (*call_wrap_t) (::PamacDatabase* self, const char* pkgname);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_aur_pkg;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_database_get_aur_pkg_async (PamacDatabase* self, const gchar* pkgname, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_aur_pkg_async (::PamacDatabase* self, const char* pkgname, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_aur_pkg_async (const std::string & pkgname, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* pkgname, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_aur_pkg_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (pkgname_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// PamacAURPackage* pamac_database_get_aur_pkg_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::PamacAURPackage* pamac_database_get_aur_pkg_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
Pamac::AURPackage base::DatabaseBase::get_aur_pkg_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::PamacAURPackage* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_aur_pkg_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GHashTable* pamac_database_get_aur_pkgs (PamacDatabase* self, GPtrArray* pkgnames);
// ::GHashTable* pamac_database_get_aur_pkgs (::PamacDatabase* self, ::GPtrArray* pkgnames);
std::map<std::string, Pamac::AURPackage> base::DatabaseBase::get_aur_pkgs (GLib::PtrArray pkgnames) noexcept
{
  typedef ::GHashTable* (*call_wrap_t) (::PamacDatabase* self, ::GPtrArray* pkgnames);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_aur_pkgs;
  auto pkgnames_to_c = gi::unwrap (pkgnames, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GPtrArray*) (pkgnames_to_c));
  return gi::detail::wrap_map<std::string, Pamac::AURPackage> (_temp_ret, gi::transfer_full);
}

// void pamac_database_get_aur_pkgs_async (PamacDatabase* self, GPtrArray* pkgnames, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_aur_pkgs_async (::PamacDatabase* self, ::GPtrArray* pkgnames, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_aur_pkgs_async (GLib::PtrArray pkgnames, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, ::GPtrArray* pkgnames, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_aur_pkgs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto pkgnames_to_c = gi::unwrap (pkgnames, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (::GPtrArray*) (pkgnames_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GHashTable* pamac_database_get_aur_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GHashTable* pamac_database_get_aur_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
std::map<std::string, Pamac::AURPackage> base::DatabaseBase::get_aur_pkgs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GHashTable* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_aur_pkgs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::detail::wrap_map<std::string, Pamac::AURPackage> (_temp_ret, gi::transfer_full);
}

// void pamac_database_refresh_tmp_files_dbs_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_refresh_tmp_files_dbs_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::refresh_tmp_files_dbs_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_refresh_tmp_files_dbs_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// void pamac_database_refresh_tmp_files_dbs_finish (PamacDatabase* self, GAsyncResult* _res_);
// void pamac_database_refresh_tmp_files_dbs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
void base::DatabaseBase::refresh_tmp_files_dbs_finish (Gio::AsyncResult _res_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_refresh_tmp_files_dbs_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
}

// void pamac_database_refresh_tmp_files_dbs (PamacDatabase* self);
// void pamac_database_refresh_tmp_files_dbs (::PamacDatabase* self);
void base::DatabaseBase::refresh_tmp_files_dbs () noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_refresh_tmp_files_dbs;
  call_wrap_v ((::PamacDatabase*) (gobj_()));
}

// GDateTime* pamac_database_get_last_refresh_time (PamacDatabase* self);
// ::GDateTime* pamac_database_get_last_refresh_time (::PamacDatabase* self);
GLib::DateTime base::DatabaseBase::get_last_refresh_time () noexcept
{
  typedef ::GDateTime* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_last_refresh_time;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// gboolean pamac_database_need_refresh (PamacDatabase* self);
// gboolean pamac_database_need_refresh (::PamacDatabase* self);
bool base::DatabaseBase::need_refresh () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_need_refresh;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return _temp_ret;
}

// PamacUpdates* pamac_database_get_updates (PamacDatabase* self, gboolean use_timestamp);
// ::PamacUpdates* pamac_database_get_updates (::PamacDatabase* self, gboolean use_timestamp);
Pamac::Updates base::DatabaseBase::get_updates (gboolean use_timestamp) noexcept
{
  typedef ::PamacUpdates* (*call_wrap_t) (::PamacDatabase* self, gboolean use_timestamp);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_updates;
  auto use_timestamp_to_c = use_timestamp;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (gboolean) (use_timestamp_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_database_get_updates_async (PamacDatabase* self, gboolean use_timestamp, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_updates_async (::PamacDatabase* self, gboolean use_timestamp, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_updates_async (gboolean use_timestamp, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, gboolean use_timestamp, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_updates_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto use_timestamp_to_c = use_timestamp;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (gboolean) (use_timestamp_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// PamacUpdates* pamac_database_get_updates_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::PamacUpdates* pamac_database_get_updates_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
Pamac::Updates base::DatabaseBase::get_updates_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::PamacUpdates* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_updates_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_database_search_snaps_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_snaps_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::search_snaps_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_snaps_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_search_snaps_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_snaps_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::search_snaps_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_snaps_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// gboolean pamac_database_is_installed_snap (PamacDatabase* self, const gchar* name);
// gboolean pamac_database_is_installed_snap (::PamacDatabase* self, const char* name);
bool base::DatabaseBase::is_installed_snap (const std::string & name) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, const char* name);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_is_installed_snap;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (name_to_c));
  return _temp_ret;
}

// void pamac_database_get_snap_async (PamacDatabase* self, const gchar* name, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_snap_async (::PamacDatabase* self, const char* name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_snap_async (const std::string & name, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_snap_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (name_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// PamacSnapPackage* pamac_database_get_snap_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::PamacSnapPackage* pamac_database_get_snap_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
Pamac::SnapPackage base::DatabaseBase::get_snap_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::PamacSnapPackage* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_snap_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_database_get_installed_snaps_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_snaps_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_installed_snaps_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_snaps_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_installed_snaps_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_installed_snaps_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_installed_snaps_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_snaps_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_get_installed_snap_icon_async (PamacDatabase* self, const gchar* name, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_snap_icon_async (::PamacDatabase* self, const char* name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_installed_snap_icon_async (const std::string & name, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_snap_icon_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (name_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gchar* pamac_database_get_installed_snap_icon_finish (PamacDatabase* self, GAsyncResult* _res_);
// char* pamac_database_get_installed_snap_icon_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
std::string base::DatabaseBase::get_installed_snap_icon_finish (Gio::AsyncResult _res_) noexcept
{
  typedef char* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_snap_icon_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_database_get_category_snaps_async (PamacDatabase* self, const gchar* category, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_category_snaps_async (::PamacDatabase* self, const char* category, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_category_snaps_async (const std::string & category, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* category, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_category_snaps_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto category_to_c = gi::unwrap (category, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (category_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_category_snaps_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_category_snaps_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_category_snaps_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_category_snaps_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_refresh_flatpak_appstream_data (PamacDatabase* self);
// void pamac_database_refresh_flatpak_appstream_data (::PamacDatabase* self);
void base::DatabaseBase::refresh_flatpak_appstream_data () noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_refresh_flatpak_appstream_data;
  call_wrap_v ((::PamacDatabase*) (gobj_()));
}

// void pamac_database_refresh_flatpak_appstream_data_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_refresh_flatpak_appstream_data_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::refresh_flatpak_appstream_data_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_refresh_flatpak_appstream_data_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// void pamac_database_refresh_flatpak_appstream_data_finish (PamacDatabase* self, GAsyncResult* _res_);
// void pamac_database_refresh_flatpak_appstream_data_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
void base::DatabaseBase::refresh_flatpak_appstream_data_finish (Gio::AsyncResult _res_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_refresh_flatpak_appstream_data_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
}

// GPtrArray* pamac_database_get_flatpak_remotes_names (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_flatpak_remotes_names (::PamacDatabase* self);
GLib::PtrArray base::DatabaseBase::get_flatpak_remotes_names () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_flatpak_remotes_names;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_get_installed_flatpaks_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_flatpaks_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_installed_flatpaks_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_flatpaks_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacDatabase*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_installed_flatpaks_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_installed_flatpaks_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_installed_flatpaks_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_installed_flatpaks_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// void pamac_database_search_flatpaks_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_flatpaks_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::search_flatpaks_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_flatpaks_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto search_string_to_c = gi::unwrap (search_string, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (search_string_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_search_flatpaks_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_flatpaks_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::search_flatpaks_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_search_flatpaks_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// gboolean pamac_database_is_installed_flatpak (PamacDatabase* self, const gchar* name);
// gboolean pamac_database_is_installed_flatpak (::PamacDatabase* self, const char* name);
bool base::DatabaseBase::is_installed_flatpak (const std::string & name) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacDatabase* self, const char* name);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_is_installed_flatpak;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (name_to_c));
  return _temp_ret;
}

// void pamac_database_get_flatpak_async (PamacDatabase* self, const gchar* id, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_flatpak_async (::PamacDatabase* self, const char* id, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_flatpak_async (const std::string & id, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* id, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_flatpak_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto id_to_c = gi::unwrap (id, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (id_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// PamacFlatpakPackage* pamac_database_get_flatpak_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::PamacFlatpakPackage* pamac_database_get_flatpak_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
Pamac::FlatpakPackage base::DatabaseBase::get_flatpak_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::PamacFlatpakPackage* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_flatpak_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_database_get_category_flatpaks_async (PamacDatabase* self, const gchar* category, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_category_flatpaks_async (::PamacDatabase* self, const char* category, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::DatabaseBase::get_category_flatpaks_async (const std::string & category, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, const char* category, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_category_flatpaks_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto category_to_c = gi::unwrap (category, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (const char*) (category_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_database_get_category_flatpaks_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_category_flatpaks_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GLib::PtrArray base::DatabaseBase::get_category_flatpaks_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacDatabase* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_category_flatpaks_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_container, gi::direction_out);
}

// PamacConfig* pamac_database_get_config (PamacDatabase* self);
// ::PamacConfig* pamac_database_get_config (::PamacDatabase* self);
Pamac::Config base::DatabaseBase::get_config () noexcept
{
  typedef ::PamacConfig* (*call_wrap_t) (::PamacDatabase* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_get_config;
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_database_set_config (PamacDatabase* self, PamacConfig* value);
// void pamac_database_set_config (::PamacDatabase* self, ::PamacConfig* value);
void base::DatabaseBase::set_config (Pamac::Config value) noexcept
{
  typedef void (*call_wrap_t) (::PamacDatabase* self, ::PamacConfig* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_database_set_config;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacDatabase*) (gobj_()), (::PamacConfig*) (value_to_c));
}



} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/database_extra_def_impl.hpp>)
#include <pamac/database_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/database_extra_impl.hpp>)
#include <pamac/database_extra_impl.hpp>
#endif
#endif

#endif
