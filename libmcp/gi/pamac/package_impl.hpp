// AUTO-GENERATED

#ifndef _GI_PAMAC_PACKAGE_IMPL_HPP_
#define _GI_PAMAC_PACKAGE_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// const gchar* pamac_package_get_name (PamacPackage* self);
// const char* pamac_package_get_name (::PamacPackage* self);
std::string base::PackageBase::get_name () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_name;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_package_set_name (PamacPackage* self, const gchar* value);
// void pamac_package_set_name (::PamacPackage* self, const char* value);
void base::PackageBase::set_name (const std::string & value) noexcept
{
  typedef void (*call_wrap_t) (::PamacPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_set_name;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacPackage*) (gobj_()), (const char*) (value_to_c));
}

// const gchar* pamac_package_get_id (PamacPackage* self);
// const char* pamac_package_get_id (::PamacPackage* self);
std::string base::PackageBase::get_id () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_id;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_package_set_id (PamacPackage* self, const gchar* value);
// void pamac_package_set_id (::PamacPackage* self, const char* value);
void base::PackageBase::set_id (const std::string & value) noexcept
{
  typedef void (*call_wrap_t) (::PamacPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_set_id;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacPackage*) (gobj_()), (const char*) (value_to_c));
}

// const gchar* pamac_package_get_app_name (PamacPackage* self);
// const char* pamac_package_get_app_name (::PamacPackage* self);
std::string base::PackageBase::get_app_name () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_app_name;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_package_get_app_id (PamacPackage* self);
// const char* pamac_package_get_app_id (::PamacPackage* self);
std::string base::PackageBase::get_app_id () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_app_id;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_package_get_version (PamacPackage* self);
// const char* pamac_package_get_version (::PamacPackage* self);
std::string base::PackageBase::get_version () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_version;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_package_set_version (PamacPackage* self, const gchar* value);
// void pamac_package_set_version (::PamacPackage* self, const char* value);
void base::PackageBase::set_version (const std::string & value) noexcept
{
  typedef void (*call_wrap_t) (::PamacPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_set_version;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacPackage*) (gobj_()), (const char*) (value_to_c));
}

// const gchar* pamac_package_get_installed_version (PamacPackage* self);
// const char* pamac_package_get_installed_version (::PamacPackage* self);
std::string base::PackageBase::get_installed_version () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_installed_version;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_package_set_installed_version (PamacPackage* self, const gchar* value);
// void pamac_package_set_installed_version (::PamacPackage* self, const char* value);
void base::PackageBase::set_installed_version (const std::string & value) noexcept
{
  typedef void (*call_wrap_t) (::PamacPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_set_installed_version;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacPackage*) (gobj_()), (const char*) (value_to_c));
}
void base::PackageBase::set_installed_version () noexcept
{
  typedef void (*call_wrap_t) (::PamacPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_set_installed_version;
  auto value_to_c = nullptr;
  call_wrap_v ((::PamacPackage*) (gobj_()), (const char*) (value_to_c));
}

// const gchar* pamac_package_get_desc (PamacPackage* self);
// const char* pamac_package_get_desc (::PamacPackage* self);
std::string base::PackageBase::get_desc () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_desc;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_package_set_desc (PamacPackage* self, const gchar* value);
// void pamac_package_set_desc (::PamacPackage* self, const char* value);
void base::PackageBase::set_desc (const std::string & value) noexcept
{
  typedef void (*call_wrap_t) (::PamacPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_set_desc;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacPackage*) (gobj_()), (const char*) (value_to_c));
}
void base::PackageBase::set_desc () noexcept
{
  typedef void (*call_wrap_t) (::PamacPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_set_desc;
  auto value_to_c = nullptr;
  call_wrap_v ((::PamacPackage*) (gobj_()), (const char*) (value_to_c));
}

// const gchar* pamac_package_get_long_desc (PamacPackage* self);
// const char* pamac_package_get_long_desc (::PamacPackage* self);
std::string base::PackageBase::get_long_desc () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_long_desc;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_package_get_repo (PamacPackage* self);
// const char* pamac_package_get_repo (::PamacPackage* self);
std::string base::PackageBase::get_repo () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_repo;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_package_set_repo (PamacPackage* self, const gchar* value);
// void pamac_package_set_repo (::PamacPackage* self, const char* value);
void base::PackageBase::set_repo (const std::string & value) noexcept
{
  typedef void (*call_wrap_t) (::PamacPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_set_repo;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacPackage*) (gobj_()), (const char*) (value_to_c));
}
void base::PackageBase::set_repo () noexcept
{
  typedef void (*call_wrap_t) (::PamacPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_set_repo;
  auto value_to_c = nullptr;
  call_wrap_v ((::PamacPackage*) (gobj_()), (const char*) (value_to_c));
}

// const gchar* pamac_package_get_launchable (PamacPackage* self);
// const char* pamac_package_get_launchable (::PamacPackage* self);
std::string base::PackageBase::get_launchable () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_launchable;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_package_get_license (PamacPackage* self);
// const char* pamac_package_get_license (::PamacPackage* self);
std::string base::PackageBase::get_license () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_license;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_package_get_url (PamacPackage* self);
// const char* pamac_package_get_url (::PamacPackage* self);
std::string base::PackageBase::get_url () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_url;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_package_get_icon (PamacPackage* self);
// const char* pamac_package_get_icon (::PamacPackage* self);
std::string base::PackageBase::get_icon () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_icon;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// guint64 pamac_package_get_installed_size (PamacPackage* self);
// guint64 pamac_package_get_installed_size (::PamacPackage* self);
guint64 base::PackageBase::get_installed_size () noexcept
{
  typedef guint64 (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_installed_size;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return _temp_ret;
}

// guint64 pamac_package_get_download_size (PamacPackage* self);
// guint64 pamac_package_get_download_size (::PamacPackage* self);
guint64 base::PackageBase::get_download_size () noexcept
{
  typedef guint64 (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_download_size;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return _temp_ret;
}

// GDateTime* pamac_package_get_install_date (PamacPackage* self);
// ::GDateTime* pamac_package_get_install_date (::PamacPackage* self);
GLib::DateTime base::PackageBase::get_install_date () noexcept
{
  typedef ::GDateTime* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_install_date;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_package_get_screenshots (PamacPackage* self);
// ::GPtrArray* pamac_package_get_screenshots (::PamacPackage* self);
GLib::PtrArray base::PackageBase::get_screenshots () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_package_get_screenshots;
  auto _temp_ret = call_wrap_v ((::PamacPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}


} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/package_extra_def_impl.hpp>)
#include <pamac/package_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/package_extra_impl.hpp>)
#include <pamac/package_extra_impl.hpp>
#endif
#endif

#endif
