// AUTO-GENERATED

#ifndef _GI_PAMAC_PAMAC_HPP_
#define _GI_PAMAC_PAMAC_HPP_

#include <gi/gi.hpp>

#include <gobject/gobject.hpp>
#include <glib/glib.hpp>
#include <gio/gio.hpp>


#if defined(__has_include)
#if __has_include(<pamac/pamac_setup_pre_def.hpp>)
#include <pamac/pamac_setup_pre_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/pamac_setup_pre.hpp>)
#include <pamac/pamac_setup_pre.hpp>
#endif
#endif

#include <pamac.h>

#if defined(__has_include)
#if __has_include(<pamac/pamac_setup_post_def.hpp>)
#include <pamac/pamac_setup_post_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/pamac_setup_post.hpp>)
#include <pamac/pamac_setup_post.hpp>
#endif
#endif


GI_DISABLE_DEPRECATED_WARN_BEGIN

#include "_types.hpp"
#include "_enums.hpp"
#include "_flags.hpp"
#include "_constants.hpp"
#include "_callbacks.hpp"


#include "alpmpackage.hpp"
#include "aurpackage.hpp"
#include "config.hpp"
#include "database.hpp"
#include "flatpakpackage.hpp"
#include "package.hpp"
#include "snappackage.hpp"
#include "transaction.hpp"
#include "transactionsummary.hpp"
#include "updates.hpp"
#include "updateschecker.hpp"

#include "_functions.hpp"


#if defined(__has_include)
#if __has_include(<pamac/pamac_extra_def.hpp>)
#include <pamac/pamac_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/pamac_extra.hpp>)
#include <pamac/pamac_extra.hpp>
#endif
#endif

GI_DISABLE_DEPRECATED_WARN_END

#if defined(GI_INLINE) || defined(GI_INCLUDE_IMPL)
#include "pamac_impl.hpp"
#endif

#endif
