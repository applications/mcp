// AUTO-GENERATED

#ifndef _GI_PAMAC__FUNCTIONS_IMPL_HPP_
#define _GI_PAMAC__FUNCTIONS_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

// gchar* pamac_get_version ();
// char* pamac_get_version ();
std::string get_version () noexcept
{
  typedef char* (*call_wrap_t) ();
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_get_version;
  auto _temp_ret = call_wrap_v ();
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

} // namespace Pamac

} // namespace repository

} // namespace gi

#endif
