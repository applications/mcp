// AUTO-GENERATED

#ifndef _GI_PAMAC_TRANSACTIONSUMMARY_IMPL_HPP_
#define _GI_PAMAC_TRANSACTIONSUMMARY_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// GPtrArray* pamac_transaction_summary_get_to_install (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_install (::PamacTransactionSummary* self);
GLib::PtrArray base::TransactionSummaryBase::get_to_install () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacTransactionSummary* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_summary_get_to_install;
  auto _temp_ret = call_wrap_v ((::PamacTransactionSummary*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_transaction_summary_get_to_upgrade (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_upgrade (::PamacTransactionSummary* self);
GLib::PtrArray base::TransactionSummaryBase::get_to_upgrade () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacTransactionSummary* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_summary_get_to_upgrade;
  auto _temp_ret = call_wrap_v ((::PamacTransactionSummary*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_transaction_summary_get_to_downgrade (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_downgrade (::PamacTransactionSummary* self);
GLib::PtrArray base::TransactionSummaryBase::get_to_downgrade () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacTransactionSummary* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_summary_get_to_downgrade;
  auto _temp_ret = call_wrap_v ((::PamacTransactionSummary*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_transaction_summary_get_to_reinstall (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_reinstall (::PamacTransactionSummary* self);
GLib::PtrArray base::TransactionSummaryBase::get_to_reinstall () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacTransactionSummary* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_summary_get_to_reinstall;
  auto _temp_ret = call_wrap_v ((::PamacTransactionSummary*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_transaction_summary_get_to_remove (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_remove (::PamacTransactionSummary* self);
GLib::PtrArray base::TransactionSummaryBase::get_to_remove () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacTransactionSummary* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_summary_get_to_remove;
  auto _temp_ret = call_wrap_v ((::PamacTransactionSummary*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_transaction_summary_get_conflicts_to_remove (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_conflicts_to_remove (::PamacTransactionSummary* self);
GLib::PtrArray base::TransactionSummaryBase::get_conflicts_to_remove () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacTransactionSummary* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_summary_get_conflicts_to_remove;
  auto _temp_ret = call_wrap_v ((::PamacTransactionSummary*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_transaction_summary_get_to_build (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_build (::PamacTransactionSummary* self);
GLib::PtrArray base::TransactionSummaryBase::get_to_build () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacTransactionSummary* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_summary_get_to_build;
  auto _temp_ret = call_wrap_v ((::PamacTransactionSummary*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}


} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/transactionsummary_extra_def_impl.hpp>)
#include <pamac/transactionsummary_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/transactionsummary_extra_impl.hpp>)
#include <pamac/transactionsummary_extra_impl.hpp>
#endif
#endif

#endif
