// AUTO-GENERATED

#ifndef _GI_PAMAC_PAMAC_IMPL_HPP_
#define _GI_PAMAC_PAMAC_IMPL_HPP_

#include "pamac.hpp"

GI_DISABLE_DEPRECATED_WARN_BEGIN


#include "alpmpackage_impl.hpp"
#include "aurpackage_impl.hpp"
#include "config_impl.hpp"
#include "database_impl.hpp"
#include "flatpakpackage_impl.hpp"
#include "package_impl.hpp"
#include "snappackage_impl.hpp"
#include "transaction_impl.hpp"
#include "transactionsummary_impl.hpp"
#include "updates_impl.hpp"
#include "updateschecker_impl.hpp"

#include "_functions_impl.hpp"


#if defined(__has_include)
#if __has_include(<pamac/pamac_extra_def_impl.hpp>)
#include <pamac/pamac_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/pamac_extra_impl.hpp>)
#include <pamac/pamac_extra_impl.hpp>
#endif
#endif


GI_DISABLE_DEPRECATED_WARN_END

#endif
