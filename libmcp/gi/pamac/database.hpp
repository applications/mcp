// AUTO-GENERATED

#ifndef _GI_PAMAC_DATABASE_HPP_
#define _GI_PAMAC_DATABASE_HPP_


namespace gi {

namespace repository {

namespace Pamac {

class AURPackage;
class AlpmPackage;
class Config;
class FlatpakPackage;
class Package;
class SnapPackage;
class Updates;

class Database;

namespace base {


#define GI_PAMAC_DATABASE_BASE base::DatabaseBase
class DatabaseBase : public GObject::Object
{
typedef GObject::Object super_type;
public:
typedef ::PamacDatabase BaseObjectType;

DatabaseBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_database_get_type(); } 

// PamacDatabase* pamac_database_new (PamacConfig* config);
// ::PamacDatabase* pamac_database_new (::PamacConfig* config);
static GI_INLINE_DECL Pamac::Database new_ (Pamac::Config config) noexcept;

// void pamac_database_enable_appstream (PamacDatabase* self);
// void pamac_database_enable_appstream (::PamacDatabase* self);
GI_INLINE_DECL void enable_appstream () noexcept;

// void pamac_database_refresh (PamacDatabase* self);
// void pamac_database_refresh (::PamacDatabase* self);
GI_INLINE_DECL void refresh () noexcept;

// void pamac_database_get_mirrors_countries_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_mirrors_countries_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_mirrors_countries_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_mirrors_countries_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_mirrors_countries_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_mirrors_countries_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_get_mirrors_choosen_country_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_mirrors_choosen_country_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_mirrors_choosen_country_async (Gio::AsyncReadyCallback _callback_) noexcept;

// const gchar* pamac_database_get_mirrors_choosen_country_finish (PamacDatabase* self, GAsyncResult* _res_);
// const char* pamac_database_get_mirrors_choosen_country_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL std::string get_mirrors_choosen_country_finish (Gio::AsyncResult _res_) noexcept;

// gchar* pamac_database_get_alpm_dep_name (PamacDatabase* self, const gchar* dep_string);
// char* pamac_database_get_alpm_dep_name (::PamacDatabase* self, const char* dep_string);
GI_INLINE_DECL std::string get_alpm_dep_name (const std::string & dep_string) noexcept;

// GHashTable* pamac_database_get_clean_cache_details (PamacDatabase* self);
// ::GHashTable* pamac_database_get_clean_cache_details (::PamacDatabase* self);
//GI_INLINE_DECL std::map<std::string, guint64> get_clean_cache_details () noexcept;

// void pamac_database_get_clean_cache_details_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_clean_cache_details_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
//GI_INLINE_DECL void get_clean_cache_details_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GHashTable* pamac_database_get_clean_cache_details_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GHashTable* pamac_database_get_clean_cache_details_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
//GI_INLINE_DECL std::map<std::string, guint64> get_clean_cache_details_finish (Gio::AsyncResult _res_) noexcept;

// GHashTable* pamac_database_get_build_files_details (PamacDatabase* self);
// ::GHashTable* pamac_database_get_build_files_details (::PamacDatabase* self);
//GI_INLINE_DECL std::map<std::string, guint64> get_build_files_details () noexcept;

// void pamac_database_get_build_files_details_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_build_files_details_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
//GI_INLINE_DECL void get_build_files_details_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GHashTable* pamac_database_get_build_files_details_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GHashTable* pamac_database_get_build_files_details_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
//GI_INLINE_DECL std::map<std::string, guint64> get_build_files_details_finish (Gio::AsyncResult _res_) noexcept;

// gboolean pamac_database_is_installed_pkg (PamacDatabase* self, const gchar* pkgname);
// gboolean pamac_database_is_installed_pkg (::PamacDatabase* self, const char* pkgname);
GI_INLINE_DECL bool is_installed_pkg (const std::string & pkgname) noexcept;

// PamacAlpmPackage* pamac_database_get_installed_pkg (PamacDatabase* self, const gchar* pkgname);
// ::PamacAlpmPackage* pamac_database_get_installed_pkg (::PamacDatabase* self, const char* pkgname);
GI_INLINE_DECL Pamac::AlpmPackage get_installed_pkg (const std::string & pkgname) noexcept;

// gboolean pamac_database_has_installed_satisfier (PamacDatabase* self, const gchar* depstring);
// gboolean pamac_database_has_installed_satisfier (::PamacDatabase* self, const char* depstring);
GI_INLINE_DECL bool has_installed_satisfier (const std::string & depstring) noexcept;

// PamacAlpmPackage* pamac_database_get_installed_satisfier (PamacDatabase* self, const gchar* depstring);
// ::PamacAlpmPackage* pamac_database_get_installed_satisfier (::PamacDatabase* self, const char* depstring);
GI_INLINE_DECL Pamac::AlpmPackage get_installed_satisfier (const std::string & depstring) noexcept;

// GPtrArray* pamac_database_get_installed_pkgs_by_glob (PamacDatabase* self, const gchar* glob);
// ::GPtrArray* pamac_database_get_installed_pkgs_by_glob (::PamacDatabase* self, const char* glob);
GI_INLINE_DECL GLib::PtrArray get_installed_pkgs_by_glob (const std::string & glob) noexcept;

// gboolean pamac_database_should_hold (PamacDatabase* self, const gchar* pkgname);
// gboolean pamac_database_should_hold (::PamacDatabase* self, const char* pkgname);
GI_INLINE_DECL bool should_hold (const std::string & pkgname) noexcept;

// GPtrArray* pamac_database_get_installed_pkgs (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_installed_pkgs (::PamacDatabase* self);
GI_INLINE_DECL GLib::PtrArray get_installed_pkgs () noexcept;

// void pamac_database_get_installed_pkgs_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_pkgs_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_installed_pkgs_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_installed_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_installed_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_installed_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_get_installed_apps_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_apps_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_installed_apps_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_installed_apps_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_installed_apps_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_installed_apps_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_get_explicitly_installed_pkgs (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_explicitly_installed_pkgs (::PamacDatabase* self);
GI_INLINE_DECL GLib::PtrArray get_explicitly_installed_pkgs () noexcept;

// void pamac_database_get_explicitly_installed_pkgs_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_explicitly_installed_pkgs_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_explicitly_installed_pkgs_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_explicitly_installed_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_explicitly_installed_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_explicitly_installed_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_get_foreign_pkgs (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_foreign_pkgs (::PamacDatabase* self);
GI_INLINE_DECL GLib::PtrArray get_foreign_pkgs () noexcept;

// void pamac_database_get_foreign_pkgs_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_foreign_pkgs_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_foreign_pkgs_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_foreign_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_foreign_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_foreign_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_get_orphans (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_orphans (::PamacDatabase* self);
GI_INLINE_DECL GLib::PtrArray get_orphans () noexcept;

// void pamac_database_get_orphans_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_orphans_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_orphans_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_orphans_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_orphans_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_orphans_finish (Gio::AsyncResult _res_) noexcept;

// gboolean pamac_database_is_sync_pkg (PamacDatabase* self, const gchar* pkgname);
// gboolean pamac_database_is_sync_pkg (::PamacDatabase* self, const char* pkgname);
GI_INLINE_DECL bool is_sync_pkg (const std::string & pkgname) noexcept;

// PamacAlpmPackage* pamac_database_get_sync_pkg (PamacDatabase* self, const gchar* pkgname);
// ::PamacAlpmPackage* pamac_database_get_sync_pkg (::PamacDatabase* self, const char* pkgname);
GI_INLINE_DECL Pamac::AlpmPackage get_sync_pkg (const std::string & pkgname) noexcept;

// gboolean pamac_database_has_sync_satisfier (PamacDatabase* self, const gchar* depstring);
// gboolean pamac_database_has_sync_satisfier (::PamacDatabase* self, const char* depstring);
GI_INLINE_DECL bool has_sync_satisfier (const std::string & depstring) noexcept;

// PamacAlpmPackage* pamac_database_get_sync_satisfier (PamacDatabase* self, const gchar* depstring);
// ::PamacAlpmPackage* pamac_database_get_sync_satisfier (::PamacDatabase* self, const char* depstring);
GI_INLINE_DECL Pamac::AlpmPackage get_sync_satisfier (const std::string & depstring) noexcept;

// GPtrArray* pamac_database_get_sync_pkgs_by_glob (PamacDatabase* self, const gchar* glob);
// ::GPtrArray* pamac_database_get_sync_pkgs_by_glob (::PamacDatabase* self, const char* glob);
GI_INLINE_DECL GLib::PtrArray get_sync_pkgs_by_glob (const std::string & glob) noexcept;

// PamacPackage* pamac_database_get_app_by_id (PamacDatabase* self, const gchar* app_id);
// ::PamacPackage* pamac_database_get_app_by_id (::PamacDatabase* self, const char* app_id);
GI_INLINE_DECL Pamac::Package get_app_by_id (const std::string & app_id) noexcept;

// void pamac_database_get_url_stream (PamacDatabase* self, const gchar* url, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_url_stream (::PamacDatabase* self, const char* url, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_url_stream (const std::string & url, Gio::AsyncReadyCallback _callback_) noexcept;

// GInputStream* pamac_database_get_url_stream_finish (PamacDatabase* self, GAsyncResult* _res_, GError ** error);
// ::GInputStream* pamac_database_get_url_stream_finish (::PamacDatabase* self, ::GAsyncResult* _res_, GError ** error);
GI_INLINE_DECL Gio::InputStream get_url_stream_finish (Gio::AsyncResult _res_);
GI_INLINE_DECL Gio::InputStream get_url_stream_finish (Gio::AsyncResult _res_, GLib::Error * _error) noexcept;

// GPtrArray* pamac_database_search_installed_pkgs (PamacDatabase* self, const gchar* search_string);
// ::GPtrArray* pamac_database_search_installed_pkgs (::PamacDatabase* self, const char* search_string);
GI_INLINE_DECL GLib::PtrArray search_installed_pkgs (const std::string & search_string) noexcept;

// void pamac_database_search_installed_pkgs_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_installed_pkgs_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void search_installed_pkgs_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_search_installed_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_installed_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray search_installed_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_search_repos_pkgs (PamacDatabase* self, const gchar* search_string);
// ::GPtrArray* pamac_database_search_repos_pkgs (::PamacDatabase* self, const char* search_string);
GI_INLINE_DECL GLib::PtrArray search_repos_pkgs (const std::string & search_string) noexcept;

// void pamac_database_search_repos_pkgs_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_repos_pkgs_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void search_repos_pkgs_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_search_repos_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_repos_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray search_repos_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_search_uninstalled_apps (PamacDatabase* self, GPtrArray* search_terms);
// ::GPtrArray* pamac_database_search_uninstalled_apps (::PamacDatabase* self, ::GPtrArray* search_terms);
GI_INLINE_DECL GLib::PtrArray search_uninstalled_apps (GLib::PtrArray search_terms) noexcept;

// void pamac_database_suggest_pkgnames_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_suggest_pkgnames_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void suggest_pkgnames_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_suggest_pkgnames_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_suggest_pkgnames_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray suggest_pkgnames_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_search_pkgs (PamacDatabase* self, const gchar* search_string);
// ::GPtrArray* pamac_database_search_pkgs (::PamacDatabase* self, const char* search_string);
GI_INLINE_DECL GLib::PtrArray search_pkgs (const std::string & search_string) noexcept;

// void pamac_database_search_pkgs_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_pkgs_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void search_pkgs_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_search_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray search_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_search_aur_pkgs (PamacDatabase* self, const gchar* search_string);
// ::GPtrArray* pamac_database_search_aur_pkgs (::PamacDatabase* self, const char* search_string);
GI_INLINE_DECL GLib::PtrArray search_aur_pkgs (const std::string & search_string) noexcept;

// void pamac_database_search_aur_pkgs_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_aur_pkgs_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void search_aur_pkgs_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_search_aur_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_aur_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray search_aur_pkgs_finish (Gio::AsyncResult _res_) noexcept;

//  pamac_database_search_files (PamacDatabase* self, GPtrArray* files);
//  pamac_database_search_files (::PamacDatabase* self, ::GPtrArray* files);
// SKIP; inconsistent map element info

// GPtrArray* pamac_database_get_categories_names (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_categories_names (::PamacDatabase* self);
GI_INLINE_DECL GLib::PtrArray get_categories_names () noexcept;

// void pamac_database_get_category_pkgs_async (PamacDatabase* self, const gchar* category, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_category_pkgs_async (::PamacDatabase* self, const char* category, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_category_pkgs_async (const std::string & category, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_category_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_category_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_category_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_get_repos_names (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_repos_names (::PamacDatabase* self);
GI_INLINE_DECL GLib::PtrArray get_repos_names () noexcept;

// GPtrArray* pamac_database_get_repo_pkgs (PamacDatabase* self, const gchar* repo);
// ::GPtrArray* pamac_database_get_repo_pkgs (::PamacDatabase* self, const char* repo);
GI_INLINE_DECL GLib::PtrArray get_repo_pkgs (const std::string & repo) noexcept;

// void pamac_database_get_repo_pkgs_async (PamacDatabase* self, const gchar* repo, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_repo_pkgs_async (::PamacDatabase* self, const char* repo, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_repo_pkgs_async (const std::string & repo, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_repo_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_repo_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_repo_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_get_groups_names (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_groups_names (::PamacDatabase* self);
GI_INLINE_DECL GLib::PtrArray get_groups_names () noexcept;

// GPtrArray* pamac_database_get_group_pkgs (PamacDatabase* self, const gchar* group_name);
// ::GPtrArray* pamac_database_get_group_pkgs (::PamacDatabase* self, const char* group_name);
GI_INLINE_DECL GLib::PtrArray get_group_pkgs (const std::string & group_name) noexcept;

// void pamac_database_get_group_pkgs_async (PamacDatabase* self, const gchar* group_name, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_group_pkgs_async (::PamacDatabase* self, const char* group_name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_group_pkgs_async (const std::string & group_name, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_group_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_group_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_group_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// PamacAlpmPackage* pamac_database_get_pkg (PamacDatabase* self, const gchar* pkgname);
// ::PamacAlpmPackage* pamac_database_get_pkg (::PamacDatabase* self, const char* pkgname);
GI_INLINE_DECL Pamac::AlpmPackage get_pkg (const std::string & pkgname) noexcept;

// GFile* pamac_database_clone_build_files (PamacDatabase* self, const gchar* pkgname, gboolean overwrite_files, GCancellable* cancellable);
// ::GFile* pamac_database_clone_build_files (::PamacDatabase* self, const char* pkgname, gboolean overwrite_files, ::GCancellable* cancellable);
GI_INLINE_DECL Gio::File clone_build_files (const std::string & pkgname, gboolean overwrite_files, Gio::Cancellable cancellable) noexcept;
GI_INLINE_DECL Gio::File clone_build_files (const std::string & pkgname, gboolean overwrite_files) noexcept;

// void pamac_database_clone_build_files_async (PamacDatabase* self, const gchar* pkgname, gboolean overwrite_files, GCancellable* cancellable, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_clone_build_files_async (::PamacDatabase* self, const char* pkgname, gboolean overwrite_files, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void clone_build_files_async (const std::string & pkgname, gboolean overwrite_files, Gio::Cancellable cancellable, Gio::AsyncReadyCallback _callback_) noexcept;
GI_INLINE_DECL void clone_build_files_async (const std::string & pkgname, gboolean overwrite_files, Gio::AsyncReadyCallback _callback_) noexcept;

// GFile* pamac_database_clone_build_files_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GFile* pamac_database_clone_build_files_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL Gio::File clone_build_files_finish (Gio::AsyncResult _res_) noexcept;

// gboolean pamac_database_regenerate_srcinfo (PamacDatabase* self, const gchar* pkgname, GCancellable* cancellable);
// gboolean pamac_database_regenerate_srcinfo (::PamacDatabase* self, const char* pkgname, ::GCancellable* cancellable);
GI_INLINE_DECL bool regenerate_srcinfo (const std::string & pkgname, Gio::Cancellable cancellable) noexcept;
GI_INLINE_DECL bool regenerate_srcinfo (const std::string & pkgname) noexcept;

// void pamac_database_regenerate_srcinfo_async (PamacDatabase* self, const gchar* pkgname, GCancellable* cancellable, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_regenerate_srcinfo_async (::PamacDatabase* self, const char* pkgname, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void regenerate_srcinfo_async (const std::string & pkgname, Gio::Cancellable cancellable, Gio::AsyncReadyCallback _callback_) noexcept;
GI_INLINE_DECL void regenerate_srcinfo_async (const std::string & pkgname, Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_database_regenerate_srcinfo_finish (PamacDatabase* self, GAsyncResult* _res_);
// gboolean pamac_database_regenerate_srcinfo_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool regenerate_srcinfo_finish (Gio::AsyncResult _res_) noexcept;

// PamacAURPackage* pamac_database_get_aur_pkg (PamacDatabase* self, const gchar* pkgname);
// ::PamacAURPackage* pamac_database_get_aur_pkg (::PamacDatabase* self, const char* pkgname);
GI_INLINE_DECL Pamac::AURPackage get_aur_pkg (const std::string & pkgname) noexcept;

// void pamac_database_get_aur_pkg_async (PamacDatabase* self, const gchar* pkgname, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_aur_pkg_async (::PamacDatabase* self, const char* pkgname, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_aur_pkg_async (const std::string & pkgname, Gio::AsyncReadyCallback _callback_) noexcept;

// PamacAURPackage* pamac_database_get_aur_pkg_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::PamacAURPackage* pamac_database_get_aur_pkg_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL Pamac::AURPackage get_aur_pkg_finish (Gio::AsyncResult _res_) noexcept;

// GHashTable* pamac_database_get_aur_pkgs (PamacDatabase* self, GPtrArray* pkgnames);
// ::GHashTable* pamac_database_get_aur_pkgs (::PamacDatabase* self, ::GPtrArray* pkgnames);
GI_INLINE_DECL std::map<std::string, Pamac::AURPackage> get_aur_pkgs (GLib::PtrArray pkgnames) noexcept;

// void pamac_database_get_aur_pkgs_async (PamacDatabase* self, GPtrArray* pkgnames, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_aur_pkgs_async (::PamacDatabase* self, ::GPtrArray* pkgnames, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_aur_pkgs_async (GLib::PtrArray pkgnames, Gio::AsyncReadyCallback _callback_) noexcept;

// GHashTable* pamac_database_get_aur_pkgs_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GHashTable* pamac_database_get_aur_pkgs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL std::map<std::string, Pamac::AURPackage> get_aur_pkgs_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_refresh_tmp_files_dbs_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_refresh_tmp_files_dbs_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void refresh_tmp_files_dbs_async (Gio::AsyncReadyCallback _callback_) noexcept;

// void pamac_database_refresh_tmp_files_dbs_finish (PamacDatabase* self, GAsyncResult* _res_);
// void pamac_database_refresh_tmp_files_dbs_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL void refresh_tmp_files_dbs_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_refresh_tmp_files_dbs (PamacDatabase* self);
// void pamac_database_refresh_tmp_files_dbs (::PamacDatabase* self);
GI_INLINE_DECL void refresh_tmp_files_dbs () noexcept;

// GDateTime* pamac_database_get_last_refresh_time (PamacDatabase* self);
// ::GDateTime* pamac_database_get_last_refresh_time (::PamacDatabase* self);
GI_INLINE_DECL GLib::DateTime get_last_refresh_time () noexcept;

// gboolean pamac_database_need_refresh (PamacDatabase* self);
// gboolean pamac_database_need_refresh (::PamacDatabase* self);
GI_INLINE_DECL bool need_refresh () noexcept;

// PamacUpdates* pamac_database_get_updates (PamacDatabase* self, gboolean use_timestamp);
// ::PamacUpdates* pamac_database_get_updates (::PamacDatabase* self, gboolean use_timestamp);
GI_INLINE_DECL Pamac::Updates get_updates (gboolean use_timestamp) noexcept;

// void pamac_database_get_updates_async (PamacDatabase* self, gboolean use_timestamp, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_updates_async (::PamacDatabase* self, gboolean use_timestamp, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_updates_async (gboolean use_timestamp, Gio::AsyncReadyCallback _callback_) noexcept;

// PamacUpdates* pamac_database_get_updates_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::PamacUpdates* pamac_database_get_updates_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL Pamac::Updates get_updates_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_search_snaps_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_snaps_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void search_snaps_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_search_snaps_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_snaps_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray search_snaps_finish (Gio::AsyncResult _res_) noexcept;

// gboolean pamac_database_is_installed_snap (PamacDatabase* self, const gchar* name);
// gboolean pamac_database_is_installed_snap (::PamacDatabase* self, const char* name);
GI_INLINE_DECL bool is_installed_snap (const std::string & name) noexcept;

// void pamac_database_get_snap_async (PamacDatabase* self, const gchar* name, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_snap_async (::PamacDatabase* self, const char* name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_snap_async (const std::string & name, Gio::AsyncReadyCallback _callback_) noexcept;

// PamacSnapPackage* pamac_database_get_snap_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::PamacSnapPackage* pamac_database_get_snap_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL Pamac::SnapPackage get_snap_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_get_installed_snaps_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_snaps_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_installed_snaps_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_installed_snaps_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_installed_snaps_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_installed_snaps_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_get_installed_snap_icon_async (PamacDatabase* self, const gchar* name, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_snap_icon_async (::PamacDatabase* self, const char* name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_installed_snap_icon_async (const std::string & name, Gio::AsyncReadyCallback _callback_) noexcept;

// gchar* pamac_database_get_installed_snap_icon_finish (PamacDatabase* self, GAsyncResult* _res_);
// char* pamac_database_get_installed_snap_icon_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL std::string get_installed_snap_icon_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_get_category_snaps_async (PamacDatabase* self, const gchar* category, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_category_snaps_async (::PamacDatabase* self, const char* category, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_category_snaps_async (const std::string & category, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_category_snaps_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_category_snaps_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_category_snaps_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_refresh_flatpak_appstream_data (PamacDatabase* self);
// void pamac_database_refresh_flatpak_appstream_data (::PamacDatabase* self);
GI_INLINE_DECL void refresh_flatpak_appstream_data () noexcept;

// void pamac_database_refresh_flatpak_appstream_data_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_refresh_flatpak_appstream_data_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void refresh_flatpak_appstream_data_async (Gio::AsyncReadyCallback _callback_) noexcept;

// void pamac_database_refresh_flatpak_appstream_data_finish (PamacDatabase* self, GAsyncResult* _res_);
// void pamac_database_refresh_flatpak_appstream_data_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL void refresh_flatpak_appstream_data_finish (Gio::AsyncResult _res_) noexcept;

// GPtrArray* pamac_database_get_flatpak_remotes_names (PamacDatabase* self);
// ::GPtrArray* pamac_database_get_flatpak_remotes_names (::PamacDatabase* self);
GI_INLINE_DECL GLib::PtrArray get_flatpak_remotes_names () noexcept;

// void pamac_database_get_installed_flatpaks_async (PamacDatabase* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_installed_flatpaks_async (::PamacDatabase* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_installed_flatpaks_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_installed_flatpaks_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_installed_flatpaks_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_installed_flatpaks_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_search_flatpaks_async (PamacDatabase* self, const gchar* search_string, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_search_flatpaks_async (::PamacDatabase* self, const char* search_string, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void search_flatpaks_async (const std::string & search_string, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_search_flatpaks_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_search_flatpaks_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray search_flatpaks_finish (Gio::AsyncResult _res_) noexcept;

// gboolean pamac_database_is_installed_flatpak (PamacDatabase* self, const gchar* name);
// gboolean pamac_database_is_installed_flatpak (::PamacDatabase* self, const char* name);
GI_INLINE_DECL bool is_installed_flatpak (const std::string & name) noexcept;

// void pamac_database_get_flatpak_async (PamacDatabase* self, const gchar* id, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_flatpak_async (::PamacDatabase* self, const char* id, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_flatpak_async (const std::string & id, Gio::AsyncReadyCallback _callback_) noexcept;

// PamacFlatpakPackage* pamac_database_get_flatpak_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::PamacFlatpakPackage* pamac_database_get_flatpak_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL Pamac::FlatpakPackage get_flatpak_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_database_get_category_flatpaks_async (PamacDatabase* self, const gchar* category, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_database_get_category_flatpaks_async (::PamacDatabase* self, const char* category, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_category_flatpaks_async (const std::string & category, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_database_get_category_flatpaks_finish (PamacDatabase* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_database_get_category_flatpaks_finish (::PamacDatabase* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_category_flatpaks_finish (Gio::AsyncResult _res_) noexcept;

gi::property_proxy<Pamac::Config, base::DatabaseBase> property_config()
{ return gi::property_proxy<Pamac::Config, base::DatabaseBase> (*this, "config"); }
const gi::property_proxy<Pamac::Config, base::DatabaseBase> property_config() const
{ return gi::property_proxy<Pamac::Config, base::DatabaseBase> (*this, "config"); }

// PamacConfig* pamac_database_get_config (PamacDatabase* self);
// ::PamacConfig* pamac_database_get_config (::PamacDatabase* self);
GI_INLINE_DECL Pamac::Config get_config () noexcept;

// void pamac_database_set_config (PamacDatabase* self, PamacConfig* value);
// void pamac_database_set_config (::PamacDatabase* self, ::PamacConfig* value);
GI_INLINE_DECL void set_config (Pamac::Config value) noexcept;

// signal get-updates-progress
gi::signal_proxy<void(Pamac::Database, guint percent)> signal_get_updates_progress()
{ return gi::signal_proxy<void(Pamac::Database, guint percent)> (*this, "get-updates-progress"); }

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/database_extra_def.hpp>)
#include <pamac/database_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/database_extra.hpp>)
#include <pamac/database_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class Database : public GI_PAMAC_DATABASE_BASE
{ typedef GI_PAMAC_DATABASE_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacDatabase>
{ typedef Pamac::Database type; }; 

} // namespace repository

} // namespace gi

#endif
