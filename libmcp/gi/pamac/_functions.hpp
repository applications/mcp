// AUTO-GENERATED

#ifndef _GI_PAMAC__FUNCTIONS_HPP_
#define _GI_PAMAC__FUNCTIONS_HPP_

namespace gi {

namespace repository {

namespace Pamac {

// gchar* pamac_get_version ();
// char* pamac_get_version ();
GI_INLINE_DECL std::string get_version () noexcept;

} // namespace Pamac

} // namespace repository

} // namespace gi

#endif
