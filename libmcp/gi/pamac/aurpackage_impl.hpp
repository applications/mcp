// AUTO-GENERATED

#ifndef _GI_PAMAC_AURPACKAGE_IMPL_HPP_
#define _GI_PAMAC_AURPACKAGE_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// const gchar* pamac_aur_package_get_packagebase (PamacAURPackage* self);
// const char* pamac_aur_package_get_packagebase (::PamacAURPackage* self);
std::string base::AURPackageBase::get_packagebase () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacAURPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_aur_package_get_packagebase;
  auto _temp_ret = call_wrap_v ((::PamacAURPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_aur_package_set_packagebase (PamacAURPackage* self, const gchar* value);
// void pamac_aur_package_set_packagebase (::PamacAURPackage* self, const char* value);
void base::AURPackageBase::set_packagebase (const std::string & value) noexcept
{
  typedef void (*call_wrap_t) (::PamacAURPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_aur_package_set_packagebase;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacAURPackage*) (gobj_()), (const char*) (value_to_c));
}
void base::AURPackageBase::set_packagebase () noexcept
{
  typedef void (*call_wrap_t) (::PamacAURPackage* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_aur_package_set_packagebase;
  auto value_to_c = nullptr;
  call_wrap_v ((::PamacAURPackage*) (gobj_()), (const char*) (value_to_c));
}

// const gchar* pamac_aur_package_get_maintainer (PamacAURPackage* self);
// const char* pamac_aur_package_get_maintainer (::PamacAURPackage* self);
std::string base::AURPackageBase::get_maintainer () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacAURPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_aur_package_get_maintainer;
  auto _temp_ret = call_wrap_v ((::PamacAURPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// gdouble pamac_aur_package_get_popularity (PamacAURPackage* self);
// gdouble pamac_aur_package_get_popularity (::PamacAURPackage* self);
gdouble base::AURPackageBase::get_popularity () noexcept
{
  typedef gdouble (*call_wrap_t) (::PamacAURPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_aur_package_get_popularity;
  auto _temp_ret = call_wrap_v ((::PamacAURPackage*) (gobj_()));
  return _temp_ret;
}

// GDateTime* pamac_aur_package_get_lastmodified (PamacAURPackage* self);
// ::GDateTime* pamac_aur_package_get_lastmodified (::PamacAURPackage* self);
GLib::DateTime base::AURPackageBase::get_lastmodified () noexcept
{
  typedef ::GDateTime* (*call_wrap_t) (::PamacAURPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_aur_package_get_lastmodified;
  auto _temp_ret = call_wrap_v ((::PamacAURPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GDateTime* pamac_aur_package_get_outofdate (PamacAURPackage* self);
// ::GDateTime* pamac_aur_package_get_outofdate (::PamacAURPackage* self);
GLib::DateTime base::AURPackageBase::get_outofdate () noexcept
{
  typedef ::GDateTime* (*call_wrap_t) (::PamacAURPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_aur_package_get_outofdate;
  auto _temp_ret = call_wrap_v ((::PamacAURPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GDateTime* pamac_aur_package_get_firstsubmitted (PamacAURPackage* self);
// ::GDateTime* pamac_aur_package_get_firstsubmitted (::PamacAURPackage* self);
GLib::DateTime base::AURPackageBase::get_firstsubmitted () noexcept
{
  typedef ::GDateTime* (*call_wrap_t) (::PamacAURPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_aur_package_get_firstsubmitted;
  auto _temp_ret = call_wrap_v ((::PamacAURPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// guint64 pamac_aur_package_get_numvotes (PamacAURPackage* self);
// guint64 pamac_aur_package_get_numvotes (::PamacAURPackage* self);
guint64 base::AURPackageBase::get_numvotes () noexcept
{
  typedef guint64 (*call_wrap_t) (::PamacAURPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_aur_package_get_numvotes;
  auto _temp_ret = call_wrap_v ((::PamacAURPackage*) (gobj_()));
  return _temp_ret;
}


} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/aurpackage_extra_def_impl.hpp>)
#include <pamac/aurpackage_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/aurpackage_extra_impl.hpp>)
#include <pamac/aurpackage_extra_impl.hpp>
#endif
#endif

#endif
