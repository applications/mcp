// AUTO-GENERATED

#ifndef _GI_PAMAC_UPDATESCHECKER_IMPL_HPP_
#define _GI_PAMAC_UPDATESCHECKER_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// PamacUpdatesChecker* pamac_updates_checker_new ();
// ::PamacUpdatesChecker* pamac_updates_checker_new ();
Pamac::UpdatesChecker base::UpdatesCheckerBase::new_ () noexcept
{
  typedef ::PamacUpdatesChecker* (*call_wrap_t) ();
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_checker_new;
  auto _temp_ret = call_wrap_v ();
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_updates_checker_check_updates (PamacUpdatesChecker* self);
// void pamac_updates_checker_check_updates (::PamacUpdatesChecker* self);
void base::UpdatesCheckerBase::check_updates () noexcept
{
  typedef void (*call_wrap_t) (::PamacUpdatesChecker* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_checker_check_updates;
  call_wrap_v ((::PamacUpdatesChecker*) (gobj_()));
}

// guint16 pamac_updates_checker_get_updates_nb (PamacUpdatesChecker* self);
// guint16 pamac_updates_checker_get_updates_nb (::PamacUpdatesChecker* self);
guint16 base::UpdatesCheckerBase::get_updates_nb () noexcept
{
  typedef guint16 (*call_wrap_t) (::PamacUpdatesChecker* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_checker_get_updates_nb;
  auto _temp_ret = call_wrap_v ((::PamacUpdatesChecker*) (gobj_()));
  return _temp_ret;
}

// gchar** pamac_updates_checker_get_updates_list (PamacUpdatesChecker* self, gint* result_length1);
// char** pamac_updates_checker_get_updates_list (::PamacUpdatesChecker* self, gint* result_length1);
std::vector<std::string> base::UpdatesCheckerBase::get_updates_list (gint & result_length1) noexcept
{
  typedef char** (*call_wrap_t) (::PamacUpdatesChecker* self, gint* result_length1);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_checker_get_updates_list;
  gint result_length1_o {};
  auto _temp_ret = call_wrap_v ((::PamacUpdatesChecker*) (gobj_()), (gint*) (&result_length1_o));
  result_length1 = result_length1_o;
  return gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_none);
}
std::tuple<std::vector<std::string>, gint> base::UpdatesCheckerBase::get_updates_list () noexcept
{
  typedef char** (*call_wrap_t) (::PamacUpdatesChecker* self, gint* result_length1);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_checker_get_updates_list;
  gint result_length1_o {};
  auto _temp_ret = call_wrap_v ((::PamacUpdatesChecker*) (gobj_()), (gint*) (&result_length1_o));
  return std::make_tuple (gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_none), result_length1_o);
}

// guint64 pamac_updates_checker_get_refresh_period (PamacUpdatesChecker* self);
// guint64 pamac_updates_checker_get_refresh_period (::PamacUpdatesChecker* self);
guint64 base::UpdatesCheckerBase::get_refresh_period () noexcept
{
  typedef guint64 (*call_wrap_t) (::PamacUpdatesChecker* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_checker_get_refresh_period;
  auto _temp_ret = call_wrap_v ((::PamacUpdatesChecker*) (gobj_()));
  return _temp_ret;
}

// gboolean pamac_updates_checker_get_no_update_hide_icon (PamacUpdatesChecker* self);
// gboolean pamac_updates_checker_get_no_update_hide_icon (::PamacUpdatesChecker* self);
bool base::UpdatesCheckerBase::get_no_update_hide_icon () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacUpdatesChecker* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_checker_get_no_update_hide_icon;
  auto _temp_ret = call_wrap_v ((::PamacUpdatesChecker*) (gobj_()));
  return _temp_ret;
}



} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/updateschecker_extra_def_impl.hpp>)
#include <pamac/updateschecker_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/updateschecker_extra_impl.hpp>)
#include <pamac/updateschecker_extra_impl.hpp>
#endif
#endif

#endif
