// AUTO-GENERATED

#ifndef _GI_PAMAC_TRANSACTION_HPP_
#define _GI_PAMAC_TRANSACTION_HPP_


namespace gi {

namespace repository {

namespace Pamac {

class Database;
class FlatpakPackage;
class SnapPackage;
class TransactionSummary;

class Transaction;

namespace base {


#define GI_PAMAC_TRANSACTION_BASE base::TransactionBase
class TransactionBase : public GObject::Object
{
typedef GObject::Object super_type;
public:
typedef ::PamacTransaction BaseObjectType;

TransactionBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_transaction_get_type(); } 

// PamacTransaction* pamac_transaction_new (PamacDatabase* database);
// ::PamacTransaction* pamac_transaction_new (::PamacDatabase* database);
static GI_INLINE_DECL Pamac::Transaction new_ (Pamac::Database database) noexcept;

// void pamac_transaction_quit_daemon (PamacTransaction* self);
// void pamac_transaction_quit_daemon (::PamacTransaction* self);
GI_INLINE_DECL void quit_daemon () noexcept;

// void pamac_transaction_ask_commit (PamacTransaction* self, PamacTransactionSummary* summary, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_ask_commit (::PamacTransaction* self, ::PamacTransactionSummary* summary, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void ask_commit (Pamac::TransactionSummary summary, Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_transaction_ask_commit_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_ask_commit_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool ask_commit_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_ask_edit_build_files (PamacTransaction* self, PamacTransactionSummary* summary, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_ask_edit_build_files (::PamacTransaction* self, ::PamacTransactionSummary* summary, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void ask_edit_build_files (Pamac::TransactionSummary summary, Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_transaction_ask_edit_build_files_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_ask_edit_build_files_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool ask_edit_build_files_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_edit_build_files (PamacTransaction* self, GPtrArray* pkgnames, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_edit_build_files (::PamacTransaction* self, ::GPtrArray* pkgnames, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void edit_build_files (GLib::PtrArray pkgnames, Gio::AsyncReadyCallback _callback_) noexcept;

// void pamac_transaction_edit_build_files_finish (PamacTransaction* self, GAsyncResult* _res_);
// void pamac_transaction_edit_build_files_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL void edit_build_files_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_ask_import_key (PamacTransaction* self, const gchar* pkgname, const gchar* key, const gchar* owner, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_ask_import_key (::PamacTransaction* self, const char* pkgname, const char* key, const char* owner, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void ask_import_key (const std::string & pkgname, const std::string & key, const std::string & owner, Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_transaction_ask_import_key_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_ask_import_key_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool ask_import_key_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_get_build_files_async (PamacTransaction* self, const gchar* pkgname, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_get_build_files_async (::PamacTransaction* self, const char* pkgname, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_build_files_async (const std::string & pkgname, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_transaction_get_build_files_finish (PamacTransaction* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_transaction_get_build_files_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_build_files_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_choose_optdeps (PamacTransaction* self, const gchar* pkgname, GPtrArray* optdeps, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_choose_optdeps (::PamacTransaction* self, const char* pkgname, ::GPtrArray* optdeps, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void choose_optdeps (const std::string & pkgname, GLib::PtrArray optdeps, Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_transaction_choose_optdeps_finish (PamacTransaction* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_transaction_choose_optdeps_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray choose_optdeps_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_choose_provider (PamacTransaction* self, const gchar* depend, GPtrArray* providers, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_choose_provider (::PamacTransaction* self, const char* depend, ::GPtrArray* providers, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void choose_provider (const std::string & depend, GLib::PtrArray providers, Gio::AsyncReadyCallback _callback_) noexcept;

// gint pamac_transaction_choose_provider_finish (PamacTransaction* self, GAsyncResult* _res_);
// gint pamac_transaction_choose_provider_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL gint choose_provider_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_ask_snap_install_classic (PamacTransaction* self, const gchar* name, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_ask_snap_install_classic (::PamacTransaction* self, const char* name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void ask_snap_install_classic (const std::string & name, Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_transaction_ask_snap_install_classic_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_ask_snap_install_classic_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool ask_snap_install_classic_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_get_authorization_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_get_authorization_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_authorization_async (Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_transaction_get_authorization_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_get_authorization_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool get_authorization_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_remove_authorization (PamacTransaction* self);
// void pamac_transaction_remove_authorization (::PamacTransaction* self);
GI_INLINE_DECL void remove_authorization () noexcept;

// void pamac_transaction_generate_mirrors_list_async (PamacTransaction* self, const gchar* country, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_generate_mirrors_list_async (::PamacTransaction* self, const char* country, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void generate_mirrors_list_async (const std::string & country, Gio::AsyncReadyCallback _callback_) noexcept;

// void pamac_transaction_generate_mirrors_list_finish (PamacTransaction* self, GAsyncResult* _res_);
// void pamac_transaction_generate_mirrors_list_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL void generate_mirrors_list_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_clean_cache_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_clean_cache_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void clean_cache_async (Gio::AsyncReadyCallback _callback_) noexcept;

// void pamac_transaction_clean_cache_finish (PamacTransaction* self, GAsyncResult* _res_);
// void pamac_transaction_clean_cache_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL void clean_cache_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_clean_build_files_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_clean_build_files_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void clean_build_files_async (Gio::AsyncReadyCallback _callback_) noexcept;

// void pamac_transaction_clean_build_files_finish (PamacTransaction* self, GAsyncResult* _res_);
// void pamac_transaction_clean_build_files_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL void clean_build_files_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_set_pkgreason_async (PamacTransaction* self, const gchar* pkgname, guint reason, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_set_pkgreason_async (::PamacTransaction* self, const char* pkgname, guint reason, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void set_pkgreason_async (const std::string & pkgname, guint reason, Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_transaction_set_pkgreason_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_set_pkgreason_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool set_pkgreason_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_download_updates_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_download_updates_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void download_updates_async (Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_transaction_download_updates_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_download_updates_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool download_updates_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_run_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_run_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void run_async (Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_transaction_run_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_run_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool run_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_add_pkg_to_install (PamacTransaction* self, const gchar* name);
// void pamac_transaction_add_pkg_to_install (::PamacTransaction* self, const char* name);
GI_INLINE_DECL void add_pkg_to_install (const std::string & name) noexcept;

// void pamac_transaction_add_pkg_to_remove (PamacTransaction* self, const gchar* name);
// void pamac_transaction_add_pkg_to_remove (::PamacTransaction* self, const char* name);
GI_INLINE_DECL void add_pkg_to_remove (const std::string & name) noexcept;

// void pamac_transaction_add_path_to_load (PamacTransaction* self, const gchar* path);
// void pamac_transaction_add_path_to_load (::PamacTransaction* self, const char* path);
GI_INLINE_DECL void add_path_to_load (const std::string & path) noexcept;

// void pamac_transaction_add_pkg_to_build (PamacTransaction* self, const gchar* name, gboolean clone_build_files, gboolean clone_deps_build_files);
// void pamac_transaction_add_pkg_to_build (::PamacTransaction* self, const char* name, gboolean clone_build_files, gboolean clone_deps_build_files);
GI_INLINE_DECL void add_pkg_to_build (const std::string & name, gboolean clone_build_files, gboolean clone_deps_build_files) noexcept;

// void pamac_transaction_add_temporary_ignore_pkg (PamacTransaction* self, const gchar* name);
// void pamac_transaction_add_temporary_ignore_pkg (::PamacTransaction* self, const char* name);
GI_INLINE_DECL void add_temporary_ignore_pkg (const std::string & name) noexcept;

// void pamac_transaction_add_overwrite_file (PamacTransaction* self, const gchar* glob);
// void pamac_transaction_add_overwrite_file (::PamacTransaction* self, const char* glob);
GI_INLINE_DECL void add_overwrite_file (const std::string & glob) noexcept;

// void pamac_transaction_add_pkg_to_mark_as_dep (PamacTransaction* self, const gchar* name);
// void pamac_transaction_add_pkg_to_mark_as_dep (::PamacTransaction* self, const char* name);
GI_INLINE_DECL void add_pkg_to_mark_as_dep (const std::string & name) noexcept;

// void pamac_transaction_add_pkgs_to_upgrade (PamacTransaction* self, gboolean force_refresh);
// void pamac_transaction_add_pkgs_to_upgrade (::PamacTransaction* self, gboolean force_refresh);
GI_INLINE_DECL void add_pkgs_to_upgrade (gboolean force_refresh) noexcept;

// void pamac_transaction_add_snap_to_install (PamacTransaction* self, PamacSnapPackage* pkg);
// void pamac_transaction_add_snap_to_install (::PamacTransaction* self, ::PamacSnapPackage* pkg);
GI_INLINE_DECL void add_snap_to_install (Pamac::SnapPackage pkg) noexcept;

// void pamac_transaction_add_snap_to_remove (PamacTransaction* self, PamacSnapPackage* pkg);
// void pamac_transaction_add_snap_to_remove (::PamacTransaction* self, ::PamacSnapPackage* pkg);
GI_INLINE_DECL void add_snap_to_remove (Pamac::SnapPackage pkg) noexcept;

// void pamac_transaction_snap_switch_channel_async (PamacTransaction* self, const gchar* snap_name, const gchar* channel, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_snap_switch_channel_async (::PamacTransaction* self, const char* snap_name, const char* channel, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void snap_switch_channel_async (const std::string & snap_name, const std::string & channel, Gio::AsyncReadyCallback _callback_) noexcept;

// gboolean pamac_transaction_snap_switch_channel_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_snap_switch_channel_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL bool snap_switch_channel_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_add_flatpak_to_install (PamacTransaction* self, PamacFlatpakPackage* pkg);
// void pamac_transaction_add_flatpak_to_install (::PamacTransaction* self, ::PamacFlatpakPackage* pkg);
GI_INLINE_DECL void add_flatpak_to_install (Pamac::FlatpakPackage pkg) noexcept;

// void pamac_transaction_add_flatpak_to_remove (PamacTransaction* self, PamacFlatpakPackage* pkg);
// void pamac_transaction_add_flatpak_to_remove (::PamacTransaction* self, ::PamacFlatpakPackage* pkg);
GI_INLINE_DECL void add_flatpak_to_remove (Pamac::FlatpakPackage pkg) noexcept;

// void pamac_transaction_add_flatpak_to_upgrade (PamacTransaction* self, PamacFlatpakPackage* pkg);
// void pamac_transaction_add_flatpak_to_upgrade (::PamacTransaction* self, ::PamacFlatpakPackage* pkg);
GI_INLINE_DECL void add_flatpak_to_upgrade (Pamac::FlatpakPackage pkg) noexcept;

// void pamac_transaction_run_cmd_line_async (PamacTransaction* self, GPtrArray* args, const gchar* working_directory, GCancellable* cancellable, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_run_cmd_line_async (::PamacTransaction* self, ::GPtrArray* args, const char* working_directory, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void run_cmd_line_async (GLib::PtrArray args, const std::string & working_directory, Gio::Cancellable cancellable, Gio::AsyncReadyCallback _callback_) noexcept;
GI_INLINE_DECL void run_cmd_line_async (GLib::PtrArray args, Gio::Cancellable cancellable, Gio::AsyncReadyCallback _callback_) noexcept;

// gint pamac_transaction_run_cmd_line_finish (PamacTransaction* self, GAsyncResult* _res_);
// gint pamac_transaction_run_cmd_line_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GI_INLINE_DECL gint run_cmd_line_finish (Gio::AsyncResult _res_) noexcept;

// void pamac_transaction_cancel (PamacTransaction* self);
// void pamac_transaction_cancel (::PamacTransaction* self);
GI_INLINE_DECL void cancel () noexcept;

gi::property_proxy<Pamac::Database, base::TransactionBase> property_database()
{ return gi::property_proxy<Pamac::Database, base::TransactionBase> (*this, "database"); }
const gi::property_proxy<Pamac::Database, base::TransactionBase> property_database() const
{ return gi::property_proxy<Pamac::Database, base::TransactionBase> (*this, "database"); }

// PamacDatabase* pamac_transaction_get_database (PamacTransaction* self);
// ::PamacDatabase* pamac_transaction_get_database (::PamacTransaction* self);
GI_INLINE_DECL Pamac::Database get_database () noexcept;

// void pamac_transaction_set_database (PamacTransaction* self, PamacDatabase* value);
// void pamac_transaction_set_database (::PamacTransaction* self, ::PamacDatabase* value);
GI_INLINE_DECL void set_database (Pamac::Database value) noexcept;

gi::property_proxy<bool, base::TransactionBase> property_download_only()
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "download-only"); }
const gi::property_proxy<bool, base::TransactionBase> property_download_only() const
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "download-only"); }

// gboolean pamac_transaction_get_download_only (PamacTransaction* self);
// gboolean pamac_transaction_get_download_only (::PamacTransaction* self);
GI_INLINE_DECL bool get_download_only () noexcept;

// void pamac_transaction_set_download_only (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_download_only (::PamacTransaction* self, gboolean value);
GI_INLINE_DECL void set_download_only (gboolean value) noexcept;

gi::property_proxy<bool, base::TransactionBase> property_dry_run()
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "dry-run"); }
const gi::property_proxy<bool, base::TransactionBase> property_dry_run() const
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "dry-run"); }

// gboolean pamac_transaction_get_dry_run (PamacTransaction* self);
// gboolean pamac_transaction_get_dry_run (::PamacTransaction* self);
GI_INLINE_DECL bool get_dry_run () noexcept;

// void pamac_transaction_set_dry_run (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_dry_run (::PamacTransaction* self, gboolean value);
GI_INLINE_DECL void set_dry_run (gboolean value) noexcept;

gi::property_proxy<bool, base::TransactionBase> property_install_if_needed()
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "install-if-needed"); }
const gi::property_proxy<bool, base::TransactionBase> property_install_if_needed() const
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "install-if-needed"); }

// gboolean pamac_transaction_get_install_if_needed (PamacTransaction* self);
// gboolean pamac_transaction_get_install_if_needed (::PamacTransaction* self);
GI_INLINE_DECL bool get_install_if_needed () noexcept;

// void pamac_transaction_set_install_if_needed (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_install_if_needed (::PamacTransaction* self, gboolean value);
GI_INLINE_DECL void set_install_if_needed (gboolean value) noexcept;

gi::property_proxy<bool, base::TransactionBase> property_remove_if_unneeded()
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "remove-if-unneeded"); }
const gi::property_proxy<bool, base::TransactionBase> property_remove_if_unneeded() const
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "remove-if-unneeded"); }

// gboolean pamac_transaction_get_remove_if_unneeded (PamacTransaction* self);
// gboolean pamac_transaction_get_remove_if_unneeded (::PamacTransaction* self);
GI_INLINE_DECL bool get_remove_if_unneeded () noexcept;

// void pamac_transaction_set_remove_if_unneeded (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_remove_if_unneeded (::PamacTransaction* self, gboolean value);
GI_INLINE_DECL void set_remove_if_unneeded (gboolean value) noexcept;

gi::property_proxy<bool, base::TransactionBase> property_cascade()
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "cascade"); }
const gi::property_proxy<bool, base::TransactionBase> property_cascade() const
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "cascade"); }

// gboolean pamac_transaction_get_cascade (PamacTransaction* self);
// gboolean pamac_transaction_get_cascade (::PamacTransaction* self);
GI_INLINE_DECL bool get_cascade () noexcept;

// void pamac_transaction_set_cascade (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_cascade (::PamacTransaction* self, gboolean value);
GI_INLINE_DECL void set_cascade (gboolean value) noexcept;

gi::property_proxy<bool, base::TransactionBase> property_keep_config_files()
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "keep-config-files"); }
const gi::property_proxy<bool, base::TransactionBase> property_keep_config_files() const
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "keep-config-files"); }

// gboolean pamac_transaction_get_keep_config_files (PamacTransaction* self);
// gboolean pamac_transaction_get_keep_config_files (::PamacTransaction* self);
GI_INLINE_DECL bool get_keep_config_files () noexcept;

// void pamac_transaction_set_keep_config_files (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_keep_config_files (::PamacTransaction* self, gboolean value);
GI_INLINE_DECL void set_keep_config_files (gboolean value) noexcept;

gi::property_proxy<bool, base::TransactionBase> property_install_as_dep()
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "install-as-dep"); }
const gi::property_proxy<bool, base::TransactionBase> property_install_as_dep() const
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "install-as-dep"); }

// gboolean pamac_transaction_get_install_as_dep (PamacTransaction* self);
// gboolean pamac_transaction_get_install_as_dep (::PamacTransaction* self);
GI_INLINE_DECL bool get_install_as_dep () noexcept;

// void pamac_transaction_set_install_as_dep (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_install_as_dep (::PamacTransaction* self, gboolean value);
GI_INLINE_DECL void set_install_as_dep (gboolean value) noexcept;

gi::property_proxy<bool, base::TransactionBase> property_install_as_explicit()
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "install-as-explicit"); }
const gi::property_proxy<bool, base::TransactionBase> property_install_as_explicit() const
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "install-as-explicit"); }

// gboolean pamac_transaction_get_install_as_explicit (PamacTransaction* self);
// gboolean pamac_transaction_get_install_as_explicit (::PamacTransaction* self);
GI_INLINE_DECL bool get_install_as_explicit () noexcept;

// void pamac_transaction_set_install_as_explicit (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_install_as_explicit (::PamacTransaction* self, gboolean value);
GI_INLINE_DECL void set_install_as_explicit (gboolean value) noexcept;

gi::property_proxy<bool, base::TransactionBase> property_no_refresh()
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "no-refresh"); }
const gi::property_proxy<bool, base::TransactionBase> property_no_refresh() const
{ return gi::property_proxy<bool, base::TransactionBase> (*this, "no-refresh"); }

// gboolean pamac_transaction_get_no_refresh (PamacTransaction* self);
// gboolean pamac_transaction_get_no_refresh (::PamacTransaction* self);
GI_INLINE_DECL bool get_no_refresh () noexcept;

// void pamac_transaction_set_no_refresh (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_no_refresh (::PamacTransaction* self, gboolean value);
GI_INLINE_DECL void set_no_refresh (gboolean value) noexcept;

// signal emit-action
gi::signal_proxy<void(Pamac::Transaction, const std::string & action)> signal_emit_action()
{ return gi::signal_proxy<void(Pamac::Transaction, const std::string & action)> (*this, "emit-action"); }

// signal emit-action-progress
gi::signal_proxy<void(Pamac::Transaction, const std::string & action, const std::string & status, gdouble progress)> signal_emit_action_progress()
{ return gi::signal_proxy<void(Pamac::Transaction, const std::string & action, const std::string & status, gdouble progress)> (*this, "emit-action-progress"); }

// signal emit-download-progress
gi::signal_proxy<void(Pamac::Transaction, const std::string & action, const std::string & status, gdouble progress)> signal_emit_download_progress()
{ return gi::signal_proxy<void(Pamac::Transaction, const std::string & action, const std::string & status, gdouble progress)> (*this, "emit-download-progress"); }

// signal emit-hook-progress
gi::signal_proxy<void(Pamac::Transaction, const std::string & action, const std::string & details, const std::string & status, gdouble progress)> signal_emit_hook_progress()
{ return gi::signal_proxy<void(Pamac::Transaction, const std::string & action, const std::string & details, const std::string & status, gdouble progress)> (*this, "emit-hook-progress"); }

// signal emit-script-output
gi::signal_proxy<void(Pamac::Transaction, const std::string & message)> signal_emit_script_output()
{ return gi::signal_proxy<void(Pamac::Transaction, const std::string & message)> (*this, "emit-script-output"); }

// signal emit-warning
gi::signal_proxy<void(Pamac::Transaction, const std::string & message)> signal_emit_warning()
{ return gi::signal_proxy<void(Pamac::Transaction, const std::string & message)> (*this, "emit-warning"); }

// signal emit-error
gi::signal_proxy<void(Pamac::Transaction, const std::string & message, GLib::PtrArray details)> signal_emit_error()
{ return gi::signal_proxy<void(Pamac::Transaction, const std::string & message, GLib::PtrArray details)> (*this, "emit-error"); }

// signal start-waiting
gi::signal_proxy<void(Pamac::Transaction)> signal_start_waiting()
{ return gi::signal_proxy<void(Pamac::Transaction)> (*this, "start-waiting"); }

// signal stop-waiting
gi::signal_proxy<void(Pamac::Transaction)> signal_stop_waiting()
{ return gi::signal_proxy<void(Pamac::Transaction)> (*this, "stop-waiting"); }

// signal start-preparing
gi::signal_proxy<void(Pamac::Transaction)> signal_start_preparing()
{ return gi::signal_proxy<void(Pamac::Transaction)> (*this, "start-preparing"); }

// signal stop-preparing
gi::signal_proxy<void(Pamac::Transaction)> signal_stop_preparing()
{ return gi::signal_proxy<void(Pamac::Transaction)> (*this, "stop-preparing"); }

// signal start-downloading
gi::signal_proxy<void(Pamac::Transaction)> signal_start_downloading()
{ return gi::signal_proxy<void(Pamac::Transaction)> (*this, "start-downloading"); }

// signal stop-downloading
gi::signal_proxy<void(Pamac::Transaction)> signal_stop_downloading()
{ return gi::signal_proxy<void(Pamac::Transaction)> (*this, "stop-downloading"); }

// signal start-building
gi::signal_proxy<void(Pamac::Transaction)> signal_start_building()
{ return gi::signal_proxy<void(Pamac::Transaction)> (*this, "start-building"); }

// signal stop-building
gi::signal_proxy<void(Pamac::Transaction)> signal_stop_building()
{ return gi::signal_proxy<void(Pamac::Transaction)> (*this, "stop-building"); }

// signal important-details-outpout
gi::signal_proxy<void(Pamac::Transaction, gboolean must_show)> signal_important_details_outpout()
{ return gi::signal_proxy<void(Pamac::Transaction, gboolean must_show)> (*this, "important-details-outpout"); }

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/transaction_extra_def.hpp>)
#include <pamac/transaction_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/transaction_extra.hpp>)
#include <pamac/transaction_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class Transaction : public GI_PAMAC_TRANSACTION_BASE
{ typedef GI_PAMAC_TRANSACTION_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacTransaction>
{ typedef Pamac::Transaction type; }; 

} // namespace repository

} // namespace gi

#endif
