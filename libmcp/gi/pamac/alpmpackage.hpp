// AUTO-GENERATED

#ifndef _GI_PAMAC_ALPMPACKAGE_HPP_
#define _GI_PAMAC_ALPMPACKAGE_HPP_

#include "package.hpp"

namespace gi {

namespace repository {

namespace Pamac {


class AlpmPackage;

namespace base {


#define GI_PAMAC_ALPMPACKAGE_BASE base::AlpmPackageBase
class AlpmPackageBase : public Pamac::Package
{
typedef Pamac::Package super_type;
public:
typedef ::PamacAlpmPackage BaseObjectType;

AlpmPackageBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_alpm_package_get_type(); } 

// GPtrArray* pamac_alpm_package_get_files (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_files (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_files () noexcept;

// void pamac_alpm_package_get_files_async (PamacAlpmPackage* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_alpm_package_get_files_async (::PamacAlpmPackage* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
GI_INLINE_DECL void get_files_async (Gio::AsyncReadyCallback _callback_) noexcept;

// GPtrArray* pamac_alpm_package_get_files_finish (PamacAlpmPackage* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_alpm_package_get_files_finish (::PamacAlpmPackage* self, ::GAsyncResult* _res_);
GI_INLINE_DECL GLib::PtrArray get_files_finish (Gio::AsyncResult _res_) noexcept;

gi::property_proxy<GLib::DateTime, base::AlpmPackageBase> property_build_date()
{ return gi::property_proxy<GLib::DateTime, base::AlpmPackageBase> (*this, "build-date"); }
const gi::property_proxy<GLib::DateTime, base::AlpmPackageBase> property_build_date() const
{ return gi::property_proxy<GLib::DateTime, base::AlpmPackageBase> (*this, "build-date"); }

// GDateTime* pamac_alpm_package_get_build_date (PamacAlpmPackage* self);
// ::GDateTime* pamac_alpm_package_get_build_date (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::DateTime get_build_date () noexcept;

gi::property_proxy<std::string, base::AlpmPackageBase> property_packager()
{ return gi::property_proxy<std::string, base::AlpmPackageBase> (*this, "packager"); }
const gi::property_proxy<std::string, base::AlpmPackageBase> property_packager() const
{ return gi::property_proxy<std::string, base::AlpmPackageBase> (*this, "packager"); }

// const gchar* pamac_alpm_package_get_packager (PamacAlpmPackage* self);
// const char* pamac_alpm_package_get_packager (::PamacAlpmPackage* self);
GI_INLINE_DECL std::string get_packager () noexcept;

gi::property_proxy<std::string, base::AlpmPackageBase> property_reason()
{ return gi::property_proxy<std::string, base::AlpmPackageBase> (*this, "reason"); }
const gi::property_proxy<std::string, base::AlpmPackageBase> property_reason() const
{ return gi::property_proxy<std::string, base::AlpmPackageBase> (*this, "reason"); }

// const gchar* pamac_alpm_package_get_reason (PamacAlpmPackage* self);
// const char* pamac_alpm_package_get_reason (::PamacAlpmPackage* self);
GI_INLINE_DECL std::string get_reason () noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_validations()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "validations"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_validations() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "validations"); }

// GPtrArray* pamac_alpm_package_get_validations (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_validations (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_validations () noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_groups()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "groups"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_groups() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "groups"); }

// GPtrArray* pamac_alpm_package_get_groups (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_groups (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_groups () noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_depends()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "depends"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_depends() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "depends"); }

// GPtrArray* pamac_alpm_package_get_depends (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_depends (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_depends () noexcept;

// void pamac_alpm_package_set_depends (PamacAlpmPackage* self, GPtrArray* value);
// void pamac_alpm_package_set_depends (::PamacAlpmPackage* self, ::GPtrArray* value);
GI_INLINE_DECL void set_depends (GLib::PtrArray value) noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_optdepends()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "optdepends"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_optdepends() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "optdepends"); }

// GPtrArray* pamac_alpm_package_get_optdepends (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_optdepends (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_optdepends () noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_makedepends()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "makedepends"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_makedepends() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "makedepends"); }

// GPtrArray* pamac_alpm_package_get_makedepends (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_makedepends (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_makedepends () noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_checkdepends()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "checkdepends"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_checkdepends() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "checkdepends"); }

// GPtrArray* pamac_alpm_package_get_checkdepends (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_checkdepends (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_checkdepends () noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_requiredby()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "requiredby"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_requiredby() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "requiredby"); }

// GPtrArray* pamac_alpm_package_get_requiredby (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_requiredby (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_requiredby () noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_optionalfor()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "optionalfor"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_optionalfor() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "optionalfor"); }

// GPtrArray* pamac_alpm_package_get_optionalfor (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_optionalfor (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_optionalfor () noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_provides()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "provides"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_provides() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "provides"); }

// GPtrArray* pamac_alpm_package_get_provides (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_provides (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_provides () noexcept;

// void pamac_alpm_package_set_provides (PamacAlpmPackage* self, GPtrArray* value);
// void pamac_alpm_package_set_provides (::PamacAlpmPackage* self, ::GPtrArray* value);
GI_INLINE_DECL void set_provides (GLib::PtrArray value) noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_replaces()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "replaces"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_replaces() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "replaces"); }

// GPtrArray* pamac_alpm_package_get_replaces (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_replaces (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_replaces () noexcept;

// void pamac_alpm_package_set_replaces (PamacAlpmPackage* self, GPtrArray* value);
// void pamac_alpm_package_set_replaces (::PamacAlpmPackage* self, ::GPtrArray* value);
GI_INLINE_DECL void set_replaces (GLib::PtrArray value) noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_conflicts()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "conflicts"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_conflicts() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "conflicts"); }

// GPtrArray* pamac_alpm_package_get_conflicts (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_conflicts (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_conflicts () noexcept;

// void pamac_alpm_package_set_conflicts (PamacAlpmPackage* self, GPtrArray* value);
// void pamac_alpm_package_set_conflicts (::PamacAlpmPackage* self, ::GPtrArray* value);
GI_INLINE_DECL void set_conflicts (GLib::PtrArray value) noexcept;

gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_backups()
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "backups"); }
const gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> property_backups() const
{ return gi::property_proxy<GLib::PtrArray, base::AlpmPackageBase> (*this, "backups"); }

// GPtrArray* pamac_alpm_package_get_backups (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_backups (::PamacAlpmPackage* self);
GI_INLINE_DECL GLib::PtrArray get_backups () noexcept;

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/alpmpackage_extra_def.hpp>)
#include <pamac/alpmpackage_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/alpmpackage_extra.hpp>)
#include <pamac/alpmpackage_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class AlpmPackage : public GI_PAMAC_ALPMPACKAGE_BASE
{ typedef GI_PAMAC_ALPMPACKAGE_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacAlpmPackage>
{ typedef Pamac::AlpmPackage type; }; 

} // namespace repository

} // namespace gi

#endif
