// AUTO-GENERATED

#ifndef _GI_PAMAC_AURPACKAGE_HPP_
#define _GI_PAMAC_AURPACKAGE_HPP_

#include "alpmpackage.hpp"

namespace gi {

namespace repository {

namespace Pamac {


class AURPackage;

namespace base {


#define GI_PAMAC_AURPACKAGE_BASE base::AURPackageBase
class AURPackageBase : public Pamac::AlpmPackage
{
typedef Pamac::AlpmPackage super_type;
public:
typedef ::PamacAURPackage BaseObjectType;

AURPackageBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_aur_package_get_type(); } 

gi::property_proxy<std::string, base::AURPackageBase> property_packagebase()
{ return gi::property_proxy<std::string, base::AURPackageBase> (*this, "packagebase"); }
const gi::property_proxy<std::string, base::AURPackageBase> property_packagebase() const
{ return gi::property_proxy<std::string, base::AURPackageBase> (*this, "packagebase"); }

// const gchar* pamac_aur_package_get_packagebase (PamacAURPackage* self);
// const char* pamac_aur_package_get_packagebase (::PamacAURPackage* self);
GI_INLINE_DECL std::string get_packagebase () noexcept;

// void pamac_aur_package_set_packagebase (PamacAURPackage* self, const gchar* value);
// void pamac_aur_package_set_packagebase (::PamacAURPackage* self, const char* value);
GI_INLINE_DECL void set_packagebase (const std::string & value) noexcept;
GI_INLINE_DECL void set_packagebase () noexcept;

gi::property_proxy<std::string, base::AURPackageBase> property_maintainer()
{ return gi::property_proxy<std::string, base::AURPackageBase> (*this, "maintainer"); }
const gi::property_proxy<std::string, base::AURPackageBase> property_maintainer() const
{ return gi::property_proxy<std::string, base::AURPackageBase> (*this, "maintainer"); }

// const gchar* pamac_aur_package_get_maintainer (PamacAURPackage* self);
// const char* pamac_aur_package_get_maintainer (::PamacAURPackage* self);
GI_INLINE_DECL std::string get_maintainer () noexcept;

gi::property_proxy<gdouble, base::AURPackageBase> property_popularity()
{ return gi::property_proxy<gdouble, base::AURPackageBase> (*this, "popularity"); }
const gi::property_proxy<gdouble, base::AURPackageBase> property_popularity() const
{ return gi::property_proxy<gdouble, base::AURPackageBase> (*this, "popularity"); }

// gdouble pamac_aur_package_get_popularity (PamacAURPackage* self);
// gdouble pamac_aur_package_get_popularity (::PamacAURPackage* self);
GI_INLINE_DECL gdouble get_popularity () noexcept;

gi::property_proxy<GLib::DateTime, base::AURPackageBase> property_lastmodified()
{ return gi::property_proxy<GLib::DateTime, base::AURPackageBase> (*this, "lastmodified"); }
const gi::property_proxy<GLib::DateTime, base::AURPackageBase> property_lastmodified() const
{ return gi::property_proxy<GLib::DateTime, base::AURPackageBase> (*this, "lastmodified"); }

// GDateTime* pamac_aur_package_get_lastmodified (PamacAURPackage* self);
// ::GDateTime* pamac_aur_package_get_lastmodified (::PamacAURPackage* self);
GI_INLINE_DECL GLib::DateTime get_lastmodified () noexcept;

gi::property_proxy<GLib::DateTime, base::AURPackageBase> property_outofdate()
{ return gi::property_proxy<GLib::DateTime, base::AURPackageBase> (*this, "outofdate"); }
const gi::property_proxy<GLib::DateTime, base::AURPackageBase> property_outofdate() const
{ return gi::property_proxy<GLib::DateTime, base::AURPackageBase> (*this, "outofdate"); }

// GDateTime* pamac_aur_package_get_outofdate (PamacAURPackage* self);
// ::GDateTime* pamac_aur_package_get_outofdate (::PamacAURPackage* self);
GI_INLINE_DECL GLib::DateTime get_outofdate () noexcept;

gi::property_proxy<GLib::DateTime, base::AURPackageBase> property_firstsubmitted()
{ return gi::property_proxy<GLib::DateTime, base::AURPackageBase> (*this, "firstsubmitted"); }
const gi::property_proxy<GLib::DateTime, base::AURPackageBase> property_firstsubmitted() const
{ return gi::property_proxy<GLib::DateTime, base::AURPackageBase> (*this, "firstsubmitted"); }

// GDateTime* pamac_aur_package_get_firstsubmitted (PamacAURPackage* self);
// ::GDateTime* pamac_aur_package_get_firstsubmitted (::PamacAURPackage* self);
GI_INLINE_DECL GLib::DateTime get_firstsubmitted () noexcept;

gi::property_proxy<guint64, base::AURPackageBase> property_numvotes()
{ return gi::property_proxy<guint64, base::AURPackageBase> (*this, "numvotes"); }
const gi::property_proxy<guint64, base::AURPackageBase> property_numvotes() const
{ return gi::property_proxy<guint64, base::AURPackageBase> (*this, "numvotes"); }

// guint64 pamac_aur_package_get_numvotes (PamacAURPackage* self);
// guint64 pamac_aur_package_get_numvotes (::PamacAURPackage* self);
GI_INLINE_DECL guint64 get_numvotes () noexcept;

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/aurpackage_extra_def.hpp>)
#include <pamac/aurpackage_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/aurpackage_extra.hpp>)
#include <pamac/aurpackage_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class AURPackage : public GI_PAMAC_AURPACKAGE_BASE
{ typedef GI_PAMAC_AURPACKAGE_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacAURPackage>
{ typedef Pamac::AURPackage type; }; 

} // namespace repository

} // namespace gi

#endif
