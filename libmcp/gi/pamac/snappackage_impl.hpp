// AUTO-GENERATED

#ifndef _GI_PAMAC_SNAPPACKAGE_IMPL_HPP_
#define _GI_PAMAC_SNAPPACKAGE_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// const gchar* pamac_snap_package_get_channel (PamacSnapPackage* self);
// const char* pamac_snap_package_get_channel (::PamacSnapPackage* self);
std::string base::SnapPackageBase::get_channel () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacSnapPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_snap_package_get_channel;
  auto _temp_ret = call_wrap_v ((::PamacSnapPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_snap_package_get_publisher (PamacSnapPackage* self);
// const char* pamac_snap_package_get_publisher (::PamacSnapPackage* self);
std::string base::SnapPackageBase::get_publisher () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacSnapPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_snap_package_get_publisher;
  auto _temp_ret = call_wrap_v ((::PamacSnapPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_snap_package_get_confined (PamacSnapPackage* self);
// const char* pamac_snap_package_get_confined (::PamacSnapPackage* self);
std::string base::SnapPackageBase::get_confined () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacSnapPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_snap_package_get_confined;
  auto _temp_ret = call_wrap_v ((::PamacSnapPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_snap_package_get_channels (PamacSnapPackage* self);
// ::GPtrArray* pamac_snap_package_get_channels (::PamacSnapPackage* self);
GLib::PtrArray base::SnapPackageBase::get_channels () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacSnapPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_snap_package_get_channels;
  auto _temp_ret = call_wrap_v ((::PamacSnapPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}


} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/snappackage_extra_def_impl.hpp>)
#include <pamac/snappackage_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/snappackage_extra_impl.hpp>)
#include <pamac/snappackage_extra_impl.hpp>
#endif
#endif

#endif
