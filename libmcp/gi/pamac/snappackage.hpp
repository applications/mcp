// AUTO-GENERATED

#ifndef _GI_PAMAC_SNAPPACKAGE_HPP_
#define _GI_PAMAC_SNAPPACKAGE_HPP_

#include "package.hpp"

namespace gi {

namespace repository {

namespace Pamac {


class SnapPackage;

namespace base {


#define GI_PAMAC_SNAPPACKAGE_BASE base::SnapPackageBase
class SnapPackageBase : public Pamac::Package
{
typedef Pamac::Package super_type;
public:
typedef ::PamacSnapPackage BaseObjectType;

SnapPackageBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_snap_package_get_type(); } 

gi::property_proxy<std::string, base::SnapPackageBase> property_channel()
{ return gi::property_proxy<std::string, base::SnapPackageBase> (*this, "channel"); }
const gi::property_proxy<std::string, base::SnapPackageBase> property_channel() const
{ return gi::property_proxy<std::string, base::SnapPackageBase> (*this, "channel"); }

// const gchar* pamac_snap_package_get_channel (PamacSnapPackage* self);
// const char* pamac_snap_package_get_channel (::PamacSnapPackage* self);
GI_INLINE_DECL std::string get_channel () noexcept;

gi::property_proxy<std::string, base::SnapPackageBase> property_publisher()
{ return gi::property_proxy<std::string, base::SnapPackageBase> (*this, "publisher"); }
const gi::property_proxy<std::string, base::SnapPackageBase> property_publisher() const
{ return gi::property_proxy<std::string, base::SnapPackageBase> (*this, "publisher"); }

// const gchar* pamac_snap_package_get_publisher (PamacSnapPackage* self);
// const char* pamac_snap_package_get_publisher (::PamacSnapPackage* self);
GI_INLINE_DECL std::string get_publisher () noexcept;

gi::property_proxy<std::string, base::SnapPackageBase> property_confined()
{ return gi::property_proxy<std::string, base::SnapPackageBase> (*this, "confined"); }
const gi::property_proxy<std::string, base::SnapPackageBase> property_confined() const
{ return gi::property_proxy<std::string, base::SnapPackageBase> (*this, "confined"); }

// const gchar* pamac_snap_package_get_confined (PamacSnapPackage* self);
// const char* pamac_snap_package_get_confined (::PamacSnapPackage* self);
GI_INLINE_DECL std::string get_confined () noexcept;

gi::property_proxy<GLib::PtrArray, base::SnapPackageBase> property_channels()
{ return gi::property_proxy<GLib::PtrArray, base::SnapPackageBase> (*this, "channels"); }
const gi::property_proxy<GLib::PtrArray, base::SnapPackageBase> property_channels() const
{ return gi::property_proxy<GLib::PtrArray, base::SnapPackageBase> (*this, "channels"); }

// GPtrArray* pamac_snap_package_get_channels (PamacSnapPackage* self);
// ::GPtrArray* pamac_snap_package_get_channels (::PamacSnapPackage* self);
GI_INLINE_DECL GLib::PtrArray get_channels () noexcept;

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/snappackage_extra_def.hpp>)
#include <pamac/snappackage_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/snappackage_extra.hpp>)
#include <pamac/snappackage_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class SnapPackage : public GI_PAMAC_SNAPPACKAGE_BASE
{ typedef GI_PAMAC_SNAPPACKAGE_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacSnapPackage>
{ typedef Pamac::SnapPackage type; }; 

} // namespace repository

} // namespace gi

#endif
