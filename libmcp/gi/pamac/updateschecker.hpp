// AUTO-GENERATED

#ifndef _GI_PAMAC_UPDATESCHECKER_HPP_
#define _GI_PAMAC_UPDATESCHECKER_HPP_


namespace gi {

namespace repository {

namespace Pamac {


class UpdatesChecker;

namespace base {


#define GI_PAMAC_UPDATESCHECKER_BASE base::UpdatesCheckerBase
class UpdatesCheckerBase : public GObject::Object
{
typedef GObject::Object super_type;
public:
typedef ::PamacUpdatesChecker BaseObjectType;

UpdatesCheckerBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_updates_checker_get_type(); } 

// PamacUpdatesChecker* pamac_updates_checker_new ();
// ::PamacUpdatesChecker* pamac_updates_checker_new ();
static GI_INLINE_DECL Pamac::UpdatesChecker new_ () noexcept;

// void pamac_updates_checker_check_updates (PamacUpdatesChecker* self);
// void pamac_updates_checker_check_updates (::PamacUpdatesChecker* self);
GI_INLINE_DECL void check_updates () noexcept;

gi::property_proxy<guint16, base::UpdatesCheckerBase> property_updates_nb()
{ return gi::property_proxy<guint16, base::UpdatesCheckerBase> (*this, "updates-nb"); }
const gi::property_proxy<guint16, base::UpdatesCheckerBase> property_updates_nb() const
{ return gi::property_proxy<guint16, base::UpdatesCheckerBase> (*this, "updates-nb"); }

// guint16 pamac_updates_checker_get_updates_nb (PamacUpdatesChecker* self);
// guint16 pamac_updates_checker_get_updates_nb (::PamacUpdatesChecker* self);
GI_INLINE_DECL guint16 get_updates_nb () noexcept;

// gchar** pamac_updates_checker_get_updates_list (PamacUpdatesChecker* self, gint* result_length1);
// char** pamac_updates_checker_get_updates_list (::PamacUpdatesChecker* self, gint* result_length1);
GI_INLINE_DECL std::vector<std::string> get_updates_list (gint & result_length1) noexcept;
GI_INLINE_DECL std::tuple<std::vector<std::string>, gint> get_updates_list () noexcept;

gi::property_proxy<guint64, base::UpdatesCheckerBase> property_refresh_period()
{ return gi::property_proxy<guint64, base::UpdatesCheckerBase> (*this, "refresh-period"); }
const gi::property_proxy<guint64, base::UpdatesCheckerBase> property_refresh_period() const
{ return gi::property_proxy<guint64, base::UpdatesCheckerBase> (*this, "refresh-period"); }

// guint64 pamac_updates_checker_get_refresh_period (PamacUpdatesChecker* self);
// guint64 pamac_updates_checker_get_refresh_period (::PamacUpdatesChecker* self);
GI_INLINE_DECL guint64 get_refresh_period () noexcept;

gi::property_proxy<bool, base::UpdatesCheckerBase> property_no_update_hide_icon()
{ return gi::property_proxy<bool, base::UpdatesCheckerBase> (*this, "no-update-hide-icon"); }
const gi::property_proxy<bool, base::UpdatesCheckerBase> property_no_update_hide_icon() const
{ return gi::property_proxy<bool, base::UpdatesCheckerBase> (*this, "no-update-hide-icon"); }

// gboolean pamac_updates_checker_get_no_update_hide_icon (PamacUpdatesChecker* self);
// gboolean pamac_updates_checker_get_no_update_hide_icon (::PamacUpdatesChecker* self);
GI_INLINE_DECL bool get_no_update_hide_icon () noexcept;

// signal updates-available
gi::signal_proxy<void(Pamac::UpdatesChecker, guint16 updates_nb)> signal_updates_available()
{ return gi::signal_proxy<void(Pamac::UpdatesChecker, guint16 updates_nb)> (*this, "updates-available"); }

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/updateschecker_extra_def.hpp>)
#include <pamac/updateschecker_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/updateschecker_extra.hpp>)
#include <pamac/updateschecker_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class UpdatesChecker : public GI_PAMAC_UPDATESCHECKER_BASE
{ typedef GI_PAMAC_UPDATESCHECKER_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacUpdatesChecker>
{ typedef Pamac::UpdatesChecker type; }; 

} // namespace repository

} // namespace gi

#endif
