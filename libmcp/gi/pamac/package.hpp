// AUTO-GENERATED

#ifndef _GI_PAMAC_PACKAGE_HPP_
#define _GI_PAMAC_PACKAGE_HPP_


namespace gi {

namespace repository {

namespace Pamac {


class Package;

namespace base {


#define GI_PAMAC_PACKAGE_BASE base::PackageBase
class PackageBase : public GObject::Object
{
typedef GObject::Object super_type;
public:
typedef ::PamacPackage BaseObjectType;

PackageBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_package_get_type(); } 

gi::property_proxy<std::string, base::PackageBase> property_name()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "name"); }
const gi::property_proxy<std::string, base::PackageBase> property_name() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "name"); }

// const gchar* pamac_package_get_name (PamacPackage* self);
// const char* pamac_package_get_name (::PamacPackage* self);
GI_INLINE_DECL std::string get_name () noexcept;

// void pamac_package_set_name (PamacPackage* self, const gchar* value);
// void pamac_package_set_name (::PamacPackage* self, const char* value);
GI_INLINE_DECL void set_name (const std::string & value) noexcept;

gi::property_proxy<std::string, base::PackageBase> property_id()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "id"); }
const gi::property_proxy<std::string, base::PackageBase> property_id() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "id"); }

// const gchar* pamac_package_get_id (PamacPackage* self);
// const char* pamac_package_get_id (::PamacPackage* self);
GI_INLINE_DECL std::string get_id () noexcept;

// void pamac_package_set_id (PamacPackage* self, const gchar* value);
// void pamac_package_set_id (::PamacPackage* self, const char* value);
GI_INLINE_DECL void set_id (const std::string & value) noexcept;

gi::property_proxy<std::string, base::PackageBase> property_app_name()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "app-name"); }
const gi::property_proxy<std::string, base::PackageBase> property_app_name() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "app-name"); }

// const gchar* pamac_package_get_app_name (PamacPackage* self);
// const char* pamac_package_get_app_name (::PamacPackage* self);
GI_INLINE_DECL std::string get_app_name () noexcept;

gi::property_proxy<std::string, base::PackageBase> property_app_id()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "app-id"); }
const gi::property_proxy<std::string, base::PackageBase> property_app_id() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "app-id"); }

// const gchar* pamac_package_get_app_id (PamacPackage* self);
// const char* pamac_package_get_app_id (::PamacPackage* self);
GI_INLINE_DECL std::string get_app_id () noexcept;

gi::property_proxy<std::string, base::PackageBase> property_version()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "version"); }
const gi::property_proxy<std::string, base::PackageBase> property_version() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "version"); }

// const gchar* pamac_package_get_version (PamacPackage* self);
// const char* pamac_package_get_version (::PamacPackage* self);
GI_INLINE_DECL std::string get_version () noexcept;

// void pamac_package_set_version (PamacPackage* self, const gchar* value);
// void pamac_package_set_version (::PamacPackage* self, const char* value);
GI_INLINE_DECL void set_version (const std::string & value) noexcept;

gi::property_proxy<std::string, base::PackageBase> property_installed_version()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "installed-version"); }
const gi::property_proxy<std::string, base::PackageBase> property_installed_version() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "installed-version"); }

// const gchar* pamac_package_get_installed_version (PamacPackage* self);
// const char* pamac_package_get_installed_version (::PamacPackage* self);
GI_INLINE_DECL std::string get_installed_version () noexcept;

// void pamac_package_set_installed_version (PamacPackage* self, const gchar* value);
// void pamac_package_set_installed_version (::PamacPackage* self, const char* value);
GI_INLINE_DECL void set_installed_version (const std::string & value) noexcept;
GI_INLINE_DECL void set_installed_version () noexcept;

gi::property_proxy<std::string, base::PackageBase> property_desc()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "desc"); }
const gi::property_proxy<std::string, base::PackageBase> property_desc() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "desc"); }

// const gchar* pamac_package_get_desc (PamacPackage* self);
// const char* pamac_package_get_desc (::PamacPackage* self);
GI_INLINE_DECL std::string get_desc () noexcept;

// void pamac_package_set_desc (PamacPackage* self, const gchar* value);
// void pamac_package_set_desc (::PamacPackage* self, const char* value);
GI_INLINE_DECL void set_desc (const std::string & value) noexcept;
GI_INLINE_DECL void set_desc () noexcept;

gi::property_proxy<std::string, base::PackageBase> property_long_desc()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "long-desc"); }
const gi::property_proxy<std::string, base::PackageBase> property_long_desc() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "long-desc"); }

// const gchar* pamac_package_get_long_desc (PamacPackage* self);
// const char* pamac_package_get_long_desc (::PamacPackage* self);
GI_INLINE_DECL std::string get_long_desc () noexcept;

gi::property_proxy<std::string, base::PackageBase> property_repo()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "repo"); }
const gi::property_proxy<std::string, base::PackageBase> property_repo() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "repo"); }

// const gchar* pamac_package_get_repo (PamacPackage* self);
// const char* pamac_package_get_repo (::PamacPackage* self);
GI_INLINE_DECL std::string get_repo () noexcept;

// void pamac_package_set_repo (PamacPackage* self, const gchar* value);
// void pamac_package_set_repo (::PamacPackage* self, const char* value);
GI_INLINE_DECL void set_repo (const std::string & value) noexcept;
GI_INLINE_DECL void set_repo () noexcept;

gi::property_proxy<std::string, base::PackageBase> property_launchable()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "launchable"); }
const gi::property_proxy<std::string, base::PackageBase> property_launchable() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "launchable"); }

// const gchar* pamac_package_get_launchable (PamacPackage* self);
// const char* pamac_package_get_launchable (::PamacPackage* self);
GI_INLINE_DECL std::string get_launchable () noexcept;

gi::property_proxy<std::string, base::PackageBase> property_license()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "license"); }
const gi::property_proxy<std::string, base::PackageBase> property_license() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "license"); }

// const gchar* pamac_package_get_license (PamacPackage* self);
// const char* pamac_package_get_license (::PamacPackage* self);
GI_INLINE_DECL std::string get_license () noexcept;

gi::property_proxy<std::string, base::PackageBase> property_url()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "url"); }
const gi::property_proxy<std::string, base::PackageBase> property_url() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "url"); }

// const gchar* pamac_package_get_url (PamacPackage* self);
// const char* pamac_package_get_url (::PamacPackage* self);
GI_INLINE_DECL std::string get_url () noexcept;

gi::property_proxy<std::string, base::PackageBase> property_icon()
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "icon"); }
const gi::property_proxy<std::string, base::PackageBase> property_icon() const
{ return gi::property_proxy<std::string, base::PackageBase> (*this, "icon"); }

// const gchar* pamac_package_get_icon (PamacPackage* self);
// const char* pamac_package_get_icon (::PamacPackage* self);
GI_INLINE_DECL std::string get_icon () noexcept;

gi::property_proxy<guint64, base::PackageBase> property_installed_size()
{ return gi::property_proxy<guint64, base::PackageBase> (*this, "installed-size"); }
const gi::property_proxy<guint64, base::PackageBase> property_installed_size() const
{ return gi::property_proxy<guint64, base::PackageBase> (*this, "installed-size"); }

// guint64 pamac_package_get_installed_size (PamacPackage* self);
// guint64 pamac_package_get_installed_size (::PamacPackage* self);
GI_INLINE_DECL guint64 get_installed_size () noexcept;

gi::property_proxy<guint64, base::PackageBase> property_download_size()
{ return gi::property_proxy<guint64, base::PackageBase> (*this, "download-size"); }
const gi::property_proxy<guint64, base::PackageBase> property_download_size() const
{ return gi::property_proxy<guint64, base::PackageBase> (*this, "download-size"); }

// guint64 pamac_package_get_download_size (PamacPackage* self);
// guint64 pamac_package_get_download_size (::PamacPackage* self);
GI_INLINE_DECL guint64 get_download_size () noexcept;

gi::property_proxy<GLib::DateTime, base::PackageBase> property_install_date()
{ return gi::property_proxy<GLib::DateTime, base::PackageBase> (*this, "install-date"); }
const gi::property_proxy<GLib::DateTime, base::PackageBase> property_install_date() const
{ return gi::property_proxy<GLib::DateTime, base::PackageBase> (*this, "install-date"); }

// GDateTime* pamac_package_get_install_date (PamacPackage* self);
// ::GDateTime* pamac_package_get_install_date (::PamacPackage* self);
GI_INLINE_DECL GLib::DateTime get_install_date () noexcept;

gi::property_proxy<GLib::PtrArray, base::PackageBase> property_screenshots()
{ return gi::property_proxy<GLib::PtrArray, base::PackageBase> (*this, "screenshots"); }
const gi::property_proxy<GLib::PtrArray, base::PackageBase> property_screenshots() const
{ return gi::property_proxy<GLib::PtrArray, base::PackageBase> (*this, "screenshots"); }

// GPtrArray* pamac_package_get_screenshots (PamacPackage* self);
// ::GPtrArray* pamac_package_get_screenshots (::PamacPackage* self);
GI_INLINE_DECL GLib::PtrArray get_screenshots () noexcept;

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/package_extra_def.hpp>)
#include <pamac/package_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/package_extra.hpp>)
#include <pamac/package_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class Package : public GI_PAMAC_PACKAGE_BASE
{ typedef GI_PAMAC_PACKAGE_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacPackage>
{ typedef Pamac::Package type; }; 

} // namespace repository

} // namespace gi

#endif
