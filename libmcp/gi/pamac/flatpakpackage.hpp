// AUTO-GENERATED

#ifndef _GI_PAMAC_FLATPAKPACKAGE_HPP_
#define _GI_PAMAC_FLATPAKPACKAGE_HPP_

#include "package.hpp"

namespace gi {

namespace repository {

namespace Pamac {


class FlatpakPackage;

namespace base {


#define GI_PAMAC_FLATPAKPACKAGE_BASE base::FlatpakPackageBase
class FlatpakPackageBase : public Pamac::Package
{
typedef Pamac::Package super_type;
public:
typedef ::PamacFlatpakPackage BaseObjectType;

FlatpakPackageBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_flatpak_package_get_type(); } 

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/flatpakpackage_extra_def.hpp>)
#include <pamac/flatpakpackage_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/flatpakpackage_extra.hpp>)
#include <pamac/flatpakpackage_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class FlatpakPackage : public GI_PAMAC_FLATPAKPACKAGE_BASE
{ typedef GI_PAMAC_FLATPAKPACKAGE_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacFlatpakPackage>
{ typedef Pamac::FlatpakPackage type; }; 

} // namespace repository

} // namespace gi

#endif
