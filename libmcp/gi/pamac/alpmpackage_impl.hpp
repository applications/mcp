// AUTO-GENERATED

#ifndef _GI_PAMAC_ALPMPACKAGE_IMPL_HPP_
#define _GI_PAMAC_ALPMPACKAGE_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// GPtrArray* pamac_alpm_package_get_files (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_files (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_files () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_files;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_alpm_package_get_files_async (PamacAlpmPackage* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_alpm_package_get_files_async (::PamacAlpmPackage* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::AlpmPackageBase::get_files_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacAlpmPackage* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_files_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacAlpmPackage*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_alpm_package_get_files_finish (PamacAlpmPackage* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_alpm_package_get_files_finish (::PamacAlpmPackage* self, ::GAsyncResult* _res_);
GLib::PtrArray base::AlpmPackageBase::get_files_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_files_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GDateTime* pamac_alpm_package_get_build_date (PamacAlpmPackage* self);
// ::GDateTime* pamac_alpm_package_get_build_date (::PamacAlpmPackage* self);
GLib::DateTime base::AlpmPackageBase::get_build_date () noexcept
{
  typedef ::GDateTime* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_build_date;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_alpm_package_get_packager (PamacAlpmPackage* self);
// const char* pamac_alpm_package_get_packager (::PamacAlpmPackage* self);
std::string base::AlpmPackageBase::get_packager () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_packager;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// const gchar* pamac_alpm_package_get_reason (PamacAlpmPackage* self);
// const char* pamac_alpm_package_get_reason (::PamacAlpmPackage* self);
std::string base::AlpmPackageBase::get_reason () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_reason;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_alpm_package_get_validations (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_validations (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_validations () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_validations;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_alpm_package_get_groups (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_groups (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_groups () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_groups;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_alpm_package_get_depends (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_depends (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_depends () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_depends;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_alpm_package_set_depends (PamacAlpmPackage* self, GPtrArray* value);
// void pamac_alpm_package_set_depends (::PamacAlpmPackage* self, ::GPtrArray* value);
void base::AlpmPackageBase::set_depends (GLib::PtrArray value) noexcept
{
  typedef void (*call_wrap_t) (::PamacAlpmPackage* self, ::GPtrArray* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_set_depends;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacAlpmPackage*) (gobj_()), (::GPtrArray*) (value_to_c));
}

// GPtrArray* pamac_alpm_package_get_optdepends (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_optdepends (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_optdepends () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_optdepends;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_alpm_package_get_makedepends (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_makedepends (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_makedepends () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_makedepends;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_alpm_package_get_checkdepends (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_checkdepends (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_checkdepends () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_checkdepends;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_alpm_package_get_requiredby (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_requiredby (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_requiredby () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_requiredby;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_alpm_package_get_optionalfor (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_optionalfor (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_optionalfor () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_optionalfor;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_alpm_package_get_provides (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_provides (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_provides () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_provides;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_alpm_package_set_provides (PamacAlpmPackage* self, GPtrArray* value);
// void pamac_alpm_package_set_provides (::PamacAlpmPackage* self, ::GPtrArray* value);
void base::AlpmPackageBase::set_provides (GLib::PtrArray value) noexcept
{
  typedef void (*call_wrap_t) (::PamacAlpmPackage* self, ::GPtrArray* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_set_provides;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacAlpmPackage*) (gobj_()), (::GPtrArray*) (value_to_c));
}

// GPtrArray* pamac_alpm_package_get_replaces (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_replaces (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_replaces () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_replaces;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_alpm_package_set_replaces (PamacAlpmPackage* self, GPtrArray* value);
// void pamac_alpm_package_set_replaces (::PamacAlpmPackage* self, ::GPtrArray* value);
void base::AlpmPackageBase::set_replaces (GLib::PtrArray value) noexcept
{
  typedef void (*call_wrap_t) (::PamacAlpmPackage* self, ::GPtrArray* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_set_replaces;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacAlpmPackage*) (gobj_()), (::GPtrArray*) (value_to_c));
}

// GPtrArray* pamac_alpm_package_get_conflicts (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_conflicts (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_conflicts () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_conflicts;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_alpm_package_set_conflicts (PamacAlpmPackage* self, GPtrArray* value);
// void pamac_alpm_package_set_conflicts (::PamacAlpmPackage* self, ::GPtrArray* value);
void base::AlpmPackageBase::set_conflicts (GLib::PtrArray value) noexcept
{
  typedef void (*call_wrap_t) (::PamacAlpmPackage* self, ::GPtrArray* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_set_conflicts;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacAlpmPackage*) (gobj_()), (::GPtrArray*) (value_to_c));
}

// GPtrArray* pamac_alpm_package_get_backups (PamacAlpmPackage* self);
// ::GPtrArray* pamac_alpm_package_get_backups (::PamacAlpmPackage* self);
GLib::PtrArray base::AlpmPackageBase::get_backups () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacAlpmPackage* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_alpm_package_get_backups;
  auto _temp_ret = call_wrap_v ((::PamacAlpmPackage*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}


} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/alpmpackage_extra_def_impl.hpp>)
#include <pamac/alpmpackage_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/alpmpackage_extra_impl.hpp>)
#include <pamac/alpmpackage_extra_impl.hpp>
#endif
#endif

#endif
