// AUTO-GENERATED

#ifndef _GI_PAMAC_TRANSACTION_IMPL_HPP_
#define _GI_PAMAC_TRANSACTION_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// PamacTransaction* pamac_transaction_new (PamacDatabase* database);
// ::PamacTransaction* pamac_transaction_new (::PamacDatabase* database);
Pamac::Transaction base::TransactionBase::new_ (Pamac::Database database) noexcept
{
  typedef ::PamacTransaction* (*call_wrap_t) (::PamacDatabase* database);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_new;
  auto database_to_c = gi::unwrap (database, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacDatabase*) (database_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_transaction_quit_daemon (PamacTransaction* self);
// void pamac_transaction_quit_daemon (::PamacTransaction* self);
void base::TransactionBase::quit_daemon () noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_quit_daemon;
  call_wrap_v ((::PamacTransaction*) (gobj_()));
}

// void pamac_transaction_ask_commit (PamacTransaction* self, PamacTransactionSummary* summary, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_ask_commit (::PamacTransaction* self, ::PamacTransactionSummary* summary, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::ask_commit (Pamac::TransactionSummary summary, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::PamacTransactionSummary* summary, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_ask_commit;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto summary_to_c = gi::unwrap (summary, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::PamacTransactionSummary*) (summary_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_transaction_ask_commit_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_ask_commit_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
bool base::TransactionBase::ask_commit_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_ask_commit_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_ask_edit_build_files (PamacTransaction* self, PamacTransactionSummary* summary, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_ask_edit_build_files (::PamacTransaction* self, ::PamacTransactionSummary* summary, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::ask_edit_build_files (Pamac::TransactionSummary summary, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::PamacTransactionSummary* summary, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_ask_edit_build_files;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto summary_to_c = gi::unwrap (summary, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::PamacTransactionSummary*) (summary_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_transaction_ask_edit_build_files_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_ask_edit_build_files_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
bool base::TransactionBase::ask_edit_build_files_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_ask_edit_build_files_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_edit_build_files (PamacTransaction* self, GPtrArray* pkgnames, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_edit_build_files (::PamacTransaction* self, ::GPtrArray* pkgnames, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::edit_build_files (GLib::PtrArray pkgnames, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::GPtrArray* pkgnames, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_edit_build_files;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto pkgnames_to_c = gi::unwrap (pkgnames, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::GPtrArray*) (pkgnames_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// void pamac_transaction_edit_build_files_finish (PamacTransaction* self, GAsyncResult* _res_);
// void pamac_transaction_edit_build_files_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
void base::TransactionBase::edit_build_files_finish (Gio::AsyncResult _res_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_edit_build_files_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
}

// void pamac_transaction_ask_import_key (PamacTransaction* self, const gchar* pkgname, const gchar* key, const gchar* owner, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_ask_import_key (::PamacTransaction* self, const char* pkgname, const char* key, const char* owner, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::ask_import_key (const std::string & pkgname, const std::string & key, const std::string & owner, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* pkgname, const char* key, const char* owner, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_ask_import_key;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto owner_to_c = gi::unwrap (owner, gi::transfer_none, gi::direction_in);
  auto key_to_c = gi::unwrap (key, gi::transfer_none, gi::direction_in);
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (pkgname_to_c), (const char*) (key_to_c), (const char*) (owner_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_transaction_ask_import_key_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_ask_import_key_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
bool base::TransactionBase::ask_import_key_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_ask_import_key_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_get_build_files_async (PamacTransaction* self, const gchar* pkgname, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_get_build_files_async (::PamacTransaction* self, const char* pkgname, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::get_build_files_async (const std::string & pkgname, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* pkgname, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_build_files_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (pkgname_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_transaction_get_build_files_finish (PamacTransaction* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_transaction_get_build_files_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GLib::PtrArray base::TransactionBase::get_build_files_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_build_files_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_transaction_choose_optdeps (PamacTransaction* self, const gchar* pkgname, GPtrArray* optdeps, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_choose_optdeps (::PamacTransaction* self, const char* pkgname, ::GPtrArray* optdeps, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::choose_optdeps (const std::string & pkgname, GLib::PtrArray optdeps, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* pkgname, ::GPtrArray* optdeps, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_choose_optdeps;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto optdeps_to_c = gi::unwrap (optdeps, gi::transfer_none, gi::direction_in);
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (pkgname_to_c), (::GPtrArray*) (optdeps_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// GPtrArray* pamac_transaction_choose_optdeps_finish (PamacTransaction* self, GAsyncResult* _res_);
// ::GPtrArray* pamac_transaction_choose_optdeps_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
GLib::PtrArray base::TransactionBase::choose_optdeps_finish (Gio::AsyncResult _res_) noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_choose_optdeps_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_transaction_choose_provider (PamacTransaction* self, const gchar* depend, GPtrArray* providers, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_choose_provider (::PamacTransaction* self, const char* depend, ::GPtrArray* providers, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::choose_provider (const std::string & depend, GLib::PtrArray providers, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* depend, ::GPtrArray* providers, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_choose_provider;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto providers_to_c = gi::unwrap (providers, gi::transfer_none, gi::direction_in);
  auto depend_to_c = gi::unwrap (depend, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (depend_to_c), (::GPtrArray*) (providers_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gint pamac_transaction_choose_provider_finish (PamacTransaction* self, GAsyncResult* _res_);
// gint pamac_transaction_choose_provider_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
gint base::TransactionBase::choose_provider_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gint (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_choose_provider_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_ask_snap_install_classic (PamacTransaction* self, const gchar* name, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_ask_snap_install_classic (::PamacTransaction* self, const char* name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::ask_snap_install_classic (const std::string & name, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* name, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_ask_snap_install_classic;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (name_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_transaction_ask_snap_install_classic_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_ask_snap_install_classic_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
bool base::TransactionBase::ask_snap_install_classic_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_ask_snap_install_classic_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_get_authorization_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_get_authorization_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::get_authorization_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_authorization_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_transaction_get_authorization_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_get_authorization_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
bool base::TransactionBase::get_authorization_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_authorization_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_remove_authorization (PamacTransaction* self);
// void pamac_transaction_remove_authorization (::PamacTransaction* self);
void base::TransactionBase::remove_authorization () noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_remove_authorization;
  call_wrap_v ((::PamacTransaction*) (gobj_()));
}

// void pamac_transaction_generate_mirrors_list_async (PamacTransaction* self, const gchar* country, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_generate_mirrors_list_async (::PamacTransaction* self, const char* country, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::generate_mirrors_list_async (const std::string & country, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* country, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_generate_mirrors_list_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto country_to_c = gi::unwrap (country, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (country_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// void pamac_transaction_generate_mirrors_list_finish (PamacTransaction* self, GAsyncResult* _res_);
// void pamac_transaction_generate_mirrors_list_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
void base::TransactionBase::generate_mirrors_list_finish (Gio::AsyncResult _res_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_generate_mirrors_list_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
}

// void pamac_transaction_clean_cache_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_clean_cache_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::clean_cache_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_clean_cache_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// void pamac_transaction_clean_cache_finish (PamacTransaction* self, GAsyncResult* _res_);
// void pamac_transaction_clean_cache_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
void base::TransactionBase::clean_cache_finish (Gio::AsyncResult _res_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_clean_cache_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
}

// void pamac_transaction_clean_build_files_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_clean_build_files_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::clean_build_files_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_clean_build_files_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// void pamac_transaction_clean_build_files_finish (PamacTransaction* self, GAsyncResult* _res_);
// void pamac_transaction_clean_build_files_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
void base::TransactionBase::clean_build_files_finish (Gio::AsyncResult _res_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_clean_build_files_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
}

// void pamac_transaction_set_pkgreason_async (PamacTransaction* self, const gchar* pkgname, guint reason, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_set_pkgreason_async (::PamacTransaction* self, const char* pkgname, guint reason, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::set_pkgreason_async (const std::string & pkgname, guint reason, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* pkgname, guint reason, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_pkgreason_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto reason_to_c = reason;
  auto pkgname_to_c = gi::unwrap (pkgname, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (pkgname_to_c), (guint) (reason_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_transaction_set_pkgreason_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_set_pkgreason_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
bool base::TransactionBase::set_pkgreason_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_pkgreason_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_download_updates_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_download_updates_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::download_updates_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_download_updates_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_transaction_download_updates_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_download_updates_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
bool base::TransactionBase::download_updates_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_download_updates_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_run_async (PamacTransaction* self, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_run_async (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::run_async (Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_run_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_transaction_run_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_run_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
bool base::TransactionBase::run_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_run_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_add_pkg_to_install (PamacTransaction* self, const gchar* name);
// void pamac_transaction_add_pkg_to_install (::PamacTransaction* self, const char* name);
void base::TransactionBase::add_pkg_to_install (const std::string & name) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* name);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_pkg_to_install;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (name_to_c));
}

// void pamac_transaction_add_pkg_to_remove (PamacTransaction* self, const gchar* name);
// void pamac_transaction_add_pkg_to_remove (::PamacTransaction* self, const char* name);
void base::TransactionBase::add_pkg_to_remove (const std::string & name) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* name);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_pkg_to_remove;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (name_to_c));
}

// void pamac_transaction_add_path_to_load (PamacTransaction* self, const gchar* path);
// void pamac_transaction_add_path_to_load (::PamacTransaction* self, const char* path);
void base::TransactionBase::add_path_to_load (const std::string & path) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* path);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_path_to_load;
  auto path_to_c = gi::unwrap (path, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (path_to_c));
}

// void pamac_transaction_add_pkg_to_build (PamacTransaction* self, const gchar* name, gboolean clone_build_files, gboolean clone_deps_build_files);
// void pamac_transaction_add_pkg_to_build (::PamacTransaction* self, const char* name, gboolean clone_build_files, gboolean clone_deps_build_files);
void base::TransactionBase::add_pkg_to_build (const std::string & name, gboolean clone_build_files, gboolean clone_deps_build_files) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* name, gboolean clone_build_files, gboolean clone_deps_build_files);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_pkg_to_build;
  auto clone_deps_build_files_to_c = clone_deps_build_files;
  auto clone_build_files_to_c = clone_build_files;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (name_to_c), (gboolean) (clone_build_files_to_c), (gboolean) (clone_deps_build_files_to_c));
}

// void pamac_transaction_add_temporary_ignore_pkg (PamacTransaction* self, const gchar* name);
// void pamac_transaction_add_temporary_ignore_pkg (::PamacTransaction* self, const char* name);
void base::TransactionBase::add_temporary_ignore_pkg (const std::string & name) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* name);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_temporary_ignore_pkg;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (name_to_c));
}

// void pamac_transaction_add_overwrite_file (PamacTransaction* self, const gchar* glob);
// void pamac_transaction_add_overwrite_file (::PamacTransaction* self, const char* glob);
void base::TransactionBase::add_overwrite_file (const std::string & glob) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* glob);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_overwrite_file;
  auto glob_to_c = gi::unwrap (glob, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (glob_to_c));
}

// void pamac_transaction_add_pkg_to_mark_as_dep (PamacTransaction* self, const gchar* name);
// void pamac_transaction_add_pkg_to_mark_as_dep (::PamacTransaction* self, const char* name);
void base::TransactionBase::add_pkg_to_mark_as_dep (const std::string & name) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* name);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_pkg_to_mark_as_dep;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (name_to_c));
}

// void pamac_transaction_add_pkgs_to_upgrade (PamacTransaction* self, gboolean force_refresh);
// void pamac_transaction_add_pkgs_to_upgrade (::PamacTransaction* self, gboolean force_refresh);
void base::TransactionBase::add_pkgs_to_upgrade (gboolean force_refresh) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean force_refresh);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_pkgs_to_upgrade;
  auto force_refresh_to_c = force_refresh;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (force_refresh_to_c));
}

// void pamac_transaction_add_snap_to_install (PamacTransaction* self, PamacSnapPackage* pkg);
// void pamac_transaction_add_snap_to_install (::PamacTransaction* self, ::PamacSnapPackage* pkg);
void base::TransactionBase::add_snap_to_install (Pamac::SnapPackage pkg) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::PamacSnapPackage* pkg);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_snap_to_install;
  auto pkg_to_c = gi::unwrap (pkg, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::PamacSnapPackage*) (pkg_to_c));
}

// void pamac_transaction_add_snap_to_remove (PamacTransaction* self, PamacSnapPackage* pkg);
// void pamac_transaction_add_snap_to_remove (::PamacTransaction* self, ::PamacSnapPackage* pkg);
void base::TransactionBase::add_snap_to_remove (Pamac::SnapPackage pkg) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::PamacSnapPackage* pkg);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_snap_to_remove;
  auto pkg_to_c = gi::unwrap (pkg, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::PamacSnapPackage*) (pkg_to_c));
}

// void pamac_transaction_snap_switch_channel_async (PamacTransaction* self, const gchar* snap_name, const gchar* channel, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_snap_switch_channel_async (::PamacTransaction* self, const char* snap_name, const char* channel, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::snap_switch_channel_async (const std::string & snap_name, const std::string & channel, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, const char* snap_name, const char* channel, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_snap_switch_channel_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto channel_to_c = gi::unwrap (channel, gi::transfer_none, gi::direction_in);
  auto snap_name_to_c = gi::unwrap (snap_name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (const char*) (snap_name_to_c), (const char*) (channel_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gboolean pamac_transaction_snap_switch_channel_finish (PamacTransaction* self, GAsyncResult* _res_);
// gboolean pamac_transaction_snap_switch_channel_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
bool base::TransactionBase::snap_switch_channel_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_snap_switch_channel_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_add_flatpak_to_install (PamacTransaction* self, PamacFlatpakPackage* pkg);
// void pamac_transaction_add_flatpak_to_install (::PamacTransaction* self, ::PamacFlatpakPackage* pkg);
void base::TransactionBase::add_flatpak_to_install (Pamac::FlatpakPackage pkg) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::PamacFlatpakPackage* pkg);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_flatpak_to_install;
  auto pkg_to_c = gi::unwrap (pkg, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::PamacFlatpakPackage*) (pkg_to_c));
}

// void pamac_transaction_add_flatpak_to_remove (PamacTransaction* self, PamacFlatpakPackage* pkg);
// void pamac_transaction_add_flatpak_to_remove (::PamacTransaction* self, ::PamacFlatpakPackage* pkg);
void base::TransactionBase::add_flatpak_to_remove (Pamac::FlatpakPackage pkg) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::PamacFlatpakPackage* pkg);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_flatpak_to_remove;
  auto pkg_to_c = gi::unwrap (pkg, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::PamacFlatpakPackage*) (pkg_to_c));
}

// void pamac_transaction_add_flatpak_to_upgrade (PamacTransaction* self, PamacFlatpakPackage* pkg);
// void pamac_transaction_add_flatpak_to_upgrade (::PamacTransaction* self, ::PamacFlatpakPackage* pkg);
void base::TransactionBase::add_flatpak_to_upgrade (Pamac::FlatpakPackage pkg) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::PamacFlatpakPackage* pkg);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_add_flatpak_to_upgrade;
  auto pkg_to_c = gi::unwrap (pkg, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::PamacFlatpakPackage*) (pkg_to_c));
}

// void pamac_transaction_run_cmd_line_async (PamacTransaction* self, GPtrArray* args, const gchar* working_directory, GCancellable* cancellable, GAsyncReadyCallback _callback_, void* _callback__target);
// void pamac_transaction_run_cmd_line_async (::PamacTransaction* self, ::GPtrArray* args, const char* working_directory, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
void base::TransactionBase::run_cmd_line_async (GLib::PtrArray args, const std::string & working_directory, Gio::Cancellable cancellable, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::GPtrArray* args, const char* working_directory, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_run_cmd_line_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto working_directory_to_c = gi::unwrap (working_directory, gi::transfer_none, gi::direction_in);
  auto args_to_c = gi::unwrap (args, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::GPtrArray*) (args_to_c), (const char*) (working_directory_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}
void base::TransactionBase::run_cmd_line_async (GLib::PtrArray args, Gio::Cancellable cancellable, Gio::AsyncReadyCallback _callback_) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::GPtrArray* args, const char* working_directory, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type _callback_, void* _callback__target);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_run_cmd_line_async;
  auto _callback__wrap_ = _callback_ ? unwrap (std::move (_callback_), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto working_directory_to_c = nullptr;
  auto args_to_c = gi::unwrap (args, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::GPtrArray*) (args_to_c), (const char*) (working_directory_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (_callback__wrap_ ? &_callback__wrap_->wrapper : nullptr), (void*) (_callback__wrap_));
}

// gint pamac_transaction_run_cmd_line_finish (PamacTransaction* self, GAsyncResult* _res_);
// gint pamac_transaction_run_cmd_line_finish (::PamacTransaction* self, ::GAsyncResult* _res_);
gint base::TransactionBase::run_cmd_line_finish (Gio::AsyncResult _res_) noexcept
{
  typedef gint (*call_wrap_t) (::PamacTransaction* self, ::GAsyncResult* _res_);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_run_cmd_line_finish;
  auto _res__to_c = gi::unwrap (_res_, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()), (::GAsyncResult*) (_res__to_c));
  return _temp_ret;
}

// void pamac_transaction_cancel (PamacTransaction* self);
// void pamac_transaction_cancel (::PamacTransaction* self);
void base::TransactionBase::cancel () noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_cancel;
  call_wrap_v ((::PamacTransaction*) (gobj_()));
}

// PamacDatabase* pamac_transaction_get_database (PamacTransaction* self);
// ::PamacDatabase* pamac_transaction_get_database (::PamacTransaction* self);
Pamac::Database base::TransactionBase::get_database () noexcept
{
  typedef ::PamacDatabase* (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_database;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_transaction_set_database (PamacTransaction* self, PamacDatabase* value);
// void pamac_transaction_set_database (::PamacTransaction* self, ::PamacDatabase* value);
void base::TransactionBase::set_database (Pamac::Database value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, ::PamacDatabase* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_database;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacTransaction*) (gobj_()), (::PamacDatabase*) (value_to_c));
}

// gboolean pamac_transaction_get_download_only (PamacTransaction* self);
// gboolean pamac_transaction_get_download_only (::PamacTransaction* self);
bool base::TransactionBase::get_download_only () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_download_only;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return _temp_ret;
}

// void pamac_transaction_set_download_only (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_download_only (::PamacTransaction* self, gboolean value);
void base::TransactionBase::set_download_only (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_download_only;
  auto value_to_c = value;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_transaction_get_dry_run (PamacTransaction* self);
// gboolean pamac_transaction_get_dry_run (::PamacTransaction* self);
bool base::TransactionBase::get_dry_run () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_dry_run;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return _temp_ret;
}

// void pamac_transaction_set_dry_run (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_dry_run (::PamacTransaction* self, gboolean value);
void base::TransactionBase::set_dry_run (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_dry_run;
  auto value_to_c = value;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_transaction_get_install_if_needed (PamacTransaction* self);
// gboolean pamac_transaction_get_install_if_needed (::PamacTransaction* self);
bool base::TransactionBase::get_install_if_needed () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_install_if_needed;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return _temp_ret;
}

// void pamac_transaction_set_install_if_needed (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_install_if_needed (::PamacTransaction* self, gboolean value);
void base::TransactionBase::set_install_if_needed (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_install_if_needed;
  auto value_to_c = value;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_transaction_get_remove_if_unneeded (PamacTransaction* self);
// gboolean pamac_transaction_get_remove_if_unneeded (::PamacTransaction* self);
bool base::TransactionBase::get_remove_if_unneeded () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_remove_if_unneeded;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return _temp_ret;
}

// void pamac_transaction_set_remove_if_unneeded (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_remove_if_unneeded (::PamacTransaction* self, gboolean value);
void base::TransactionBase::set_remove_if_unneeded (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_remove_if_unneeded;
  auto value_to_c = value;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_transaction_get_cascade (PamacTransaction* self);
// gboolean pamac_transaction_get_cascade (::PamacTransaction* self);
bool base::TransactionBase::get_cascade () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_cascade;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return _temp_ret;
}

// void pamac_transaction_set_cascade (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_cascade (::PamacTransaction* self, gboolean value);
void base::TransactionBase::set_cascade (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_cascade;
  auto value_to_c = value;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_transaction_get_keep_config_files (PamacTransaction* self);
// gboolean pamac_transaction_get_keep_config_files (::PamacTransaction* self);
bool base::TransactionBase::get_keep_config_files () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_keep_config_files;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return _temp_ret;
}

// void pamac_transaction_set_keep_config_files (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_keep_config_files (::PamacTransaction* self, gboolean value);
void base::TransactionBase::set_keep_config_files (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_keep_config_files;
  auto value_to_c = value;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_transaction_get_install_as_dep (PamacTransaction* self);
// gboolean pamac_transaction_get_install_as_dep (::PamacTransaction* self);
bool base::TransactionBase::get_install_as_dep () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_install_as_dep;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return _temp_ret;
}

// void pamac_transaction_set_install_as_dep (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_install_as_dep (::PamacTransaction* self, gboolean value);
void base::TransactionBase::set_install_as_dep (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_install_as_dep;
  auto value_to_c = value;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_transaction_get_install_as_explicit (PamacTransaction* self);
// gboolean pamac_transaction_get_install_as_explicit (::PamacTransaction* self);
bool base::TransactionBase::get_install_as_explicit () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_install_as_explicit;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return _temp_ret;
}

// void pamac_transaction_set_install_as_explicit (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_install_as_explicit (::PamacTransaction* self, gboolean value);
void base::TransactionBase::set_install_as_explicit (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_install_as_explicit;
  auto value_to_c = value;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_transaction_get_no_refresh (PamacTransaction* self);
// gboolean pamac_transaction_get_no_refresh (::PamacTransaction* self);
bool base::TransactionBase::get_no_refresh () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacTransaction* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_get_no_refresh;
  auto _temp_ret = call_wrap_v ((::PamacTransaction*) (gobj_()));
  return _temp_ret;
}

// void pamac_transaction_set_no_refresh (PamacTransaction* self, gboolean value);
// void pamac_transaction_set_no_refresh (::PamacTransaction* self, gboolean value);
void base::TransactionBase::set_no_refresh (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacTransaction* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_transaction_set_no_refresh;
  auto value_to_c = value;
  call_wrap_v ((::PamacTransaction*) (gobj_()), (gboolean) (value_to_c));
}


















} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/transaction_extra_def_impl.hpp>)
#include <pamac/transaction_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/transaction_extra_impl.hpp>)
#include <pamac/transaction_extra_impl.hpp>
#endif
#endif

#endif
