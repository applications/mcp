// AUTO-GENERATED

#ifndef _GI_PAMAC_CONFIG_HPP_
#define _GI_PAMAC_CONFIG_HPP_


namespace gi {

namespace repository {

namespace Pamac {


class Config;

namespace base {


#define GI_PAMAC_CONFIG_BASE base::ConfigBase
class ConfigBase : public GObject::Object
{
typedef GObject::Object super_type;
public:
typedef ::PamacConfig BaseObjectType;

ConfigBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_config_get_type(); } 

// PamacConfig* pamac_config_new (const gchar* conf_path);
// ::PamacConfig* pamac_config_new (const char* conf_path);
static GI_INLINE_DECL Pamac::Config new_ (const std::string & conf_path) noexcept;

// void pamac_config_add_ignorepkg (PamacConfig* self, const gchar* name);
// void pamac_config_add_ignorepkg (::PamacConfig* self, const char* name);
GI_INLINE_DECL void add_ignorepkg (const std::string & name) noexcept;

// void pamac_config_remove_ignorepkg (PamacConfig* self, const gchar* name);
// void pamac_config_remove_ignorepkg (::PamacConfig* self, const char* name);
GI_INLINE_DECL void remove_ignorepkg (const std::string & name) noexcept;

// void pamac_config_reload (PamacConfig* self);
// void pamac_config_reload (::PamacConfig* self);
GI_INLINE_DECL void reload () noexcept;

// void pamac_config_save (PamacConfig* self);
// void pamac_config_save (::PamacConfig* self);
GI_INLINE_DECL void save () noexcept;

gi::property_proxy<std::string, base::ConfigBase> property_conf_path()
{ return gi::property_proxy<std::string, base::ConfigBase> (*this, "conf-path"); }
const gi::property_proxy<std::string, base::ConfigBase> property_conf_path() const
{ return gi::property_proxy<std::string, base::ConfigBase> (*this, "conf-path"); }

// const gchar* pamac_config_get_conf_path (PamacConfig* self);
// const char* pamac_config_get_conf_path (::PamacConfig* self);
GI_INLINE_DECL std::string get_conf_path () noexcept;

gi::property_proxy<bool, base::ConfigBase> property_recurse()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "recurse"); }
const gi::property_proxy<bool, base::ConfigBase> property_recurse() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "recurse"); }

// gboolean pamac_config_get_recurse (PamacConfig* self);
// gboolean pamac_config_get_recurse (::PamacConfig* self);
GI_INLINE_DECL bool get_recurse () noexcept;

// void pamac_config_set_recurse (PamacConfig* self, gboolean value);
// void pamac_config_set_recurse (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_recurse (gboolean value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_keep_built_pkgs()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "keep-built-pkgs"); }
const gi::property_proxy<bool, base::ConfigBase> property_keep_built_pkgs() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "keep-built-pkgs"); }

// gboolean pamac_config_get_keep_built_pkgs (PamacConfig* self);
// gboolean pamac_config_get_keep_built_pkgs (::PamacConfig* self);
GI_INLINE_DECL bool get_keep_built_pkgs () noexcept;

// void pamac_config_set_keep_built_pkgs (PamacConfig* self, gboolean value);
// void pamac_config_set_keep_built_pkgs (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_keep_built_pkgs (gboolean value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_enable_downgrade()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "enable-downgrade"); }
const gi::property_proxy<bool, base::ConfigBase> property_enable_downgrade() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "enable-downgrade"); }

// gboolean pamac_config_get_enable_downgrade (PamacConfig* self);
// gboolean pamac_config_get_enable_downgrade (::PamacConfig* self);
GI_INLINE_DECL bool get_enable_downgrade () noexcept;

// void pamac_config_set_enable_downgrade (PamacConfig* self, gboolean value);
// void pamac_config_set_enable_downgrade (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_enable_downgrade (gboolean value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_simple_install()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "simple-install"); }
const gi::property_proxy<bool, base::ConfigBase> property_simple_install() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "simple-install"); }

// gboolean pamac_config_get_simple_install (PamacConfig* self);
// gboolean pamac_config_get_simple_install (::PamacConfig* self);
GI_INLINE_DECL bool get_simple_install () noexcept;

// void pamac_config_set_simple_install (PamacConfig* self, gboolean value);
// void pamac_config_set_simple_install (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_simple_install (gboolean value) noexcept;

gi::property_proxy<guint64, base::ConfigBase> property_refresh_period()
{ return gi::property_proxy<guint64, base::ConfigBase> (*this, "refresh-period"); }
const gi::property_proxy<guint64, base::ConfigBase> property_refresh_period() const
{ return gi::property_proxy<guint64, base::ConfigBase> (*this, "refresh-period"); }

// guint64 pamac_config_get_refresh_period (PamacConfig* self);
// guint64 pamac_config_get_refresh_period (::PamacConfig* self);
GI_INLINE_DECL guint64 get_refresh_period () noexcept;

// void pamac_config_set_refresh_period (PamacConfig* self, guint64 value);
// void pamac_config_set_refresh_period (::PamacConfig* self, guint64 value);
GI_INLINE_DECL void set_refresh_period (guint64 value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_no_update_hide_icon()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "no-update-hide-icon"); }
const gi::property_proxy<bool, base::ConfigBase> property_no_update_hide_icon() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "no-update-hide-icon"); }

// gboolean pamac_config_get_no_update_hide_icon (PamacConfig* self);
// gboolean pamac_config_get_no_update_hide_icon (::PamacConfig* self);
GI_INLINE_DECL bool get_no_update_hide_icon () noexcept;

// void pamac_config_set_no_update_hide_icon (PamacConfig* self, gboolean value);
// void pamac_config_set_no_update_hide_icon (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_no_update_hide_icon (gboolean value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_enable_aur()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "enable-aur"); }
const gi::property_proxy<bool, base::ConfigBase> property_enable_aur() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "enable-aur"); }

// gboolean pamac_config_get_enable_aur (PamacConfig* self);
// gboolean pamac_config_get_enable_aur (::PamacConfig* self);
GI_INLINE_DECL bool get_enable_aur () noexcept;

// void pamac_config_set_enable_aur (PamacConfig* self, gboolean value);
// void pamac_config_set_enable_aur (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_enable_aur (gboolean value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_support_snap()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "support-snap"); }
const gi::property_proxy<bool, base::ConfigBase> property_support_snap() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "support-snap"); }

// gboolean pamac_config_get_support_snap (PamacConfig* self);
// gboolean pamac_config_get_support_snap (::PamacConfig* self);
GI_INLINE_DECL bool get_support_snap () noexcept;

gi::property_proxy<bool, base::ConfigBase> property_enable_snap()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "enable-snap"); }
const gi::property_proxy<bool, base::ConfigBase> property_enable_snap() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "enable-snap"); }

// gboolean pamac_config_get_enable_snap (PamacConfig* self);
// gboolean pamac_config_get_enable_snap (::PamacConfig* self);
GI_INLINE_DECL bool get_enable_snap () noexcept;

// void pamac_config_set_enable_snap (PamacConfig* self, gboolean value);
// void pamac_config_set_enable_snap (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_enable_snap (gboolean value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_support_flatpak()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "support-flatpak"); }
const gi::property_proxy<bool, base::ConfigBase> property_support_flatpak() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "support-flatpak"); }

// gboolean pamac_config_get_support_flatpak (PamacConfig* self);
// gboolean pamac_config_get_support_flatpak (::PamacConfig* self);
GI_INLINE_DECL bool get_support_flatpak () noexcept;

gi::property_proxy<bool, base::ConfigBase> property_enable_flatpak()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "enable-flatpak"); }
const gi::property_proxy<bool, base::ConfigBase> property_enable_flatpak() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "enable-flatpak"); }

// gboolean pamac_config_get_enable_flatpak (PamacConfig* self);
// gboolean pamac_config_get_enable_flatpak (::PamacConfig* self);
GI_INLINE_DECL bool get_enable_flatpak () noexcept;

// void pamac_config_set_enable_flatpak (PamacConfig* self, gboolean value);
// void pamac_config_set_enable_flatpak (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_enable_flatpak (gboolean value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_check_flatpak_updates()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "check-flatpak-updates"); }
const gi::property_proxy<bool, base::ConfigBase> property_check_flatpak_updates() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "check-flatpak-updates"); }

// gboolean pamac_config_get_check_flatpak_updates (PamacConfig* self);
// gboolean pamac_config_get_check_flatpak_updates (::PamacConfig* self);
GI_INLINE_DECL bool get_check_flatpak_updates () noexcept;

// void pamac_config_set_check_flatpak_updates (PamacConfig* self, gboolean value);
// void pamac_config_set_check_flatpak_updates (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_check_flatpak_updates (gboolean value) noexcept;

gi::property_proxy<std::string, base::ConfigBase> property_aur_build_dir()
{ return gi::property_proxy<std::string, base::ConfigBase> (*this, "aur-build-dir"); }
const gi::property_proxy<std::string, base::ConfigBase> property_aur_build_dir() const
{ return gi::property_proxy<std::string, base::ConfigBase> (*this, "aur-build-dir"); }

// const gchar* pamac_config_get_aur_build_dir (PamacConfig* self);
// const char* pamac_config_get_aur_build_dir (::PamacConfig* self);
GI_INLINE_DECL std::string get_aur_build_dir () noexcept;

// void pamac_config_set_aur_build_dir (PamacConfig* self, const gchar* value);
// void pamac_config_set_aur_build_dir (::PamacConfig* self, const char* value);
GI_INLINE_DECL void set_aur_build_dir (const std::string & value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_check_aur_updates()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "check-aur-updates"); }
const gi::property_proxy<bool, base::ConfigBase> property_check_aur_updates() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "check-aur-updates"); }

// gboolean pamac_config_get_check_aur_updates (PamacConfig* self);
// gboolean pamac_config_get_check_aur_updates (::PamacConfig* self);
GI_INLINE_DECL bool get_check_aur_updates () noexcept;

// void pamac_config_set_check_aur_updates (PamacConfig* self, gboolean value);
// void pamac_config_set_check_aur_updates (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_check_aur_updates (gboolean value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_check_aur_vcs_updates()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "check-aur-vcs-updates"); }
const gi::property_proxy<bool, base::ConfigBase> property_check_aur_vcs_updates() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "check-aur-vcs-updates"); }

// gboolean pamac_config_get_check_aur_vcs_updates (PamacConfig* self);
// gboolean pamac_config_get_check_aur_vcs_updates (::PamacConfig* self);
GI_INLINE_DECL bool get_check_aur_vcs_updates () noexcept;

// void pamac_config_set_check_aur_vcs_updates (PamacConfig* self, gboolean value);
// void pamac_config_set_check_aur_vcs_updates (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_check_aur_vcs_updates (gboolean value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_download_updates()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "download-updates"); }
const gi::property_proxy<bool, base::ConfigBase> property_download_updates() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "download-updates"); }

// gboolean pamac_config_get_download_updates (PamacConfig* self);
// gboolean pamac_config_get_download_updates (::PamacConfig* self);
GI_INLINE_DECL bool get_download_updates () noexcept;

// void pamac_config_set_download_updates (PamacConfig* self, gboolean value);
// void pamac_config_set_download_updates (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_download_updates (gboolean value) noexcept;

gi::property_proxy<guint64, base::ConfigBase> property_max_parallel_downloads()
{ return gi::property_proxy<guint64, base::ConfigBase> (*this, "max-parallel-downloads"); }
const gi::property_proxy<guint64, base::ConfigBase> property_max_parallel_downloads() const
{ return gi::property_proxy<guint64, base::ConfigBase> (*this, "max-parallel-downloads"); }

// guint64 pamac_config_get_max_parallel_downloads (PamacConfig* self);
// guint64 pamac_config_get_max_parallel_downloads (::PamacConfig* self);
GI_INLINE_DECL guint64 get_max_parallel_downloads () noexcept;

// void pamac_config_set_max_parallel_downloads (PamacConfig* self, guint64 value);
// void pamac_config_set_max_parallel_downloads (::PamacConfig* self, guint64 value);
GI_INLINE_DECL void set_max_parallel_downloads (guint64 value) noexcept;

gi::property_proxy<guint64, base::ConfigBase> property_clean_keep_num_pkgs()
{ return gi::property_proxy<guint64, base::ConfigBase> (*this, "clean-keep-num-pkgs"); }
const gi::property_proxy<guint64, base::ConfigBase> property_clean_keep_num_pkgs() const
{ return gi::property_proxy<guint64, base::ConfigBase> (*this, "clean-keep-num-pkgs"); }

// guint64 pamac_config_get_clean_keep_num_pkgs (PamacConfig* self);
// guint64 pamac_config_get_clean_keep_num_pkgs (::PamacConfig* self);
GI_INLINE_DECL guint64 get_clean_keep_num_pkgs () noexcept;

// void pamac_config_set_clean_keep_num_pkgs (PamacConfig* self, guint64 value);
// void pamac_config_set_clean_keep_num_pkgs (::PamacConfig* self, guint64 value);
GI_INLINE_DECL void set_clean_keep_num_pkgs (guint64 value) noexcept;

gi::property_proxy<bool, base::ConfigBase> property_clean_rm_only_uninstalled()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "clean-rm-only-uninstalled"); }
const gi::property_proxy<bool, base::ConfigBase> property_clean_rm_only_uninstalled() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "clean-rm-only-uninstalled"); }

// gboolean pamac_config_get_clean_rm_only_uninstalled (PamacConfig* self);
// gboolean pamac_config_get_clean_rm_only_uninstalled (::PamacConfig* self);
GI_INLINE_DECL bool get_clean_rm_only_uninstalled () noexcept;

// void pamac_config_set_clean_rm_only_uninstalled (PamacConfig* self, gboolean value);
// void pamac_config_set_clean_rm_only_uninstalled (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_clean_rm_only_uninstalled (gboolean value) noexcept;

// GHashTable* pamac_config_get_environment_variables (PamacConfig* self);
// ::GHashTable* pamac_config_get_environment_variables (::PamacConfig* self);
GI_INLINE_DECL std::map<std::string, std::string> get_environment_variables () noexcept;

gi::property_proxy<std::string, base::ConfigBase> property_db_path()
{ return gi::property_proxy<std::string, base::ConfigBase> (*this, "db-path"); }
const gi::property_proxy<std::string, base::ConfigBase> property_db_path() const
{ return gi::property_proxy<std::string, base::ConfigBase> (*this, "db-path"); }

// const gchar* pamac_config_get_db_path (PamacConfig* self);
// const char* pamac_config_get_db_path (::PamacConfig* self);
GI_INLINE_DECL std::string get_db_path () noexcept;

gi::property_proxy<bool, base::ConfigBase> property_checkspace()
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "checkspace"); }
const gi::property_proxy<bool, base::ConfigBase> property_checkspace() const
{ return gi::property_proxy<bool, base::ConfigBase> (*this, "checkspace"); }

// gboolean pamac_config_get_checkspace (PamacConfig* self);
// gboolean pamac_config_get_checkspace (::PamacConfig* self);
GI_INLINE_DECL bool get_checkspace () noexcept;

// void pamac_config_set_checkspace (PamacConfig* self, gboolean value);
// void pamac_config_set_checkspace (::PamacConfig* self, gboolean value);
GI_INLINE_DECL void set_checkspace (gboolean value) noexcept;

//  pamac_config_get_ignorepkgs (PamacConfig* self);
//  pamac_config_get_ignorepkgs (::PamacConfig* self);
// SKIP; inconsistent type info

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/config_extra_def.hpp>)
#include <pamac/config_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/config_extra.hpp>)
#include <pamac/config_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class Config : public GI_PAMAC_CONFIG_BASE
{ typedef GI_PAMAC_CONFIG_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacConfig>
{ typedef Pamac::Config type; }; 

} // namespace repository

} // namespace gi

#endif
