// AUTO-GENERATED

#ifndef _GI_PAMAC_CONFIG_IMPL_HPP_
#define _GI_PAMAC_CONFIG_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// PamacConfig* pamac_config_new (const gchar* conf_path);
// ::PamacConfig* pamac_config_new (const char* conf_path);
Pamac::Config base::ConfigBase::new_ (const std::string & conf_path) noexcept
{
  typedef ::PamacConfig* (*call_wrap_t) (const char* conf_path);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_new;
  auto conf_path_to_c = gi::unwrap (conf_path, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((const char*) (conf_path_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void pamac_config_add_ignorepkg (PamacConfig* self, const gchar* name);
// void pamac_config_add_ignorepkg (::PamacConfig* self, const char* name);
void base::ConfigBase::add_ignorepkg (const std::string & name) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, const char* name);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_add_ignorepkg;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacConfig*) (gobj_()), (const char*) (name_to_c));
}

// void pamac_config_remove_ignorepkg (PamacConfig* self, const gchar* name);
// void pamac_config_remove_ignorepkg (::PamacConfig* self, const char* name);
void base::ConfigBase::remove_ignorepkg (const std::string & name) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, const char* name);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_remove_ignorepkg;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacConfig*) (gobj_()), (const char*) (name_to_c));
}

// void pamac_config_reload (PamacConfig* self);
// void pamac_config_reload (::PamacConfig* self);
void base::ConfigBase::reload () noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_reload;
  call_wrap_v ((::PamacConfig*) (gobj_()));
}

// void pamac_config_save (PamacConfig* self);
// void pamac_config_save (::PamacConfig* self);
void base::ConfigBase::save () noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_save;
  call_wrap_v ((::PamacConfig*) (gobj_()));
}

// const gchar* pamac_config_get_conf_path (PamacConfig* self);
// const char* pamac_config_get_conf_path (::PamacConfig* self);
std::string base::ConfigBase::get_conf_path () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_conf_path;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// gboolean pamac_config_get_recurse (PamacConfig* self);
// gboolean pamac_config_get_recurse (::PamacConfig* self);
bool base::ConfigBase::get_recurse () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_recurse;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_recurse (PamacConfig* self, gboolean value);
// void pamac_config_set_recurse (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_recurse (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_recurse;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_config_get_keep_built_pkgs (PamacConfig* self);
// gboolean pamac_config_get_keep_built_pkgs (::PamacConfig* self);
bool base::ConfigBase::get_keep_built_pkgs () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_keep_built_pkgs;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_keep_built_pkgs (PamacConfig* self, gboolean value);
// void pamac_config_set_keep_built_pkgs (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_keep_built_pkgs (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_keep_built_pkgs;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_config_get_enable_downgrade (PamacConfig* self);
// gboolean pamac_config_get_enable_downgrade (::PamacConfig* self);
bool base::ConfigBase::get_enable_downgrade () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_enable_downgrade;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_enable_downgrade (PamacConfig* self, gboolean value);
// void pamac_config_set_enable_downgrade (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_enable_downgrade (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_enable_downgrade;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_config_get_simple_install (PamacConfig* self);
// gboolean pamac_config_get_simple_install (::PamacConfig* self);
bool base::ConfigBase::get_simple_install () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_simple_install;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_simple_install (PamacConfig* self, gboolean value);
// void pamac_config_set_simple_install (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_simple_install (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_simple_install;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// guint64 pamac_config_get_refresh_period (PamacConfig* self);
// guint64 pamac_config_get_refresh_period (::PamacConfig* self);
guint64 base::ConfigBase::get_refresh_period () noexcept
{
  typedef guint64 (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_refresh_period;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_refresh_period (PamacConfig* self, guint64 value);
// void pamac_config_set_refresh_period (::PamacConfig* self, guint64 value);
void base::ConfigBase::set_refresh_period (guint64 value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, guint64 value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_refresh_period;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (guint64) (value_to_c));
}

// gboolean pamac_config_get_no_update_hide_icon (PamacConfig* self);
// gboolean pamac_config_get_no_update_hide_icon (::PamacConfig* self);
bool base::ConfigBase::get_no_update_hide_icon () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_no_update_hide_icon;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_no_update_hide_icon (PamacConfig* self, gboolean value);
// void pamac_config_set_no_update_hide_icon (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_no_update_hide_icon (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_no_update_hide_icon;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_config_get_enable_aur (PamacConfig* self);
// gboolean pamac_config_get_enable_aur (::PamacConfig* self);
bool base::ConfigBase::get_enable_aur () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_enable_aur;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_enable_aur (PamacConfig* self, gboolean value);
// void pamac_config_set_enable_aur (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_enable_aur (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_enable_aur;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_config_get_support_snap (PamacConfig* self);
// gboolean pamac_config_get_support_snap (::PamacConfig* self);
bool base::ConfigBase::get_support_snap () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_support_snap;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// gboolean pamac_config_get_enable_snap (PamacConfig* self);
// gboolean pamac_config_get_enable_snap (::PamacConfig* self);
bool base::ConfigBase::get_enable_snap () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_enable_snap;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_enable_snap (PamacConfig* self, gboolean value);
// void pamac_config_set_enable_snap (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_enable_snap (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_enable_snap;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_config_get_support_flatpak (PamacConfig* self);
// gboolean pamac_config_get_support_flatpak (::PamacConfig* self);
bool base::ConfigBase::get_support_flatpak () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_support_flatpak;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// gboolean pamac_config_get_enable_flatpak (PamacConfig* self);
// gboolean pamac_config_get_enable_flatpak (::PamacConfig* self);
bool base::ConfigBase::get_enable_flatpak () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_enable_flatpak;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_enable_flatpak (PamacConfig* self, gboolean value);
// void pamac_config_set_enable_flatpak (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_enable_flatpak (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_enable_flatpak;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_config_get_check_flatpak_updates (PamacConfig* self);
// gboolean pamac_config_get_check_flatpak_updates (::PamacConfig* self);
bool base::ConfigBase::get_check_flatpak_updates () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_check_flatpak_updates;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_check_flatpak_updates (PamacConfig* self, gboolean value);
// void pamac_config_set_check_flatpak_updates (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_check_flatpak_updates (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_check_flatpak_updates;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// const gchar* pamac_config_get_aur_build_dir (PamacConfig* self);
// const char* pamac_config_get_aur_build_dir (::PamacConfig* self);
std::string base::ConfigBase::get_aur_build_dir () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_aur_build_dir;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void pamac_config_set_aur_build_dir (PamacConfig* self, const gchar* value);
// void pamac_config_set_aur_build_dir (::PamacConfig* self, const char* value);
void base::ConfigBase::set_aur_build_dir (const std::string & value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_aur_build_dir;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::PamacConfig*) (gobj_()), (const char*) (value_to_c));
}

// gboolean pamac_config_get_check_aur_updates (PamacConfig* self);
// gboolean pamac_config_get_check_aur_updates (::PamacConfig* self);
bool base::ConfigBase::get_check_aur_updates () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_check_aur_updates;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_check_aur_updates (PamacConfig* self, gboolean value);
// void pamac_config_set_check_aur_updates (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_check_aur_updates (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_check_aur_updates;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_config_get_check_aur_vcs_updates (PamacConfig* self);
// gboolean pamac_config_get_check_aur_vcs_updates (::PamacConfig* self);
bool base::ConfigBase::get_check_aur_vcs_updates () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_check_aur_vcs_updates;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_check_aur_vcs_updates (PamacConfig* self, gboolean value);
// void pamac_config_set_check_aur_vcs_updates (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_check_aur_vcs_updates (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_check_aur_vcs_updates;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// gboolean pamac_config_get_download_updates (PamacConfig* self);
// gboolean pamac_config_get_download_updates (::PamacConfig* self);
bool base::ConfigBase::get_download_updates () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_download_updates;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_download_updates (PamacConfig* self, gboolean value);
// void pamac_config_set_download_updates (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_download_updates (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_download_updates;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// guint64 pamac_config_get_max_parallel_downloads (PamacConfig* self);
// guint64 pamac_config_get_max_parallel_downloads (::PamacConfig* self);
guint64 base::ConfigBase::get_max_parallel_downloads () noexcept
{
  typedef guint64 (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_max_parallel_downloads;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_max_parallel_downloads (PamacConfig* self, guint64 value);
// void pamac_config_set_max_parallel_downloads (::PamacConfig* self, guint64 value);
void base::ConfigBase::set_max_parallel_downloads (guint64 value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, guint64 value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_max_parallel_downloads;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (guint64) (value_to_c));
}

// guint64 pamac_config_get_clean_keep_num_pkgs (PamacConfig* self);
// guint64 pamac_config_get_clean_keep_num_pkgs (::PamacConfig* self);
guint64 base::ConfigBase::get_clean_keep_num_pkgs () noexcept
{
  typedef guint64 (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_clean_keep_num_pkgs;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_clean_keep_num_pkgs (PamacConfig* self, guint64 value);
// void pamac_config_set_clean_keep_num_pkgs (::PamacConfig* self, guint64 value);
void base::ConfigBase::set_clean_keep_num_pkgs (guint64 value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, guint64 value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_clean_keep_num_pkgs;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (guint64) (value_to_c));
}

// gboolean pamac_config_get_clean_rm_only_uninstalled (PamacConfig* self);
// gboolean pamac_config_get_clean_rm_only_uninstalled (::PamacConfig* self);
bool base::ConfigBase::get_clean_rm_only_uninstalled () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_clean_rm_only_uninstalled;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_clean_rm_only_uninstalled (PamacConfig* self, gboolean value);
// void pamac_config_set_clean_rm_only_uninstalled (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_clean_rm_only_uninstalled (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_clean_rm_only_uninstalled;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

// GHashTable* pamac_config_get_environment_variables (PamacConfig* self);
// ::GHashTable* pamac_config_get_environment_variables (::PamacConfig* self);
std::map<std::string, std::string> base::ConfigBase::get_environment_variables () noexcept
{
  typedef ::GHashTable* (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_environment_variables;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return gi::detail::wrap_map<std::string, std::string> (_temp_ret, gi::transfer_none);
}

// const gchar* pamac_config_get_db_path (PamacConfig* self);
// const char* pamac_config_get_db_path (::PamacConfig* self);
std::string base::ConfigBase::get_db_path () noexcept
{
  typedef const char* (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_db_path;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// gboolean pamac_config_get_checkspace (PamacConfig* self);
// gboolean pamac_config_get_checkspace (::PamacConfig* self);
bool base::ConfigBase::get_checkspace () noexcept
{
  typedef gboolean (*call_wrap_t) (::PamacConfig* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_get_checkspace;
  auto _temp_ret = call_wrap_v ((::PamacConfig*) (gobj_()));
  return _temp_ret;
}

// void pamac_config_set_checkspace (PamacConfig* self, gboolean value);
// void pamac_config_set_checkspace (::PamacConfig* self, gboolean value);
void base::ConfigBase::set_checkspace (gboolean value) noexcept
{
  typedef void (*call_wrap_t) (::PamacConfig* self, gboolean value);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_config_set_checkspace;
  auto value_to_c = value;
  call_wrap_v ((::PamacConfig*) (gobj_()), (gboolean) (value_to_c));
}

//  pamac_config_get_ignorepkgs (PamacConfig* self);
//  pamac_config_get_ignorepkgs (::PamacConfig* self);
// SKIP; inconsistent type info


} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/config_extra_def_impl.hpp>)
#include <pamac/config_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/config_extra_impl.hpp>)
#include <pamac/config_extra_impl.hpp>
#endif
#endif

#endif
