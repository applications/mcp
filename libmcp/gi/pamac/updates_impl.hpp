// AUTO-GENERATED

#ifndef _GI_PAMAC_UPDATES_IMPL_HPP_
#define _GI_PAMAC_UPDATES_IMPL_HPP_

namespace gi {

namespace repository {

namespace Pamac {

namespace base {

// GPtrArray* pamac_updates_get_repos_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_repos_updates (::PamacUpdates* self);
GLib::PtrArray base::UpdatesBase::get_repos_updates () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacUpdates* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_get_repos_updates;
  auto _temp_ret = call_wrap_v ((::PamacUpdates*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_updates_get_ignored_repos_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_ignored_repos_updates (::PamacUpdates* self);
GLib::PtrArray base::UpdatesBase::get_ignored_repos_updates () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacUpdates* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_get_ignored_repos_updates;
  auto _temp_ret = call_wrap_v ((::PamacUpdates*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_updates_get_aur_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_aur_updates (::PamacUpdates* self);
GLib::PtrArray base::UpdatesBase::get_aur_updates () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacUpdates* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_get_aur_updates;
  auto _temp_ret = call_wrap_v ((::PamacUpdates*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_updates_get_ignored_aur_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_ignored_aur_updates (::PamacUpdates* self);
GLib::PtrArray base::UpdatesBase::get_ignored_aur_updates () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacUpdates* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_get_ignored_aur_updates;
  auto _temp_ret = call_wrap_v ((::PamacUpdates*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_updates_get_outofdate (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_outofdate (::PamacUpdates* self);
GLib::PtrArray base::UpdatesBase::get_outofdate () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacUpdates* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_get_outofdate;
  auto _temp_ret = call_wrap_v ((::PamacUpdates*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GPtrArray* pamac_updates_get_flatpak_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_flatpak_updates (::PamacUpdates* self);
GLib::PtrArray base::UpdatesBase::get_flatpak_updates () noexcept
{
  typedef ::GPtrArray* (*call_wrap_t) (::PamacUpdates* self);
  call_wrap_t call_wrap_v = (call_wrap_t) pamac_updates_get_flatpak_updates;
  auto _temp_ret = call_wrap_v ((::PamacUpdates*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}


} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/updates_extra_def_impl.hpp>)
#include <pamac/updates_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/updates_extra_impl.hpp>)
#include <pamac/updates_extra_impl.hpp>
#endif
#endif

#endif
