// AUTO-GENERATED

#ifndef _GI_PAMAC_UPDATES_HPP_
#define _GI_PAMAC_UPDATES_HPP_


namespace gi {

namespace repository {

namespace Pamac {


class Updates;

namespace base {


#define GI_PAMAC_UPDATES_BASE base::UpdatesBase
class UpdatesBase : public GObject::Object
{
typedef GObject::Object super_type;
public:
typedef ::PamacUpdates BaseObjectType;

UpdatesBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_updates_get_type(); } 

gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_repos_updates()
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "repos-updates"); }
const gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_repos_updates() const
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "repos-updates"); }

// GPtrArray* pamac_updates_get_repos_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_repos_updates (::PamacUpdates* self);
GI_INLINE_DECL GLib::PtrArray get_repos_updates () noexcept;

gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_ignored_repos_updates()
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "ignored-repos-updates"); }
const gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_ignored_repos_updates() const
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "ignored-repos-updates"); }

// GPtrArray* pamac_updates_get_ignored_repos_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_ignored_repos_updates (::PamacUpdates* self);
GI_INLINE_DECL GLib::PtrArray get_ignored_repos_updates () noexcept;

gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_aur_updates()
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "aur-updates"); }
const gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_aur_updates() const
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "aur-updates"); }

// GPtrArray* pamac_updates_get_aur_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_aur_updates (::PamacUpdates* self);
GI_INLINE_DECL GLib::PtrArray get_aur_updates () noexcept;

gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_ignored_aur_updates()
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "ignored-aur-updates"); }
const gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_ignored_aur_updates() const
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "ignored-aur-updates"); }

// GPtrArray* pamac_updates_get_ignored_aur_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_ignored_aur_updates (::PamacUpdates* self);
GI_INLINE_DECL GLib::PtrArray get_ignored_aur_updates () noexcept;

gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_outofdate()
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "outofdate"); }
const gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_outofdate() const
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "outofdate"); }

// GPtrArray* pamac_updates_get_outofdate (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_outofdate (::PamacUpdates* self);
GI_INLINE_DECL GLib::PtrArray get_outofdate () noexcept;

gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_flatpak_updates()
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "flatpak-updates"); }
const gi::property_proxy<GLib::PtrArray, base::UpdatesBase> property_flatpak_updates() const
{ return gi::property_proxy<GLib::PtrArray, base::UpdatesBase> (*this, "flatpak-updates"); }

// GPtrArray* pamac_updates_get_flatpak_updates (PamacUpdates* self);
// ::GPtrArray* pamac_updates_get_flatpak_updates (::PamacUpdates* self);
GI_INLINE_DECL GLib::PtrArray get_flatpak_updates () noexcept;

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/updates_extra_def.hpp>)
#include <pamac/updates_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/updates_extra.hpp>)
#include <pamac/updates_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class Updates : public GI_PAMAC_UPDATES_BASE
{ typedef GI_PAMAC_UPDATES_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacUpdates>
{ typedef Pamac::Updates type; }; 

} // namespace repository

} // namespace gi

#endif
