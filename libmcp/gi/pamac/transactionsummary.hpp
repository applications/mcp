// AUTO-GENERATED

#ifndef _GI_PAMAC_TRANSACTIONSUMMARY_HPP_
#define _GI_PAMAC_TRANSACTIONSUMMARY_HPP_


namespace gi {

namespace repository {

namespace Pamac {


class TransactionSummary;

namespace base {


#define GI_PAMAC_TRANSACTIONSUMMARY_BASE base::TransactionSummaryBase
class TransactionSummaryBase : public GObject::Object
{
typedef GObject::Object super_type;
public:
typedef ::PamacTransactionSummary BaseObjectType;

TransactionSummaryBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return pamac_transaction_summary_get_type(); } 

gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_install()
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-install"); }
const gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_install() const
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-install"); }

// GPtrArray* pamac_transaction_summary_get_to_install (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_install (::PamacTransactionSummary* self);
GI_INLINE_DECL GLib::PtrArray get_to_install () noexcept;

gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_upgrade()
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-upgrade"); }
const gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_upgrade() const
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-upgrade"); }

// GPtrArray* pamac_transaction_summary_get_to_upgrade (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_upgrade (::PamacTransactionSummary* self);
GI_INLINE_DECL GLib::PtrArray get_to_upgrade () noexcept;

gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_downgrade()
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-downgrade"); }
const gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_downgrade() const
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-downgrade"); }

// GPtrArray* pamac_transaction_summary_get_to_downgrade (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_downgrade (::PamacTransactionSummary* self);
GI_INLINE_DECL GLib::PtrArray get_to_downgrade () noexcept;

gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_reinstall()
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-reinstall"); }
const gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_reinstall() const
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-reinstall"); }

// GPtrArray* pamac_transaction_summary_get_to_reinstall (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_reinstall (::PamacTransactionSummary* self);
GI_INLINE_DECL GLib::PtrArray get_to_reinstall () noexcept;

gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_remove()
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-remove"); }
const gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_remove() const
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-remove"); }

// GPtrArray* pamac_transaction_summary_get_to_remove (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_remove (::PamacTransactionSummary* self);
GI_INLINE_DECL GLib::PtrArray get_to_remove () noexcept;

gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_conflicts_to_remove()
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "conflicts-to-remove"); }
const gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_conflicts_to_remove() const
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "conflicts-to-remove"); }

// GPtrArray* pamac_transaction_summary_get_conflicts_to_remove (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_conflicts_to_remove (::PamacTransactionSummary* self);
GI_INLINE_DECL GLib::PtrArray get_conflicts_to_remove () noexcept;

gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_build()
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-build"); }
const gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_build() const
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-build"); }

// GPtrArray* pamac_transaction_summary_get_to_build (PamacTransactionSummary* self);
// ::GPtrArray* pamac_transaction_summary_get_to_build (::PamacTransactionSummary* self);
GI_INLINE_DECL GLib::PtrArray get_to_build () noexcept;

gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_aur_pkgbases_to_build()
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "aur-pkgbases-to-build"); }
const gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_aur_pkgbases_to_build() const
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "aur-pkgbases-to-build"); }

gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_load()
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-load"); }
const gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> property_to_load() const
{ return gi::property_proxy<GLib::PtrArray, base::TransactionSummaryBase> (*this, "to-load"); }

}; // class

} // namespace base

} // namespace Pamac

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<pamac/transactionsummary_extra_def.hpp>)
#include <pamac/transactionsummary_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<pamac/transactionsummary_extra.hpp>)
#include <pamac/transactionsummary_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Pamac {

class TransactionSummary : public GI_PAMAC_TRANSACTIONSUMMARY_BASE
{ typedef GI_PAMAC_TRANSACTIONSUMMARY_BASE super_type; using super_type::super_type; };

} // namespace Pamac

template<> struct declare_cpptype_of<::PamacTransactionSummary>
{ typedef Pamac::TransactionSummary type; }; 

} // namespace repository

} // namespace gi

#endif
