#pragma once

#include <pamac.h>

// 'internal' Vala visibility shows up as public in GIR, we need stubs so bindings will compile

extern "C" {
inline void pamac_package_set_name(PamacPackage *, const char *) {}
inline void pamac_package_set_id(PamacPackage *, const char *) {}
inline void pamac_package_set_version(PamacPackage *, const char *) {}
inline void pamac_package_set_installed_version(PamacPackage *, const char *) {}
inline void pamac_package_set_repo(PamacPackage *, const char *) {}
inline void pamac_package_set_desc(PamacPackage *, const char *) {}
inline void pamac_aur_package_set_packagebase(PamacAURPackage *, const char *) {}
inline void pamac_alpm_package_set_depends(PamacAlpmPackage *, GPtrArray *) {}
inline void pamac_alpm_package_set_provides(PamacAlpmPackage *, GPtrArray *) {}
inline void pamac_alpm_package_set_replaces(PamacAlpmPackage *, GPtrArray *) {}
inline void pamac_alpm_package_set_conflicts(PamacAlpmPackage *, GPtrArray *) {}
}
