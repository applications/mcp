// AUTO-GENERATED

#ifndef _GI_GIO_ACTIONGROUP_HPP_
#define _GI_GIO_ACTIONGROUP_HPP_


namespace gi {

namespace repository {

namespace Gio {


class ActionGroup;

namespace base {


#define GI_GIO_ACTIONGROUP_BASE base::ActionGroupBase
class ActionGroupBase : public gi::InterfaceBase
{
typedef gi::InterfaceBase super_type;
public:
typedef ::GActionGroup BaseObjectType;

ActionGroupBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return g_action_group_get_type(); } 

// void g_action_group_action_added (GActionGroup* action_group, const gchar* action_name);
// void g_action_group_action_added (::GActionGroup* action_group, const char* action_name);
GI_INLINE_DECL void action_added (const std::string & action_name) noexcept;

// void g_action_group_action_enabled_changed (GActionGroup* action_group, const gchar* action_name, gboolean enabled);
// void g_action_group_action_enabled_changed (::GActionGroup* action_group, const char* action_name, gboolean enabled);
GI_INLINE_DECL void action_enabled_changed (const std::string & action_name, gboolean enabled) noexcept;

// void g_action_group_action_removed (GActionGroup* action_group, const gchar* action_name);
// void g_action_group_action_removed (::GActionGroup* action_group, const char* action_name);
GI_INLINE_DECL void action_removed (const std::string & action_name) noexcept;

// void g_action_group_action_state_changed (GActionGroup* action_group, const gchar* action_name, GVariant* state);
// void g_action_group_action_state_changed (::GActionGroup* action_group, const char* action_name, ::GVariant* state);
GI_INLINE_DECL void action_state_changed (const std::string & action_name, GLib::Variant state) noexcept;

// void g_action_group_activate_action (GActionGroup* action_group, const gchar* action_name, GVariant* parameter);
// void g_action_group_activate_action (::GActionGroup* action_group, const char* action_name, ::GVariant* parameter);
GI_INLINE_DECL void activate_action (const std::string & action_name, GLib::Variant parameter) noexcept;
GI_INLINE_DECL void activate_action (const std::string & action_name) noexcept;

// void g_action_group_change_action_state (GActionGroup* action_group, const gchar* action_name, GVariant* value);
// void g_action_group_change_action_state (::GActionGroup* action_group, const char* action_name, ::GVariant* value);
GI_INLINE_DECL void change_action_state (const std::string & action_name, GLib::Variant value) noexcept;

// gboolean g_action_group_get_action_enabled (GActionGroup* action_group, const gchar* action_name);
// gboolean g_action_group_get_action_enabled (::GActionGroup* action_group, const char* action_name);
GI_INLINE_DECL bool get_action_enabled (const std::string & action_name) noexcept;

// const GVariantType* g_action_group_get_action_parameter_type (GActionGroup* action_group, const gchar* action_name);
// const ::GVariantType* g_action_group_get_action_parameter_type (::GActionGroup* action_group, const char* action_name);
GI_INLINE_DECL GLib::VariantType get_action_parameter_type (const std::string & action_name) noexcept;

// GVariant* g_action_group_get_action_state (GActionGroup* action_group, const gchar* action_name);
// ::GVariant* g_action_group_get_action_state (::GActionGroup* action_group, const char* action_name);
GI_INLINE_DECL GLib::Variant get_action_state (const std::string & action_name) noexcept;

// GVariant* g_action_group_get_action_state_hint (GActionGroup* action_group, const gchar* action_name);
// ::GVariant* g_action_group_get_action_state_hint (::GActionGroup* action_group, const char* action_name);
GI_INLINE_DECL GLib::Variant get_action_state_hint (const std::string & action_name) noexcept;

// const GVariantType* g_action_group_get_action_state_type (GActionGroup* action_group, const gchar* action_name);
// const ::GVariantType* g_action_group_get_action_state_type (::GActionGroup* action_group, const char* action_name);
GI_INLINE_DECL GLib::VariantType get_action_state_type (const std::string & action_name) noexcept;

// gboolean g_action_group_has_action (GActionGroup* action_group, const gchar* action_name);
// gboolean g_action_group_has_action (::GActionGroup* action_group, const char* action_name);
GI_INLINE_DECL bool has_action (const std::string & action_name) noexcept;

// gchar** g_action_group_list_actions (GActionGroup* action_group);
// char** g_action_group_list_actions (::GActionGroup* action_group);
GI_INLINE_DECL std::vector<std::string> list_actions () noexcept;

// gboolean g_action_group_query_action (GActionGroup* action_group, const gchar* action_name, gboolean* enabled, const GVariantType** parameter_type, const GVariantType** state_type, GVariant** state_hint, GVariant** state);
// gboolean g_action_group_query_action (::GActionGroup* action_group, const char* action_name, gboolean* enabled, const ::GVariantType** parameter_type, const ::GVariantType** state_type, ::GVariant** state_hint, ::GVariant** state);
GI_INLINE_DECL bool query_action (const std::string & action_name, bool & enabled, GLib::VariantType * parameter_type = nullptr, GLib::VariantType * state_type = nullptr, GLib::Variant * state_hint = nullptr, GLib::Variant * state = nullptr) noexcept;
GI_INLINE_DECL std::tuple<bool, bool, GLib::VariantType, GLib::VariantType, GLib::Variant, GLib::Variant> query_action (const std::string & action_name) noexcept;

// signal action-added
gi::signal_proxy<void(Gio::ActionGroup, const std::string & action_name)> signal_action_added()
{ return gi::signal_proxy<void(Gio::ActionGroup, const std::string & action_name)> (*this, "action-added"); }

// signal action-enabled-changed
gi::signal_proxy<void(Gio::ActionGroup, const std::string & action_name, gboolean enabled)> signal_action_enabled_changed()
{ return gi::signal_proxy<void(Gio::ActionGroup, const std::string & action_name, gboolean enabled)> (*this, "action-enabled-changed"); }

// signal action-removed
gi::signal_proxy<void(Gio::ActionGroup, const std::string & action_name)> signal_action_removed()
{ return gi::signal_proxy<void(Gio::ActionGroup, const std::string & action_name)> (*this, "action-removed"); }

// signal action-state-changed
gi::signal_proxy<void(Gio::ActionGroup, const std::string & action_name, GLib::Variant value)> signal_action_state_changed()
{ return gi::signal_proxy<void(Gio::ActionGroup, const std::string & action_name, GLib::Variant value)> (*this, "action-state-changed"); }

}; // class

} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/actiongroup_extra_def.hpp>)
#include <gio/actiongroup_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/actiongroup_extra.hpp>)
#include <gio/actiongroup_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Gio {

class ActionGroup : public GI_GIO_ACTIONGROUP_BASE
{ typedef GI_GIO_ACTIONGROUP_BASE super_type; using super_type::super_type; };

} // namespace Gio

template<> struct declare_cpptype_of<::GActionGroup>
{ typedef Gio::ActionGroup type; }; 

} // namespace repository

} // namespace gi

#endif
