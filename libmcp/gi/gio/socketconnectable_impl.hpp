// AUTO-GENERATED

#ifndef _GI_GIO_SOCKETCONNECTABLE_IMPL_HPP_
#define _GI_GIO_SOCKETCONNECTABLE_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// GSocketAddressEnumerator* g_socket_connectable_enumerate (GSocketConnectable* connectable);
// ::GSocketAddressEnumerator* g_socket_connectable_enumerate (::GSocketConnectable* connectable);
Gio::SocketAddressEnumerator base::SocketConnectableBase::enumerate () noexcept
{
  typedef ::GSocketAddressEnumerator* (*call_wrap_t) (::GSocketConnectable* connectable);
  call_wrap_t call_wrap_v = (call_wrap_t) g_socket_connectable_enumerate;
  auto _temp_ret = call_wrap_v ((::GSocketConnectable*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GSocketAddressEnumerator* g_socket_connectable_proxy_enumerate (GSocketConnectable* connectable);
// ::GSocketAddressEnumerator* g_socket_connectable_proxy_enumerate (::GSocketConnectable* connectable);
Gio::SocketAddressEnumerator base::SocketConnectableBase::proxy_enumerate () noexcept
{
  typedef ::GSocketAddressEnumerator* (*call_wrap_t) (::GSocketConnectable* connectable);
  call_wrap_t call_wrap_v = (call_wrap_t) g_socket_connectable_proxy_enumerate;
  auto _temp_ret = call_wrap_v ((::GSocketConnectable*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// gchar* g_socket_connectable_to_string (GSocketConnectable* connectable);
// char* g_socket_connectable_to_string (::GSocketConnectable* connectable);
std::string base::SocketConnectableBase::to_string () noexcept
{
  typedef char* (*call_wrap_t) (::GSocketConnectable* connectable);
  call_wrap_t call_wrap_v = (call_wrap_t) g_socket_connectable_to_string;
  auto _temp_ret = call_wrap_v ((::GSocketConnectable*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}


} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/socketconnectable_extra_def_impl.hpp>)
#include <gio/socketconnectable_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/socketconnectable_extra_impl.hpp>)
#include <gio/socketconnectable_extra_impl.hpp>
#endif
#endif

#endif
