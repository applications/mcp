// AUTO-GENERATED

#ifndef _GI_GIO_FILEOUTPUTSTREAM_HPP_
#define _GI_GIO_FILEOUTPUTSTREAM_HPP_

#include "outputstream.hpp"

namespace gi {

namespace repository {

namespace Gio {

class AsyncResult;
class Cancellable;
class FileInfo;
class Seekable;

class FileOutputStream;

namespace base {


#define GI_GIO_FILEOUTPUTSTREAM_BASE base::FileOutputStreamBase
class FileOutputStreamBase : public Gio::OutputStream
{
typedef Gio::OutputStream super_type;
public:
typedef ::GFileOutputStream BaseObjectType;

FileOutputStreamBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return g_file_output_stream_get_type(); } 

GI_INLINE_DECL Gio::Seekable interface_ (gi::interface_tag<Gio::Seekable>);

GI_INLINE_DECL operator Gio::Seekable ();

// char* g_file_output_stream_get_etag (GFileOutputStream* stream);
// char* g_file_output_stream_get_etag (::GFileOutputStream* stream);
GI_INLINE_DECL std::string get_etag () noexcept;

// GFileInfo* g_file_output_stream_query_info (GFileOutputStream* stream, const char* attributes, GCancellable* cancellable, GError ** error);
// ::GFileInfo* g_file_output_stream_query_info (::GFileOutputStream* stream, const char* attributes, ::GCancellable* cancellable, GError ** error);
GI_INLINE_DECL Gio::FileInfo query_info (const std::string & attributes, Gio::Cancellable cancellable);
GI_INLINE_DECL Gio::FileInfo query_info (const std::string & attributes);
GI_INLINE_DECL Gio::FileInfo query_info (const std::string & attributes, Gio::Cancellable cancellable, GLib::Error * _error) noexcept;
GI_INLINE_DECL Gio::FileInfo query_info (const std::string & attributes, GLib::Error * _error) noexcept;

// void g_file_output_stream_query_info_async (GFileOutputStream* stream, const char* attributes, int io_priority, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_file_output_stream_query_info_async (::GFileOutputStream* stream, const char* attributes, gint io_priority, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
GI_INLINE_DECL void query_info_async (const std::string & attributes, gint io_priority, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept;
GI_INLINE_DECL void query_info_async (const std::string & attributes, gint io_priority, Gio::AsyncReadyCallback callback) noexcept;

// GFileInfo* g_file_output_stream_query_info_finish (GFileOutputStream* stream, GAsyncResult* result, GError ** error);
// ::GFileInfo* g_file_output_stream_query_info_finish (::GFileOutputStream* stream, ::GAsyncResult* result, GError ** error);
GI_INLINE_DECL Gio::FileInfo query_info_finish (Gio::AsyncResult result);
GI_INLINE_DECL Gio::FileInfo query_info_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept;

}; // class

} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/fileoutputstream_extra_def.hpp>)
#include <gio/fileoutputstream_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/fileoutputstream_extra.hpp>)
#include <gio/fileoutputstream_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Gio {

class FileOutputStream : public GI_GIO_FILEOUTPUTSTREAM_BASE
{ typedef GI_GIO_FILEOUTPUTSTREAM_BASE super_type; using super_type::super_type; };

} // namespace Gio

template<> struct declare_cpptype_of<::GFileOutputStream>
{ typedef Gio::FileOutputStream type; }; 

} // namespace repository

} // namespace gi

#endif
