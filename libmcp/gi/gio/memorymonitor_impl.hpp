// AUTO-GENERATED

#ifndef _GI_GIO_MEMORYMONITOR_IMPL_HPP_
#define _GI_GIO_MEMORYMONITOR_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// GMemoryMonitor* g_memory_monitor_dup_default ();
// ::GMemoryMonitor* g_memory_monitor_dup_default ();
Gio::MemoryMonitor base::MemoryMonitorBase::dup_default () noexcept
{
  typedef ::GMemoryMonitor* (*call_wrap_t) ();
  call_wrap_t call_wrap_v = (call_wrap_t) g_memory_monitor_dup_default;
  auto _temp_ret = call_wrap_v ();
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}



} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/memorymonitor_extra_def_impl.hpp>)
#include <gio/memorymonitor_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/memorymonitor_extra_impl.hpp>)
#include <gio/memorymonitor_extra_impl.hpp>
#endif
#endif

#endif
