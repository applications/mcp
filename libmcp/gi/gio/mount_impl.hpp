// AUTO-GENERATED

#ifndef _GI_GIO_MOUNT_IMPL_HPP_
#define _GI_GIO_MOUNT_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// gboolean g_mount_can_eject (GMount* mount);
// gboolean g_mount_can_eject (::GMount* mount);
bool base::MountBase::can_eject () noexcept
{
  typedef gboolean (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_can_eject;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return _temp_ret;
}

// gboolean g_mount_can_unmount (GMount* mount);
// gboolean g_mount_can_unmount (::GMount* mount);
bool base::MountBase::can_unmount () noexcept
{
  typedef gboolean (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_can_unmount;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return _temp_ret;
}

// void g_mount_eject (GMount* mount, GMountUnmountFlags flags, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_eject (::GMount* mount, ::GMountUnmountFlags flags, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
// IGNORE; deprecated

// gboolean g_mount_eject_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_eject_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
// IGNORE; deprecated

// void g_mount_eject_with_operation (GMount* mount, GMountUnmountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_eject_with_operation (::GMount* mount, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
void base::MountBase::eject_with_operation (Gio::MountUnmountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_eject_with_operation;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto mount_operation_to_c = gi::unwrap (mount_operation, gi::transfer_none, gi::direction_in);
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GMount*) (gobj_()), (::GMountUnmountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}
void base::MountBase::eject_with_operation (Gio::MountUnmountFlags flags, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_eject_with_operation;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  auto mount_operation_to_c = nullptr;
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GMount*) (gobj_()), (::GMountUnmountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}

// gboolean g_mount_eject_with_operation_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_eject_with_operation_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
bool base::MountBase::eject_with_operation_finish (Gio::AsyncResult result)
{
  typedef gboolean (*call_wrap_t) (::GMount* mount, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_eject_with_operation_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (::GAsyncResult*) (result_to_c), &error);
  gi::check_error (error);
  return _temp_ret;
}
bool base::MountBase::eject_with_operation_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept
{
  typedef gboolean (*call_wrap_t) (::GMount* mount, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_eject_with_operation_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (::GAsyncResult*) (result_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return _temp_ret;
}

// GFile* g_mount_get_default_location (GMount* mount);
// ::GFile* g_mount_get_default_location (::GMount* mount);
Gio::File base::MountBase::get_default_location () noexcept
{
  typedef ::GFile* (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_get_default_location;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GDrive* g_mount_get_drive (GMount* mount);
// ::GDrive* g_mount_get_drive (::GMount* mount);
Gio::Drive base::MountBase::get_drive () noexcept
{
  typedef ::GDrive* (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_get_drive;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GIcon* g_mount_get_icon (GMount* mount);
// ::GIcon* g_mount_get_icon (::GMount* mount);
Gio::Icon base::MountBase::get_icon () noexcept
{
  typedef ::GIcon* (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_get_icon;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// char* g_mount_get_name (GMount* mount);
// char* g_mount_get_name (::GMount* mount);
std::string base::MountBase::get_name () noexcept
{
  typedef char* (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_get_name;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GFile* g_mount_get_root (GMount* mount);
// ::GFile* g_mount_get_root (::GMount* mount);
Gio::File base::MountBase::get_root () noexcept
{
  typedef ::GFile* (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_get_root;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// const gchar* g_mount_get_sort_key (GMount* mount);
// const char* g_mount_get_sort_key (::GMount* mount);
std::string base::MountBase::get_sort_key () noexcept
{
  typedef const char* (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_get_sort_key;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GIcon* g_mount_get_symbolic_icon (GMount* mount);
// ::GIcon* g_mount_get_symbolic_icon (::GMount* mount);
Gio::Icon base::MountBase::get_symbolic_icon () noexcept
{
  typedef ::GIcon* (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_get_symbolic_icon;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// char* g_mount_get_uuid (GMount* mount);
// char* g_mount_get_uuid (::GMount* mount);
std::string base::MountBase::get_uuid () noexcept
{
  typedef char* (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_get_uuid;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GVolume* g_mount_get_volume (GMount* mount);
// ::GVolume* g_mount_get_volume (::GMount* mount);
Gio::Volume base::MountBase::get_volume () noexcept
{
  typedef ::GVolume* (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_get_volume;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void g_mount_guess_content_type (GMount* mount, gboolean force_rescan, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_guess_content_type (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
void base::MountBase::guess_content_type (gboolean force_rescan, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_guess_content_type;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto force_rescan_to_c = force_rescan;
  call_wrap_v ((::GMount*) (gobj_()), (gboolean) (force_rescan_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}
void base::MountBase::guess_content_type (gboolean force_rescan, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_guess_content_type;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  auto force_rescan_to_c = force_rescan;
  call_wrap_v ((::GMount*) (gobj_()), (gboolean) (force_rescan_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}

// gchar** g_mount_guess_content_type_finish (GMount* mount, GAsyncResult* result, GError ** error);
// char** g_mount_guess_content_type_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
std::vector<std::string> base::MountBase::guess_content_type_finish (Gio::AsyncResult result)
{
  typedef char** (*call_wrap_t) (::GMount* mount, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_guess_content_type_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (::GAsyncResult*) (result_to_c), &error);
  gi::check_error (error);
  return gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_full);
}
std::vector<std::string> base::MountBase::guess_content_type_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept
{
  typedef char** (*call_wrap_t) (::GMount* mount, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_guess_content_type_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (::GAsyncResult*) (result_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_full);
}

// gchar** g_mount_guess_content_type_sync (GMount* mount, gboolean force_rescan, GCancellable* cancellable, GError ** error);
// char** g_mount_guess_content_type_sync (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, GError ** error);
std::vector<std::string> base::MountBase::guess_content_type_sync (gboolean force_rescan, Gio::Cancellable cancellable)
{
  typedef char** (*call_wrap_t) (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_guess_content_type_sync;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto force_rescan_to_c = force_rescan;
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (gboolean) (force_rescan_to_c), (::GCancellable*) (cancellable_to_c), &error);
  gi::check_error (error);
  return gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_full);
}
std::vector<std::string> base::MountBase::guess_content_type_sync (gboolean force_rescan)
{
  typedef char** (*call_wrap_t) (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_guess_content_type_sync;
  auto cancellable_to_c = nullptr;
  auto force_rescan_to_c = force_rescan;
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (gboolean) (force_rescan_to_c), (::GCancellable*) (cancellable_to_c), &error);
  gi::check_error (error);
  return gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_full);
}
std::vector<std::string> base::MountBase::guess_content_type_sync (gboolean force_rescan, Gio::Cancellable cancellable, GLib::Error * _error) noexcept
{
  typedef char** (*call_wrap_t) (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_guess_content_type_sync;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto force_rescan_to_c = force_rescan;
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (gboolean) (force_rescan_to_c), (::GCancellable*) (cancellable_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_full);
}
std::vector<std::string> base::MountBase::guess_content_type_sync (gboolean force_rescan, GLib::Error * _error) noexcept
{
  typedef char** (*call_wrap_t) (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_guess_content_type_sync;
  auto cancellable_to_c = nullptr;
  auto force_rescan_to_c = force_rescan;
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (gboolean) (force_rescan_to_c), (::GCancellable*) (cancellable_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_full);
}

// gboolean g_mount_is_shadowed (GMount* mount);
// gboolean g_mount_is_shadowed (::GMount* mount);
bool base::MountBase::is_shadowed () noexcept
{
  typedef gboolean (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_is_shadowed;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()));
  return _temp_ret;
}

// void g_mount_remount (GMount* mount, GMountMountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_remount (::GMount* mount, ::GMountMountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
void base::MountBase::remount (Gio::MountMountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount, ::GMountMountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_remount;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto mount_operation_to_c = gi::unwrap (mount_operation, gi::transfer_none, gi::direction_in);
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GMount*) (gobj_()), (::GMountMountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}
void base::MountBase::remount (Gio::MountMountFlags flags, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount, ::GMountMountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_remount;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  auto mount_operation_to_c = nullptr;
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GMount*) (gobj_()), (::GMountMountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}

// gboolean g_mount_remount_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_remount_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
bool base::MountBase::remount_finish (Gio::AsyncResult result)
{
  typedef gboolean (*call_wrap_t) (::GMount* mount, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_remount_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (::GAsyncResult*) (result_to_c), &error);
  gi::check_error (error);
  return _temp_ret;
}
bool base::MountBase::remount_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept
{
  typedef gboolean (*call_wrap_t) (::GMount* mount, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_remount_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (::GAsyncResult*) (result_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return _temp_ret;
}

// void g_mount_shadow (GMount* mount);
// void g_mount_shadow (::GMount* mount);
void base::MountBase::shadow () noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_shadow;
  call_wrap_v ((::GMount*) (gobj_()));
}

// void g_mount_unmount (GMount* mount, GMountUnmountFlags flags, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_unmount (::GMount* mount, ::GMountUnmountFlags flags, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
// IGNORE; deprecated

// gboolean g_mount_unmount_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_unmount_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
// IGNORE; deprecated

// void g_mount_unmount_with_operation (GMount* mount, GMountUnmountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_unmount_with_operation (::GMount* mount, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
void base::MountBase::unmount_with_operation (Gio::MountUnmountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_unmount_with_operation;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto mount_operation_to_c = gi::unwrap (mount_operation, gi::transfer_none, gi::direction_in);
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GMount*) (gobj_()), (::GMountUnmountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}
void base::MountBase::unmount_with_operation (Gio::MountUnmountFlags flags, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_unmount_with_operation;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  auto mount_operation_to_c = nullptr;
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GMount*) (gobj_()), (::GMountUnmountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}

// gboolean g_mount_unmount_with_operation_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_unmount_with_operation_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
bool base::MountBase::unmount_with_operation_finish (Gio::AsyncResult result)
{
  typedef gboolean (*call_wrap_t) (::GMount* mount, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_unmount_with_operation_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (::GAsyncResult*) (result_to_c), &error);
  gi::check_error (error);
  return _temp_ret;
}
bool base::MountBase::unmount_with_operation_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept
{
  typedef gboolean (*call_wrap_t) (::GMount* mount, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_unmount_with_operation_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GMount*) (gobj_()), (::GAsyncResult*) (result_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return _temp_ret;
}

// void g_mount_unshadow (GMount* mount);
// void g_mount_unshadow (::GMount* mount);
void base::MountBase::unshadow () noexcept
{
  typedef void (*call_wrap_t) (::GMount* mount);
  call_wrap_t call_wrap_v = (call_wrap_t) g_mount_unshadow;
  call_wrap_v ((::GMount*) (gobj_()));
}





} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/mount_extra_def_impl.hpp>)
#include <gio/mount_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/mount_extra_impl.hpp>)
#include <gio/mount_extra_impl.hpp>
#endif
#endif

#endif
