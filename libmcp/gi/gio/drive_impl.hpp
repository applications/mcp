// AUTO-GENERATED

#ifndef _GI_GIO_DRIVE_IMPL_HPP_
#define _GI_GIO_DRIVE_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// gboolean g_drive_can_eject (GDrive* drive);
// gboolean g_drive_can_eject (::GDrive* drive);
bool base::DriveBase::can_eject () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_can_eject;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// gboolean g_drive_can_poll_for_media (GDrive* drive);
// gboolean g_drive_can_poll_for_media (::GDrive* drive);
bool base::DriveBase::can_poll_for_media () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_can_poll_for_media;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// gboolean g_drive_can_start (GDrive* drive);
// gboolean g_drive_can_start (::GDrive* drive);
bool base::DriveBase::can_start () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_can_start;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// gboolean g_drive_can_start_degraded (GDrive* drive);
// gboolean g_drive_can_start_degraded (::GDrive* drive);
bool base::DriveBase::can_start_degraded () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_can_start_degraded;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// gboolean g_drive_can_stop (GDrive* drive);
// gboolean g_drive_can_stop (::GDrive* drive);
bool base::DriveBase::can_stop () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_can_stop;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// void g_drive_eject (GDrive* drive, GMountUnmountFlags flags, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_eject (::GDrive* drive, ::GMountUnmountFlags flags, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
// IGNORE; deprecated

// gboolean g_drive_eject_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_eject_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
// IGNORE; deprecated

// void g_drive_eject_with_operation (GDrive* drive, GMountUnmountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_eject_with_operation (::GDrive* drive, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
void base::DriveBase::eject_with_operation (Gio::MountUnmountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GDrive* drive, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_eject_with_operation;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto mount_operation_to_c = gi::unwrap (mount_operation, gi::transfer_none, gi::direction_in);
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GDrive*) (gobj_()), (::GMountUnmountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}
void base::DriveBase::eject_with_operation (Gio::MountUnmountFlags flags, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GDrive* drive, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_eject_with_operation;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  auto mount_operation_to_c = nullptr;
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GDrive*) (gobj_()), (::GMountUnmountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}

// gboolean g_drive_eject_with_operation_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_eject_with_operation_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
bool base::DriveBase::eject_with_operation_finish (Gio::AsyncResult result)
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_eject_with_operation_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()), (::GAsyncResult*) (result_to_c), &error);
  gi::check_error (error);
  return _temp_ret;
}
bool base::DriveBase::eject_with_operation_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_eject_with_operation_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()), (::GAsyncResult*) (result_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return _temp_ret;
}

// char** g_drive_enumerate_identifiers (GDrive* drive);
// char** g_drive_enumerate_identifiers (::GDrive* drive);
std::vector<std::string> base::DriveBase::enumerate_identifiers () noexcept
{
  typedef char** (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_enumerate_identifiers;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_full);
}

// GIcon* g_drive_get_icon (GDrive* drive);
// ::GIcon* g_drive_get_icon (::GDrive* drive);
Gio::Icon base::DriveBase::get_icon () noexcept
{
  typedef ::GIcon* (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_get_icon;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// char* g_drive_get_identifier (GDrive* drive, const char* kind);
// char* g_drive_get_identifier (::GDrive* drive, const char* kind);
std::string base::DriveBase::get_identifier (const std::string & kind) noexcept
{
  typedef char* (*call_wrap_t) (::GDrive* drive, const char* kind);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_get_identifier;
  auto kind_to_c = gi::unwrap (kind, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()), (const char*) (kind_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// char* g_drive_get_name (GDrive* drive);
// char* g_drive_get_name (::GDrive* drive);
std::string base::DriveBase::get_name () noexcept
{
  typedef char* (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_get_name;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// const gchar* g_drive_get_sort_key (GDrive* drive);
// const char* g_drive_get_sort_key (::GDrive* drive);
std::string base::DriveBase::get_sort_key () noexcept
{
  typedef const char* (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_get_sort_key;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GDriveStartStopType g_drive_get_start_stop_type (GDrive* drive);
// ::GDriveStartStopType g_drive_get_start_stop_type (::GDrive* drive);
Gio::DriveStartStopType base::DriveBase::get_start_stop_type () noexcept
{
  typedef ::GDriveStartStopType (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_get_start_stop_type;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return gi::wrap (_temp_ret);
}

// GIcon* g_drive_get_symbolic_icon (GDrive* drive);
// ::GIcon* g_drive_get_symbolic_icon (::GDrive* drive);
Gio::Icon base::DriveBase::get_symbolic_icon () noexcept
{
  typedef ::GIcon* (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_get_symbolic_icon;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GList* g_drive_get_volumes (GDrive* drive);
// ::GList* g_drive_get_volumes (::GDrive* drive);
std::vector<Gio::Volume> base::DriveBase::get_volumes () noexcept
{
  typedef ::GList* (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_get_volumes;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return gi::detail::wrap_list<Gio::Volume> (_temp_ret, gi::transfer_full);
}

// gboolean g_drive_has_media (GDrive* drive);
// gboolean g_drive_has_media (::GDrive* drive);
bool base::DriveBase::has_media () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_has_media;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// gboolean g_drive_has_volumes (GDrive* drive);
// gboolean g_drive_has_volumes (::GDrive* drive);
bool base::DriveBase::has_volumes () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_has_volumes;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// gboolean g_drive_is_media_check_automatic (GDrive* drive);
// gboolean g_drive_is_media_check_automatic (::GDrive* drive);
bool base::DriveBase::is_media_check_automatic () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_is_media_check_automatic;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// gboolean g_drive_is_media_removable (GDrive* drive);
// gboolean g_drive_is_media_removable (::GDrive* drive);
bool base::DriveBase::is_media_removable () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_is_media_removable;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// gboolean g_drive_is_removable (GDrive* drive);
// gboolean g_drive_is_removable (::GDrive* drive);
bool base::DriveBase::is_removable () noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_is_removable;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()));
  return _temp_ret;
}

// void g_drive_poll_for_media (GDrive* drive, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_poll_for_media (::GDrive* drive, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
void base::DriveBase::poll_for_media (Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GDrive* drive, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_poll_for_media;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GDrive*) (gobj_()), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}
void base::DriveBase::poll_for_media (Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GDrive* drive, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_poll_for_media;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  call_wrap_v ((::GDrive*) (gobj_()), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}

// gboolean g_drive_poll_for_media_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_poll_for_media_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
bool base::DriveBase::poll_for_media_finish (Gio::AsyncResult result)
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_poll_for_media_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()), (::GAsyncResult*) (result_to_c), &error);
  gi::check_error (error);
  return _temp_ret;
}
bool base::DriveBase::poll_for_media_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_poll_for_media_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()), (::GAsyncResult*) (result_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return _temp_ret;
}

// void g_drive_start (GDrive* drive, GDriveStartFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_start (::GDrive* drive, ::GDriveStartFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
void base::DriveBase::start (Gio::DriveStartFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GDrive* drive, ::GDriveStartFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_start;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto mount_operation_to_c = gi::unwrap (mount_operation, gi::transfer_none, gi::direction_in);
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GDrive*) (gobj_()), (::GDriveStartFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}
void base::DriveBase::start (Gio::DriveStartFlags flags, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GDrive* drive, ::GDriveStartFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_start;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  auto mount_operation_to_c = nullptr;
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GDrive*) (gobj_()), (::GDriveStartFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}

// gboolean g_drive_start_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_start_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
bool base::DriveBase::start_finish (Gio::AsyncResult result)
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_start_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()), (::GAsyncResult*) (result_to_c), &error);
  gi::check_error (error);
  return _temp_ret;
}
bool base::DriveBase::start_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_start_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()), (::GAsyncResult*) (result_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return _temp_ret;
}

// void g_drive_stop (GDrive* drive, GMountUnmountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_stop (::GDrive* drive, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
void base::DriveBase::stop (Gio::MountUnmountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GDrive* drive, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_stop;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto mount_operation_to_c = gi::unwrap (mount_operation, gi::transfer_none, gi::direction_in);
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GDrive*) (gobj_()), (::GMountUnmountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}
void base::DriveBase::stop (Gio::MountUnmountFlags flags, Gio::AsyncReadyCallback callback) noexcept
{
  typedef void (*call_wrap_t) (::GDrive* drive, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_stop;
  auto callback_wrap_ = callback ? unwrap (std::move (callback), gi::scope_async) : nullptr;
  auto cancellable_to_c = nullptr;
  auto mount_operation_to_c = nullptr;
  auto flags_to_c = gi::unwrap (flags);
  call_wrap_v ((::GDrive*) (gobj_()), (::GMountUnmountFlags) (flags_to_c), (::GMountOperation*) (mount_operation_to_c), (::GCancellable*) (cancellable_to_c), (Gio::AsyncReadyCallback::cfunction_type) (callback_wrap_ ? &callback_wrap_->wrapper : nullptr), (void*) (callback_wrap_));
}

// gboolean g_drive_stop_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_stop_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
bool base::DriveBase::stop_finish (Gio::AsyncResult result)
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_stop_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()), (::GAsyncResult*) (result_to_c), &error);
  gi::check_error (error);
  return _temp_ret;
}
bool base::DriveBase::stop_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept
{
  typedef gboolean (*call_wrap_t) (::GDrive* drive, ::GAsyncResult* result, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_drive_stop_finish;
  auto result_to_c = gi::unwrap (result, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GDrive*) (gobj_()), (::GAsyncResult*) (result_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return _temp_ret;
}






} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/drive_extra_def_impl.hpp>)
#include <gio/drive_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/drive_extra_impl.hpp>)
#include <gio/drive_extra_impl.hpp>
#endif
#endif

#endif
