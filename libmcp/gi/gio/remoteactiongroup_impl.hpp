// AUTO-GENERATED

#ifndef _GI_GIO_REMOTEACTIONGROUP_IMPL_HPP_
#define _GI_GIO_REMOTEACTIONGROUP_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// void g_remote_action_group_activate_action_full (GRemoteActionGroup* remote, const gchar* action_name, GVariant* parameter, GVariant* platform_data);
// void g_remote_action_group_activate_action_full (::GRemoteActionGroup* remote, const char* action_name, ::GVariant* parameter, ::GVariant* platform_data);
void base::RemoteActionGroupBase::activate_action_full (const std::string & action_name, GLib::Variant parameter, GLib::Variant platform_data) noexcept
{
  typedef void (*call_wrap_t) (::GRemoteActionGroup* remote, const char* action_name, ::GVariant* parameter, ::GVariant* platform_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_remote_action_group_activate_action_full;
  auto platform_data_to_c = gi::unwrap (platform_data, gi::transfer_none, gi::direction_in);
  auto parameter_to_c = gi::unwrap (parameter, gi::transfer_none, gi::direction_in);
  auto action_name_to_c = gi::unwrap (action_name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GRemoteActionGroup*) (gobj_()), (const char*) (action_name_to_c), (::GVariant*) (parameter_to_c), (::GVariant*) (platform_data_to_c));
}
void base::RemoteActionGroupBase::activate_action_full (const std::string & action_name, GLib::Variant platform_data) noexcept
{
  typedef void (*call_wrap_t) (::GRemoteActionGroup* remote, const char* action_name, ::GVariant* parameter, ::GVariant* platform_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_remote_action_group_activate_action_full;
  auto platform_data_to_c = gi::unwrap (platform_data, gi::transfer_none, gi::direction_in);
  auto parameter_to_c = nullptr;
  auto action_name_to_c = gi::unwrap (action_name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GRemoteActionGroup*) (gobj_()), (const char*) (action_name_to_c), (::GVariant*) (parameter_to_c), (::GVariant*) (platform_data_to_c));
}

// void g_remote_action_group_change_action_state_full (GRemoteActionGroup* remote, const gchar* action_name, GVariant* value, GVariant* platform_data);
// void g_remote_action_group_change_action_state_full (::GRemoteActionGroup* remote, const char* action_name, ::GVariant* value, ::GVariant* platform_data);
void base::RemoteActionGroupBase::change_action_state_full (const std::string & action_name, GLib::Variant value, GLib::Variant platform_data) noexcept
{
  typedef void (*call_wrap_t) (::GRemoteActionGroup* remote, const char* action_name, ::GVariant* value, ::GVariant* platform_data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_remote_action_group_change_action_state_full;
  auto platform_data_to_c = gi::unwrap (platform_data, gi::transfer_none, gi::direction_in);
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  auto action_name_to_c = gi::unwrap (action_name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GRemoteActionGroup*) (gobj_()), (const char*) (action_name_to_c), (::GVariant*) (value_to_c), (::GVariant*) (platform_data_to_c));
}


} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/remoteactiongroup_extra_def_impl.hpp>)
#include <gio/remoteactiongroup_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/remoteactiongroup_extra_impl.hpp>)
#include <gio/remoteactiongroup_extra_impl.hpp>
#endif
#endif

#endif
