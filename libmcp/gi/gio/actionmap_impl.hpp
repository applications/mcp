// AUTO-GENERATED

#ifndef _GI_GIO_ACTIONMAP_IMPL_HPP_
#define _GI_GIO_ACTIONMAP_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// void g_action_map_add_action (GActionMap* action_map, GAction* action);
// void g_action_map_add_action (::GActionMap* action_map, ::GAction* action);
void base::ActionMapBase::add_action (Gio::Action action) noexcept
{
  typedef void (*call_wrap_t) (::GActionMap* action_map, ::GAction* action);
  call_wrap_t call_wrap_v = (call_wrap_t) g_action_map_add_action;
  auto action_to_c = gi::unwrap (action, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GActionMap*) (gobj_()), (::GAction*) (action_to_c));
}

// void g_action_map_add_action_entries (GActionMap* action_map, const GActionEntry* entries, gint n_entries, gpointer user_data);
// void g_action_map_add_action_entries (::GActionMap* action_map, const ::GActionEntry** entries, gint n_entries, void* user_data);
// SKIP; inconsistent in entries pointer depth (1 vs 2)

// GAction* g_action_map_lookup_action (GActionMap* action_map, const gchar* action_name);
// ::GAction* g_action_map_lookup_action (::GActionMap* action_map, const char* action_name);
Gio::Action base::ActionMapBase::lookup_action (const std::string & action_name) noexcept
{
  typedef ::GAction* (*call_wrap_t) (::GActionMap* action_map, const char* action_name);
  call_wrap_t call_wrap_v = (call_wrap_t) g_action_map_lookup_action;
  auto action_name_to_c = gi::unwrap (action_name, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GActionMap*) (gobj_()), (const char*) (action_name_to_c));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void g_action_map_remove_action (GActionMap* action_map, const gchar* action_name);
// void g_action_map_remove_action (::GActionMap* action_map, const char* action_name);
void base::ActionMapBase::remove_action (const std::string & action_name) noexcept
{
  typedef void (*call_wrap_t) (::GActionMap* action_map, const char* action_name);
  call_wrap_t call_wrap_v = (call_wrap_t) g_action_map_remove_action;
  auto action_name_to_c = gi::unwrap (action_name, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GActionMap*) (gobj_()), (const char*) (action_name_to_c));
}


} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/actionmap_extra_def_impl.hpp>)
#include <gio/actionmap_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/actionmap_extra_impl.hpp>)
#include <gio/actionmap_extra_impl.hpp>
#endif
#endif

#endif
