// AUTO-GENERATED

#ifndef _GI_GIO_DBUSOBJECTMANAGER_IMPL_HPP_
#define _GI_GIO_DBUSOBJECTMANAGER_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// GDBusInterface* g_dbus_object_manager_get_interface (GDBusObjectManager* manager, const gchar* object_path, const gchar* interface_name);
// ::GDBusInterface* g_dbus_object_manager_get_interface (::GDBusObjectManager* manager, const char* object_path, const char* interface_name);
Gio::DBusInterface base::DBusObjectManagerBase::get_interface (const std::string & object_path, const std::string & interface_name) noexcept
{
  typedef ::GDBusInterface* (*call_wrap_t) (::GDBusObjectManager* manager, const char* object_path, const char* interface_name);
  call_wrap_t call_wrap_v = (call_wrap_t) g_dbus_object_manager_get_interface;
  auto interface_name_to_c = gi::unwrap (interface_name, gi::transfer_none, gi::direction_in);
  auto object_path_to_c = gi::unwrap (object_path, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GDBusObjectManager*) (gobj_()), (const char*) (object_path_to_c), (const char*) (interface_name_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GDBusObject* g_dbus_object_manager_get_object (GDBusObjectManager* manager, const gchar* object_path);
// ::GDBusObject* g_dbus_object_manager_get_object (::GDBusObjectManager* manager, const char* object_path);
Gio::DBusObject base::DBusObjectManagerBase::get_object (const std::string & object_path) noexcept
{
  typedef ::GDBusObject* (*call_wrap_t) (::GDBusObjectManager* manager, const char* object_path);
  call_wrap_t call_wrap_v = (call_wrap_t) g_dbus_object_manager_get_object;
  auto object_path_to_c = gi::unwrap (object_path, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GDBusObjectManager*) (gobj_()), (const char*) (object_path_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// const gchar* g_dbus_object_manager_get_object_path (GDBusObjectManager* manager);
// const char* g_dbus_object_manager_get_object_path (::GDBusObjectManager* manager);
std::string base::DBusObjectManagerBase::get_object_path () noexcept
{
  typedef const char* (*call_wrap_t) (::GDBusObjectManager* manager);
  call_wrap_t call_wrap_v = (call_wrap_t) g_dbus_object_manager_get_object_path;
  auto _temp_ret = call_wrap_v ((::GDBusObjectManager*) (gobj_()));
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// GList* g_dbus_object_manager_get_objects (GDBusObjectManager* manager);
// ::GList* g_dbus_object_manager_get_objects (::GDBusObjectManager* manager);
std::vector<Gio::DBusObject> base::DBusObjectManagerBase::get_objects () noexcept
{
  typedef ::GList* (*call_wrap_t) (::GDBusObjectManager* manager);
  call_wrap_t call_wrap_v = (call_wrap_t) g_dbus_object_manager_get_objects;
  auto _temp_ret = call_wrap_v ((::GDBusObjectManager*) (gobj_()));
  return gi::detail::wrap_list<Gio::DBusObject> (_temp_ret, gi::transfer_full);
}






} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/dbusobjectmanager_extra_def_impl.hpp>)
#include <gio/dbusobjectmanager_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/dbusobjectmanager_extra_impl.hpp>)
#include <gio/dbusobjectmanager_extra_impl.hpp>
#endif
#endif

#endif
