// AUTO-GENERATED

#ifndef _GI_GIO_THREADEDSOCKETSERVICE_IMPL_HPP_
#define _GI_GIO_THREADEDSOCKETSERVICE_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// GSocketService* g_threaded_socket_service_new (int max_threads);
// ::GThreadedSocketService* g_threaded_socket_service_new (gint max_threads);
Gio::ThreadedSocketService base::ThreadedSocketServiceBase::new_ (gint max_threads) noexcept
{
  typedef ::GThreadedSocketService* (*call_wrap_t) (gint max_threads);
  call_wrap_t call_wrap_v = (call_wrap_t) g_threaded_socket_service_new;
  auto max_threads_to_c = max_threads;
  auto _temp_ret = call_wrap_v ((gint) (max_threads_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}



} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/threadedsocketservice_extra_def_impl.hpp>)
#include <gio/threadedsocketservice_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/threadedsocketservice_extra_impl.hpp>)
#include <gio/threadedsocketservice_extra_impl.hpp>
#endif
#endif

#endif
