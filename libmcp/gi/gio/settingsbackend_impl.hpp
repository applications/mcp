// AUTO-GENERATED

#ifndef _GI_GIO_SETTINGSBACKEND_IMPL_HPP_
#define _GI_GIO_SETTINGSBACKEND_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// void g_settings_backend_flatten_tree (GTree* tree, gchar** path, const gchar*** keys, GVariant*** values);
// void g_settings_backend_flatten_tree (::GTree* tree, char** path, const char*** keys, ::GVariant*** values);
void base::SettingsBackendBase::flatten_tree (GLib::Tree tree, std::string & path, std::vector<std::string> & keys, std::vector<GLib::Variant> * values) noexcept
{
  typedef void (*call_wrap_t) (::GTree* tree, char** path, const char*** keys, ::GVariant*** values);
  call_wrap_t call_wrap_v = (call_wrap_t) g_settings_backend_flatten_tree;
  ::GVariant** values_o {};
  const char** keys_o {};
  char* path_o {};
  auto tree_to_c = gi::unwrap (tree, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GTree*) (tree_to_c), (char**) (&path_o), (const char***) (&keys_o), (::GVariant***) (values ? &values_o : nullptr));
  if (values) *values = gi::detail::wrap_list<GLib::Variant> (values_o, gi::transfer_container);
  keys = gi::detail::wrap_list<std::string> (keys_o, gi::transfer_container);
  path = gi::wrap (path_o, gi::transfer_full, gi::direction_out);
}
std::tuple<std::string, std::vector<std::string>, std::vector<GLib::Variant>> base::SettingsBackendBase::flatten_tree (GLib::Tree tree) noexcept
{
  typedef void (*call_wrap_t) (::GTree* tree, char** path, const char*** keys, ::GVariant*** values);
  call_wrap_t call_wrap_v = (call_wrap_t) g_settings_backend_flatten_tree;
  ::GVariant** values_o {};
  const char** keys_o {};
  char* path_o {};
  auto tree_to_c = gi::unwrap (tree, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GTree*) (tree_to_c), (char**) (&path_o), (const char***) (&keys_o), (::GVariant***) (&values_o));
  return std::make_tuple (gi::wrap (path_o, gi::transfer_full, gi::direction_out), gi::detail::wrap_list<std::string> (keys_o, gi::transfer_container), gi::detail::wrap_list<GLib::Variant> (values_o, gi::transfer_container));
}

// GSettingsBackend* g_settings_backend_get_default ();
// ::GSettingsBackend* g_settings_backend_get_default ();
Gio::SettingsBackend base::SettingsBackendBase::get_default () noexcept
{
  typedef ::GSettingsBackend* (*call_wrap_t) ();
  call_wrap_t call_wrap_v = (call_wrap_t) g_settings_backend_get_default;
  auto _temp_ret = call_wrap_v ();
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void g_settings_backend_changed (GSettingsBackend* backend, const gchar* key, gpointer origin_tag);
// void g_settings_backend_changed (::GSettingsBackend* backend, const char* key, void* origin_tag);
void base::SettingsBackendBase::changed (const std::string & key, void* origin_tag) noexcept
{
  typedef void (*call_wrap_t) (::GSettingsBackend* backend, const char* key, void* origin_tag);
  call_wrap_t call_wrap_v = (call_wrap_t) g_settings_backend_changed;
  auto origin_tag_to_c = origin_tag;
  auto key_to_c = gi::unwrap (key, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GSettingsBackend*) (gobj_()), (const char*) (key_to_c), (void*) (origin_tag_to_c));
}

// void g_settings_backend_changed_tree (GSettingsBackend* backend, GTree* tree, gpointer origin_tag);
// void g_settings_backend_changed_tree (::GSettingsBackend* backend, ::GTree* tree, void* origin_tag);
void base::SettingsBackendBase::changed_tree (GLib::Tree tree, void* origin_tag) noexcept
{
  typedef void (*call_wrap_t) (::GSettingsBackend* backend, ::GTree* tree, void* origin_tag);
  call_wrap_t call_wrap_v = (call_wrap_t) g_settings_backend_changed_tree;
  auto origin_tag_to_c = origin_tag;
  auto tree_to_c = gi::unwrap (tree, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GSettingsBackend*) (gobj_()), (::GTree*) (tree_to_c), (void*) (origin_tag_to_c));
}

// void g_settings_backend_keys_changed (GSettingsBackend* backend, const gchar* path, const gchar* const* items, gpointer origin_tag);
// void g_settings_backend_keys_changed (::GSettingsBackend* backend, const char* path, const char** items, void* origin_tag);
void base::SettingsBackendBase::keys_changed (const std::string & path, const std::vector<std::string> & items, void* origin_tag) noexcept
{
  typedef void (*call_wrap_t) (::GSettingsBackend* backend, const char* path, const char** items, void* origin_tag);
  call_wrap_t call_wrap_v = (call_wrap_t) g_settings_backend_keys_changed;
  auto origin_tag_to_c = origin_tag;
  auto items_i = detail::make_list_unwrap_range<traits::ctype<std::string>::type> (items);
  auto items_w = unwrap (items_i, gi::transfer_none, direction_in);
  auto items_to_c = items_w.gobj_(false);
  auto path_to_c = gi::unwrap (path, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GSettingsBackend*) (gobj_()), (const char*) (path_to_c), (const char**) (items_to_c), (void*) (origin_tag_to_c));
}

// void g_settings_backend_path_changed (GSettingsBackend* backend, const gchar* path, gpointer origin_tag);
// void g_settings_backend_path_changed (::GSettingsBackend* backend, const char* path, void* origin_tag);
void base::SettingsBackendBase::path_changed (const std::string & path, void* origin_tag) noexcept
{
  typedef void (*call_wrap_t) (::GSettingsBackend* backend, const char* path, void* origin_tag);
  call_wrap_t call_wrap_v = (call_wrap_t) g_settings_backend_path_changed;
  auto origin_tag_to_c = origin_tag;
  auto path_to_c = gi::unwrap (path, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GSettingsBackend*) (gobj_()), (const char*) (path_to_c), (void*) (origin_tag_to_c));
}

// void g_settings_backend_path_writable_changed (GSettingsBackend* backend, const gchar* path);
// void g_settings_backend_path_writable_changed (::GSettingsBackend* backend, const char* path);
void base::SettingsBackendBase::path_writable_changed (const std::string & path) noexcept
{
  typedef void (*call_wrap_t) (::GSettingsBackend* backend, const char* path);
  call_wrap_t call_wrap_v = (call_wrap_t) g_settings_backend_path_writable_changed;
  auto path_to_c = gi::unwrap (path, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GSettingsBackend*) (gobj_()), (const char*) (path_to_c));
}

// void g_settings_backend_writable_changed (GSettingsBackend* backend, const gchar* key);
// void g_settings_backend_writable_changed (::GSettingsBackend* backend, const char* key);
void base::SettingsBackendBase::writable_changed (const std::string & key) noexcept
{
  typedef void (*call_wrap_t) (::GSettingsBackend* backend, const char* key);
  call_wrap_t call_wrap_v = (call_wrap_t) g_settings_backend_writable_changed;
  auto key_to_c = gi::unwrap (key, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GSettingsBackend*) (gobj_()), (const char*) (key_to_c));
}


} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/settingsbackend_extra_def_impl.hpp>)
#include <gio/settingsbackend_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/settingsbackend_extra_impl.hpp>)
#include <gio/settingsbackend_extra_impl.hpp>
#endif
#endif

#endif
