// AUTO-GENERATED

#ifndef _GI_GIO_MOUNT_HPP_
#define _GI_GIO_MOUNT_HPP_


namespace gi {

namespace repository {

namespace Gio {

class AsyncResult;
class Cancellable;
class Drive;
class File;
class Icon;
class MountOperation;
class Volume;

class Mount;

namespace base {


#define GI_GIO_MOUNT_BASE base::MountBase
class MountBase : public gi::InterfaceBase
{
typedef gi::InterfaceBase super_type;
public:
typedef ::GMount BaseObjectType;

MountBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return g_mount_get_type(); } 

// gboolean g_mount_can_eject (GMount* mount);
// gboolean g_mount_can_eject (::GMount* mount);
GI_INLINE_DECL bool can_eject () noexcept;

// gboolean g_mount_can_unmount (GMount* mount);
// gboolean g_mount_can_unmount (::GMount* mount);
GI_INLINE_DECL bool can_unmount () noexcept;

// void g_mount_eject (GMount* mount, GMountUnmountFlags flags, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_eject (::GMount* mount, ::GMountUnmountFlags flags, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
// IGNORE; deprecated

// gboolean g_mount_eject_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_eject_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
// IGNORE; deprecated

// void g_mount_eject_with_operation (GMount* mount, GMountUnmountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_eject_with_operation (::GMount* mount, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
GI_INLINE_DECL void eject_with_operation (Gio::MountUnmountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept;
GI_INLINE_DECL void eject_with_operation (Gio::MountUnmountFlags flags, Gio::AsyncReadyCallback callback) noexcept;

// gboolean g_mount_eject_with_operation_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_eject_with_operation_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
GI_INLINE_DECL bool eject_with_operation_finish (Gio::AsyncResult result);
GI_INLINE_DECL bool eject_with_operation_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept;

// GFile* g_mount_get_default_location (GMount* mount);
// ::GFile* g_mount_get_default_location (::GMount* mount);
GI_INLINE_DECL Gio::File get_default_location () noexcept;

// GDrive* g_mount_get_drive (GMount* mount);
// ::GDrive* g_mount_get_drive (::GMount* mount);
GI_INLINE_DECL Gio::Drive get_drive () noexcept;

// GIcon* g_mount_get_icon (GMount* mount);
// ::GIcon* g_mount_get_icon (::GMount* mount);
GI_INLINE_DECL Gio::Icon get_icon () noexcept;

// char* g_mount_get_name (GMount* mount);
// char* g_mount_get_name (::GMount* mount);
GI_INLINE_DECL std::string get_name () noexcept;

// GFile* g_mount_get_root (GMount* mount);
// ::GFile* g_mount_get_root (::GMount* mount);
GI_INLINE_DECL Gio::File get_root () noexcept;

// const gchar* g_mount_get_sort_key (GMount* mount);
// const char* g_mount_get_sort_key (::GMount* mount);
GI_INLINE_DECL std::string get_sort_key () noexcept;

// GIcon* g_mount_get_symbolic_icon (GMount* mount);
// ::GIcon* g_mount_get_symbolic_icon (::GMount* mount);
GI_INLINE_DECL Gio::Icon get_symbolic_icon () noexcept;

// char* g_mount_get_uuid (GMount* mount);
// char* g_mount_get_uuid (::GMount* mount);
GI_INLINE_DECL std::string get_uuid () noexcept;

// GVolume* g_mount_get_volume (GMount* mount);
// ::GVolume* g_mount_get_volume (::GMount* mount);
GI_INLINE_DECL Gio::Volume get_volume () noexcept;

// void g_mount_guess_content_type (GMount* mount, gboolean force_rescan, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_guess_content_type (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
GI_INLINE_DECL void guess_content_type (gboolean force_rescan, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept;
GI_INLINE_DECL void guess_content_type (gboolean force_rescan, Gio::AsyncReadyCallback callback) noexcept;

// gchar** g_mount_guess_content_type_finish (GMount* mount, GAsyncResult* result, GError ** error);
// char** g_mount_guess_content_type_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
GI_INLINE_DECL std::vector<std::string> guess_content_type_finish (Gio::AsyncResult result);
GI_INLINE_DECL std::vector<std::string> guess_content_type_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept;

// gchar** g_mount_guess_content_type_sync (GMount* mount, gboolean force_rescan, GCancellable* cancellable, GError ** error);
// char** g_mount_guess_content_type_sync (::GMount* mount, gboolean force_rescan, ::GCancellable* cancellable, GError ** error);
GI_INLINE_DECL std::vector<std::string> guess_content_type_sync (gboolean force_rescan, Gio::Cancellable cancellable);
GI_INLINE_DECL std::vector<std::string> guess_content_type_sync (gboolean force_rescan);
GI_INLINE_DECL std::vector<std::string> guess_content_type_sync (gboolean force_rescan, Gio::Cancellable cancellable, GLib::Error * _error) noexcept;
GI_INLINE_DECL std::vector<std::string> guess_content_type_sync (gboolean force_rescan, GLib::Error * _error) noexcept;

// gboolean g_mount_is_shadowed (GMount* mount);
// gboolean g_mount_is_shadowed (::GMount* mount);
GI_INLINE_DECL bool is_shadowed () noexcept;

// void g_mount_remount (GMount* mount, GMountMountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_remount (::GMount* mount, ::GMountMountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
GI_INLINE_DECL void remount (Gio::MountMountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept;
GI_INLINE_DECL void remount (Gio::MountMountFlags flags, Gio::AsyncReadyCallback callback) noexcept;

// gboolean g_mount_remount_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_remount_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
GI_INLINE_DECL bool remount_finish (Gio::AsyncResult result);
GI_INLINE_DECL bool remount_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept;

// void g_mount_shadow (GMount* mount);
// void g_mount_shadow (::GMount* mount);
GI_INLINE_DECL void shadow () noexcept;

// void g_mount_unmount (GMount* mount, GMountUnmountFlags flags, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_unmount (::GMount* mount, ::GMountUnmountFlags flags, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
// IGNORE; deprecated

// gboolean g_mount_unmount_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_unmount_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
// IGNORE; deprecated

// void g_mount_unmount_with_operation (GMount* mount, GMountUnmountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_mount_unmount_with_operation (::GMount* mount, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
GI_INLINE_DECL void unmount_with_operation (Gio::MountUnmountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept;
GI_INLINE_DECL void unmount_with_operation (Gio::MountUnmountFlags flags, Gio::AsyncReadyCallback callback) noexcept;

// gboolean g_mount_unmount_with_operation_finish (GMount* mount, GAsyncResult* result, GError ** error);
// gboolean g_mount_unmount_with_operation_finish (::GMount* mount, ::GAsyncResult* result, GError ** error);
GI_INLINE_DECL bool unmount_with_operation_finish (Gio::AsyncResult result);
GI_INLINE_DECL bool unmount_with_operation_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept;

// void g_mount_unshadow (GMount* mount);
// void g_mount_unshadow (::GMount* mount);
GI_INLINE_DECL void unshadow () noexcept;

// signal changed
gi::signal_proxy<void(Gio::Mount)> signal_changed()
{ return gi::signal_proxy<void(Gio::Mount)> (*this, "changed"); }

// signal pre-unmount
gi::signal_proxy<void(Gio::Mount)> signal_pre_unmount()
{ return gi::signal_proxy<void(Gio::Mount)> (*this, "pre-unmount"); }

// signal unmounted
gi::signal_proxy<void(Gio::Mount)> signal_unmounted()
{ return gi::signal_proxy<void(Gio::Mount)> (*this, "unmounted"); }

}; // class

} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/mount_extra_def.hpp>)
#include <gio/mount_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/mount_extra.hpp>)
#include <gio/mount_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Gio {

class Mount : public GI_GIO_MOUNT_BASE
{ typedef GI_GIO_MOUNT_BASE super_type; using super_type::super_type; };

} // namespace Gio

template<> struct declare_cpptype_of<::GMount>
{ typedef Gio::Mount type; }; 

} // namespace repository

} // namespace gi

#endif
