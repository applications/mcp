// AUTO-GENERATED

#ifndef _GI_GIO_LISTSTORE_HPP_
#define _GI_GIO_LISTSTORE_HPP_


namespace gi {

namespace repository {

namespace Gio {

class ListModel;

class ListStore;

namespace base {


#define GI_GIO_LISTSTORE_BASE base::ListStoreBase
class ListStoreBase : public GObject::Object
{
typedef GObject::Object super_type;
public:
typedef ::GListStore BaseObjectType;

ListStoreBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return g_list_store_get_type(); } 

GI_INLINE_DECL Gio::ListModel interface_ (gi::interface_tag<Gio::ListModel>);

GI_INLINE_DECL operator Gio::ListModel ();

// GListStore* g_list_store_new (GType item_type);
// ::GListStore* g_list_store_new (GType item_type);
static GI_INLINE_DECL Gio::ListStore new_ (GType item_type) noexcept;

// void g_list_store_append (GListStore* store, gpointer item);
// void g_list_store_append (::GListStore* store, ::GObject* item);
GI_INLINE_DECL void append (GObject::Object item) noexcept;

// gboolean g_list_store_find (GListStore* store, gpointer item, guint* position);
// gboolean g_list_store_find (::GListStore* store, ::GObject* item, guint* position);
GI_INLINE_DECL bool find (GObject::Object item, guint * position = nullptr) noexcept;
GI_INLINE_DECL std::tuple<bool, guint> find (GObject::Object item) noexcept;

// gboolean g_list_store_find_with_equal_func (GListStore* store, gpointer item, GEqualFunc equal_func, guint* position);
// gboolean g_list_store_find_with_equal_func (::GListStore* store, ::GObject* item,  equal_func, guint* position);
// SKIP; equal_func type  not supported

// gboolean g_list_store_find_with_equal_func_full (GListStore* store, gpointer item, GEqualFuncFull equal_func, gpointer user_data, guint* position);
// gboolean g_list_store_find_with_equal_func_full (::GListStore* store, ::GObject* item, GLib::EqualFuncFull::cfunction_type equal_func, void* user_data, guint* position);
GI_INLINE_DECL bool find_with_equal_func_full (GObject::Object item, GLib::EqualFuncFull equal_func, guint * position = nullptr) noexcept;
GI_INLINE_DECL std::tuple<bool, guint> find_with_equal_func_full (GObject::Object item, GLib::EqualFuncFull equal_func) noexcept;

// void g_list_store_insert (GListStore* store, guint position, gpointer item);
// void g_list_store_insert (::GListStore* store, guint position, ::GObject* item);
GI_INLINE_DECL void insert (guint position, GObject::Object item) noexcept;

// guint g_list_store_insert_sorted (GListStore* store, gpointer item, GCompareDataFunc compare_func, gpointer user_data);
// guint g_list_store_insert_sorted (::GListStore* store, ::GObject* item, GLib::CompareDataFunc::cfunction_type compare_func, void* user_data);
GI_INLINE_DECL guint insert_sorted (GObject::Object item, GLib::CompareDataFunc compare_func) noexcept;

// void g_list_store_remove (GListStore* store, guint position);
// void g_list_store_remove (::GListStore* store, guint position);
GI_INLINE_DECL void remove (guint position) noexcept;

// void g_list_store_remove_all (GListStore* store);
// void g_list_store_remove_all (::GListStore* store);
GI_INLINE_DECL void remove_all () noexcept;

// void g_list_store_sort (GListStore* store, GCompareDataFunc compare_func, gpointer user_data);
// void g_list_store_sort (::GListStore* store, GLib::CompareDataFunc::cfunction_type compare_func, void* user_data);
GI_INLINE_DECL void sort (GLib::CompareDataFunc compare_func) noexcept;

// void g_list_store_splice (GListStore* store, guint position, guint n_removals, gpointer* additions, guint n_additions);
// void g_list_store_splice (::GListStore* store, guint position, guint n_removals, ::GObject** additions, guint n_additions);
GI_INLINE_DECL void splice (guint position, guint n_removals, GObject::Object * additions, guint n_additions) noexcept;

gi::property_proxy<GType, base::ListStoreBase> property_item_type()
{ return gi::property_proxy<GType, base::ListStoreBase> (*this, "item-type"); }
const gi::property_proxy<GType, base::ListStoreBase> property_item_type() const
{ return gi::property_proxy<GType, base::ListStoreBase> (*this, "item-type"); }

gi::property_proxy<guint, base::ListStoreBase> property_n_items()
{ return gi::property_proxy<guint, base::ListStoreBase> (*this, "n-items"); }
const gi::property_proxy<guint, base::ListStoreBase> property_n_items() const
{ return gi::property_proxy<guint, base::ListStoreBase> (*this, "n-items"); }

}; // class

} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/liststore_extra_def.hpp>)
#include <gio/liststore_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/liststore_extra.hpp>)
#include <gio/liststore_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Gio {

class ListStore : public GI_GIO_LISTSTORE_BASE
{ typedef GI_GIO_LISTSTORE_BASE super_type; using super_type::super_type; };

} // namespace Gio

template<> struct declare_cpptype_of<::GListStore>
{ typedef Gio::ListStore type; }; 

} // namespace repository

} // namespace gi

#endif
