// AUTO-GENERATED

#ifndef _GI_GIO_DRIVE_HPP_
#define _GI_GIO_DRIVE_HPP_


namespace gi {

namespace repository {

namespace Gio {

class AsyncResult;
class Cancellable;
class Icon;
class MountOperation;
class Volume;

class Drive;

namespace base {


#define GI_GIO_DRIVE_BASE base::DriveBase
class DriveBase : public gi::InterfaceBase
{
typedef gi::InterfaceBase super_type;
public:
typedef ::GDrive BaseObjectType;

DriveBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return g_drive_get_type(); } 

// gboolean g_drive_can_eject (GDrive* drive);
// gboolean g_drive_can_eject (::GDrive* drive);
GI_INLINE_DECL bool can_eject () noexcept;

// gboolean g_drive_can_poll_for_media (GDrive* drive);
// gboolean g_drive_can_poll_for_media (::GDrive* drive);
GI_INLINE_DECL bool can_poll_for_media () noexcept;

// gboolean g_drive_can_start (GDrive* drive);
// gboolean g_drive_can_start (::GDrive* drive);
GI_INLINE_DECL bool can_start () noexcept;

// gboolean g_drive_can_start_degraded (GDrive* drive);
// gboolean g_drive_can_start_degraded (::GDrive* drive);
GI_INLINE_DECL bool can_start_degraded () noexcept;

// gboolean g_drive_can_stop (GDrive* drive);
// gboolean g_drive_can_stop (::GDrive* drive);
GI_INLINE_DECL bool can_stop () noexcept;

// void g_drive_eject (GDrive* drive, GMountUnmountFlags flags, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_eject (::GDrive* drive, ::GMountUnmountFlags flags, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
// IGNORE; deprecated

// gboolean g_drive_eject_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_eject_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
// IGNORE; deprecated

// void g_drive_eject_with_operation (GDrive* drive, GMountUnmountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_eject_with_operation (::GDrive* drive, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
GI_INLINE_DECL void eject_with_operation (Gio::MountUnmountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept;
GI_INLINE_DECL void eject_with_operation (Gio::MountUnmountFlags flags, Gio::AsyncReadyCallback callback) noexcept;

// gboolean g_drive_eject_with_operation_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_eject_with_operation_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
GI_INLINE_DECL bool eject_with_operation_finish (Gio::AsyncResult result);
GI_INLINE_DECL bool eject_with_operation_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept;

// char** g_drive_enumerate_identifiers (GDrive* drive);
// char** g_drive_enumerate_identifiers (::GDrive* drive);
GI_INLINE_DECL std::vector<std::string> enumerate_identifiers () noexcept;

// GIcon* g_drive_get_icon (GDrive* drive);
// ::GIcon* g_drive_get_icon (::GDrive* drive);
GI_INLINE_DECL Gio::Icon get_icon () noexcept;

// char* g_drive_get_identifier (GDrive* drive, const char* kind);
// char* g_drive_get_identifier (::GDrive* drive, const char* kind);
GI_INLINE_DECL std::string get_identifier (const std::string & kind) noexcept;

// char* g_drive_get_name (GDrive* drive);
// char* g_drive_get_name (::GDrive* drive);
GI_INLINE_DECL std::string get_name () noexcept;

// const gchar* g_drive_get_sort_key (GDrive* drive);
// const char* g_drive_get_sort_key (::GDrive* drive);
GI_INLINE_DECL std::string get_sort_key () noexcept;

// GDriveStartStopType g_drive_get_start_stop_type (GDrive* drive);
// ::GDriveStartStopType g_drive_get_start_stop_type (::GDrive* drive);
GI_INLINE_DECL Gio::DriveStartStopType get_start_stop_type () noexcept;

// GIcon* g_drive_get_symbolic_icon (GDrive* drive);
// ::GIcon* g_drive_get_symbolic_icon (::GDrive* drive);
GI_INLINE_DECL Gio::Icon get_symbolic_icon () noexcept;

// GList* g_drive_get_volumes (GDrive* drive);
// ::GList* g_drive_get_volumes (::GDrive* drive);
GI_INLINE_DECL std::vector<Gio::Volume> get_volumes () noexcept;

// gboolean g_drive_has_media (GDrive* drive);
// gboolean g_drive_has_media (::GDrive* drive);
GI_INLINE_DECL bool has_media () noexcept;

// gboolean g_drive_has_volumes (GDrive* drive);
// gboolean g_drive_has_volumes (::GDrive* drive);
GI_INLINE_DECL bool has_volumes () noexcept;

// gboolean g_drive_is_media_check_automatic (GDrive* drive);
// gboolean g_drive_is_media_check_automatic (::GDrive* drive);
GI_INLINE_DECL bool is_media_check_automatic () noexcept;

// gboolean g_drive_is_media_removable (GDrive* drive);
// gboolean g_drive_is_media_removable (::GDrive* drive);
GI_INLINE_DECL bool is_media_removable () noexcept;

// gboolean g_drive_is_removable (GDrive* drive);
// gboolean g_drive_is_removable (::GDrive* drive);
GI_INLINE_DECL bool is_removable () noexcept;

// void g_drive_poll_for_media (GDrive* drive, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_poll_for_media (::GDrive* drive, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
GI_INLINE_DECL void poll_for_media (Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept;
GI_INLINE_DECL void poll_for_media (Gio::AsyncReadyCallback callback) noexcept;

// gboolean g_drive_poll_for_media_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_poll_for_media_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
GI_INLINE_DECL bool poll_for_media_finish (Gio::AsyncResult result);
GI_INLINE_DECL bool poll_for_media_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept;

// void g_drive_start (GDrive* drive, GDriveStartFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_start (::GDrive* drive, ::GDriveStartFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
GI_INLINE_DECL void start (Gio::DriveStartFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept;
GI_INLINE_DECL void start (Gio::DriveStartFlags flags, Gio::AsyncReadyCallback callback) noexcept;

// gboolean g_drive_start_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_start_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
GI_INLINE_DECL bool start_finish (Gio::AsyncResult result);
GI_INLINE_DECL bool start_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept;

// void g_drive_stop (GDrive* drive, GMountUnmountFlags flags, GMountOperation* mount_operation, GCancellable* cancellable, GAsyncReadyCallback callback, gpointer user_data);
// void g_drive_stop (::GDrive* drive, ::GMountUnmountFlags flags, ::GMountOperation* mount_operation, ::GCancellable* cancellable, Gio::AsyncReadyCallback::cfunction_type callback, void* user_data);
GI_INLINE_DECL void stop (Gio::MountUnmountFlags flags, Gio::MountOperation mount_operation, Gio::Cancellable cancellable, Gio::AsyncReadyCallback callback) noexcept;
GI_INLINE_DECL void stop (Gio::MountUnmountFlags flags, Gio::AsyncReadyCallback callback) noexcept;

// gboolean g_drive_stop_finish (GDrive* drive, GAsyncResult* result, GError ** error);
// gboolean g_drive_stop_finish (::GDrive* drive, ::GAsyncResult* result, GError ** error);
GI_INLINE_DECL bool stop_finish (Gio::AsyncResult result);
GI_INLINE_DECL bool stop_finish (Gio::AsyncResult result, GLib::Error * _error) noexcept;

// signal changed
gi::signal_proxy<void(Gio::Drive)> signal_changed()
{ return gi::signal_proxy<void(Gio::Drive)> (*this, "changed"); }

// signal disconnected
gi::signal_proxy<void(Gio::Drive)> signal_disconnected()
{ return gi::signal_proxy<void(Gio::Drive)> (*this, "disconnected"); }

// signal eject-button
gi::signal_proxy<void(Gio::Drive)> signal_eject_button()
{ return gi::signal_proxy<void(Gio::Drive)> (*this, "eject-button"); }

// signal stop-button
gi::signal_proxy<void(Gio::Drive)> signal_stop_button()
{ return gi::signal_proxy<void(Gio::Drive)> (*this, "stop-button"); }

}; // class

} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/drive_extra_def.hpp>)
#include <gio/drive_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/drive_extra.hpp>)
#include <gio/drive_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Gio {

class Drive : public GI_GIO_DRIVE_BASE
{ typedef GI_GIO_DRIVE_BASE super_type; using super_type::super_type; };

} // namespace Gio

template<> struct declare_cpptype_of<::GDrive>
{ typedef Gio::Drive type; }; 

} // namespace repository

} // namespace gi

#endif
