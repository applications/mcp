// AUTO-GENERATED

#ifndef _GI_GIO_SEEKABLE_HPP_
#define _GI_GIO_SEEKABLE_HPP_


namespace gi {

namespace repository {

namespace Gio {

class Cancellable;

class Seekable;

namespace base {


#define GI_GIO_SEEKABLE_BASE base::SeekableBase
class SeekableBase : public gi::InterfaceBase
{
typedef gi::InterfaceBase super_type;
public:
typedef ::GSeekable BaseObjectType;

SeekableBase (std::nullptr_t = nullptr) : super_type() {}

BaseObjectType *gobj_() { return (BaseObjectType*) super_type::gobj_(); }
const BaseObjectType *gobj_() const { return (const BaseObjectType*) super_type::gobj_(); }
BaseObjectType *gobj_copy_() const { return (BaseObjectType*) super_type::gobj_copy_(); }

static GType get_type_ () G_GNUC_CONST { return g_seekable_get_type(); } 

// gboolean g_seekable_can_seek (GSeekable* seekable);
// gboolean g_seekable_can_seek (::GSeekable* seekable);
GI_INLINE_DECL bool can_seek () noexcept;

// gboolean g_seekable_can_truncate (GSeekable* seekable);
// gboolean g_seekable_can_truncate (::GSeekable* seekable);
GI_INLINE_DECL bool can_truncate () noexcept;

// gboolean g_seekable_seek (GSeekable* seekable, goffset offset, GSeekType type, GCancellable* cancellable, GError ** error);
// gboolean g_seekable_seek (::GSeekable* seekable, gint64 offset, ::GSeekType type, ::GCancellable* cancellable, GError ** error);
GI_INLINE_DECL bool seek (gint64 offset, GLib::SeekType type, Gio::Cancellable cancellable);
GI_INLINE_DECL bool seek (gint64 offset, GLib::SeekType type);
GI_INLINE_DECL bool seek (gint64 offset, GLib::SeekType type, Gio::Cancellable cancellable, GLib::Error * _error) noexcept;
GI_INLINE_DECL bool seek (gint64 offset, GLib::SeekType type, GLib::Error * _error) noexcept;

// goffset g_seekable_tell (GSeekable* seekable);
// gint64 g_seekable_tell (::GSeekable* seekable);
GI_INLINE_DECL gint64 tell () noexcept;

// gboolean g_seekable_truncate (GSeekable* seekable, goffset offset, GCancellable* cancellable, GError ** error);
// gboolean g_seekable_truncate (::GSeekable* seekable, gint64 offset, ::GCancellable* cancellable, GError ** error);
GI_INLINE_DECL bool truncate (gint64 offset, Gio::Cancellable cancellable);
GI_INLINE_DECL bool truncate (gint64 offset);
GI_INLINE_DECL bool truncate (gint64 offset, Gio::Cancellable cancellable, GLib::Error * _error) noexcept;
GI_INLINE_DECL bool truncate (gint64 offset, GLib::Error * _error) noexcept;

}; // class

} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/seekable_extra_def.hpp>)
#include <gio/seekable_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/seekable_extra.hpp>)
#include <gio/seekable_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace Gio {

class Seekable : public GI_GIO_SEEKABLE_BASE
{ typedef GI_GIO_SEEKABLE_BASE super_type; using super_type::super_type; };

} // namespace Gio

template<> struct declare_cpptype_of<::GSeekable>
{ typedef Gio::Seekable type; }; 

} // namespace repository

} // namespace gi

#endif
