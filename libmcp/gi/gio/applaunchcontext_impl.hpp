// AUTO-GENERATED

#ifndef _GI_GIO_APPLAUNCHCONTEXT_IMPL_HPP_
#define _GI_GIO_APPLAUNCHCONTEXT_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// GAppLaunchContext* g_app_launch_context_new ();
// ::GAppLaunchContext* g_app_launch_context_new ();
Gio::AppLaunchContext base::AppLaunchContextBase::new_ () noexcept
{
  typedef ::GAppLaunchContext* (*call_wrap_t) ();
  call_wrap_t call_wrap_v = (call_wrap_t) g_app_launch_context_new;
  auto _temp_ret = call_wrap_v ();
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// char* g_app_launch_context_get_display (GAppLaunchContext* context, GAppInfo* info, GList* files);
// char* g_app_launch_context_get_display (::GAppLaunchContext* context, ::GAppInfo* info, ::GList* files);
std::string base::AppLaunchContextBase::get_display (Gio::AppInfo info, const std::vector<Gio::File> & files) noexcept
{
  typedef char* (*call_wrap_t) (::GAppLaunchContext* context, ::GAppInfo* info, ::GList* files);
  call_wrap_t call_wrap_v = (call_wrap_t) g_app_launch_context_get_display;
  auto files_i = detail::make_list_unwrap_range<::GList> (files);
  auto files_w = unwrap (files_i, gi::transfer_none, direction_in);
  auto files_to_c = files_w.gobj_(false);
  auto info_to_c = gi::unwrap (info, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GAppLaunchContext*) (gobj_()), (::GAppInfo*) (info_to_c), (::GList*) (files_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// char** g_app_launch_context_get_environment (GAppLaunchContext* context);
// char** g_app_launch_context_get_environment (::GAppLaunchContext* context);
std::vector<std::string> base::AppLaunchContextBase::get_environment () noexcept
{
  typedef char** (*call_wrap_t) (::GAppLaunchContext* context);
  call_wrap_t call_wrap_v = (call_wrap_t) g_app_launch_context_get_environment;
  auto _temp_ret = call_wrap_v ((::GAppLaunchContext*) (gobj_()));
  return gi::detail::wrap_list<std::string> (_temp_ret, gi::transfer_full);
}

// char* g_app_launch_context_get_startup_notify_id (GAppLaunchContext* context, GAppInfo* info, GList* files);
// char* g_app_launch_context_get_startup_notify_id (::GAppLaunchContext* context, ::GAppInfo* info, ::GList* files);
std::string base::AppLaunchContextBase::get_startup_notify_id (Gio::AppInfo info, const std::vector<Gio::File> & files) noexcept
{
  typedef char* (*call_wrap_t) (::GAppLaunchContext* context, ::GAppInfo* info, ::GList* files);
  call_wrap_t call_wrap_v = (call_wrap_t) g_app_launch_context_get_startup_notify_id;
  auto files_i = detail::make_list_unwrap_range<::GList> (files);
  auto files_w = unwrap (files_i, gi::transfer_none, direction_in);
  auto files_to_c = files_w.gobj_(false);
  auto info_to_c = gi::unwrap (info, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GAppLaunchContext*) (gobj_()), (::GAppInfo*) (info_to_c), (::GList*) (files_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// void g_app_launch_context_launch_failed (GAppLaunchContext* context, const char* startup_notify_id);
// void g_app_launch_context_launch_failed (::GAppLaunchContext* context, const char* startup_notify_id);
void base::AppLaunchContextBase::launch_failed (const std::string & startup_notify_id) noexcept
{
  typedef void (*call_wrap_t) (::GAppLaunchContext* context, const char* startup_notify_id);
  call_wrap_t call_wrap_v = (call_wrap_t) g_app_launch_context_launch_failed;
  auto startup_notify_id_to_c = gi::unwrap (startup_notify_id, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GAppLaunchContext*) (gobj_()), (const char*) (startup_notify_id_to_c));
}

// void g_app_launch_context_setenv (GAppLaunchContext* context, const char* variable, const char* value);
// void g_app_launch_context_setenv (::GAppLaunchContext* context, const char* variable, const char* value);
void base::AppLaunchContextBase::setenv (const std::string & variable, const std::string & value) noexcept
{
  typedef void (*call_wrap_t) (::GAppLaunchContext* context, const char* variable, const char* value);
  call_wrap_t call_wrap_v = (call_wrap_t) g_app_launch_context_setenv;
  auto value_to_c = gi::unwrap (value, gi::transfer_none, gi::direction_in);
  auto variable_to_c = gi::unwrap (variable, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GAppLaunchContext*) (gobj_()), (const char*) (variable_to_c), (const char*) (value_to_c));
}

// void g_app_launch_context_unsetenv (GAppLaunchContext* context, const char* variable);
// void g_app_launch_context_unsetenv (::GAppLaunchContext* context, const char* variable);
void base::AppLaunchContextBase::unsetenv (const std::string & variable) noexcept
{
  typedef void (*call_wrap_t) (::GAppLaunchContext* context, const char* variable);
  call_wrap_t call_wrap_v = (call_wrap_t) g_app_launch_context_unsetenv;
  auto variable_to_c = gi::unwrap (variable, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GAppLaunchContext*) (gobj_()), (const char*) (variable_to_c));
}





} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/applaunchcontext_extra_def_impl.hpp>)
#include <gio/applaunchcontext_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/applaunchcontext_extra_impl.hpp>)
#include <gio/applaunchcontext_extra_impl.hpp>
#endif
#endif

#endif
