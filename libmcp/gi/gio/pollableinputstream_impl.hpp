// AUTO-GENERATED

#ifndef _GI_GIO_POLLABLEINPUTSTREAM_IMPL_HPP_
#define _GI_GIO_POLLABLEINPUTSTREAM_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// gboolean g_pollable_input_stream_can_poll (GPollableInputStream* stream);
// gboolean g_pollable_input_stream_can_poll (::GPollableInputStream* stream);
bool base::PollableInputStreamBase::can_poll () noexcept
{
  typedef gboolean (*call_wrap_t) (::GPollableInputStream* stream);
  call_wrap_t call_wrap_v = (call_wrap_t) g_pollable_input_stream_can_poll;
  auto _temp_ret = call_wrap_v ((::GPollableInputStream*) (gobj_()));
  return _temp_ret;
}

// GSource* g_pollable_input_stream_create_source (GPollableInputStream* stream, GCancellable* cancellable);
// ::GSource* g_pollable_input_stream_create_source (::GPollableInputStream* stream, ::GCancellable* cancellable);
GLib::Source base::PollableInputStreamBase::create_source (Gio::Cancellable cancellable) noexcept
{
  typedef ::GSource* (*call_wrap_t) (::GPollableInputStream* stream, ::GCancellable* cancellable);
  call_wrap_t call_wrap_v = (call_wrap_t) g_pollable_input_stream_create_source;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPollableInputStream*) (gobj_()), (::GCancellable*) (cancellable_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}
GLib::Source base::PollableInputStreamBase::create_source () noexcept
{
  typedef ::GSource* (*call_wrap_t) (::GPollableInputStream* stream, ::GCancellable* cancellable);
  call_wrap_t call_wrap_v = (call_wrap_t) g_pollable_input_stream_create_source;
  auto cancellable_to_c = nullptr;
  auto _temp_ret = call_wrap_v ((::GPollableInputStream*) (gobj_()), (::GCancellable*) (cancellable_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// gboolean g_pollable_input_stream_is_readable (GPollableInputStream* stream);
// gboolean g_pollable_input_stream_is_readable (::GPollableInputStream* stream);
bool base::PollableInputStreamBase::is_readable () noexcept
{
  typedef gboolean (*call_wrap_t) (::GPollableInputStream* stream);
  call_wrap_t call_wrap_v = (call_wrap_t) g_pollable_input_stream_is_readable;
  auto _temp_ret = call_wrap_v ((::GPollableInputStream*) (gobj_()));
  return _temp_ret;
}

// gssize g_pollable_input_stream_read_nonblocking (GPollableInputStream* stream, void* buffer, gsize count, GCancellable* cancellable, GError ** error);
// gssize g_pollable_input_stream_read_nonblocking (::GPollableInputStream* stream, guint8* buffer, gsize count, ::GCancellable* cancellable, GError ** error);
gssize base::PollableInputStreamBase::read_nonblocking (guint8 * buffer, gsize count, Gio::Cancellable cancellable)
{
  typedef gssize (*call_wrap_t) (::GPollableInputStream* stream, guint8* buffer, gsize count, ::GCancellable* cancellable, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_pollable_input_stream_read_nonblocking;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GPollableInputStream*) (gobj_()), (guint8*) (buffer), (gsize) (count), (::GCancellable*) (cancellable_to_c), &error);
  gi::check_error (error);
  return _temp_ret;
}
gssize base::PollableInputStreamBase::read_nonblocking (guint8 * buffer, gsize count)
{
  typedef gssize (*call_wrap_t) (::GPollableInputStream* stream, guint8* buffer, gsize count, ::GCancellable* cancellable, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_pollable_input_stream_read_nonblocking;
  auto cancellable_to_c = nullptr;
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((::GPollableInputStream*) (gobj_()), (guint8*) (buffer), (gsize) (count), (::GCancellable*) (cancellable_to_c), &error);
  gi::check_error (error);
  return _temp_ret;
}
gssize base::PollableInputStreamBase::read_nonblocking (guint8 * buffer, gsize count, Gio::Cancellable cancellable, GLib::Error * _error) noexcept
{
  typedef gssize (*call_wrap_t) (::GPollableInputStream* stream, guint8* buffer, gsize count, ::GCancellable* cancellable, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_pollable_input_stream_read_nonblocking;
  auto cancellable_to_c = gi::unwrap (cancellable, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GPollableInputStream*) (gobj_()), (guint8*) (buffer), (gsize) (count), (::GCancellable*) (cancellable_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return _temp_ret;
}
gssize base::PollableInputStreamBase::read_nonblocking (guint8 * buffer, gsize count, GLib::Error * _error) noexcept
{
  typedef gssize (*call_wrap_t) (::GPollableInputStream* stream, guint8* buffer, gsize count, ::GCancellable* cancellable, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_pollable_input_stream_read_nonblocking;
  auto cancellable_to_c = nullptr;
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((::GPollableInputStream*) (gobj_()), (guint8*) (buffer), (gsize) (count), (::GCancellable*) (cancellable_to_c), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return _temp_ret;
}


} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/pollableinputstream_extra_def_impl.hpp>)
#include <gio/pollableinputstream_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/pollableinputstream_extra_impl.hpp>)
#include <gio/pollableinputstream_extra_impl.hpp>
#endif
#endif

#endif
