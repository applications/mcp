// AUTO-GENERATED

#ifndef _GI_GIO_VOLUMEMONITOR_IMPL_HPP_
#define _GI_GIO_VOLUMEMONITOR_IMPL_HPP_

namespace gi {

namespace repository {

namespace Gio {

namespace base {

// GVolume* g_volume_monitor_adopt_orphan_mount (GMount* mount);
// ::GVolume* g_volume_monitor_adopt_orphan_mount (::GMount* mount);
// IGNORE; deprecated

// GVolumeMonitor* g_volume_monitor_get ();
// ::GVolumeMonitor* g_volume_monitor_get ();
Gio::VolumeMonitor base::VolumeMonitorBase::get () noexcept
{
  typedef ::GVolumeMonitor* (*call_wrap_t) ();
  call_wrap_t call_wrap_v = (call_wrap_t) g_volume_monitor_get;
  auto _temp_ret = call_wrap_v ();
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GList* g_volume_monitor_get_connected_drives (GVolumeMonitor* volume_monitor);
// ::GList* g_volume_monitor_get_connected_drives (::GVolumeMonitor* volume_monitor);
std::vector<Gio::Drive> base::VolumeMonitorBase::get_connected_drives () noexcept
{
  typedef ::GList* (*call_wrap_t) (::GVolumeMonitor* volume_monitor);
  call_wrap_t call_wrap_v = (call_wrap_t) g_volume_monitor_get_connected_drives;
  auto _temp_ret = call_wrap_v ((::GVolumeMonitor*) (gobj_()));
  return gi::detail::wrap_list<Gio::Drive> (_temp_ret, gi::transfer_full);
}

// GMount* g_volume_monitor_get_mount_for_uuid (GVolumeMonitor* volume_monitor, const char* uuid);
// ::GMount* g_volume_monitor_get_mount_for_uuid (::GVolumeMonitor* volume_monitor, const char* uuid);
Gio::Mount base::VolumeMonitorBase::get_mount_for_uuid (const std::string & uuid) noexcept
{
  typedef ::GMount* (*call_wrap_t) (::GVolumeMonitor* volume_monitor, const char* uuid);
  call_wrap_t call_wrap_v = (call_wrap_t) g_volume_monitor_get_mount_for_uuid;
  auto uuid_to_c = gi::unwrap (uuid, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GVolumeMonitor*) (gobj_()), (const char*) (uuid_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GList* g_volume_monitor_get_mounts (GVolumeMonitor* volume_monitor);
// ::GList* g_volume_monitor_get_mounts (::GVolumeMonitor* volume_monitor);
std::vector<Gio::Mount> base::VolumeMonitorBase::get_mounts () noexcept
{
  typedef ::GList* (*call_wrap_t) (::GVolumeMonitor* volume_monitor);
  call_wrap_t call_wrap_v = (call_wrap_t) g_volume_monitor_get_mounts;
  auto _temp_ret = call_wrap_v ((::GVolumeMonitor*) (gobj_()));
  return gi::detail::wrap_list<Gio::Mount> (_temp_ret, gi::transfer_full);
}

// GVolume* g_volume_monitor_get_volume_for_uuid (GVolumeMonitor* volume_monitor, const char* uuid);
// ::GVolume* g_volume_monitor_get_volume_for_uuid (::GVolumeMonitor* volume_monitor, const char* uuid);
Gio::Volume base::VolumeMonitorBase::get_volume_for_uuid (const std::string & uuid) noexcept
{
  typedef ::GVolume* (*call_wrap_t) (::GVolumeMonitor* volume_monitor, const char* uuid);
  call_wrap_t call_wrap_v = (call_wrap_t) g_volume_monitor_get_volume_for_uuid;
  auto uuid_to_c = gi::unwrap (uuid, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GVolumeMonitor*) (gobj_()), (const char*) (uuid_to_c));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GList* g_volume_monitor_get_volumes (GVolumeMonitor* volume_monitor);
// ::GList* g_volume_monitor_get_volumes (::GVolumeMonitor* volume_monitor);
std::vector<Gio::Volume> base::VolumeMonitorBase::get_volumes () noexcept
{
  typedef ::GList* (*call_wrap_t) (::GVolumeMonitor* volume_monitor);
  call_wrap_t call_wrap_v = (call_wrap_t) g_volume_monitor_get_volumes;
  auto _temp_ret = call_wrap_v ((::GVolumeMonitor*) (gobj_()));
  return gi::detail::wrap_list<Gio::Volume> (_temp_ret, gi::transfer_full);
}














} // namespace base

} // namespace Gio

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gio/volumemonitor_extra_def_impl.hpp>)
#include <gio/volumemonitor_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gio/volumemonitor_extra_impl.hpp>)
#include <gio/volumemonitor_extra_impl.hpp>
#endif
#endif

#endif
