// AUTO-GENERATED

#ifndef _GI_GOBJECT_INITIALLYUNOWNED_IMPL_HPP_
#define _GI_GOBJECT_INITIALLYUNOWNED_IMPL_HPP_

namespace gi {

namespace repository {

namespace GObject {

namespace base {


} // namespace base

} // namespace GObject

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<gobject/initiallyunowned_extra_def_impl.hpp>)
#include <gobject/initiallyunowned_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<gobject/initiallyunowned_extra_impl.hpp>)
#include <gobject/initiallyunowned_extra_impl.hpp>
#endif
#endif

#endif
