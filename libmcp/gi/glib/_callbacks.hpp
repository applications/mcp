// AUTO-GENERATED

#ifndef _GI_GLIB__CALLBACKS_HPP_
#define _GI_GLIB__CALLBACKS_HPP_

namespace gi {

namespace repository {

namespace GLib {

class Error;
class Hook;
class HookList;
class IOChannel;
class MatchInfo;
class Node;
class OptionGroup;
class PollFD;
class Scanner;
class Source;
class String;

// typedef void (*GChildWatchFunc) (GPid pid, gint wait_status, gpointer user_data);
// typedef void (*GChildWatchFunc) (::GPid pid, gint wait_status, void* user_data);
typedef gi::detail::callback<void(::GPid pid, gint wait_status), gi::transfer_full_t, gi::transfer_none_t, gi::transfer_none_t> ChildWatchFunc;

// typedef void (*GClearHandleFunc) (guint handle_id);
// typedef void (*GClearHandleFunc) (guint handle_id);
// SKIP; not a callback since no user_data

// typedef gint (*GCompareDataFunc) (gconstpointer a, gconstpointer b, gpointer user_data);
// typedef gint (*GCompareDataFunc) (const void* a, const void* b, void* user_data);
typedef gi::detail::callback<gint(const void* a, const void* b), gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t> CompareDataFunc;

// typedef gint (*GCompareFunc) (gconstpointer a, gconstpointer b);
// typedef gint (*GCompareFunc) (const void* a, const void* b);
// SKIP; not a callback since no user_data

// typedef gpointer (*GCopyFunc) (gconstpointer src, gpointer user_data);
// typedef void* (*GCopyFunc) (const void* src, void* user_data);
typedef gi::detail::callback<gpointer(const void* src), gi::transfer_none_t, gi::transfer_none_t> CopyFunc;

// typedef void (*GDataForeachFunc) (GQuark key_id, gpointer data, gpointer user_data);
// typedef void (*GDataForeachFunc) (::GQuark key_id, void* data, void* user_data);
typedef gi::detail::callback<void(::GQuark key_id, void* data), gi::transfer_full_t, gi::transfer_none_t, gi::transfer_none_t> DataForeachFunc;

// typedef gpointer (*GDuplicateFunc) (gpointer data, gpointer user_data);
// typedef void* (*GDuplicateFunc) (void* data, void* user_data);
typedef gi::detail::callback<gpointer(void* data), gi::transfer_none_t, gi::transfer_none_t> DuplicateFunc;

// typedef gboolean (*GEqualFunc) (gconstpointer a, gconstpointer b);
// typedef gboolean (*GEqualFunc) (const void* a, const void* b);
// SKIP; not a callback since no user_data

// typedef gboolean (*GEqualFuncFull) (gconstpointer a, gconstpointer b, gpointer user_data);
// typedef gboolean (*GEqualFuncFull) (const void* a, const void* b, void* user_data);
typedef gi::detail::callback<bool(const void* a, const void* b), gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t> EqualFuncFull;

// typedef void (*GErrorClearFunc) (GError* error);
// typedef void (*GErrorClearFunc) (::GError* error);
// SKIP; not a callback since no user_data

// typedef void (*GErrorCopyFunc) (const GError* src_error, GError* dest_error);
// typedef void (*GErrorCopyFunc) (const ::GError* src_error, ::GError* dest_error);
// SKIP; not a callback since no user_data

// typedef void (*GErrorInitFunc) (GError* error);
// typedef void (*GErrorInitFunc) (::GError* error);
// SKIP; not a callback since no user_data

// typedef void (*GFreeFunc) (gpointer data);
// typedef void (*GFreeFunc) (void* data);
// SKIP; not a callback since no user_data

// typedef void (*GFunc) (gpointer data, gpointer user_data);
// typedef void (*GFunc) (void* data, void* user_data);
typedef gi::detail::callback<void(void* data), gi::transfer_full_t, gi::transfer_none_t> Func;

// typedef void (*GHFunc) (gpointer key, gpointer value, gpointer user_data);
// typedef void (*GHFunc) (void* key, void* value, void* user_data);
typedef gi::detail::callback<void(void* key, void* value), gi::transfer_full_t, gi::transfer_none_t, gi::transfer_none_t> HFunc;

// typedef gboolean (*GHRFunc) (gpointer key, gpointer value, gpointer user_data);
// typedef gboolean (*GHRFunc) (void* key, void* value, void* user_data);
typedef gi::detail::callback<bool(void* key, void* value), gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t> HRFunc;

// typedef guint (*GHashFunc) (gconstpointer key);
// typedef guint (*GHashFunc) (const void* key);
// SKIP; not a callback since no user_data

// typedef gboolean (*GHookCheckFunc) (gpointer data);
// typedef gboolean (*GHookCheckFunc) (void* data);
// SKIP; not a callback since no user_data

// typedef gboolean (*GHookCheckMarshaller) (GHook* hook, gpointer user_data);
// typedef gboolean (*GHookCheckMarshaller) (::GHook* hook, void* user_data);
typedef gi::detail::callback<bool(GLib::Hook hook), gi::transfer_none_t, gi::transfer_none_t> HookCheckMarshaller;

// typedef gint (*GHookCompareFunc) (GHook* new_hook, GHook* sibling);
// typedef gint (*GHookCompareFunc) (::GHook* new_hook, ::GHook* sibling);
// SKIP; not a callback since no user_data

// typedef void (*GHookFinalizeFunc) (GHookList* hook_list, GHook* hook);
// typedef void (*GHookFinalizeFunc) (::GHookList* hook_list, ::GHook* hook);
// SKIP; not a callback since no user_data

// typedef gboolean (*GHookFindFunc) (GHook* hook, gpointer user_data);
// typedef gboolean (*GHookFindFunc) (::GHook* hook, void* user_data);
typedef gi::detail::callback<bool(GLib::Hook hook), gi::transfer_none_t, gi::transfer_none_t> HookFindFunc;

// typedef void (*GHookFunc) (gpointer data);
// typedef void (*GHookFunc) (void* data);
// SKIP; not a callback since no user_data

// typedef void (*GHookMarshaller) (GHook* hook, gpointer user_data);
// typedef void (*GHookMarshaller) (::GHook* hook, void* user_data);
typedef gi::detail::callback<void(GLib::Hook hook), gi::transfer_full_t, gi::transfer_none_t> HookMarshaller;

// typedef gboolean (*GIOFunc) (GIOChannel* source, GIOCondition condition, gpointer user_data);
// typedef gboolean (*GIOFunc) (::GIOChannel* source, ::GIOCondition condition, void* user_data);
typedef gi::detail::callback<bool(GLib::IOChannel source, GLib::IOCondition condition), gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t> IOFunc;

// typedef void (*GLogFunc) (const gchar* log_domain, GLogLevelFlags log_level, const gchar* message, gpointer user_data);
// typedef void (*GLogFunc) (const char* log_domain, ::GLogLevelFlags log_level, const char* message, void* user_data);
typedef gi::detail::callback<void(const std::string & log_domain, GLib::LogLevelFlags log_level, const std::string & message), gi::transfer_full_t, gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t> LogFunc;

// typedef GLogWriterOutput (*GLogWriterFunc) (GLogLevelFlags log_level, const GLogField* fields, gsize n_fields, gpointer user_data);
// typedef ::GLogWriterOutput (*GLogWriterFunc) (::GLogLevelFlags log_level, const ::GLogField** fields, gsize n_fields, void* user_data);
// SKIP; callback container parameter not supported

// typedef void (*GNodeForeachFunc) (GNode* node, gpointer user_data);
// typedef void (*GNodeForeachFunc) (::GNode* node, void* user_data);
typedef gi::detail::callback<void(GLib::Node node), gi::transfer_full_t, gi::transfer_none_t> NodeForeachFunc;

// typedef gboolean (*GNodeTraverseFunc) (GNode* node, gpointer user_data);
// typedef gboolean (*GNodeTraverseFunc) (::GNode* node, void* user_data);
typedef gi::detail::callback<bool(GLib::Node node), gi::transfer_none_t, gi::transfer_none_t> NodeTraverseFunc;

// typedef gboolean (*GOptionArgFunc) (const gchar* option_name, const gchar* value, gpointer user_data, GError ** error);
// typedef gboolean (*GOptionArgFunc) (const char* option_name, const char* value, void* user_data, GError ** error);
// SKIP; callee GError not supported

// typedef void (*GOptionErrorFunc) (GOptionContext* context, GOptionGroup* group, gpointer user_data, GError ** error);
// typedef void (*GOptionErrorFunc) ( context, ::GOptionGroup* group, void* user_data, GError ** error);
// SKIP; callee GError not supported

// typedef gboolean (*GOptionParseFunc) (GOptionContext* context, GOptionGroup* group, gpointer user_data, GError ** error);
// typedef gboolean (*GOptionParseFunc) ( context, ::GOptionGroup* group, void* user_data, GError ** error);
// SKIP; callee GError not supported

// typedef gint (*GPollFunc) (GPollFD* ufds, guint nfsd, gint timeout_);
// typedef gint (*GPollFunc) (::GPollFD* ufds, guint nfsd, gint timeout_);
// SKIP; not a callback since no user_data

// typedef void (*GPrintFunc) (const gchar* string);
// typedef void (*GPrintFunc) (const char* string);
// SKIP; not a callback since no user_data

// typedef gboolean (*GRegexEvalCallback) (const GMatchInfo* match_info, GString* result, gpointer user_data);
// typedef gboolean (*GRegexEvalCallback) (const ::GMatchInfo* match_info, ::GString* result, void* user_data);
typedef gi::detail::callback<bool(const GLib::MatchInfo & match_info, GLib::String result), gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t> RegexEvalCallback;

// typedef void (*GScannerMsgFunc) (GScanner* scanner, gchar* message, gboolean error);
// typedef void (*GScannerMsgFunc) (::GScanner* scanner, char* message, gboolean error);
// SKIP; not a callback since no user_data

// typedef gint (*GSequenceIterCompareFunc) (GSequenceIter* a, GSequenceIter* b, gpointer user_data);
// typedef gint (*GSequenceIterCompareFunc) ( a,  b, void* user_data);
// SKIP; b type  not supported, a type  not supported

// typedef void (*GSourceDisposeFunc) (GSource* source);
// typedef void (*GSourceDisposeFunc) (::GSource* source);
// SKIP; not a callback since no user_data

// typedef void (*GSourceDummyMarshal) ();
// typedef void (*GSourceDummyMarshal) ();
// SKIP; not a callback since no user_data

// typedef gboolean (*GSourceFunc) (gpointer user_data);
// typedef gboolean (*GSourceFunc) (void* user_data);
typedef gi::detail::callback<bool(), gi::transfer_none_t> SourceFunc;

// typedef void (*GSourceOnceFunc) (gpointer user_data);
// typedef void (*GSourceOnceFunc) (void* user_data);
typedef gi::detail::callback<void(), gi::transfer_full_t> SourceOnceFunc;

// typedef void (*GSpawnChildSetupFunc) (gpointer user_data);
// typedef void (*GSpawnChildSetupFunc) (void* user_data);
typedef gi::detail::callback<void(), gi::transfer_full_t> SpawnChildSetupFunc;

// typedef void (*GTestDataFunc) (gconstpointer user_data);
// typedef void (*GTestDataFunc) (const void* user_data);
// SKIP; invalid type user_data, not a callback since no user_data

// typedef void (*GTestFixtureFunc) (gpointer fixture, gconstpointer user_data);
// typedef void (*GTestFixtureFunc) (void* fixture, const void* user_data);
// SKIP; invalid type user_data, not a callback since no user_data

// typedef void (*GTestFunc) ();
// typedef void (*GTestFunc) ();
// SKIP; not a callback since no user_data

// typedef gboolean (*GTestLogFatalFunc) (const gchar* log_domain, GLogLevelFlags log_level, const gchar* message, gpointer user_data);
// typedef gboolean (*GTestLogFatalFunc) (const char* log_domain, ::GLogLevelFlags log_level, const char* message, void* user_data);
typedef gi::detail::callback<bool(const std::string & log_domain, GLib::LogLevelFlags log_level, const std::string & message), gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t> TestLogFatalFunc;

// typedef gpointer (*GThreadFunc) (gpointer user_data);
// typedef void* (*GThreadFunc) (void* user_data);
typedef gi::detail::callback<gpointer(), gi::transfer_none_t> ThreadFunc;

// typedef const gchar* (*GTranslateFunc) (const gchar* str, gpointer user_data);
// typedef const char* (*GTranslateFunc) (const char* str, void* user_data);
// SKIP; invalid callback return transfer none

// typedef gboolean (*GTraverseFunc) (gpointer key, gpointer value, gpointer user_data);
// typedef gboolean (*GTraverseFunc) (void* key, void* value, void* user_data);
typedef gi::detail::callback<bool(void* key, void* value), gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t> TraverseFunc;

// typedef gboolean (*GTraverseNodeFunc) (GTreeNode* node, gpointer user_data);
// typedef gboolean (*GTraverseNodeFunc) ( node, void* user_data);
// SKIP; node type  not supported

// typedef gboolean (*GUnixFDSourceFunc) (gint fd, GIOCondition condition, gpointer user_data);
// typedef gboolean (*GUnixFDSourceFunc) (gint fd, ::GIOCondition condition, void* user_data);
typedef gi::detail::callback<bool(gint fd, GLib::IOCondition condition), gi::transfer_none_t, gi::transfer_none_t, gi::transfer_none_t> UnixFDSourceFunc;

// typedef void (*GVoidFunc) ();
// typedef void (*GVoidFunc) ();
// SKIP; not a callback since no user_data

} // namespace GLib

} // namespace repository

} // namespace gi

#endif
