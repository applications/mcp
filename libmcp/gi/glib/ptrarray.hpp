// AUTO-GENERATED

#ifndef _GI_GLIB_PTRARRAY_HPP_
#define _GI_GLIB_PTRARRAY_HPP_


namespace gi {

namespace repository {

namespace GLib {


class PtrArray;

namespace base {


#define GI_GLIB_PTRARRAY_BASE base::PtrArrayBase
class PtrArrayBase : public gi::detail::GBoxedWrapper<PtrArrayBase, ::GPtrArray>
{
typedef gi::detail::GBoxedWrapper<PtrArrayBase, ::GPtrArray> super_type;
public:

PtrArrayBase (std::nullptr_t = nullptr) : super_type() {}

static GType get_type_ () G_GNUC_CONST { return g_ptr_array_get_type(); } 

// guint PtrArray::len (const ::GPtrArray* obj);
// guint PtrArray::len (const ::GPtrArray* obj);
GI_INLINE_DECL guint len_ () const noexcept;

//  PtrArray::len (::GPtrArray* obj, guint _value);
// void PtrArray::len (::GPtrArray* obj, guint _value);
GI_INLINE_DECL void len_ (guint _value) noexcept;

// void g_ptr_array_add (GPtrArray* array, gpointer data);
// void g_ptr_array_add (::GPtrArray* array, void* data);
static GI_INLINE_DECL void add (GLib::PtrArray array, void* data) noexcept;

// GPtrArray* g_ptr_array_copy (GPtrArray* array, GCopyFunc func, gpointer user_data);
// ::GPtrArray* g_ptr_array_copy (::GPtrArray* array, GLib::CopyFunc::cfunction_type func, void* user_data);
// IGNORE; not introspectable, callback misses scope info

// void g_ptr_array_extend (GPtrArray* array_to_extend, GPtrArray* array, GCopyFunc func, gpointer user_data);
// void g_ptr_array_extend (::GPtrArray* array_to_extend, ::GPtrArray* array, GLib::CopyFunc::cfunction_type func, void* user_data);
// IGNORE; not introspectable, callback misses scope info

// void g_ptr_array_extend_and_steal (GPtrArray* array_to_extend, GPtrArray* array);
// void g_ptr_array_extend_and_steal (::GPtrArray* array_to_extend, ::GPtrArray* array);
//static GI_INLINE_DECL void extend_and_steal (GLib::PtrArray array_to_extend, GLib::PtrArray array) noexcept;

// gboolean g_ptr_array_find (GPtrArray* haystack, gconstpointer needle, guint* index_);
// gboolean g_ptr_array_find (::GPtrArray* haystack, const void* needle, guint* index_);
static GI_INLINE_DECL bool find (GLib::PtrArray haystack, const void* needle, guint * index_ = nullptr) noexcept;
static GI_INLINE_DECL std::tuple<bool, guint> find (GLib::PtrArray haystack, const void* needle) noexcept;

// gboolean g_ptr_array_find_with_equal_func (GPtrArray* haystack, gconstpointer needle, GEqualFunc equal_func, guint* index_);
// gboolean g_ptr_array_find_with_equal_func (::GPtrArray* haystack, const void* needle,  equal_func, guint* index_);
// IGNORE; not introspectable, equal_func type  not supported

// void g_ptr_array_foreach (GPtrArray* array, GFunc func, gpointer user_data);
// void g_ptr_array_foreach (::GPtrArray* array, GLib::Func::cfunction_type func, void* user_data);
// IGNORE; not introspectable, callback misses scope info

// gpointer* g_ptr_array_free (GPtrArray* array, gboolean free_seg);
// void* g_ptr_array_free (::GPtrArray* array, gboolean free_seg);
// IGNORE; marked ignore

// void g_ptr_array_insert (GPtrArray* array, gint index_, gpointer data);
// void g_ptr_array_insert (::GPtrArray* array, gint index_, void* data);
static GI_INLINE_DECL void insert (GLib::PtrArray array, gint index_, void* data) noexcept;

// gboolean g_ptr_array_is_null_terminated (GPtrArray* array);
// gboolean g_ptr_array_is_null_terminated (::GPtrArray* array);
static GI_INLINE_DECL bool is_null_terminated (GLib::PtrArray array) noexcept;

// FAILURE on g_ptr_array_new; No such node (<xmlattr>.transfer-ownership)
// GPtrArray* g_ptr_array_new_full (guint reserved_size, GDestroyNotify element_free_func);
// ::GPtrArray* g_ptr_array_new_full (guint reserved_size, GLib::DestroyNotify::cfunction_type element_free_func);
// IGNORE; not introspectable, callback misses closure info

// GPtrArray* g_ptr_array_new_null_terminated (guint reserved_size, GDestroyNotify element_free_func, gboolean null_terminated);
// ::GPtrArray* g_ptr_array_new_null_terminated (guint reserved_size, GLib::DestroyNotify::cfunction_type element_free_func, gboolean null_terminated);
// IGNORE; not introspectable, callback misses closure info

// GPtrArray* g_ptr_array_new_with_free_func (GDestroyNotify element_free_func);
// ::GPtrArray* g_ptr_array_new_with_free_func (GLib::DestroyNotify::cfunction_type element_free_func);
// IGNORE; not introspectable, callback misses closure info

// FAILURE on g_ptr_array_ref; No such node (<xmlattr>.transfer-ownership)
// gboolean g_ptr_array_remove (GPtrArray* array, gpointer data);
// gboolean g_ptr_array_remove (::GPtrArray* array, void* data);
static GI_INLINE_DECL bool remove (GLib::PtrArray array, void* data) noexcept;

// gboolean g_ptr_array_remove_fast (GPtrArray* array, gpointer data);
// gboolean g_ptr_array_remove_fast (::GPtrArray* array, void* data);
static GI_INLINE_DECL bool remove_fast (GLib::PtrArray array, void* data) noexcept;

// gpointer g_ptr_array_remove_index (GPtrArray* array, guint index_);
// void* g_ptr_array_remove_index (::GPtrArray* array, guint index_);
static GI_INLINE_DECL gpointer remove_index (GLib::PtrArray array, guint index_) noexcept;

// gpointer g_ptr_array_remove_index_fast (GPtrArray* array, guint index_);
// void* g_ptr_array_remove_index_fast (::GPtrArray* array, guint index_);
static GI_INLINE_DECL gpointer remove_index_fast (GLib::PtrArray array, guint index_) noexcept;

// FAILURE on g_ptr_array_remove_range; No such node (<xmlattr>.transfer-ownership)
// void g_ptr_array_set_free_func (GPtrArray* array, GDestroyNotify element_free_func);
// void g_ptr_array_set_free_func (::GPtrArray* array, GLib::DestroyNotify::cfunction_type element_free_func);
// IGNORE; not introspectable, callback misses closure info

// void g_ptr_array_set_size (GPtrArray* array, gint length);
// void g_ptr_array_set_size (::GPtrArray* array, gint length);
static GI_INLINE_DECL void set_size (GLib::PtrArray array, gint length) noexcept;

// FAILURE on g_ptr_array_sized_new; No such node (<xmlattr>.transfer-ownership)
// void g_ptr_array_sort (GPtrArray* array, GCompareFunc compare_func);
// void g_ptr_array_sort (::GPtrArray* array,  compare_func);
// IGNORE; not introspectable, compare_func type  not supported

// void g_ptr_array_sort_with_data (GPtrArray* array, GCompareDataFunc compare_func, gpointer user_data);
// void g_ptr_array_sort_with_data (::GPtrArray* array, GLib::CompareDataFunc::cfunction_type compare_func, void* user_data);
// IGNORE; not introspectable, callback misses scope info

// gpointer* g_ptr_array_steal (GPtrArray* array, gsize* len);
// void* g_ptr_array_steal (::GPtrArray* array, gsize* len);
// IGNORE; not introspectable, inconsistent return  pointer depth (2 vs 1)

// gpointer g_ptr_array_steal_index (GPtrArray* array, guint index_);
// void* g_ptr_array_steal_index (::GPtrArray* array, guint index_);
static GI_INLINE_DECL gpointer steal_index (GLib::PtrArray array, guint index_) noexcept;

// gpointer g_ptr_array_steal_index_fast (GPtrArray* array, guint index_);
// void* g_ptr_array_steal_index_fast (::GPtrArray* array, guint index_);
static GI_INLINE_DECL gpointer steal_index_fast (GLib::PtrArray array, guint index_) noexcept;

// void g_ptr_array_unref (GPtrArray* array);
// void g_ptr_array_unref (::GPtrArray* array);
// IGNORE; marked ignore

}; // class

} // namespace base

} // namespace GLib

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<glib/ptrarray_extra_def.hpp>)
#include <glib/ptrarray_extra_def.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<glib/ptrarray_extra.hpp>)
#include <glib/ptrarray_extra.hpp>
#endif
#endif

namespace gi {

namespace repository {

namespace GLib {

class PtrArray : public GI_GLIB_PTRARRAY_BASE
{ typedef GI_GLIB_PTRARRAY_BASE super_type; using super_type::super_type; };

} // namespace GLib

template<> struct declare_cpptype_of<::GPtrArray>
{ typedef GLib::PtrArray type; }; 

} // namespace repository

} // namespace gi

#endif
