// AUTO-GENERATED

#ifndef _GI_GLIB_THREAD_IMPL_HPP_
#define _GI_GLIB_THREAD_IMPL_HPP_

namespace gi {

namespace repository {

namespace GLib {

namespace base {

// GThread* g_thread_new (const gchar* name, GThreadFunc func, gpointer data);
// ::GThread* g_thread_new (const char* name, GLib::ThreadFunc::cfunction_type func, void* data);
GLib::Thread base::ThreadBase::new_ (const std::string & name, GLib::ThreadFunc func) noexcept
{
  typedef ::GThread* (*call_wrap_t) (const char* name, GLib::ThreadFunc::cfunction_type func, void* data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_new;
  auto func_wrap_ = func ? unwrap (std::move (func), gi::scope_async) : nullptr;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((const char*) (name_to_c), (GLib::ThreadFunc::cfunction_type) (func_wrap_ ? &func_wrap_->wrapper : nullptr), (void*) (func_wrap_));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}
GLib::Thread base::ThreadBase::new_ (GLib::ThreadFunc func) noexcept
{
  typedef ::GThread* (*call_wrap_t) (const char* name, GLib::ThreadFunc::cfunction_type func, void* data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_new;
  auto func_wrap_ = func ? unwrap (std::move (func), gi::scope_async) : nullptr;
  auto name_to_c = nullptr;
  auto _temp_ret = call_wrap_v ((const char*) (name_to_c), (GLib::ThreadFunc::cfunction_type) (func_wrap_ ? &func_wrap_->wrapper : nullptr), (void*) (func_wrap_));
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// GThread* g_thread_try_new (const gchar* name, GThreadFunc func, gpointer data, GError ** error);
// ::GThread* g_thread_try_new (const char* name, GLib::ThreadFunc::cfunction_type func, void* data, GError ** error);
GLib::Thread base::ThreadBase::try_new (const std::string & name, GLib::ThreadFunc func)
{
  typedef ::GThread* (*call_wrap_t) (const char* name, GLib::ThreadFunc::cfunction_type func, void* data, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_try_new;
  auto func_wrap_ = func ? unwrap (std::move (func), gi::scope_async) : nullptr;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((const char*) (name_to_c), (GLib::ThreadFunc::cfunction_type) (func_wrap_ ? &func_wrap_->wrapper : nullptr), (void*) (func_wrap_), &error);
  gi::check_error (error);
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}
GLib::Thread base::ThreadBase::try_new (GLib::ThreadFunc func)
{
  typedef ::GThread* (*call_wrap_t) (const char* name, GLib::ThreadFunc::cfunction_type func, void* data, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_try_new;
  auto func_wrap_ = func ? unwrap (std::move (func), gi::scope_async) : nullptr;
  auto name_to_c = nullptr;
  GError *error = NULL;
  auto _temp_ret = call_wrap_v ((const char*) (name_to_c), (GLib::ThreadFunc::cfunction_type) (func_wrap_ ? &func_wrap_->wrapper : nullptr), (void*) (func_wrap_), &error);
  gi::check_error (error);
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}
GLib::Thread base::ThreadBase::try_new (const std::string & name, GLib::ThreadFunc func, GLib::Error * _error) noexcept
{
  typedef ::GThread* (*call_wrap_t) (const char* name, GLib::ThreadFunc::cfunction_type func, void* data, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_try_new;
  auto func_wrap_ = func ? unwrap (std::move (func), gi::scope_async) : nullptr;
  auto name_to_c = gi::unwrap (name, gi::transfer_none, gi::direction_in);
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((const char*) (name_to_c), (GLib::ThreadFunc::cfunction_type) (func_wrap_ ? &func_wrap_->wrapper : nullptr), (void*) (func_wrap_), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}
GLib::Thread base::ThreadBase::try_new (GLib::ThreadFunc func, GLib::Error * _error) noexcept
{
  typedef ::GThread* (*call_wrap_t) (const char* name, GLib::ThreadFunc::cfunction_type func, void* data, GError ** error);
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_try_new;
  auto func_wrap_ = func ? unwrap (std::move (func), gi::scope_async) : nullptr;
  auto name_to_c = nullptr;
  GError* _error_o {};
  auto _temp_ret = call_wrap_v ((const char*) (name_to_c), (GLib::ThreadFunc::cfunction_type) (func_wrap_ ? &func_wrap_->wrapper : nullptr), (void*) (func_wrap_), _error ? &_error_o : nullptr);
  if (_error) *_error = gi::wrap (_error_o, gi::transfer_full, gi::direction_out);
  return gi::wrap (_temp_ret, gi::transfer_full, gi::direction_out);
}

// gpointer g_thread_join (GThread* thread);
// void* g_thread_join (::GThread* thread);
gpointer base::ThreadBase::join () noexcept
{
  typedef void* (*call_wrap_t) (::GThread* thread);
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_join;
  auto _temp_ret = call_wrap_v ((::GThread*) (gobj_()));
  return _temp_ret;
}

// GThread* g_thread_ref (GThread* thread);
// ::GThread* g_thread_ref (::GThread* thread);
// IGNORE; marked ignore

// void g_thread_unref (GThread* thread);
// void g_thread_unref (::GThread* thread);
// IGNORE; marked ignore

// GQuark g_thread_error_quark ();
// ::GQuark g_thread_error_quark ();
GLib::Quark base::ThreadBase::error_quark () noexcept
{
  typedef ::GQuark (*call_wrap_t) ();
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_error_quark;
  auto _temp_ret = call_wrap_v ();
  return _temp_ret;
}

// void g_thread_exit (gpointer retval);
// void g_thread_exit (void* retval);
void base::ThreadBase::exit (void* retval) noexcept
{
  typedef void (*call_wrap_t) (void* retval);
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_exit;
  auto retval_to_c = retval;
  call_wrap_v ((void*) (retval_to_c));
}

// GThread* g_thread_self ();
// ::GThread* g_thread_self ();
GLib::Thread base::ThreadBase::self () noexcept
{
  typedef ::GThread* (*call_wrap_t) ();
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_self;
  auto _temp_ret = call_wrap_v ();
  return gi::wrap (_temp_ret, gi::transfer_none, gi::direction_out);
}

// void g_thread_yield ();
// void g_thread_yield ();
void base::ThreadBase::yield () noexcept
{
  typedef void (*call_wrap_t) ();
  call_wrap_t call_wrap_v = (call_wrap_t) g_thread_yield;
  call_wrap_v ();
}


} // namespace base

} // namespace GLib

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<glib/thread_extra_def_impl.hpp>)
#include <glib/thread_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<glib/thread_extra_impl.hpp>)
#include <glib/thread_extra_impl.hpp>
#endif
#endif

#endif
