// AUTO-GENERATED

#ifndef _GI_GLIB_PTRARRAY_IMPL_HPP_
#define _GI_GLIB_PTRARRAY_IMPL_HPP_

namespace gi {

namespace repository {

namespace GLib {

namespace base {

static guint _field_len_get (const ::GPtrArray* obj) { return (guint) obj->len; }
// guint PtrArray::len (const ::GPtrArray* obj);
// guint PtrArray::len (const ::GPtrArray* obj);
guint base::PtrArrayBase::len_ () const noexcept
{
  typedef guint (*call_wrap_t) (const ::GPtrArray* obj);
  call_wrap_t call_wrap_v = (call_wrap_t) _field_len_get;
  auto _temp_ret = call_wrap_v ((const ::GPtrArray*) (gobj_()));
  return _temp_ret;
}

static void _field_len_set (::GPtrArray* obj, guint _value) { obj->len = (decltype(obj->len)) _value; }
//  PtrArray::len (::GPtrArray* obj, guint _value);
// void PtrArray::len (::GPtrArray* obj, guint _value);
void base::PtrArrayBase::len_ (guint _value) noexcept
{
  typedef void (*call_wrap_t) (::GPtrArray* obj, guint _value);
  call_wrap_t call_wrap_v = (call_wrap_t) _field_len_set;
  auto _value_to_c = _value;
  call_wrap_v ((::GPtrArray*) (gobj_()), (guint) (_value_to_c));
}

// void g_ptr_array_add (GPtrArray* array, gpointer data);
// void g_ptr_array_add (::GPtrArray* array, void* data);
void base::PtrArrayBase::add (GLib::PtrArray array, void* data) noexcept
{
  typedef void (*call_wrap_t) (::GPtrArray* array, void* data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_add;
  auto data_to_c = data;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GPtrArray*) (array_to_c), (void*) (data_to_c));
}

// GPtrArray* g_ptr_array_copy (GPtrArray* array, GCopyFunc func, gpointer user_data);
// ::GPtrArray* g_ptr_array_copy (::GPtrArray* array, GLib::CopyFunc::cfunction_type func, void* user_data);
// IGNORE; not introspectable, callback misses scope info

// void g_ptr_array_extend (GPtrArray* array_to_extend, GPtrArray* array, GCopyFunc func, gpointer user_data);
// void g_ptr_array_extend (::GPtrArray* array_to_extend, ::GPtrArray* array, GLib::CopyFunc::cfunction_type func, void* user_data);
// IGNORE; not introspectable, callback misses scope info

// void g_ptr_array_extend_and_steal (GPtrArray* array_to_extend, GPtrArray* array);
// void g_ptr_array_extend_and_steal (::GPtrArray* array_to_extend, ::GPtrArray* array);
//void base::PtrArrayBase::extend_and_steal (GLib::PtrArray array_to_extend, GLib::PtrArray array) noexcept
//{
//  typedef void (*call_wrap_t) (::GPtrArray* array_to_extend, ::GPtrArray* array);
//  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_extend_and_steal;
//  auto array_to_c = gi::unwrap (array, gi::transfer_container, gi::direction_in);
//  auto array_to_extend_to_c = gi::unwrap (array_to_extend, gi::transfer_none, gi::direction_in);
//  call_wrap_v ((::GPtrArray*) (array_to_extend_to_c), (::GPtrArray*) (array_to_c));
//}

// gboolean g_ptr_array_find (GPtrArray* haystack, gconstpointer needle, guint* index_);
// gboolean g_ptr_array_find (::GPtrArray* haystack, const void* needle, guint* index_);
bool base::PtrArrayBase::find (GLib::PtrArray haystack, const void* needle, guint * index_) noexcept
{
  typedef gboolean (*call_wrap_t) (::GPtrArray* haystack, const void* needle, guint* index_);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_find;
  guint index__o {};
  auto needle_to_c = needle;
  auto haystack_to_c = gi::unwrap (haystack, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPtrArray*) (haystack_to_c), (const void*) (needle_to_c), (guint*) (index_ ? &index__o : nullptr));
  if (index_) *index_ = index__o;
  return _temp_ret;
}
std::tuple<bool, guint> base::PtrArrayBase::find (GLib::PtrArray haystack, const void* needle) noexcept
{
  typedef gboolean (*call_wrap_t) (::GPtrArray* haystack, const void* needle, guint* index_);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_find;
  guint index__o {};
  auto needle_to_c = needle;
  auto haystack_to_c = gi::unwrap (haystack, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPtrArray*) (haystack_to_c), (const void*) (needle_to_c), (guint*) (&index__o));
  return std::make_tuple (_temp_ret, index__o);
}

// gboolean g_ptr_array_find_with_equal_func (GPtrArray* haystack, gconstpointer needle, GEqualFunc equal_func, guint* index_);
// gboolean g_ptr_array_find_with_equal_func (::GPtrArray* haystack, const void* needle,  equal_func, guint* index_);
// IGNORE; not introspectable, equal_func type  not supported

// void g_ptr_array_foreach (GPtrArray* array, GFunc func, gpointer user_data);
// void g_ptr_array_foreach (::GPtrArray* array, GLib::Func::cfunction_type func, void* user_data);
// IGNORE; not introspectable, callback misses scope info

// gpointer* g_ptr_array_free (GPtrArray* array, gboolean free_seg);
// void* g_ptr_array_free (::GPtrArray* array, gboolean free_seg);
// IGNORE; marked ignore

// void g_ptr_array_insert (GPtrArray* array, gint index_, gpointer data);
// void g_ptr_array_insert (::GPtrArray* array, gint index_, void* data);
void base::PtrArrayBase::insert (GLib::PtrArray array, gint index_, void* data) noexcept
{
  typedef void (*call_wrap_t) (::GPtrArray* array, gint index_, void* data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_insert;
  auto data_to_c = data;
  auto index__to_c = index_;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GPtrArray*) (array_to_c), (gint) (index__to_c), (void*) (data_to_c));
}

// gboolean g_ptr_array_is_null_terminated (GPtrArray* array);
// gboolean g_ptr_array_is_null_terminated (::GPtrArray* array);
bool base::PtrArrayBase::is_null_terminated (GLib::PtrArray array) noexcept
{
  typedef gboolean (*call_wrap_t) (::GPtrArray* array);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_is_null_terminated;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPtrArray*) (array_to_c));
  return _temp_ret;
}

// FAILURE on g_ptr_array_new; No such node (<xmlattr>.transfer-ownership)
// GPtrArray* g_ptr_array_new_full (guint reserved_size, GDestroyNotify element_free_func);
// ::GPtrArray* g_ptr_array_new_full (guint reserved_size, GLib::DestroyNotify::cfunction_type element_free_func);
// IGNORE; not introspectable, callback misses closure info

// GPtrArray* g_ptr_array_new_null_terminated (guint reserved_size, GDestroyNotify element_free_func, gboolean null_terminated);
// ::GPtrArray* g_ptr_array_new_null_terminated (guint reserved_size, GLib::DestroyNotify::cfunction_type element_free_func, gboolean null_terminated);
// IGNORE; not introspectable, callback misses closure info

// GPtrArray* g_ptr_array_new_with_free_func (GDestroyNotify element_free_func);
// ::GPtrArray* g_ptr_array_new_with_free_func (GLib::DestroyNotify::cfunction_type element_free_func);
// IGNORE; not introspectable, callback misses closure info

// FAILURE on g_ptr_array_ref; No such node (<xmlattr>.transfer-ownership)
// gboolean g_ptr_array_remove (GPtrArray* array, gpointer data);
// gboolean g_ptr_array_remove (::GPtrArray* array, void* data);
bool base::PtrArrayBase::remove (GLib::PtrArray array, void* data) noexcept
{
  typedef gboolean (*call_wrap_t) (::GPtrArray* array, void* data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_remove;
  auto data_to_c = data;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPtrArray*) (array_to_c), (void*) (data_to_c));
  return _temp_ret;
}

// gboolean g_ptr_array_remove_fast (GPtrArray* array, gpointer data);
// gboolean g_ptr_array_remove_fast (::GPtrArray* array, void* data);
bool base::PtrArrayBase::remove_fast (GLib::PtrArray array, void* data) noexcept
{
  typedef gboolean (*call_wrap_t) (::GPtrArray* array, void* data);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_remove_fast;
  auto data_to_c = data;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPtrArray*) (array_to_c), (void*) (data_to_c));
  return _temp_ret;
}

// gpointer g_ptr_array_remove_index (GPtrArray* array, guint index_);
// void* g_ptr_array_remove_index (::GPtrArray* array, guint index_);
gpointer base::PtrArrayBase::remove_index (GLib::PtrArray array, guint index_) noexcept
{
  typedef void* (*call_wrap_t) (::GPtrArray* array, guint index_);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_remove_index;
  auto index__to_c = index_;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPtrArray*) (array_to_c), (guint) (index__to_c));
  return _temp_ret;
}

// gpointer g_ptr_array_remove_index_fast (GPtrArray* array, guint index_);
// void* g_ptr_array_remove_index_fast (::GPtrArray* array, guint index_);
gpointer base::PtrArrayBase::remove_index_fast (GLib::PtrArray array, guint index_) noexcept
{
  typedef void* (*call_wrap_t) (::GPtrArray* array, guint index_);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_remove_index_fast;
  auto index__to_c = index_;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPtrArray*) (array_to_c), (guint) (index__to_c));
  return _temp_ret;
}

// FAILURE on g_ptr_array_remove_range; No such node (<xmlattr>.transfer-ownership)
// void g_ptr_array_set_free_func (GPtrArray* array, GDestroyNotify element_free_func);
// void g_ptr_array_set_free_func (::GPtrArray* array, GLib::DestroyNotify::cfunction_type element_free_func);
// IGNORE; not introspectable, callback misses closure info

// void g_ptr_array_set_size (GPtrArray* array, gint length);
// void g_ptr_array_set_size (::GPtrArray* array, gint length);
void base::PtrArrayBase::set_size (GLib::PtrArray array, gint length) noexcept
{
  typedef void (*call_wrap_t) (::GPtrArray* array, gint length);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_set_size;
  auto length_to_c = length;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  call_wrap_v ((::GPtrArray*) (array_to_c), (gint) (length_to_c));
}

// FAILURE on g_ptr_array_sized_new; No such node (<xmlattr>.transfer-ownership)
// void g_ptr_array_sort (GPtrArray* array, GCompareFunc compare_func);
// void g_ptr_array_sort (::GPtrArray* array,  compare_func);
// IGNORE; not introspectable, compare_func type  not supported

// void g_ptr_array_sort_with_data (GPtrArray* array, GCompareDataFunc compare_func, gpointer user_data);
// void g_ptr_array_sort_with_data (::GPtrArray* array, GLib::CompareDataFunc::cfunction_type compare_func, void* user_data);
// IGNORE; not introspectable, callback misses scope info

// gpointer* g_ptr_array_steal (GPtrArray* array, gsize* len);
// void* g_ptr_array_steal (::GPtrArray* array, gsize* len);
// IGNORE; not introspectable, inconsistent return  pointer depth (2 vs 1)

// gpointer g_ptr_array_steal_index (GPtrArray* array, guint index_);
// void* g_ptr_array_steal_index (::GPtrArray* array, guint index_);
gpointer base::PtrArrayBase::steal_index (GLib::PtrArray array, guint index_) noexcept
{
  typedef void* (*call_wrap_t) (::GPtrArray* array, guint index_);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_steal_index;
  auto index__to_c = index_;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPtrArray*) (array_to_c), (guint) (index__to_c));
  return _temp_ret;
}

// gpointer g_ptr_array_steal_index_fast (GPtrArray* array, guint index_);
// void* g_ptr_array_steal_index_fast (::GPtrArray* array, guint index_);
gpointer base::PtrArrayBase::steal_index_fast (GLib::PtrArray array, guint index_) noexcept
{
  typedef void* (*call_wrap_t) (::GPtrArray* array, guint index_);
  call_wrap_t call_wrap_v = (call_wrap_t) g_ptr_array_steal_index_fast;
  auto index__to_c = index_;
  auto array_to_c = gi::unwrap (array, gi::transfer_none, gi::direction_in);
  auto _temp_ret = call_wrap_v ((::GPtrArray*) (array_to_c), (guint) (index__to_c));
  return _temp_ret;
}

// void g_ptr_array_unref (GPtrArray* array);
// void g_ptr_array_unref (::GPtrArray* array);
// IGNORE; marked ignore


} // namespace base

} // namespace GLib

} // namespace repository

} // namespace gi


#if defined(__has_include)
#if __has_include(<glib/ptrarray_extra_def_impl.hpp>)
#include <glib/ptrarray_extra_def_impl.hpp>
#endif
#endif


#if defined(__has_include)
#if __has_include(<glib/ptrarray_extra_impl.hpp>)
#include <glib/ptrarray_extra_impl.hpp>
#endif
#endif

#endif
