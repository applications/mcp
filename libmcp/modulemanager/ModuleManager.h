/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once

#include "ModuleDescription.h"

#include <kangaru/kangaru.hpp>

#include <memory>
#include <string>
#include <vector>

#include <sigc++/signal.h>

namespace MCP::Lib::Common::Modules {

template<typename TView>
class ModuleManagerBase
{
public:
    ModuleManagerBase() = default;
    virtual ~ModuleManagerBase() = default;

    template<MCP::Lib::Common::Modules::Description<TView> T>
    void load_from_description()
    {
        T::register_dependencies(m_container);

        auto *view = T::create_view();

        T::inject(view, m_container);

        T::init(view);

        const auto &it = m_loaded_modules.emplace_back(
            std::pair{T::meta_info(), std::unique_ptr<TView>(view)});

        module_added.emit(it.first, it.second.get());
    }

    kgr::container &container() { return m_container; }

    sigc::signal<void(const ModuleMetaInfo &, TView *)> module_added;

protected:
    kgr::container m_container;
    std::vector<std::pair<ModuleMetaInfo, std::unique_ptr<TView>>> m_loaded_modules;
};

} // namespace MCP::Lib::Common::Modules
