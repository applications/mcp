/* === This file is part of MCP ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#pragma once
#include <array>
#include <string>

#include <kangaru/container.hpp>

namespace MCP::Lib::Common::Modules {

struct ModuleMetaInfo
{
    std::string id;
    std::string name;
    std::string description;
    std::string icon;
};

template<const char Name[], const char Description[], const char Icon[]>
struct ModuleDescription
{
    static ModuleMetaInfo meta_info()
    {
        return {.id = Name, .name = Name, .description = Description, .icon = Icon};
    }
};

template<typename T, typename TView>
concept Description = requires(T, TView *view, kgr::container &container)
{
    T::inject(view, container);
    T::create_view();
    T::meta_info();
    T::register_dependencies(container);
    T::init(view);
};

} // namespace MCP::Lib::Common::Modules

template<>
struct std::hash<MCP::Lib::Common::Modules::ModuleMetaInfo>
{
    std::size_t operator()(const auto &module)
    {
        return std::hash(std::tuple{module.id, module.name, module.description, module.icon});
    }
};
