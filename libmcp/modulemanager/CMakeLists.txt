## FetchContent dependencies

message(STATUS "Downloading kangaru dependency...")

FetchContent_Declare(
    kangaru
    GIT_REPOSITORY https://github.com/gracicot/kangaru.git
    GIT_TAG        v4.3.1
    GIT_PROGRESS   TRUE
    USES_TERMINAL_DOWNLOAD TRUE
)

set(KANGARU_REVERSE_DESTRUCTION ON)
FetchContent_GetProperties(kangaru)
if(NOT kangaru_POPULATED)
    FetchContent_Populate(kangaru)
    add_subdirectory(${kangaru_SOURCE_DIR} ${kangaru_BINARY_DIR} EXCLUDE_FROM_ALL)
endif(NOT kangaru_POPULATED)

target_sources(libmcp PRIVATE
    ModuleDescription.h
    ModuleManager.h
)

target_link_libraries(libmcp-common
    PUBLIC
    kangaru::kangaru
)
