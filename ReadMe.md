# Manjaro Control Panel

A modular control panel that allows you to execute some useful Manjaro Linux maintenance routines. A successor of Manjaro Settings Manager.

Available modules:

* Kernel management
* Language packages dashboard
* Manjaro HardWare Detection tool frontend (Drivers installation, hardware list)
